#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <libgen.h>

#ifdef _DEBUG_
#define DEBUG_PRINT(message, value)	printf("DEBUG:"message"\n", value);
#else
#define DEBUG_PRINT(message, value)
#endif

#define AM_SERVER_REMOTE_CONTROLLER_VERSION	"$Revision$"

#define AM_SERVER_CTRL_PORT_DEFAULT			20001

#define RESPONSE_LENGTH							128
#define RESPONSE_RECV_TIMEOUT_MS				1000	/* milliseconds */

/* The exit codes as known by Nagios */
#define EXIT_CODE_OK						0
#define EXIT_CODE_WARNING					1
#define EXIT_CODE_CRITICAL					2
#define EXIT_CODE_UNKNOWN_STATE				3

#define NCOMMANDS							25
/* define the commands */
char*	commands[NCOMMANDS]={
		(char*)"SERVER_ID",
		(char*)"START_TIME",
		(char*)"UP_TIME",
		(char*)"SERVER_STOP",
		(char*)"LDAP_UPDATED",
		(char*)"PAP_UPDATED",
		(char*)"SECONDARY_SERVERS",
		(char*)"BUSY_RESP",
		(char*)"BUSY_RESP_ERR",
		(char*)"NEW_REQ",
		(char*)"REQ_PROC_ERR",
		(char*)"REQ_PROC_OK",
		(char*)"REQ_WHEN_BUSY",
		(char*)"AVG_RATE_BUSY_RESP",
		(char*)"AVG_RATE_BUSY_RESP_ERR",
		(char*)"AVG_RATE_NEW_REQ",
		(char*)"AVG_RATE_REQ_PROC_ERR",
		(char*)"AVG_RATE_REQ_PROC_OK",
		(char*)"AVG_RATE_REQ_WHEN_BUSY",
		(char*)"PEAK_RATE_BUSY_RESP",
		(char*)"PEAK_RATE_BUSY_RESP_ERR",
		(char*)"PEAK_RATE_NEW_REQ",
		(char*)"PEAK_RATE_REQ_PROC_ERR",
		(char*)"PEAK_RATE_REQ_PROC_OK",
		(char*)"PEAK_RATE_REQ_WHEN_BUSY"
		};

char*	commandsDescription[NCOMMANDS]={
		(char*)"Ask the Access Manager server identifier",
		(char*)"Ask the Access Manager server start time",
		(char*)"Ask the Access Manager server up time",
		(char*)"Ask the Access Manager to shut down",
		(char*)"Inform the Access Manager server that the LDAP information was updated",
		(char*)"Inform the Access Manager server that the Policy Administration Point was updated",
		(char*)"Ask the list of secondary servers",
		(char*)"Ask the number of busy responses",
		(char*)"Ask the number of busy response errors",
		(char*)"Ask the number of new requests for authorization received from clients",
		(char*)"Ask the number of requests processed with error",
		(char*)"Ask the number of requests processed successfully",
		(char*)"Ask the number of new requests for authorization received from clients when the server was busy",
		(char*)"Ask the average rate of busy responses",
		(char*)"Ask the average rate of busy responses errors",
		(char*)"Ask the average rate of new requests for authorization received from clients",
		(char*)"Ask the average rate of requests processed with error",
		(char*)"Ask the average rate of requests processed successfully",
		(char*)"Ask the average rate of new requests for authorization received from clients when the server was busy",
		(char*)"Ask the peak rate of busy responses",
		(char*)"Ask the peak rate of busy responses errors",
		(char*)"Ask the peak rate of new requests for authorization received from clients",
		(char*)"Ask the peak rate of requests processed with error",
		(char*)"Ask the peak rate of requests processed successfully",
		(char*)"Ask the peak rate of new requests for authorization received from clients when the server was busy"
		};

/* indicate with 1 if the server should respond, with 0 if just send the command */
int		commandsReply[NCOMMANDS]={
					1,
					1,
					1,
					0,
					0,
					0,
					1,
					1,
					1,
					1,
					1,
					1,
					1,
					1,
					1,
					1,
					1,
					1,
					1,
					1,
					1,
					1,
					1,
					1,
					1
				};

/* the response string */
char response[RESPONSE_LENGTH + 1];

/* the timeout for the response receiving from the server*/
int responseReceiveTimeoutms;

/* the datagram socket used in the communication */
int sockfd;

/* Nagios mode if the variable is != 0*/
int nagiosMode;
/* Nagios mode: the warning limit*/
long nagiosWarningLimit;
/* Nagios mode: the critical limit*/
long nagiosCriticalLimit;

/* Display the help screen */
void printHelp(char *programName){
	int i;
	printf("Version: %s built on %s %s\n", AM_SERVER_REMOTE_CONTROLLER_VERSION, __DATE__, __TIME__);
	printf("Usage: %s <-s server_name> <-p port_number> <-c command> [-N [-W warning_limit] [-C critical_limit]] [-t timeout] [-h]\n", basename(programName));
	printf("Options:\n");
	printf("   -s the machine name that runs the Access Manager server\n");
	printf("   -p the port number on which the Access Manager listens for control messages. Default %i\n", AM_SERVER_CTRL_PORT_DEFAULT);
	printf("   -c the command to send to the server. The valid commands are:\n");
	for(i = 0; i < NCOMMANDS; i++){
		printf("      %-21s %s\n", commands[i], commandsDescription[i]);
	}
	printf("   -N enable the Nagios mode of displaying messages and the return code schema\n");
	printf("   -W the warning limit in Nagios mode. If the value received from the server is bigger than warning_limit,then warning code is returned.\n");
	printf("   -C the critical limit in Nagios mode. If the value received from the server is bigger than critical_limit,then critical code is returned.\n");
	printf("   -t timeout (in milliseconds) for the response to be received from the server. Default %i\n", RESPONSE_RECV_TIMEOUT_MS);
	printf("   -h display this help screen\n");
}

/* Send message to a destination specified as a hostname
   Return 0 if OK, !=0 if error
*/
int sendMessage(const char *dest, const long port, const char* message){
    struct sockaddr_in amServerAddr; /* connector's address information */
    struct hostent *he;
    int numbytes;

    if ((he=gethostbyname(dest)) == NULL) {  /* get the host info */
        herror("gethostbyname");
        return 1;
    }

    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
        perror("socket");
        return 2;
    }

    amServerAddr.sin_family = AF_INET;   /* host byte order */
    amServerAddr.sin_port = htons(port); /* short, network byte order */
    amServerAddr.sin_addr = *((struct in_addr *)he->h_addr);
    memset(amServerAddr.sin_zero, '\0', sizeof(amServerAddr.sin_zero));

    DEBUG_PRINT("Sending message [%s]", message);
    if ((numbytes = sendto(sockfd, message, strlen(message), 0,
             (struct sockaddr *)&amServerAddr, sizeof(amServerAddr))) == -1) {
        perror("sendto");
        return 3;
    }
    DEBUG_PRINT("Message sent to address [%s]", inet_ntoa(amServerAddr.sin_addr));
    DEBUG_PRINT("Message sent to port [%d]", ntohs(amServerAddr.sin_port));

    if (!nagiosMode) {
	    printf("Message [%s] sent to [%s(%s):%d]\n",
	    		message,
	    		dest,
	    		inet_ntoa(amServerAddr.sin_addr),
	    		ntohs(amServerAddr.sin_port));
    }

    return 0;
}

/* Receive message from the same socket where the message has been sent
   Return null if no message received, != null if there is response
*/
char* receiveMessage(){

	char * receivedMessage = NULL;
	struct sockaddr_in sender;
	socklen_t length = sizeof(struct sockaddr_in);
	int nbytes = -1;

 	fd_set fds;
    int retcode;
    struct timeval tv;

	if (sockfd <= 0){
		DEBUG_PRINT("Invalid socked [%d]", sockfd);
		return receivedMessage;
	}

    /* set up the file descriptor set */
    FD_ZERO(&fds);
    FD_SET(sockfd, &fds);

    /* set up the struct timeval for the timeout */
    tv.tv_sec = 0;
    tv.tv_usec = responseReceiveTimeoutms * 1000;	/* this value is microseconds */

    /* wait until timeout or data received */
    retcode = select(sockfd + 1, &fds, NULL, NULL, &tv);
    if (retcode == 0){
    	printf("TIMEOUT OCCURED (%d ms)!", responseReceiveTimeoutms);
    } else if (retcode == -1){
    	perror("select");
    } else {
		/* Read the data */
		if ((nbytes = recvfrom(sockfd, response, RESPONSE_LENGTH, 0, (struct sockaddr*)&sender, &length)) < 0){
			perror("recvfrom");
		} else {
			response[nbytes] = '\0';
			receivedMessage = response;

			if (! nagiosMode) {
				/* print the sender address and the message */
				printf("Response [%s] received from [%s:%d]\n",
						receivedMessage,
						inet_ntoa(sender.sin_addr),
						ntohs(sender.sin_port));
			}
		}
    }

	return receivedMessage;
}

int main(int argc, char *argv[]){

	/* define and initialize the variables; */
	char *amServerName = NULL;
	long amServerCtrlPort = AM_SERVER_CTRL_PORT_DEFAULT;
	int commandIdx = -1;
	int i;
	char * messageReceived = NULL;
	int retcode = EXIT_CODE_OK;

	int option = -1;
	long receivedNumber = 0;

	responseReceiveTimeoutms = RESPONSE_RECV_TIMEOUT_MS;

	nagiosMode = 0 ; 		/* Nagios mode disabled by default*/
	nagiosWarningLimit = 0;	/* No Nagios warning limit */
	nagiosCriticalLimit = 0;/* No Nagios critical limit*/

	/* parse the command line arguments */
	while ((option=getopt(argc, argv, "s:p:c:NW:C:t:h")) != -1){
		switch(option){
		case 's':
			amServerName = optarg;
			DEBUG_PRINT("Server name: %s", amServerName);
			break;
		case 'p':
			amServerCtrlPort = atol(optarg);
			if (amServerCtrlPort <= 0){
				printf("Error while reading the port number from [%s] or it has a negative value. Abort!\n", optarg);
				exit(EXIT_CODE_UNKNOWN_STATE);
			}
			DEBUG_PRINT("Server control port: %ld", amServerCtrlPort);
			break;
		case 'c':
			for(i = 0; i < NCOMMANDS; i++){
				if (strcmp(commands[i], optarg) == 0) {
					if (commandIdx >= 0){
						printf("Warning: the command has been already selected as [%s].\n", commands[commandIdx]);
						printf("Warning: The new command is [%s].\n", optarg);
					}
					commandIdx = i;
				}
			}
			if (commandIdx < 0){
				printf("Command unrecognized [%s]\n", optarg);
			}
			DEBUG_PRINT("Command:%s", optarg)
			DEBUG_PRINT("Command index:%d", commandIdx)
			break;
		case 'N':
			nagiosMode = 1;
			DEBUG_PRINT("Nagios mode ENABLED! (%d)", nagiosMode);
			break;
		case 'W':
			nagiosWarningLimit = atol(optarg);
			if (nagiosWarningLimit == 0){
				printf("The Nagios warning limit is 0. Disabled!\n");
			}
			DEBUG_PRINT("The Nagios warning limit is: %ld", nagiosWarningLimit);
			break;
		case 'C':
			nagiosCriticalLimit = atol(optarg);
			if (nagiosCriticalLimit == 0){
				printf("The Nagios critical limit is 0. Disabled!\n");
			}
			DEBUG_PRINT("The Nagios critical limit is: %ld", nagiosCriticalLimit);
			break;
		case 't':
			responseReceiveTimeoutms = atoi(optarg);
			if (responseReceiveTimeoutms == 0){
				printf("The receive timeout value is 0!\n");
			}
			DEBUG_PRINT("The receive timeout value is: %ld", responseReceiveTimeoutms);
			break;
		case 'h':
			printHelp(argv[0]);
			exit(0);
		case '?':
			if (isprint(optopt))
				fprintf(stderr, "Unknown option `-%c'.\n", optopt);
			else
				fprintf(stderr, "Unknown option character `\\x%x'.\n",   optopt);
			break;
	   default:
			printHelp(argv[0]);
			exit(0);
		}
	}

	if (amServerName == NULL || strlen(amServerName) == 0) {
		printf("The Access Manager server hostname must be specified!\n");
		exit(EXIT_CODE_UNKNOWN_STATE);
	}

	if (amServerCtrlPort <= 0) {
		printf("The Access Manager server port number must be positive!\n");
		exit(EXIT_CODE_UNKNOWN_STATE);
	}

	if (commandIdx < 0) {
		printf("The command to send to the Access Manager server must be specified!\n");
		exit(EXIT_CODE_UNKNOWN_STATE);
	}

	sockfd = -1;

	/* send the message */
	if (sendMessage(amServerName, amServerCtrlPort, commands[commandIdx]) == 0){
		/* wait for reply if necessary */
		if (commandsReply[commandIdx]){
			DEBUG_PRINT("Receiving response for command [%s]", commands[commandIdx]);
			messageReceived = receiveMessage();
			if (messageReceived == NULL){
				printf("Response not received!\n");
				retcode = EXIT_CODE_CRITICAL;
			}else if (nagiosMode ) {
				/* Print the message in a special format for Nagios*/
				printf("AM_SERVER: %s %s",
						messageReceived,
						commands[commandIdx]);

				/* Try to convert the message into a number to verify the warning and critical limits */
				receivedNumber = atol(messageReceived);
				if (receivedNumber > 0){
					if (nagiosWarningLimit > 0 && receivedNumber > nagiosWarningLimit) {
						retcode = EXIT_CODE_WARNING;
					}
					if (nagiosCriticalLimit > 0 && receivedNumber > nagiosCriticalLimit) {
						retcode = EXIT_CODE_CRITICAL;
					}
				}
				switch (retcode){
				case EXIT_CODE_OK		: printf(" ");break;
				case EXIT_CODE_WARNING	: printf(" !WARNING!");break;
				case EXIT_CODE_CRITICAL	: printf(" !!!CRITICAL!!!");break;
				case EXIT_CODE_UNKNOWN_STATE	: printf(" ?UNKNOWN?");break;
				default					: printf(" return code unknown"); break;
				}

				printf("\n");
			}
		} else {
			DEBUG_PRINT("Response not expected for command [%s]", commands[commandIdx]);
			printf("Response not expected!\n");
		}
	}

	if (sockfd > 0){
		close(sockfd);
	}

    return retcode;
}
