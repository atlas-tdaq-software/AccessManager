package cern.atlas.daq.am.policy_browser;

import cern.atlas.daq.am.policy_browser.client.PolicyBrowserTest;
import com.google.gwt.junit.tools.GWTTestSuite;
import junit.framework.Test;
import junit.framework.TestSuite;

public class PolicyBrowserSuite extends GWTTestSuite {
  public static Test suite() {
    TestSuite suite = new TestSuite("Tests for PolicyBrowser");
    suite.addTestSuite(PolicyBrowserTest.class);
    return suite;
  }
}
