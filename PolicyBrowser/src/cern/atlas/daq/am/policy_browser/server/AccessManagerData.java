package cern.atlas.daq.am.policy_browser.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AccessManagerData extends HttpServlet {

//	private static String[] roles = { "ATLAS:BASIC:access",
//		"ATLAS:ControlRoom", "ATLAS:ControlRoom:expert", "ATLAS:POOLCOND",
//		"ATLAS:POOLCOND:SystemAccount", "ATLAS:RunCoordinator",
//		"ATLAS:ShiftLeader", "ATLAS:external", "ATLAS:observer",
//		"ATLAS:projector", "ATLAS:www", "ATLAS:www:remote",
//		"BASIC:swinstaller", "BCM", "BCM:DAQ:expert", "BCM:RolesAdmin",
//		"BCM:remote", "BCM:shifter", "CAL", "CAL:DAQ:expert",
//		"CAL:MON:expert", "CAL:RolesAdmin", "CAL:remote", "CAL:shifter",
//		"CAL:www", "CRD:ACRexternal:shifter", "CRD:ACRprojector:shifter",
//		"CRD:ATLAS:observer", "CRD:CAL:expert", "CRD:CAL:shifter",
//		"CRD:DCS:expert", "CRD:DCS:shifter", "CRD:DQM:expert",
//		"CRD:DQM:shifter", "CRD:ID:expert", "CRD:ID:shifter",
//		"CRD:LAR:expert", "CRD:LAR:shifter", "CRD:LBCF:expert",
//		"CRD:LBCF:shifter", "CRD:MUON:expert", "CRD:MUON:shifter",
//		"CRD:SHLD:shifter", "CRD:TDAQ:expert", "CRD:TDAQ:shifter",
//		"CRD:TIL:expert", "CRD:TIL:shifter", "CRD:TRG:expert",
//		"CRD:TRG:shifter", "CRD:slimos", "CTP", "CTP:DAQ:expert",
//		"CTP:MON:expert", "CTP:RolesAdmin", "CTP:remote", "DAQ:expert",
//		"DAQ:shifter", "DB:expert", "DCS", "DCS:ALL", "DCS:BCM:admin",
//		"DCS:BCM:expert", "DCS:BCM:observer", "DCS:BCM:operator",
//		"DCS:BIS:admin", "DCS:BIS:expert", "DCS:BIS:operator",
//		"DCS:BISRP:expert", "DCS:BISUP:expert", "DCS:CAL:expert",
//		"DCS:CAL:operator", "DCS:CIC:admin", "DCS:CIC:expert",
//		"DCS:CIC:observer", "DCS:CIC:operator", "DCS:CSC:admin",
//		"DCS:CSC:expert", "DCS:CSC:observer", "DCS:CSC:operator",
//		"DCS:CSCDET:expert", "DCS:ClusterAccess", "DCS:DBM:admin",
//		"DCS:DSS:admin", "DCS:DSS:expert", "DCS:DSS:observer",
//		"DCS:FWD:expert", "DCS:FWD:operator", "DCS:GAS:admin",
//		"DCS:GAS:expert", "DCS:GAS:observer", "DCS:GCS:admin",
//		"DCS:GCS:expert", "DCS:GCS:observer", "DCS:GCS:operator",
//		"DCS:GCSSYSOV:expert", "DCS:GCSSYSOV:operator", "DCS:IDE:admin",
//		"DCS:IDE:expert", "DCS:IDE:observer", "DCS:IDE:operator",
//		"DCS:IDG:expert", "DCS:IDG:operator", "DCS:LAR:admin",
//		"DCS:LAR:expert", "DCS:LAR:observer", "DCS:LAR:operator",
//		"DCS:LARHECLV:expert", "DCS:LARHECLV:operator", "DCS:LARHV:admin",
//		"DCS:LARHV:expert", "DCS:LARHV:observer", "DCS:LARHV:operator",
//		"DCS:LARPUR:expert", "DCS:LCD:admin", "DCS:LCD:expert",
//		"DCS:LCD:observer", "DCS:LCD:operator", "DCS:LHC:admin",
//		"DCS:LHC:expert", "DCS:LHC:observer", "DCS:LHC:operator",
//		"DCS:MDT:admin", "DCS:MDT:expert", "DCS:MDT:observer",
//		"DCS:MDT:operator", "DCS:MDTALGN:admin", "DCS:MDTALGN:expert",
//		"DCS:MDTDET:expert", "DCS:MDTGAS:expert", "DCS:MDTGAS:operator",
//		"DCS:MDTGMC:admin", "DCS:MDTGMC:expert", "DCS:MUO:admin",
//		"DCS:MUO:expert", "DCS:MUO:observer", "DCS:MUO:operator",
//		"DCS:MUOBIS:expert", "DCS:MUOCRATES:operator", "DCS:PIX:admin",
//		"DCS:PIX:expert", "DCS:PIX:observer", "DCS:PIX:operator",
//		"DCS:PIXINF:expert", "DCS:PIXRC:expert", "DCS:RPC:admin",
//		"DCS:RPC:expert", "DCS:RPC:observer", "DCS:RPC:operator",
//		"DCS:RPCDET:expert", "DCS:RPCL1:expert", "DCS:RPCL1:operator",
//		"DCS:RPO:admin", "DCS:RPO:expert", "DCS:RPO:observer",
//		"DCS:RPO:operator", "DCS:RolesAdmin", "DCS:SCT:admin",
//		"DCS:SCT:expert", "DCS:SCT:observer", "DCS:SCT:operator",
//		"DCS:SCTBIS:expert", "DCS:SHL:operator", "DCS:TDQ:admin",
//		"DCS:TDQ:expert", "DCS:TDQ:observer", "DCS:TDQ:operator",
//		"DCS:TDQADM:expert", "DCS:TDQL1RPC:expert",
//		"DCS:TDQL1RPC:operator", "DCS:TDQL1TGC:expert",
//		"DCS:TDQL1TGC:operator", "DCS:TGC:admin", "DCS:TGC:expert",
//		"DCS:TGC:observer", "DCS:TGC:operator", "DCS:TGCDET:expert",
//		"DCS:TI:admin", "DCS:TI:expert", "DCS:TI:observer",
//		"DCS:TIL:admin", "DCS:TIL:expert", "DCS:TIL:observer",
//		"DCS:TIL:operator", "DCS:TRT:admin", "DCS:TRT:expert",
//		"DCS:TRT:observer", "DCS:TRT:operator", "DCS:TRTDET:expert",
//		"DCS:TRTGAS:admin", "DCS:TRTGAS:expert", "DCS:TRTGAS:observer",
//		"DCS:TRTGGSS:admin", "DCS:TRTGGSS:expert", "DCS:TRTHV:admin",
//		"DCS:TRTHV:expert", "DCS:TRTLV:admin", "DCS:TRTLV:expert",
//		"DCS:ZDC:admin", "DCS:ZDC:expert", "DCS:ZDC:observer",
//		"DCS:ZDC:operator", "DCS:observer", "DCS:remote", "DCS:shifter",
//		"DCS:www", "DCSG", "DQM", "DQM:RolesAdmin", "DQM:expert",
//		"DQM:remote", "DQM:shifter", "DQM:www", "GAS:remote",
//		"ID:MON:expert", "ID:RolesAdmin", "ID:remote", "ID:shifter",
//		"IDE:RolesAdmin", "INDET", "L1C", "L1C:DAQ:expert",
//		"L1C:DQM:expert", "L1C:MON:expert", "L1C:RolesAdmin", "L1C:remote",
//		"L1C:swinstaller", "L1C:www", "LAR", "LAR:DAQ:expert",
//		"LAR:MON:expert", "LAR:RolesAdmin", "LAR:ServiceAccount:DAQ",
//		"LAR:ServiceAccount:DCS", "LAR:remote", "LAR:shifter",
//		"LAR:swinstaller", "LAR:www", "LBCF", "LBCF:DAQ:expert",
//		"LBCF:RolesAdmin", "LBCF:remote", "LBCF:shifter", "LBCF:www",
//		"LUCID", "LUCID:DAQ:expert", "LUCID:MON:expert",
//		"LUCID:RolesAdmin", "LUCID:remote", "MEDIPIX", "MPX:RolesAdmin",
//		"MPX:expert", "MPX:remote", "MUON:DAQ:expert", "MUON:MON:expert",
//		"MUON:RolesAdmin", "MUON:RunCoordinator", "MUON:expert",
//		"MUON:remote", "MUON:shifter", "MUON:swinstaller", "MUON:www",
//		"MUONS", "OFFLINE:swinstaller", "PIX", "PIX:DAQ:expert",
//		"PIX:MON:expert", "PIX:RolesAdmin", "PIX:SBC:expert", "PIX:remote",
//		"PIX:shifter", "PIX:www", "PIXDAQ", "PRESERIES",
//		"PRESERIES:swinstaller", "PRESERIES:user", "RPO", "RPO:DAQ:expert",
//		"RPO:RolesAdmin", "RPO:remote", "RPO:shifter", "RUNCOM:developer",
//		"RemoteAccess", "SCT", "SCT:DAQ:expert", "SCT:FSI:expert",
//		"SCT:MON:expert", "SCT:RolesAdmin", "SCT:remote", "SCT:shifter",
//		"SCT:www", "SHLD:shifter", "SLIMOS:shifter", "SYSADMINS", "SYSLOG",
//		"TDAQ", "TDAQ:BEAMSPOT:expert", "TDAQ:CASTOR:swinstaller",
//		"TDAQ:CC:expert", "TDAQ:L2:expert", "TDAQ:NETWORK:expert",
//		"TDAQ:RMON:expert", "TDAQ:ROS:expert", "TDAQ:RolesAdmin",
//		"TDAQ:SBC:expert", "TDAQ:SYSADMIN", "TDAQ:SYSADMIN:expert",
//		"TDAQ:commisioner", "TDAQ:expert", "TDAQ:genericadmin",
//		"TDAQ:patchinstaller", "TDAQ:remote", "TDAQ:shifter",
//		"TDAQ:swinstaller", "TDAQ:systemaccount", "TDAQ:www",
//		"TI:RolesAdmin", "TI:expert", "TI:remote", "TIL:CES:expert",
//		"TIL:DAQ:expert", "TIL:LAS:expert", "TIL:MON:expert",
//		"TIL:RolesAdmin", "TIL:remote", "TIL:shifter", "TIL:www", "TILE",
//		"TRG:MON:expert", "TRG:RolesAdmin", "TRG:confinstaller",
//		"TRG:expert", "TRG:patchinstaller", "TRG:remote", "TRG:shifter",
//		"TRG:swinstaller", "TRT", "TRT:DAQ:expert", "TRT:MON:expert",
//		"TRT:RolesAdmin", "TRT:remote", "TRT:shifter", "TRT:www", "ZDC",
//		"ZDC:DAQ:expert", "ZDC:RolesAdmin", "ZDC:remote", "www" };

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
	throws ServletException, IOException {

		String[] stockSymbols = req.getParameter("q").split(" ");

		if(stockSymbols.length == 1 && stockSymbols[0].compareTo("list-roles") == 0) {
			//
			// We are going to read a file called configuration.properties. This
			// file is placed under the WEB-INF directory.
			//
			String filename = "/WEB-INF/data.json";

			ServletContext context = getServletContext();

			//
			// First get the file InputStream using ServletContext.getResourceAsStream()
			// method.
			//
			InputStream is = context.getResourceAsStream(filename);
			if (is != null) {
				InputStreamReader isr = new InputStreamReader(is);
				BufferedReader reader = new BufferedReader(isr);
				PrintWriter writer = resp.getWriter();
				String text = "";

				//
				// We read the file line by line and later will be displayed on the
				// browser page.
				//
				while ((text = reader.readLine()) != null) {
					writer.println(text);
				}
			}
		}
//		else {
//			PrintWriter out = resp.getWriter();
//			out.println('[');
//			for (String role : roles) {
//				out.print('"');
//				out.print(role);
//				out.println("\",");
//			}
//			out.println(']');
//			out.flush();
//		}
	}

}

