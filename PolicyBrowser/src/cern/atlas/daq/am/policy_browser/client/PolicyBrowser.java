package cern.atlas.daq.am.policy_browser.client;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.OpenEvent;
import com.google.gwt.event.logical.shared.OpenHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONBoolean;
import com.google.gwt.json.client.JSONException;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.ui.DecoratorPanel;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class PolicyBrowser implements EntryPoint {

	private static final String JSON_URL = GWT.getModuleBaseURL() + "data?q=";

	private VerticalPanel mainPanel = new VerticalPanel();

	private HorizontalPanel mainBox = new HorizontalPanel();
	private Label infoMsgLabel = new Label();
	private Label errorMsgLabel = new Label();

	private VerticalPanel rolesPanel = new VerticalPanel();
	private ListBox rolesBox = new ListBox(true);
	private int rolesBoxWidth = 0;
	private ListBox categoryBoxDCS = new ListBox(false);
	private ListBox categoryBoxAbstract = new ListBox(false);
	private TextBox filterRoleTextBox = new TextBox();
	private String filterRoleValue = new String();
	private DisclosurePanel disclosureFilter = new DisclosurePanel("Show Roles of Users");
	private VerticalPanel disclosureFilterPanel = new VerticalPanel();
	private ListBox usersBox = new ListBox(true);
	private int usersBoxWidth = 0;
	private TextBox userBoxFilter = new TextBox();
	private String filterUserValue = new String();

	private HorizontalPanel navigationBox = new HorizontalPanel();
	private PushButton navigateFirstButton = new PushButton(new Image(new String("images/go-first.png")));
	private PushButton navigatePrevButton = new PushButton(new Image(new String("images/go-previous.png")));
	private PushButton navigateNextButton = new PushButton(new Image(new String("images/go-next.png")));
	private PushButton navigateLastButton = new PushButton(new Image(new String("images/go-last.png")));
//	private Button navigateFirstButton = new Button("<<");
//	private Button navigatePrevButton = new Button(" < ");
//	private Button navigateNextButton = new Button(" > ");
//	private Button navigateLastButton = new Button(">>");

	private VerticalPanel searchPanel = new VerticalPanel();
	private DecoratorPanel decPanel = new DecoratorPanel();

	private RolePanel rolePanel = new RolePanel(this);

	private Role[] roles = null;
	private Rule[] rules = null;
	private Sudo[] sudos = null;
	private User[] users = null;
	private String[] hosts = null;
	
	private Role[] selectedRoles = null;
	private User[] selectedUsers = null;
	
	/// new code
	
	ArrayList<Role> navigation = new ArrayList<Role>();
	int navigation_pos = 0;

	private void enableButton(PushButton button) {
		if(button.isEnabled() == false) { button.setEnabled(true); }
	}

	private void disableButton(PushButton button) {
		if(button.isEnabled() == true) { button.setEnabled(false); }
	}
	
	private void navigationShow() {
		if(navigation_pos <= navigation.size()) {
			rolePanel.draw(navigation.get(navigation_pos - 1));
		}

		if(navigation_pos == 1) {
			disableButton(navigateFirstButton);
			disableButton(navigatePrevButton);
		}
		else {
			enableButton(navigateFirstButton);
			enableButton(navigatePrevButton);
		}

		if(navigation_pos == navigation.size()) {
			disableButton(navigateLastButton);
			disableButton(navigateNextButton);
		}
		else {
			enableButton(navigateLastButton);
			enableButton(navigateNextButton);
		}
	}
	
	public void navigationAdd(Role r) {
		while(navigation_pos < navigation.size()) {
			navigation.remove(navigation.size()-1);
		}

		if(navigation_pos == 0 || navigation.get(navigation_pos - 1) != r) {
			navigation.add(r);
		}

		navigation_pos = navigation.size();
		navigationShow();
	}

	private void navigateFirst() {
        if(navigation_pos != 1) {
          navigation_pos = 1;
          navigationShow();
        }
	}

	private void navigateLast() {
		if(navigation_pos != navigation.size()) {
			navigation_pos = navigation.size();
			navigationShow();
		}
	}

	private void navigateNext() {
        if(navigation_pos < navigation.size()) {
          navigation_pos++;
          navigationShow();
        }
	}

	private void navigatePrev() {
        if(navigation_pos > 1) {
          navigation_pos--;
          navigationShow();
        }
	}

	/// end of new code

	public void onModuleLoad() {
		categoryBoxDCS.addItem("Show ALL Roles");
		categoryBoxDCS.addItem("Hide DCS Roles");
		categoryBoxDCS.addItem("Show DCS Roles Only");
		categoryBoxDCS.addStyleName("categoryBox");

		categoryBoxAbstract.addItem("Show ALL Roles");
		categoryBoxAbstract.addItem("Hide Assignable Roles");
		categoryBoxAbstract.addItem("Show Assignable Roles Only");
		categoryBoxAbstract.addStyleName("categoryBox");

		// Add a handler to handle drop box events

		categoryBoxDCS.addChangeHandler(new ChangeHandler() {
			public void onChange(ChangeEvent event) {
				refreshRolesList();
			}
		});

		categoryBoxAbstract.addChangeHandler(new ChangeHandler() {
			public void onChange(ChangeEvent event) {
				refreshRolesList();
			}
		});

		rolesPanel.add(new HTML("<b>Access Manager Roles:</b>"));
		rolesBox.addStyleName("rolesBox");
		rolesBox.setTitle("Select one role to see details");

		rolesPanel.add(rolesBox);

		decPanel.setWidget(searchPanel);
		decPanel.addStyleName("rolesSearchPanel");
		rolesPanel.add(decPanel);

		navigateFirstButton.setEnabled(false);
		navigatePrevButton.setEnabled(false);
		navigateNextButton.setEnabled(false);
		navigateLastButton.setEnabled(false);
		
		navigateFirstButton.setTitle("Go first");
		navigatePrevButton.setTitle("Go previous");
		navigateNextButton.setTitle("Go next");
		navigateLastButton.setTitle("Go last");

		
		navigationBox.add(navigateFirstButton);
		navigationBox.add(navigatePrevButton);
		navigationBox.add(navigateNextButton);
		navigationBox.add(navigateLastButton);

		rolesPanel.add(navigationBox);

		searchPanel.addStyleName("searchPanel");
		
		searchPanel.add(new HTML("<b>Show / Hide DCS Roles:</b>"));
		searchPanel.add(categoryBoxDCS);

		searchPanel.add(new HTML("<b>Show / Hide Assignable Roles:</b>"));
		searchPanel.add(categoryBoxAbstract);

		searchPanel.add(new HTML("<b>Filter Roles by Name:</b>"));
		searchPanel.add(filterRoleTextBox);
		filterRoleTextBox.addStyleName("filterRolesTextBox");
		filterRoleTextBox.setTitle("Filter roles by characters mask");

		rolesBox.setVisibleItemCount(20);

		rolesBox.addChangeHandler(new ChangeHandler() {
			public void onChange(ChangeEvent event) {
				refreshRole();
			}
		});

		// Add a KeyUpHandler
		filterRoleTextBox.addKeyUpHandler(new KeyUpHandler() {
			public void onKeyUp(KeyUpEvent event) {
				filterRolesList();
			}
		});

		// Add a ClickHandler
		filterRoleTextBox.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				filterRolesList();
			}
		});

		usersBox.setVisibleItemCount(6);
		usersBox.addStyleName("usersBox");
		usersBox.setTitle("Select users to see their roles");

		usersBox.addChangeHandler(new ChangeHandler() {
			public void onChange(ChangeEvent event) {
				refreshRolesList();
			}
		});
		
		disclosureFilterPanel.add(new HTML("<b>User Names:</b>"));
		disclosureFilterPanel.add(usersBox);

		disclosureFilterPanel.add(new HTML("<b>Filter Users by Name:</b>"));
		userBoxFilter.setTitle("Filter users by characters mask");
		userBoxFilter.setVisibleLength(16);
		userBoxFilter.addStyleName("filterUsersTextBox");
		disclosureFilterPanel.add(userBoxFilter);

		disclosureFilterPanel.addStyleName("searchPanel");

		userBoxFilter.addKeyUpHandler(new KeyUpHandler() {
			public void onKeyUp(KeyUpEvent event) {
				filterUsersList();
			}
		});

		// Add a ClickHandler
		userBoxFilter.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				filterUsersList();
			}
		});
		
		disclosureFilter.setContent(disclosureFilterPanel);
		disclosureFilter.setOpen(true);
		disclosureFilter.setTitle("Open to show roles of selected users");

		disclosureFilter.addStyleName("disclosureFilter");
		disclosureFilter.addCloseHandler(new CloseHandler<DisclosurePanel>() {
			public void onClose(CloseEvent<DisclosurePanel> event) {
				refreshRolesList();
			}
		});
		disclosureFilter.addOpenHandler(new OpenHandler<DisclosurePanel>() {
			public void onOpen(OpenEvent<DisclosurePanel> event) {
				refreshRolesList();
			}
		});
		searchPanel.add(disclosureFilter);
		
//		navigationBox.add(navigateFirstButton);
//		navigationBox.add(navigatePrevButton);
//		navigationBox.add(navigateNextButton);
//		navigationBox.add(navigateLastButton);

		navigateFirstButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if(navigation.isEmpty() == false) {
					navigateFirst();
				}
			}
		});
		
		navigatePrevButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if(navigation.isEmpty() == false) {
					navigatePrev();
				}
			}
		});

		navigateNextButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if(navigation.isEmpty() == false) {
					navigateNext();
				}
			}
		});

		navigateLastButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if(navigation.isEmpty() == false) {
					navigateLast();
				}
			}
		});

		mainBox.add(rolesPanel);
		mainBox.add(rolePanel.getPanel());

		mainPanel.add(mainBox);
		mainPanel.add(infoMsgLabel);
		mainPanel.add(errorMsgLabel);

		// Associate the Main panel with the HTML host page.
		RootPanel.get("rolesBrowser").add(mainPanel);

		getRolesNames();

		filterRoleTextBox.setFocus(true);
	}

	
	private void refreshRole() {
		int idx = rolesBox.getSelectedIndex();

		if(idx == -1) return;

		navigationAdd(selectedRoles[idx]);
	}

	// Only refresh list of roles if there are changes in filter
	private void filterRolesList() {
		final String symbol = filterRoleTextBox.getText().toUpperCase().trim();

		if(symbol.compareTo(filterRoleValue) != 0) {
			filterRoleValue = symbol;
			refreshRolesList();
		}

	}

	// Only refresh list of users if there are changes in filter
	private void filterUsersList() {
		final String symbol = userBoxFilter.getText().toUpperCase().trim();

		if(symbol.compareTo(filterUserValue) != 0) {
			filterUserValue = symbol;
			refreshUsersList();
		}
	}
	
	// Refresh list of roles
	private void refreshUsersList() {
		usersBox.clear();

		final String symbol = userBoxFilter.getText().trim();

		if(users != null) {
			int j = 0;
			for(int i=0; i < users.length; ++i) {
				User user = users[i];
				if(symbol.length() != 0 && user.getName().indexOf(symbol) == -1) continue;
				usersBox.addItem(users[i].getName());
				selectedUsers[j++] = user;
			}

			if(usersBoxWidth == 0) {
				usersBoxWidth = usersBox.getOffsetWidth();
			    String w = Integer.toString(usersBoxWidth) + "px";
				usersBox.setWidth(w);
				disclosureFilter.setOpen(false);
			}
		}
	}
	
	// Refresh list of roles
	private void refreshRolesList() {
		rolesBox.clear();

		int dcs_idx = categoryBoxDCS.getSelectedIndex();
		int abstract_idx = categoryBoxAbstract.getSelectedIndex();

		final String symbol = filterRoleTextBox.getText().toUpperCase().trim();

		Role[] rs = null;

		if(disclosureFilter.isOpen()) {
			int x = usersBox.getSelectedIndex();
			if(x != -1) {
				ArrayList<User> selected = new ArrayList<User>();
				selected.add(selectedUsers[x]);
				while(++x < usersBox.getItemCount()) {
					if(usersBox.isItemSelected(x)) {
						selected.add(selectedUsers[x]);
					}
				}
				
				TreeSet<Role> roles_of_users = new TreeSet<Role>();
				
				for(int i = 0 ; i < selected.size(); ++i) {
					ArrayList<Role> roles = selected.get(i).getAssignedRoles();
					if(roles != null) {
						for(int j = 0; j < roles.size(); ++j) {
							roles_of_users.add(roles.get(j));
						}
					}

					roles = selected.get(i).getEnabledRoles();
					if(roles != null) {
						for(int j = 0; j < roles.size(); ++j) {
							roles_of_users.add(roles.get(j));
						}
					}
				}
				
				int len_of_rs = roles_of_users.size();
				if(len_of_rs > 0) {
					rs = new Role[len_of_rs];
					Iterator<Role> itr = roles_of_users.iterator();
					int i = 0;
					while(itr.hasNext())  {
					  rs[i++] = itr.next();
					}
				}
			}
		}

		if(rs == null) {
			rs = roles;
		}

		if(rs != null) {
			int j = 0;
			for(int i=0; i < rs.length; ++i) {
				Role role = rs[i];
				if(dcs_idx == role.getDCS_Category()) continue;
				if(abstract_idx == role.getAbstractCategory()) continue;
				if(symbol.length() != 0 && role.getUpperCaseName().indexOf(symbol) == -1) continue;
				rolesBox.addItem(role.getName());
				selectedRoles[j++] = role;
			}

			// fix width or roles box when it is drawn first time and has max width
			if(rolesBoxWidth == 0) {
				rolesBoxWidth = rolesBox.getOffsetWidth();
				String w = Integer.toString(rolesBoxWidth) + "px";
				rolesBox.setWidth(w);
			}
		}
	}

	private JSONArray v2array(JSONObject o, String key, String what) {
		if(o.containsKey(key) == false) {
			return null;
		}

		JSONValue val = o.get(key);

		if(val == null) {
			displayError("Unexpected JSON: cannot get key \"" + key + "\" of " + what);
			return null;
		}

		JSONArray v = val.isArray();
		if(v == null) {
			displayError("Unexpected JSON: value of key \"" + key + "\" of " + what + " is not an array");
			return null;
		}

		return v;
	}

	private String get_string(JSONObject o, String key, String what) {
		JSONValue v = o.get(key);

		if(v == null) {
			displayError("Unexpected JSON: cannot get key \"" + key + "\" of " + what);
			return null;
		}

		JSONString s = v.isString();

		if(s == null) {
			displayError("Unexpected JSON: key \"" + key + "\" of " + what + " is not a string");
			return null;
		}

		return s.stringValue();
	}

	private JSONArray get_json_array(JSONObject o, String key, String what) {
		JSONValue v = o.get(key);

		if(v == null) {
			displayError("Unexpected JSON: cannot get key \"" + key + "\" of " + what);
			return null;
		}

		JSONArray a = v.isArray();

		if(a == null) {
			displayError("Unexpected JSON: key \"" + key + "\" of " + what + " is not a string");
			return null;
		}

		return a;
	}

	private Boolean get_boolean(JSONObject o, String key, Boolean defaut_value) {
		JSONValue v = o.get(key);

		if(v == null) {
			return defaut_value;
		}

		JSONBoolean b = v.isBoolean();

		if(b == null) {
			return defaut_value;
		}

		return b.booleanValue() ;
	}

	private String[] get_strings(JSONObject o, String key, String what) {
		JSONArray a = v2array(o, key, what);
		if(a == null) { return null; }

		String[] strings = new String[a.size()];

		for (int i = 0; i < a.size(); i++) {
			JSONString s = a.get(i).isString();
			if(s != null) {
				strings[i] = s.stringValue();
			}
			else {
				displayError("Unexpected JSON: value " + i + " of key \"" + key + "\" of " + what + " is not a string");
				return null;
			}
		}

		return strings;
	}

	private int[] get_ints(JSONObject o, String key, String what) {
		JSONArray a = v2array(o, key, what);
		if(a == null) { return null; }

		int[] ints = new int[a.size()];

		for (int i = 0; i < a.size(); i++) {
			JSONNumber n = a.get(i).isNumber();
			if(n != null) {
				ints[i] = (int) (n.doubleValue());
			}
			else {
				displayError("Unexpected JSON: value " + i + " of key \"" + key + "\" of " + what + " is not a number");
				return null;
			}
		}

		return ints;
	}

	private void getRolesNames() {
        // FIXME uncomment for debug
		String url = JSON_URL;
		url += "list-roles";

		// URL                      = http://pc-atlas-www-dev/tdaq/PolicyBrowser.html
		// GWT.getModuleBaseURL()   = http://pc-atlas-www-dev/tdaq/policybrowser/ 
		// GWT.getHostPageBaseURL() = http://pc-atlas-www-dev/tdaq/

        // FIXME uncomment for production
		// String url = GWT.getHostPageBaseURL() + "GetJSON.php";

		//url = "http://atlas-project-tdaq-cc.web.cern.ch/atlas-project-tdaq-cc/cgi/GetJSON.php";
		//System.out.println("url: " + url);
		url = URL.encode(url);

		// Send request to server and catch any errors.

		RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, url);

		try {
			builder.sendRequest(null, new RequestCallback() {
				public void onError(Request request, Throwable exception) {
					displayError("Couldn't retrieve JSON for list-roles request");
				}

				public void onResponseReceived(Request request, Response response) {
					if (200 == response.getStatusCode()) {
						//System.out.println("text: " + response.getText());
						
						JSONValue value = null;
						try {
						    value = JSONParser.parseStrict(response.getText());
						}
						catch(JSONException e) {
							displayError("Couldn't read data: " + e.getMessage());
							return;
						}

						JSONObject obj = value.isObject();

						if(obj != null) {
							// read last updated info message
							
							String info = get_string(obj, "last-updated", "top-level object");

							if(info != null) {
							  infoMsgLabel.setText("Last updated: " + info);
							}

							// read hosts

							hosts = get_strings(obj, "hosts", "top-level object");

							// read users

							JSONArray jusers = v2array(obj, "users", "top-level object");
							users = new User[jusers.size()];
							selectedUsers = new User[jusers.size()];

							for(int i = 0; i < jusers.size(); ++i) {
								JSONString s = jusers.get(i).isString();
								if(s != null) {
									users[i] = new User(s.stringValue());
								}
								else {
									displayError("Unexpected JSON: value " + i + " of \"users\" is not a string");
									users[i] = null;
								}
							}
	
							// read sudos

							JSONArray jsudos = v2array(obj, "sudos", "top-level object");
							sudos = new Sudo[jsudos.size()];

							for(int i = 0; i < jsudos.size(); ++i) {
								JSONObject jsudo = jsudos.get(i).isObject();

								String what = "sudo[" + i + ']';

								if(jsudo == null) {
									displayError("Bad JSON: cannot get " + what);
									return;
								}

								String name = get_string(jsudo, "n", what);
								String description = get_string(jsudo, "d", what);
								String run_as = get_string(jsudo, "r", what);
								Boolean authenticate = get_boolean(jsudo, "a", true);
								String[] commands = get_strings(jsudo, "c", what);

								if(name == null || description == null || run_as == null || commands == null) {
									return;
								}

								sudos[i] = new Sudo(name, description, run_as, authenticate, commands);
							}
							
							// read rules

							JSONArray jrules = v2array(obj, "rules", "top-level object");
							rules = new Rule[jrules.size()];

							for(int i = 0; i < jrules.size(); ++i) {
								JSONObject jrule = jrules.get(i).isObject();

								String what = "rule[" + i + ']';

								if(jrule == null) {
									displayError("Bad JSON: cannot get " + what);
									return;
								}

								String name = get_string(jrule, "n", what);
								JSONArray ja = get_json_array(jrule, "c", what);

								if(name == null || ja == null) {
									return;
								}

								ArrayList<TreeMap<String, String>> data = new ArrayList<TreeMap<String, String>>();

								for(int j = 0; j < ja.size(); ++j) {
									TreeMap<String, String> m = new TreeMap<String, String>();
									JSONObject jo = ja.get(j).isObject();
									Set<String> keys = jo.keySet();
									
									Iterator<String> itr = keys.iterator();
									while(itr.hasNext())  {
									  String key = itr.next();
									  String val = get_string(jo, key, what);
									  m.put(key, val);
									}

									data.add(m);
								}

								rules[i] = new Rule(name, data);
							}


							// read roles

							JSONArray jroles = v2array(obj, "roles", "top-level object");
							roles = new Role[jroles.size()];
							selectedRoles = new Role[jroles.size()];

							for(int i = 0; i < jroles.size(); ++i) {
								//System.out.println("role: " + i + " of " + jroles.size() + " is " + jroles.get(i));

								JSONObject jrole = jroles.get(i).isObject();
								Role r = new Role();

								String what = "role[" + i + ']';

								String name = get_string(jrole, "n", what);
								Boolean assignable = get_boolean(jrole, "a", true);
								Role[] base_roles = null;
								Sudo[] direct_sudos = null;
								Sudo[] derived_sudos = null;
								String[] login_hosts = null;
								User[] assigned_users = null;
								User[] enabled_users = null;
								Rule[] direct_rules = null;
								Rule[] derived_rules = null;

								int[] ints = get_ints(jrole, "s", what);
								if(ints != null) {
									derived_sudos = new Sudo[ints.length];
									for(int j = 0; j < ints.length; ++j) {
										derived_sudos[j] = sudos[ints[j]];
									}
								}

								ints = get_ints(jrole, "S", what);
								if(ints != null) {
									direct_sudos = new Sudo[ints.length];
									for(int j = 0; j < ints.length; ++j) {
										direct_sudos[j] = sudos[ints[j]];
									}
								}

								ints = get_ints(jrole, "h", what);
								if(ints != null) {
									login_hosts = new String[ints.length];
									for(int j = 0; j < ints.length; ++j) {
										login_hosts[j] = hosts[ints[j]];
									}
								}

								ints = get_ints(jrole, "u", what);
								if(ints != null) {
									assigned_users = new User[ints.length];
									for(int j = 0; j < ints.length; ++j) {
										assigned_users[j] = users[ints[j]];
										users[ints[j]].assigned_roles.add(r);
									}
								}

								ints = get_ints(jrole, "U", what);
								if(ints != null) {
									enabled_users = new User[ints.length];
									for(int j = 0; j < ints.length; ++j) {
										enabled_users[j] = users[ints[j]];
										users[ints[j]].enabled_roles.add(r);
									}
								}

								ints = get_ints(jrole, "l", what);
								if(ints != null) {
									derived_rules = new Rule[ints.length];
									for(int j = 0; j < ints.length; ++j) {
										derived_rules[j] = rules[ints[j]];
									}
								}

								ints = get_ints(jrole, "L", what);
								if(ints != null) {
									direct_rules = new Rule[ints.length];
									for(int j = 0; j < ints.length; ++j) {
										direct_rules[j] = rules[ints[j]];
									}
								}

								if(name == null) {
									return;
								}

								r.init(name, assignable, base_roles, direct_sudos, derived_sudos, login_hosts, assigned_users, enabled_users, direct_rules, derived_rules);
								roles[i] = r;
							}

							for(int i = 0; i < jroles.size(); ++i) {
								JSONObject jrole = jroles.get(i).isObject();

								String what = "role[" + i + ']';

								int[] ints = get_ints(jrole, "r", what);
								if(ints != null) {
									Role[] base_roles = new Role[ints.length];
									roles[i].base_roles = base_roles;
									for(int j = 0; j < ints.length; ++j) {
										base_roles[j] = roles[ints[j]];
										roles[ints[j]].addDerivedRole(roles[i]);
									}
								}
							}
						}
						else {
							displayError("Unexpected JSON: cannot get object");
							return;
						}
						
						refreshRolesList();
						refreshUsersList();
					}
					else {
						displayError("Couldn't retrieve JSON (" + response.getStatusText() + ")");
					}
				}
			});
		} catch (RequestException e) {
			displayError("Couldn't retrieve JSON, caught RequestException: " + e.getMessage());
		} catch (RuntimeException e) {
			displayError("Couldn't retrieve JSON, caught RuntimeException: " + e.getMessage());
		}

	}

	private void displayError(String error) {
		errorMsgLabel.setText("Error: " + error);
		errorMsgLabel.setVisible(true);
	}
	
	public void cleanNodesInfo() {
		if(roles != null) {
			for(int i = 0; i < roles.length; ++i) {
				roles[i].node = null;
			}
		}

		Node.s_id = 0;
	}
	
}
