package cern.atlas.daq.am.policy_browser.client;

public class NodesPair {

  private Node p_first = null;
  private Node p_second = null;

  public NodesPair(Node first, Node second) {
	  p_first = first;
	  p_second = second;
  }

  public Node getFirst() {
	  return p_first;
  }

  public Node getSecond() {
	  return p_second;
  }

}
