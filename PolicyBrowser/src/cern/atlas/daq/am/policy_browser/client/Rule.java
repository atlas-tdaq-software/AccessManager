package cern.atlas.daq.am.policy_browser.client;

import java.util.ArrayList;
import java.util.TreeMap;

public class Rule implements Comparable<Role> {

	private String name;
	ArrayList<TreeMap<String, String>> data = new ArrayList<TreeMap<String, String>>();

	public Rule(String n, ArrayList<TreeMap<String, String>> d) {
		name = n;
		data = d;
	}

	public String getName() {
		return this.name;
	}

	public ArrayList<TreeMap<String, String>> getData() {
		return this.data;
	}

	public int compareTo(Role o) {
		return this.getName().compareTo(o.getName());
	}

}
