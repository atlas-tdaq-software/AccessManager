package cern.atlas.daq.am.policy_browser.client;

public class Sudo {

	private String name;
	private String description;
	private String run_as;
	private Boolean authenticate;
	private String[] commands;
	
	public Sudo(String n, String d, String r, Boolean a, String[] c) {
		name = n;
		description = d;
		run_as = r;
		authenticate = a;
		commands = c;
	}

	public String getName() {
		return this.name;
	}

	public String getDescription() {
		return this.description;
	}

	public String getRunAs() {
		return this.run_as;
	}

	public Boolean getAuthenticate() {
		return this.authenticate;
	}

	public String[] getCommands() {
		return this.commands;
	}

}
