package cern.atlas.daq.am.policy_browser.client;

import java.util.ArrayList;

/**
 * 
 * The class implements node of inheritance graph.
 * It is used to convert such graph to tree duplicating nodes having multiple parents.
 *
 */

public class Node {

	private Role p_role;
	private int p_id;
	private String p_name;
	private ArrayList<Node> p_parents = new ArrayList<Node>();
	private ArrayList<Node> p_children = null;
	
	static int s_id = 0;

	public Node(Role r) {
		p_role = r;
		p_id = s_id++;
		p_name = r.getName() + p_id;
	}
	
	public String getName() {
		return p_name;
	}
	
	ArrayList<Node> getChildren() {
		return p_children;
	}
	
	Role getRole() {
		return p_role;
	}
	
	int getId() {
		return p_id;
	}

	public void addChild(Node n) {
		if(p_children == null) {
			p_children = new ArrayList<Node>();
		}
		else {
			for(int i = 0; i < p_children.size(); ++i) {
				if(p_children.get(i).p_role == n.p_role) {
					///System.out.println("skip add node " + n + " of " + this);
					return;
				}
			}
		}

		p_children.add(n);
	}
	
	public void replaceChild(Node n) {
		for(int i = 0; true; ++i) {
			if(p_children.get(i).p_role == n.p_role) {
				p_children.set(i, n);
				break;
			}
		}
	}

	public void addParent(Node n) {
		p_parents.add(n);
	}

	public Node copy() {
		Node n = new Node(p_role);
		
		n.p_parents = new ArrayList<Node>(p_parents);

		if(p_children != null) {
			n.p_children = new ArrayList<Node>(p_children.size());
			for(int i = 0; i < p_children.size(); ++i) {
				n.addChild(p_children.get(i).copy());
			}
		}

		return n;
	}

	public void split() {
		if(p_children != null) {
			for(int i = 0; i < p_children.size(); ++i) {
				p_children.get(i).split();
			}
		}

		for(int i = 1; i < p_parents.size(); ++i) {
			Node p = p_parents.get(i);
			if(p != null) {
				Node n = copy();
				p.replaceChild(n);
				p_parents.set(i, null);
			}
		}
	}

	public void fill(ArrayList<NodesPair> inheritanceHierarchy) {
		if(p_children != null) {
			for(int i = 0; i < p_children.size(); ++i) {
				inheritanceHierarchy.add(new NodesPair(p_children.get(i), this));
				p_children.get(i).fill(inheritanceHierarchy);
			}
		}
	}
		
	public void print(String prefix) {
		System.out.println(prefix + "Node " + p_role.getName() + " (" + this + ')');
		String prefix2 = new String(prefix + "  ");
		String prefix4 = new String(prefix + "    ");

		System.out.print(prefix2 + "parents: ");
		if(p_parents != null) {
			for(int i = 0; i < p_parents.size(); ++i) {
				Node p = p_parents.get(i);
				if(p != null) {
					System.out.print(p.p_role.getName() + " (" + p + "), ");
				}
				else {
					System.out.print("(null), ");
				}
			}
		}
		else {
			System.out.print("[null]");
		}
		System.out.print("\n");
		
		System.out.println(prefix2 + "children:");
		if(p_children != null) {
			for(int i = 0; i < p_children.size(); ++i) {
				p_children.get(i).print(prefix4);
			}
		}
	}
}
