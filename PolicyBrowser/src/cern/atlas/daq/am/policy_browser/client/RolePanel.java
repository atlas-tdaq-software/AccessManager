package cern.atlas.daq.am.policy_browser.client;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.user.client.ui.DecoratorPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.Selection;
import com.google.gwt.visualization.client.VisualizationUtils;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.events.SelectHandler;
import com.google.gwt.visualization.client.visualizations.OrgChart;
import com.google.gwt.visualization.client.visualizations.OrgChart.Options;

public class RolePanel {
	private static final int MIN_LISTBOX_H = 5;
	private static final int MAX_LISTBOX_H = 20;

	private VerticalPanel rolePanel = new VerticalPanel();
	private HTML roleHeaderHTML = new HTML();

	private DecoratorPanel baseInheritanceHierarchyDecPanel = new DecoratorPanel();
	private VerticalPanel baseInheritanceHierarchyPanel = new VerticalPanel();
	private HTML baseInheritanceHierarchyHTML = new HTML();
	private OrgChart baseInheritanceHierarchyChart = null;
	private DataTable baseInheritanceHierarchyTable = null;
	private Options baseInheritanceHierarchyOptions = null;

	private DecoratorPanel derivedInheritanceHierarchyDecPanel = new DecoratorPanel();
	private VerticalPanel derivedInheritanceHierarchyPanel = new VerticalPanel();
	private HTML derivedInheritanceHierarchyHTML = new HTML();
	private OrgChart derivedInheritanceHierarchyChart = null;
	private DataTable derivedInheritanceHierarchyTable = null;
	private Options derivedInheritanceHierarchyOptions = null;

	private DecoratorPanel sudoTableDecPanel = new DecoratorPanel();
	private VerticalPanel sudoTablePanel = new VerticalPanel();
	private FlexTable roleDirectSudoTable = new FlexTable();
	private FlexTable roleDerivedSudoTable = new FlexTable();
	
	private DecoratorPanel rulesDecPanel = new DecoratorPanel();
	private VerticalPanel rulesPanel = new VerticalPanel();
    private HorizontalPanel listBoxes = new HorizontalPanel();
	private DecoratorPanel hostsDecPanel = new DecoratorPanel();
	private VerticalPanel hostsPanel = new VerticalPanel();
	private HTML hostsBoxHeaderHTML = new HTML();
	private ListBox hostsTable = new ListBox(true);
	private DecoratorPanel usersDecPanel = new DecoratorPanel();
	private VerticalPanel usersPanel = new VerticalPanel();
	private HTML usersBoxHeaderHTML = new HTML();
	private ListBox usersTable = new ListBox(true);
	private Role[] base_roles = null;
	private Role[] derived_roles = null;

	private PolicyBrowser mainWindow = null;

    private DataTable makeOrgChartTable() {
    	DataTable table = DataTable.create();

    	table.addColumn(ColumnType.STRING, "Name");
    	table.addColumn(ColumnType.STRING, "Manager");
    	table.addColumn(ColumnType.STRING, "ToolTip");

    	return table;
    }
    
    private Options makeOrgChartOptions() {
    	Options options = Options.create();

    	options.setAllowHtml(true);

    	return options;
    }
    
    private void setSudoTableProperties(FlexTable table) {
		table.setText(0, 0, "Name");
		table.setText(0, 1, "Commands");
		table.setText(0, 2, "Runs As");
		table.setText(0, 3, "Description");
		table.getRowFormatter().addStyleName(0, "tableH");
		table.addStyleName("sudosList");
		table.setCellPadding(3);
		table.setVisible(false);
    }
	
    RolePanel(PolicyBrowser w) {
    	mainWindow = w;

    	setSudoTableProperties(roleDirectSudoTable);
       	setSudoTableProperties(roleDerivedSudoTable);

		rolePanel.add(roleHeaderHTML);

		rolePanel.add(baseInheritanceHierarchyDecPanel);
		baseInheritanceHierarchyDecPanel.setVisible(false);
		baseInheritanceHierarchyDecPanel.addStyleName("decRolePanel");

		rolePanel.add(derivedInheritanceHierarchyDecPanel);
		derivedInheritanceHierarchyDecPanel.setVisible(false);
		derivedInheritanceHierarchyDecPanel.addStyleName("decRolePanel");

	    // Create a callback to be called when the visualization API has been loaded.
	    Runnable onLoadCallback = new Runnable() {
	      public void run() {
	    	baseInheritanceHierarchyTable = makeOrgChartTable();
	    	baseInheritanceHierarchyOptions = makeOrgChartOptions();

			baseInheritanceHierarchyChart = new OrgChart(baseInheritanceHierarchyTable, baseInheritanceHierarchyOptions);
			baseInheritanceHierarchyChart.addStyleName("roleOrgChart");

			baseInheritanceHierarchyChart.addSelectHandler(new SelectHandler() {
	            @Override
	            public void onSelect(SelectEvent event) {
	              JsArray<Selection> s = baseInheritanceHierarchyChart.getSelections();
	              mainWindow.navigationAdd(base_roles[s.get(0).getRow()]);
	            }
	          });

			baseInheritanceHierarchyPanel.add(baseInheritanceHierarchyHTML);
			baseInheritanceHierarchyPanel.add(baseInheritanceHierarchyChart);

			baseInheritanceHierarchyDecPanel.add(baseInheritanceHierarchyPanel);


			derivedInheritanceHierarchyTable = makeOrgChartTable();
			derivedInheritanceHierarchyOptions = makeOrgChartOptions();
			derivedInheritanceHierarchyOptions.setColor("#DBE4ED");

			derivedInheritanceHierarchyChart = new OrgChart(derivedInheritanceHierarchyTable, derivedInheritanceHierarchyOptions);
			derivedInheritanceHierarchyChart.addStyleName("roleOrgChart");

			derivedInheritanceHierarchyChart.addSelectHandler(new SelectHandler() {
	            @Override
	            public void onSelect(SelectEvent event) {
	              JsArray<Selection> s = derivedInheritanceHierarchyChart.getSelections();
	              mainWindow.navigationAdd(derived_roles[s.get(0).getRow()]);
	            }
	          });

			derivedInheritanceHierarchyPanel.add(derivedInheritanceHierarchyHTML);
			derivedInheritanceHierarchyPanel.add(derivedInheritanceHierarchyChart);

			derivedInheritanceHierarchyDecPanel.add(derivedInheritanceHierarchyPanel);
	      }
	    };

	    // Load the visualization api, passing the onLoadCallback to be called when loading is done.
	    VisualizationUtils.loadVisualizationApi(onLoadCallback, OrgChart.PACKAGE);

		sudoTableDecPanel.add(sudoTablePanel);
		sudoTableDecPanel.setVisible(false);
		sudoTableDecPanel.addStyleName("decRolePanel");

		rolePanel.add(sudoTableDecPanel);

/*		sudoDerivedTablePanel.add(roleDerivedSudoTableHeaderHTML);
		sudoDerivedTablePanel.add(roleDerivedSudoTable);

		sudoDerivedTableDecPanel.add(sudoDerivedTablePanel);
		sudoDerivedTableDecPanel.setVisible(false);
		sudoDerivedTableDecPanel.addStyleName("decRolePanel");

		rolePanel.add(sudoDerivedTableDecPanel);*/

		rulesDecPanel.add(rulesPanel);
		rulesDecPanel.setVisible(false);
		rulesDecPanel.addStyleName("decRolePanel");

		rolePanel.add(rulesDecPanel);

		hostsTable.setVisibleItemCount(MIN_LISTBOX_H);
		usersTable.setVisibleItemCount(MIN_LISTBOX_H);

		hostsDecPanel.setVisible(false);
		usersDecPanel.setVisible(false);
		hostsDecPanel.addStyleName("decRolePanel");
		usersDecPanel.addStyleName("decRolePanel");

		hostsPanel.add(hostsBoxHeaderHTML);
		hostsPanel.add(hostsTable);

		usersPanel.add(usersBoxHeaderHTML);
		usersPanel.add(usersTable);

		hostsDecPanel.add(hostsPanel);
		usersDecPanel.add(usersPanel);

		listBoxes.add(hostsDecPanel);
		listBoxes.add(usersDecPanel);

		rolePanel.add(listBoxes);
   }

    public VerticalPanel getPanel() {
    	return rolePanel;
    }

    private void addRole(Node n, int idx, DataTable table, Role[] roles) {
    	Role r = n.getRole();
    	if(r.getIsAssignable()) {
    		table.setCell(idx, 0, n.getName(), "<b>" + r.getName() + "</b>", null);
    	}
    	else {
    		table.setCell(idx, 0, n.getName(), "<i>" + r.getName() + "</i>", null);
    	}
    	roles[idx] = r;
    }

    private void clearTable(FlexTable table) {
		for(int i = table.getRowCount() - 1; i > 0; --i) {
			table.removeRow(i);
		}

    }
    
    private void addRule(FlexTable table, String title) {
    	if(table != null) {
    		HTML header = new HTML("<h3>" + title + "</h3>");
    		rulesPanel.add(header);
    		rulesPanel.add(table);
    	}
    }

    private void draw_sudo_info(Sudo[] sudos, FlexTable table) {
		clearTable(table);

		if(sudos != null) {
			HTMLTable.RowFormatter rf = table.getRowFormatter();
			table.getRowFormatter().addStyleName(0, "watchListR1");

			for(int i = 0; i < sudos.length;) {
				Sudo s = sudos[i];

				String commands_str = new String();
				String[] commands = s.getCommands();
				if(commands != null) {
					for(int j = 0; j < commands.length; ++j) {
						if(j != 0) commands_str += "<br>\n";
						commands_str += commands[j];
					}
				}

				++i;

				table.setText(i, 0, s.getName());
				table.setHTML(i, 1, commands_str);
				table.setText(i, 2, s.getRunAs());
				table.setText(i, 3, s.getDescription());

			    rf.addStyleName(i, ((i % 2) == 0) ? "tableR1" : "tableR2");
			}

			if(table.isVisible() == false) {
				table.setVisible(true);
			}
		}
		else {
			if(table.isVisible() == true) {
				table.setVisible(false);
			}
		}
    }
    
    void draw(Role role) {
    	
    	// make decoration panels visible on first call

    	if(baseInheritanceHierarchyDecPanel.isVisible() == false) {
    		baseInheritanceHierarchyDecPanel.setVisible(true);
    		derivedInheritanceHierarchyDecPanel.setVisible(true);
    		sudoTableDecPanel.setVisible(true);
    		rulesDecPanel.setVisible(true);
    		hostsDecPanel.setVisible(true);
    		usersDecPanel.setVisible(true);
    	}


    	// clear sudo vertical panel; it is filled as needed depending on direct/derived sudo rules availability

    	sudoTablePanel.clear();


    	// set title of Roles panel

    	roleHeaderHTML.setHTML("<h2>" + (role.getIsAssignable() ? "" : "Non-") + "Assignable Role \"" + role.getName() + "\"</h2>");


		// fill base inheritance hierarchy box

		String text = "<h3>Inheritance Hierarchy: Base Roles</h3>";
		
		if(role.getBaseRoles() != null) {
			baseInheritanceHierarchyTable = makeOrgChartTable();

			//baseInheritanceHierarchyTable = DataTable.create();

	  		//baseInheritanceHierarchyTable.addColumn(ColumnType.STRING, "Name");
			//baseInheritanceHierarchyTable.addColumn(ColumnType.STRING, "Manager");
			//baseInheritanceHierarchyTable.addColumn(ColumnType.STRING, "ToolTip");

			ArrayList<NodesPair> inheritanceHierarchy = new ArrayList<NodesPair>();

			mainWindow.cleanNodesInfo();
			role.makeBaseInheritanceGraph(null);
			//role.node.print("==");
			role.node.split();
			//role.node.print("");

			role.node.fill(inheritanceHierarchy);

			base_roles = new Role[inheritanceHierarchy.size() + 1];

			baseInheritanceHierarchyTable.addRows(inheritanceHierarchy.size() + 1);
			addRole(role.node, 0, baseInheritanceHierarchyTable, base_roles);

			for(int i = 0 ; i < inheritanceHierarchy.size();) {
				NodesPair p = inheritanceHierarchy.get(i++);
				addRole(p.getFirst(), i, baseInheritanceHierarchyTable, base_roles);
				baseInheritanceHierarchyTable.setValue(i, 1, p.getSecond().getName());
			}

			baseInheritanceHierarchyChart.draw(baseInheritanceHierarchyTable, baseInheritanceHierarchyOptions);

			if(baseInheritanceHierarchyChart.isVisible() == false) {
				baseInheritanceHierarchyChart.setVisible(true);
			}
		}
		else {
			if(baseInheritanceHierarchyChart.isVisible() == true) {
				baseInheritanceHierarchyChart.setVisible(false);
			}

			text += "there are no base roles";
		}

		baseInheritanceHierarchyHTML.setHTML(text);


		
		// fill derived inheritance hierarchy box

		text = "<h3>Inheritance Hierarchy: Derived Roles</h3>";

		if(role.getDerivedRoles() != null) {
			derivedInheritanceHierarchyTable = makeOrgChartTable();
			
	    	//derivedInheritanceHierarchyTable = DataTable.create();

	  		//derivedInheritanceHierarchyTable.addColumn(ColumnType.STRING, "Name");
			//derivedInheritanceHierarchyTable.addColumn(ColumnType.STRING, "Manager");
			//derivedInheritanceHierarchyTable.addColumn(ColumnType.STRING, "ToolTip");

			ArrayList<NodesPair> inheritanceHierarchy = new ArrayList<NodesPair>();

			mainWindow.cleanNodesInfo();
			role.makeDerivedInheritanceGraph(null);
			role.node.split();

			role.node.fill(inheritanceHierarchy);

			derived_roles = new Role[inheritanceHierarchy.size() + 1];

			derivedInheritanceHierarchyTable.addRows(inheritanceHierarchy.size() + 1);
			addRole(role.node, 0, derivedInheritanceHierarchyTable, derived_roles);

			for(int i = 0 ; i < inheritanceHierarchy.size();) {
				NodesPair p = inheritanceHierarchy.get(i++);
				addRole(p.getFirst(), i, derivedInheritanceHierarchyTable, derived_roles);
				derivedInheritanceHierarchyTable.setValue(i, 1, p.getSecond().getName());
			}

			derivedInheritanceHierarchyChart.draw(derivedInheritanceHierarchyTable, derivedInheritanceHierarchyOptions);

			if(derivedInheritanceHierarchyChart.isVisible() == false) {
				derivedInheritanceHierarchyChart.setVisible(true);
			}
		}
		else {
			if(derivedInheritanceHierarchyChart.isVisible() == true) {
				derivedInheritanceHierarchyChart.setVisible(false);
			}

			text += "there are no derived roles";
		}

		derivedInheritanceHierarchyHTML.setHTML(text);


		// fill direct sudo commands box

		if(role.getDirectSudos() == null && role.getDerivedSudos() == null) {
			sudoTablePanel.add(new HTML("<h3>Sudo Commands</h3>no sudo commands"));
		}
		else {
			if(role.getDirectSudos() != null) {
				sudoTablePanel.add(new HTML("<h3>Direct Sudo Commands</h3>"));
				sudoTablePanel.add(roleDirectSudoTable);
				draw_sudo_info(role.getDirectSudos(), roleDirectSudoTable);
			}
			
			if(role.getDerivedSudos() != null) {
				sudoTablePanel.add(new HTML("<h3>Derived Sudo Commands</h3>"));
				sudoTablePanel.add(roleDerivedSudoTable);
				draw_sudo_info(role.getDerivedSudos(), roleDerivedSudoTable);
			}
		}

		
		// fill rules box

		rulesPanel.clear();

		Rule[] direct_rules = role.getDirectRules();
		Rule[] all_rules = role.getAllRules();

		if(direct_rules == null && all_rules == null) {
            rulesPanel.add(new HTML("<h3>Rules</h3>no rules"));
		}
		else {
			RulesPanel direct_rules_panel = (direct_rules != null ? new RulesPanel(direct_rules) : null);
			RulesPanel derived_rules_panel = (all_rules != null ? new RulesPanel(all_rules) : null);

			if(direct_rules_panel != null) {
				addRule(direct_rules_panel.getDatabaseTable(), "Direct OKS Database Rules");
			}

			if(derived_rules_panel != null) {
				addRule(derived_rules_panel.getDatabaseTable(), "Derived OKS Database Rules");
			}
			
			if(direct_rules_panel != null) {
				addRule(direct_rules_panel.getOperatingSystemTable(), "Direct Operating System Rules");
			}

			if(derived_rules_panel != null) {
				addRule(derived_rules_panel.getOperatingSystemTable(), "Derived Operating System Rules");
			}

			if(direct_rules_panel != null) {
				addRule(direct_rules_panel.getProcessManagerTable(), "Direct Process Manager Rules");
			}

			if(derived_rules_panel != null) {
				addRule(derived_rules_panel.getProcessManagerTable(), "Derived Process Manager Rules");
			}

			if(direct_rules_panel != null) {
				addRule(direct_rules_panel.getResourceManagerTable(), "Direct Resource Manager Rules");
			}

			if(derived_rules_panel != null) {
				addRule(derived_rules_panel.getResourceManagerTable(), "Derived Resource Manager Rules");
			}
			
			if(direct_rules_panel != null) {
				addRule(direct_rules_panel.getRunControlTable(), "Direct Run Control Rules");
			}

			if(derived_rules_panel != null) {
				addRule(derived_rules_panel.getRunControlTable(), "Derived Run Control Rules");
			}
			
			if(direct_rules_panel != null) {
				addRule(direct_rules_panel.getTriggerCommanderTable(), "Direct Trigger Commander Rules");
			}

			if(derived_rules_panel != null) {
				addRule(derived_rules_panel.getTriggerCommanderTable(), "Derived Trigger Commander Rules");
			}
			
			if(direct_rules_panel != null) {
				addRule(direct_rules_panel.getIGUITable(), "Direct IGUI Rules");
			}

			if(derived_rules_panel != null) {
				addRule(derived_rules_panel.getIGUITable(), "Derived IGUI Rules");
			}
			
			if(direct_rules_panel != null) {
				addRule(direct_rules_panel.getBCMPanelTable(), "Direct BCM Panel Rules");
			}

			if(derived_rules_panel != null) {
				addRule(derived_rules_panel.getBCMPanelTable(), "Derived BCM Panel Rules");
			}
		}


		// fill login hosts box

		hostsTable.clear();

		text = "<h3>Login Hosts</h3>\n";

		String[] login_hosts = role.getLoginHosts();

		if(login_hosts != null) {
			for(int i = 0; i < login_hosts.length;) {
				String n = login_hosts[i++];
				hostsTable.addItem(n);
			}

			if(hostsTable.isVisible() == false) {
				hostsTable.setVisible(true);
			}
		}
		else {
			if(hostsTable.isVisible() == true) {
				hostsTable.setVisible(false);
			}

			text += "no hosts";
			
		}
		
		hostsBoxHeaderHTML.setHTML(text);

		
		// fill users box

		usersTable.clear();

		text = "<h3>Users";


		TreeSet<String> all_users = new TreeSet<String>();
		
		int num_of_enabled = 0;

		User[] users = role.getAssignedUsers();

		if(users != null) {
			for(int i = 0; i < users.length; ++i) {
				all_users.add(users[i].getName());
			}
		}

		users = role.getEnabledUsers();

		if(users != null) {
			num_of_enabled = users.length;

			for(int i = 0; i < num_of_enabled; ++i) {
				all_users.add(users[i].getName() + " (*)");
			}
		}

		if(all_users.size() > 0) {
			Iterator<String> itr = all_users.iterator();
			while(itr.hasNext())  {
			  usersTable.addItem(itr.next());
			}

			if(usersTable.isVisible() == false) {
				usersTable.setVisible(true);
			}

			text += " (" + num_of_enabled + "/" + all_users.size() + ")</h3>";
		}
		else {
			if(usersTable.isVisible() == true) {
				usersTable.setVisible(false);
			}

			text += "</h3>no users";
		}

		usersBoxHeaderHTML.setHTML(text);


		// adjust height of list-boxes
		
		int n = hostsTable.getItemCount();
		int n2 = usersTable.getItemCount();
		//System.out.println("DEBUG: n: " + n + ", n2: " + n2);

		if(n2 > n) n = n2;

		if(n < MIN_LISTBOX_H) n = MIN_LISTBOX_H;
		else if(n > MAX_LISTBOX_H) n = MAX_LISTBOX_H;

		//System.out.println("DEBUG: set n: " + n);

		hostsTable.setVisibleItemCount(n);
		usersTable.setVisibleItemCount(n);
   }
}
