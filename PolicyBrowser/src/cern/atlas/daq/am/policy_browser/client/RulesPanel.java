package cern.atlas.daq.am.policy_browser.client;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.TreeSet;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTMLTable;
import com.google.gwt.user.client.ui.HasVerticalAlignment;

public class RulesPanel {

	// database rules
	private TreeMap<String, TreeSet<String>> db_rules = null;
	private boolean is_db_admin;

	// other rules
	private ArrayList<TreeMap<String, String>> os_rules = null;
	private ArrayList<TreeMap<String, String>> pmg_rules = null;
	private ArrayList<TreeMap<String, String>> rm_rules = null;
	private ArrayList<TreeMap<String, String>> rc_rules = null;
	private ArrayList<TreeMap<String, String>> tc_rules = null;
	private ArrayList<TreeMap<String, String>> igui_rules = null;
	private ArrayList<TreeMap<String, String>> bcm_panel_rules = null;

	RulesPanel(Rule[] rules) {
		is_db_admin = false;

		for(int i = 0; i < rules.length; ++i) {
			ArrayList<TreeMap<String, String>> data = rules[i].getData();

			for(int j = 0; j < data.size(); ++j) {
				TreeMap<String, String> a = data.get(j);

				String category = a.get("ResourceCategory");
				if(category == null) {
					System.err.println("ERROR: no ResourceCategory found");
					continue;
				}

				if(category.compareTo("DataBase") == 0) {
					String action_id = a.get("ActionId");

					if(action_id.compareTo("admin") == 0) {
						is_db_admin = true;
						continue;
					}

					String type_path = a.get("ResourceTypePath");

					if(db_rules == null) db_rules = new TreeMap<String, TreeSet<String>>();

					// create_subdir delete_subdir create_file delete_file update_file
					TreeSet<String> action = db_rules.get(action_id);

					if(action == null) {
						action = new TreeSet<String>();
						db_rules.put(action_id, action);
					}

					action.add(type_path);
				}
				else if(category.compareTo("os") == 0) {
					if(os_rules == null) os_rules = new ArrayList<TreeMap<String, String>>();
					os_rules.add(a);
				}
				else if(category.compareTo("pmg") == 0) {
					if(pmg_rules == null) pmg_rules = new ArrayList<TreeMap<String, String>>();
					pmg_rules.add(a);
				}
				else if(category.compareTo("ResourceManager") == 0) {
					if(rm_rules == null) rm_rules = new ArrayList<TreeMap<String, String>>();
					rm_rules.add(a);
				}
				else if(category.compareTo("RunControl") == 0) {
					if(rc_rules == null) rc_rules = new ArrayList<TreeMap<String, String>>();
					rc_rules.add(a);
				}
				else if(category.compareTo("TriggerCommander") == 0) {
					if(tc_rules == null) tc_rules = new ArrayList<TreeMap<String, String>>();
					tc_rules.add(a);
				}
				else if(category.compareTo("igui") == 0) {
					if(igui_rules == null) igui_rules = new ArrayList<TreeMap<String, String>>();
					igui_rules.add(a);
				}
				else if(category.compareTo("BCM") == 0) {
					if(bcm_panel_rules == null) bcm_panel_rules = new ArrayList<TreeMap<String, String>>();
					bcm_panel_rules.add(a);
				}
				else {
					System.err.println("ERROR: unexpected category \"" + category + "\"");
				}
			}
		}
	}

	public boolean getIsDbAdmin() {
		return this.is_db_admin;
	}

	private String a2str(TreeSet<String> d) {
		String s = new String();

		if(d != null) {
			Iterator<String> itr = d.iterator();
			while(true) {
				s += itr.next();
				if(itr.hasNext() == false) {
					break;	
				}
				else {
					s += "<br>";
				}
			}
		}

		return s;
	}
	
	public FlexTable getDatabaseTable() {
		if(db_rules == null && is_db_admin == false) {
			return null;
		}
		else {
			FlexTable table = new FlexTable();
			HTMLTable.RowFormatter rf = table.getRowFormatter();

			table.setText(0, 0, "Create Directory");
			table.setText(0, 1, "Delete Directory");
			table.setText(0, 2, "Create File");
			table.setText(0, 3, "Delete File");
			table.setText(0, 4, "Update File");

			if(is_db_admin) {
				for(int i = 0; i < 5; ++i) {
					table.setText(1, i, "any (as DB admin)");
				}
			}
			else {
				table.setHTML(1, 0, a2str(db_rules.get("create_subdir")));
				table.setHTML(1, 1, a2str(db_rules.get("delete_subdir")));
				table.setHTML(1, 2, a2str(db_rules.get("create_file")));
				table.setHTML(1, 3, a2str(db_rules.get("delete_file")));
				table.setHTML(1, 4, a2str(db_rules.get("update_file")));
			}

			table.addStyleName("sudosList");
			table.setCellPadding(3);

			rf.addStyleName(0, "tableH");
			rf.addStyleName(1, "tableR2");
			rf.setVerticalAlign(1, HasVerticalAlignment.ALIGN_TOP );

			return table;
		}
	}


	private FlexTable makeTable(ArrayList<TreeMap<String, String>> rules, String[] titles, String[] keys) {
        if(rules == null) {
        	return null;
        }
        else {
			FlexTable table = new FlexTable();
			HTMLTable.RowFormatter rf = table.getRowFormatter();

			for(int i = 0 ; i < titles.length; ++i) {
				table.setText(0, i, titles[i]);
			}

			rf.addStyleName(0, "tableH");

			for(int i = 0; i < rules.size();) {
				TreeMap<String, String> m = rules.get(i);

				++i;

				for(int j = 0; j < keys.length; ++j) {
					String v = m.get(keys[j]);
					table.setText(i, j, (v != null ? v : ""));
				}

				rf.addStyleName(i, ((i % 2) == 0) ? "tableR1" : "tableR2");
			}

			table.addStyleName("sudosList");
			table.setCellPadding(3);

			return table;

        }
		
	}


    public FlexTable getProcessManagerTable() {

		final String titles[] = {
				"Action",
				"Resource",
				"Host Name",
				"Arguments",
				"Owned By Requester",
				"Partition",
				"Decision"
		};
		
		final String keys[] = {
				"ActionId",
				"ResourceId",
				"ResourceTypeHostname",
				"ResourceTypeArguments",
				"ResourceTypeOwnedByRequester",
				"ResourceTypePartition",
				"Decision"
		};

		return makeTable(pmg_rules, titles, keys);	

	}


    public FlexTable getOperatingSystemTable() {

		final String titles[] = {
				"Action",
				"Resource",
				"Type",
				"Decision"
		};
		final String keys[] = {
				"ActionId",
				"ResourceId",
				"ResourceType",
				"Decision"
		};

		return makeTable(os_rules, titles, keys);

	}


	public FlexTable getResourceManagerTable() {

		final String titles[] = {
				"Action",
				"Partition",
				"Decision"
		};

		final String keys[] = {
				"ActionId",
				"ResourceTypePartition",
				"Decision"
		};

		return makeTable(rm_rules, titles, keys);

	}

	
	public FlexTable getRunControlTable() {

		final String titles[] = {
				"Action",
				"Command",
				"Partition",
				"Decision"
		};

		final String keys[] = {
				"ActionId",
				"ResourceTypeCommand",
				"ResourceTypePartition",
				"Decision"
		};

		return makeTable(rc_rules, titles, keys);

	}

	
	public FlexTable getTriggerCommanderTable() {

		final String titles[] = {
				"Action",
				"Command",
				"Partition",
				"Decision"
		};

		final String keys[] = {
				"ActionId",
				"ResourceTypeCommand",
				"ResourceTypePartition",
				"Decision"
		};

		return makeTable(tc_rules, titles, keys);

	}

	
	public FlexTable getIGUITable() {

		final String titles[] = {
				"Action",
				"Resource",
				"Type",
				"Decision"
		};

		final String keys[] = {
				"ActionId",
				"ResourceId",
				"ResourceType",
				"Decision"
		};

		return makeTable(igui_rules, titles, keys);

	}

	
	public FlexTable getBCMPanelTable() {

		final String titles[] = {
				"Action",
				"Resource",
				"Decision"
		};

		final String keys[] = {
				"ActionId",
				"ResourceId",
				"Decision"
		};

		return makeTable(bcm_panel_rules, titles, keys);

	}
}
