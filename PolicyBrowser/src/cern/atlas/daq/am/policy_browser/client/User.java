package cern.atlas.daq.am.policy_browser.client;

import java.util.ArrayList;

public class User implements Comparable<Role> {
	private String name;
	ArrayList<Role> assigned_roles;
	ArrayList<Role> enabled_roles;

	public User(String n) {
		name = n;
		assigned_roles = new ArrayList<Role>();
		enabled_roles = new ArrayList<Role>();
	}
	
	public String getName() {
		return this.name;
	}

	public ArrayList<Role> getAssignedRoles() {
		return assigned_roles;
	}

	public ArrayList<Role> getEnabledRoles() {
		return enabled_roles;
	}

	public int compareTo(Role o) {
		return this.getName().compareTo(o.getName());
	}
}
