package cern.atlas.daq.am.policy_browser.client;

import java.lang.Comparable;
import java.util.ArrayList;

public class Role implements Comparable<Role> {

	private String name;
	private String uc_name;        // upper case name
	private int dcs_category;          // 1 = is-DCS-role; 2 = is-not-DCS-role
	private int abstract_category;     // 1 = is-assignable-role; 2 = is-non-assignable-role
	private Boolean assignable;
	Role[] base_roles;
	ArrayList<Role> derived_roles = null;
	private Sudo[] direct_sudos;
	private Sudo[] derived_sudos;
	private String[] login_hosts;
	private User[] assigned_users;
	private User[] enabled_users;
	private Rule[] direct_rules;
	private Rule[] all_rules;
	Node node;

	public Role() {
		
	}

	public void init(String n, Boolean a, Role[] r, Sudo[] ds, Sudo[] vs, String[] h, User[] au, User[] eu, Rule[] dr, Rule[] ir) {
		name = n;
		uc_name = n.toUpperCase();
		dcs_category = (name.startsWith("DCS") ? 1 : 2);
		assignable = a;
		abstract_category = (assignable ? 1 : 2);
		base_roles = r;
		direct_sudos = ds;
		derived_sudos = vs;
		login_hosts = h;
		assigned_users = au;
		enabled_users = eu;
		direct_rules = dr;
		all_rules = ir;
		node = null;
	}

	public String getName() {
		return this.name;
	}

	public String getUpperCaseName() {
		return this.uc_name;
	}

	public int getDCS_Category() {
		return dcs_category;
	}

	public int getAbstractCategory() {
		return abstract_category;
	}

	public Boolean getIsAssignable() {
		return assignable;
	}

	public Role[] getBaseRoles() {
		return base_roles;
	}

	public ArrayList<Role> getDerivedRoles() {
		return derived_roles;
	}

	public Sudo[] getDirectSudos() {
		return direct_sudos;
	}

	public Sudo[] getDerivedSudos() {
		return derived_sudos;
	}

	public String[] getLoginHosts() {
		return login_hosts;
	}

	public User[] getAssignedUsers() {
		return assigned_users;
	}
	
	public User[] getEnabledUsers() {
		return enabled_users;
	}

	public Rule[] getDirectRules() {
		return direct_rules;
	}

	public Rule[] getAllRules() {
		return all_rules;
	}
	
/*	public void fillBaseRoles(ArrayList<NodesPair> inheritanceHierarchy) {
		if(base_roles != null) {
			for(int i = 0; i < base_roles.length; ++i) {
				inheritanceHierarchy.add(new RolesPair(base_roles[i], this));
				base_roles[i].fillBaseRoles(inheritanceHierarchy);
			}
		}
	}*/
	
	public void makeBaseInheritanceGraph(Role parent) {
		if(node == null) {
			node = new Node(this);
		}

		if(parent != null) {
			parent.node.addChild(node);
			node.addParent(parent.node);
		}

		if(base_roles != null) {
			for(int i = 0; i < base_roles.length; ++i) {
				base_roles[i].makeBaseInheritanceGraph(this);
			}
		}
	}
	
	public void makeDerivedInheritanceGraph(Role parent) {
		if(node == null) {
			node = new Node(this);
		}

		if(parent != null) {
			parent.node.addChild(node);
			node.addParent(parent.node);
		}

		if(derived_roles != null) {
			for(int i = 0; i < derived_roles.size(); ++i) {
				derived_roles.get(i).makeDerivedInheritanceGraph(this);
			}
		}
	}

	public void addDerivedRole(Role r) {
		if(derived_roles == null) {
			derived_roles = new ArrayList<Role>();
		}
/*		else {
			for(int i = 0; i < derived_roles.size(); ++i) {
				if(derived_roles.get(i) == r) {
					System.out.println("skip add derived role " + r + " of " + this);
					return;
				}
			}
		}*/

		derived_roles.add(r);
	}

	public int compareTo(Role o) {
		return this.getName().compareTo(o.getName());
	}

}
