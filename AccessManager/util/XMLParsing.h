#ifndef XMLPARSING_H_
#define XMLPARSING_H_

#include <AccessManager/util/ErsIssues.h>

#include <xercesc/dom/DOMAttr.hpp>
#include <xercesc/dom/DOMNode.hpp>
#include <xercesc/dom/DOMElement.hpp>
#include <xercesc/dom/DOMNodeList.hpp>
#include <xercesc/util/XMLString.hpp>

#include <sstream>

//defines and initialize an DOMElement pointer with a DOMNode pointer if the type of DOMNode is ELEMENT_NODE
#define CREATE_DOM_ELEMENT(pElementNode, pNode) \
	DOMElement* pElementNode = NULL; \
	INIT_DOM_ELEMENT(pElementNode, (pNode)); 

//initialize an DOMElement pointer with a DOMNode pointer if the type of DOMNode is ELEMENT_NODE
#define INIT_DOM_ELEMENT(pElementNode, pNode) \
    if ((pNode)->getNodeType() == DOMNode::ELEMENT_NODE)\
		(pElementNode) = (DOMElement*)(pNode); \
	else {\
		XACMLParsingException issue(ERS_HERE, "The node type is not an ' element node'!"); \
		throw issue;\
	}


// set the max occurs unbounded to a value big enough
#define MAX_OCC_UNBOUNDED	((unsigned long)0xFFFF)

//declares and initialize a DOMNodeList of children nodes with a given tag name.
//it also checks the minimum and maximum occurs.
//MAX_OCC_UNBOUNDED macro can be used to specify a no upper limit
//throws a parsing exception if the limits check is not fulfilled 
#define GET_CHILDREN_WITH_TAG_AND_LIMITS(pChildrenElementsList, pRootElement, childTagName, minOccurs, maxOccurs) \
	DOMNodeList* pChildrenElementsList = NULL; \
	{ \
		XMLCh *_childTagName = XMLString::transcode((childTagName)); \
		pChildrenElementsList = pRootElement->getElementsByTagName(_childTagName); \
		ERS_DEBUG(2, childTagName << " tags found:" << pChildrenElementsList->getLength()); \
		if ( !( ( (pChildrenElementsList->getLength() + 1) > (minOccurs) ) && \
			    ( (maxOccurs) == (MAX_OCC_UNBOUNDED) || pChildrenElementsList->getLength() <= (maxOccurs) ) \
			   ) \
		   ){ \
			std::ostringstream errorMessage; \
			errorMessage << "Number of tags <" << (childTagName) << "> out of bound! "; \
			errorMessage << "The limits are [" << (minOccurs) << ","; \
			if ((maxOccurs)==(MAX_OCC_UNBOUNDED)) errorMessage << "unbounded"; else errorMessage << (maxOccurs); \
			errorMessage << "]"; \
			ERS_DEBUG(1, errorMessage.str()); \
			XACMLParsingException issue(ERS_HERE, errorMessage.str()); \
			throw issue; \
		} \
		XMLString::release(&_childTagName); \
	}

//overcomes the problem of getting string values out from an XMLCh structure
//declares and initializes a new std::string variable
#define XMLCH_TO_STDSTRING(pXMLCh, newSTDSTRINGvariable) \
    char* newSTDSTRINGvariable##tempChar = XMLString::transcode(pXMLCh);\
    std::string newSTDSTRINGvariable(newSTDSTRINGvariable##tempChar);\
    XMLString::release(&(newSTDSTRINGvariable##tempChar));	


//read the text content of a node and put it in a variable 
#define READ_TEXT_VALUE(pNode, textValue) \
	std::string textValue = ""; \
	for(DOMNode* child = (pNode)->getFirstChild(); child != 0; child = child->getNextSibling()){ \
	    if (child->getNodeType() == DOMNode::TEXT_NODE){ \
	    	char *_nodeValue = XMLString::transcode(child->getNodeValue() ); \
			textValue += _nodeValue; \
			XMLString::release(&_nodeValue); \
	    } \
	}

namespace daq {

  namespace am {

    std::string make_exception_text(const char * prefix, const XMLCh * what);

  }

}

#endif /*XMLPARSING_H_*/
