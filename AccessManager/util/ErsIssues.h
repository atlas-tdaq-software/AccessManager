#ifndef ERSISSUES_H_
#define ERSISSUES_H_

#include <ers/ers.h>

/** \file ErsIssues.h Defines the ers issues to be used by the AM client C++ classes  
  * \brief AM issues
  * \author Marius Leahu
  * \version 1.0 
  */
namespace daq{
	
//////////////////////// GENERIC ISSUES ///////////////////////////////////////

/** \def am::Exception This issue is the base of all AM exceptions 
 */
ERS_DECLARE_ISSUE(	am,	Exception, , )

//////////////////////// COMMUNICATION ISSUES ///////////////////////////////////////

/** \def am::CommunicationIssue This issue is reported when the communication classes
 *  encounter problems. 
 */
ERS_DECLARE_ISSUE_BASE(	am, 			// namespace
						CommunicationIssue, 	// issue name
						am::Exception,	// base issue name
						, 				// no message
						,				// no base class attributes
										// no class attribute
                 	)

/** \def am::CommunicationInit This issue is reported when the communication initialization
 *  encounters problems. 
 */
ERS_DECLARE_ISSUE_BASE(	am, 					// namespace
						CommunicationInit,		// issue name
						am::CommunicationIssue,	// base issue name
						"Communication initialization: " << message, // message
						, // no base class attributes
						((std::string)message) // attribute
                 	)

/** \def am::NetworkError This issue is reported when the network communication classes
 *  encounter problems. 
 */
ERS_DECLARE_ISSUE_BASE(	am, 					// namespace
						NetworkError,			// issue name
						am::CommunicationIssue,	// base issue name
						"Network connection to [" << server_name << ":" << server_port << "]:" << message, // message
						, // no base class attributes
						((std::string)server_name)	((unsigned short int)server_port) ((std::string)message)// attribute: AM server name and port number
                 	)

/** \def am::NetworkTimeout This issue is reported when a network connection has timeout. 
 */
ERS_DECLARE_ISSUE_BASE(	am, 					// namespace
						NetworkConnectTimeout,	// issue name
						am::NetworkError,	// base issue name
						"Timeout after " << timeout_s << "s" << timeout_ms << "ms", // message
						((std::string)server_name)	((unsigned short int)server_port) ((std::string)message),// attribute: AM server name and port number
						((unsigned int)timeout_s) ((unsigned int)timeout_ms)
                 	)

/** \def am::CommunicationStreamsError This issue is reported when the communication streams classes
 *  encounter problems. 
 */
ERS_DECLARE_ISSUE_BASE(	am, 						// namespace
						CommunicationStreamsError,	// issue name
						am::CommunicationIssue,		// base issue name
						"Communication stream error:" << message, // message
						, // no base class attributes
						((std::string)message)	// message attribute
                 	)

/** \def am::CommunicationStreamsTimeout This issue is reported when the communication streams methods
 *  are exiting with timeout. 
 */
ERS_DECLARE_ISSUE_BASE(	am, 						// namespace
						CommunicationStreamsTimeout,// issue name
						am::CommunicationStreamsError,// base issue name
						"Operation timed out after [" << seconds << "s" << miliseconds << "ms]", // message
						((std::string)operationName), // base class attributes; it will be displayed as message
						((unsigned int)seconds) ((unsigned int)miliseconds)// message attribute
                 	)


//////////////////////// XACML ISSUES ///////////////////////////////////////

/** \def am::XACMLIssue This issue is reported when the XACML classes
 *  encounter problems. 
 */
ERS_DECLARE_ISSUE_BASE(	am, 			// namespace
						XACMLIssue, 	// issue name
						am::Exception,	// base issue name
						, 				// no message
						,				// no base class attributes
										// no class attribute
                 	)

/** \def am::XACMLIllegalArgumentException This issue is reported when there is 
 *  an illegal parameter passed to a method. 
 */
ERS_DECLARE_ISSUE_BASE(	am, 						// namespace
						XACMLIllegalArgumentException,	// issue name
						am::XACMLIssue,				// base issue name
						"Illegal argument! " << message, // message
						, // no base class attributes
						((std::string)message)	// message attribute
                 	)

/** \def am::XACMLParsingException This issue is reported when there is 
 *  a problem during XML parsing. 
 */
ERS_DECLARE_ISSUE_BASE(	am, 						// namespace
						XACMLParsingException,		// issue name
						am::XACMLIssue,				// base issue name
						"XACML parsing problem: " << message, // message
						, // no base class attributes
						((std::string)message)	// message attribute
                 	)
/** \def am::UndefinedAttributeValue This issue is reported when there is no AttributeValue definition
 *  for a given data type URI. 
 */
ERS_DECLARE_ISSUE_BASE(	am, 						// namespace
						XACMLUndefinedAttributeValue,	// issue name
						am::XACMLIssue,				// base issue name
						"No AttributeValue implementation for data type [" << dataType << "]", // message
						, // no base class attributes
						((std::string)dataType)	// message attribute
                 	)
/** \def am::MissingAttributeValue This issue is reported when there is no AttributeValue assigned
 *  to a given data type URI. 
 */
ERS_DECLARE_ISSUE_BASE(	am, 						// namespace
						XACMLMissingAttributeValue,	// issue name
						am::XACMLIssue,				// base issue name
						"No value found for the identifier [" << identifier << "]", // message
						, // no base class attributes
						((std::string)identifier)	// message attribute
                 	)
/** \def am::MissingAction This issue is reported when there is no Action assigned
 *  to a given resource URI identifier. 
 */
ERS_DECLARE_ISSUE_BASE(	am, 						// namespace
						XACMLMissingAction,			// issue name
						am::XACMLIssue,				// base issue name
						"No action found for the resource identifier [" << identifier << "]", // message
						, // no base class attributes
						((std::string)identifier)	// message attribute
                 	)

//////////////////////// SERVER PROCESSING ISSUES ///////////////////////////////////////

/** \def am::ServerProcessingFailure This issue is reported when the server
 *  has failed to successfully process the authorization request. 
 */
ERS_DECLARE_ISSUE_BASE(	am, 						// namespace
						ServerProcessingIssue,	// issue name
						am::Exception,			// base issue name
						"AM server processing issue: " << message, // message
						, // no base class attributes
						((std::string)message) // attribute: the XACML status code and message
                 	)

}

#endif /*ERSISSUES_H_*/
