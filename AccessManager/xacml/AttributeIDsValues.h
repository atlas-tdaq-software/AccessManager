#ifndef ATTRIBUTEIDSVALUES_H_
#define ATTRIBUTEIDSVALUES_H_

#include <AccessManager/util/URI.h>

#include <string>
#include <map>
#include <vector>


namespace daq
{

namespace am
{

class AttributeIDsValues
{
public:
	virtual ~AttributeIDsValues();
	AttributeIDsValues();

	/**
	 * Get the value of an attribute based on the attribute URI identifier.
	 * If there are more values available, only the first one is returned.
	 * @param identifier attribute identifier
	 * @return the string value
	 * @trow XACMLMissingAttributeValue exception
	 */
	virtual const std::string& getValue(const std::string& identifier) const;

	/**
	 * Get the values of an attribute identifier
	 * @param identifier the identifier in URI format
	 * @return a vector of string values
	 * @trow XACMLMissingAttributeValue exception
	 */
	virtual const std::vector<std::string>& getValues(const std::string& identifier) const;

	/**
	 * Get the data type for the given URI identifier
	 * @param identifier URI identifier
	 * @return the data type in URI format
	 */
	virtual const std::string getDataType(const std::string& identifier) const = 0;

	/**
	 * Get the match function for the given URI identifier
	 * @param identifier URI identifier
	 * @return the match function in URI format
	 */
	virtual const std::string getMatchFunction(const std::string& identifier) const = 0;

	// sends the uri ids and their values to ostream
	friend std::ostream& operator<<(std::ostream& os, const AttributeIDsValues& attrIV);

protected:
	/**
	 * The values of each URI identifier
	 */
	std::map<std::string, std::vector<std::string> > values;

	/*
	 * Each children class should implement this method to initialize the values map.
	 */
	virtual void initValues() = 0;

	/*
	 * Method to associate one more value to the URI identifiers
	 */
	virtual void addValue(const std::string& identifier, const std::string& value);

};

}

}

#endif /*ATTRIBUTEIDSVALUES_H_*/
