#ifndef SUBJECTS_H_
#define SUBJECTS_H_

#include <AccessManager/xacml/AttributeIDsValues.h>

namespace daq
{

namespace am
{

/**
 * Abstract class to define the XACML identifiers for the subject and the
 * methods to get information about the subject.
 * @author mleahu
 *
 */
class Subjects: public AttributeIDsValues

{
public:
	Subjects();
	virtual ~Subjects();

	/**
	 * Get the URI indentifier for the Subject Id attribute
	 * @return URI identifier
	 */
	const std::string getSubjectId() const;

	/**
	 * Get the URI indentifier for the DNS name attribute
	 * @return URI identifier
	 */
	const std::string getDNSName() const;

	/**
	 * Get the URI indentifier for the IP address attribute
	 * @return URI identifier
	 */
	const std::string getIpAddress() const;

	/**
	 * Get the data type for the given URI identifier
	 * @param identifier URI identifier
	 * @return the data type in URI format
	 */
	virtual const std::string getDataType(const std::string& identifier) const;

	/**
	 * Get the match function for the given URI identifier
	 * @param identifier URI identifier
	 * @return the match function in URI format
	 */
	virtual const std::string getMatchFunction(const std::string& identifier) const;

};

}

}

#endif /*SUBJECTS_H_*/
