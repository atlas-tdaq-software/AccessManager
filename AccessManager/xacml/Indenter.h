#ifndef INDENTER_H_
#define INDENTER_H_

#include <string>
#include <ostream>

namespace daq
{

namespace am
{

/**
 * Provides flexible indenting for XML encoding.  This class generates
 * strings of spaces to be prepended to lines of XML.  The strings are
 * formed according to a specified indent width and the given depth.
 * 
 */
class Indenter
{
public:
	
	Indenter(int width = 2);
	
	// prefix increment operator
	friend const Indenter& operator++(Indenter& indt);
	
	//prefix decrement operator
	friend const Indenter& operator--(Indenter& indt);
	
	std::string makeString();
	
	friend std::ostream& operator<<(std::ostream& os, const Indenter& indt);
	  
protected:
	// the width of one indentation level
	int width;
	// the depth level
	int depth;
	// the string full of spaces used for indentation
	std::string spaces;
};

}

}

#endif /*INDENTER_H_*/
