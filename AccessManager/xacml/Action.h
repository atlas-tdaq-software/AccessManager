#ifndef ACTION_H_
#define ACTION_H_

#include <AccessManager/xacml/AttributeIDsValues.h>

namespace daq
{

namespace am
{

/**
  * Abstract class to define an action with all the necessary attributes required by XACML standard.
  * The implementations have to define the initValues private method.
  * @author mleahu
  * @version 1.0
 */
class Action: public AttributeIDsValues
{

public:
	Action();
	virtual ~Action();

	/**
	 * Get the action URI identifier
	 * @return the URI identifier
	 */
	virtual const std::string getActionId() const;

	/**
	 * Get the data type for the given URI identifier
	 * @param identifier URI identifier
	 * @return the data type in URI format
	 */
	virtual const std::string getDataType(const std::string& identifier) const;

	/**
	 * Get the match function for the given URI identifier
	 * @param identifier URI identifier
	 * @return the match function in URI format
	 */
	virtual const std::string getMatchFunction(const std::string& identifier) const;
};

}

}

#endif /*ABSTRACTACTION_H_*/
