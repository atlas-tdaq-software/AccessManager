#ifndef PMGACTION_H_
#define PMGACTION_H_

#include <AccessManager/xacml/Action.h>

namespace daq
{
  namespace am
  {

    /**
     * The Action implementation for PMG resource type.
     * The possible actions are "start" and "terminate".
     */
    class PMGAction : public daq::am::Action
    {

    private:

      void
      initValues();

    public:

      /*
       * Constructor that takes as argument the value of the PMG action.
       * The action value string has to be retrieved with one of the getValueForAction____ methods.
       */
      PMGAction(const std::string &actionValue);

      virtual
      ~PMGAction();

      /// return the value for action id in case the action is "start"
      static std::string
      getValueForActionSTART();

      /// return the value for action id in case the action is "terminate"
      static std::string
      getValueForActionTERMINATE();
    };

  }
}

#endif /*PMGACTION_H_*/
