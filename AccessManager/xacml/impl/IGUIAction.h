#ifndef IGUIACTION_H_
#define IGUIACTION_H_

#include <AccessManager/xacml/Action.h>

namespace daq
{

namespace am
{

/**
 * The Action implementation for the IGUI resource type.
 * It has by default only one action id which is "view".
 */
class IGUIAction : public Action
{
protected:
	
	/**
	 * Initialize the action id values
	 */
	void initValues();

public:
	IGUIAction();
	virtual ~IGUIAction();

	// return the value for action id in case the action is "view" 
	static std::string getValueForActionVIEW();
};

}

}

#endif /*IGUIACTION_H_*/
