#ifndef OSRESOURCE_H_
#define OSRESOURCE_H_

#include <AccessManager/xacml/impl/OSAction.h>
#include <AccessManager/xacml/Resource.h>
#include <memory>

namespace daq
{

namespace am
{

/**
 * The Resource implementation for OS resource type.
 */
class OSResource : public daq::am::Resource
{

private:

	// the current action
	std::unique_ptr<OSAction>	currentOSAction;

	/*
	 * Implementation of initValues from ABC
	 */
	void initValues();

public:

	// constructor that takes as parameter the resource location,
	// the application to be run,
	// and the application arguments
	OSResource(const std::string& resourceLocation,
				const std::string& application,
				const std::string& arguments 	= "");
	virtual ~OSResource();

	// set the action "start" for the current process
	void setAction(const std::string& action);
};

}

}

#endif /*OSRESOURCE_H_*/
