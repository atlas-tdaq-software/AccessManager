#ifndef AMRMACTION_H_
#define AMRMACTION_H_

#include <AccessManager/xacml/Action.h>

namespace daq
{

  namespace am
  {

    /**
     * The Action implementation for AMRM resource type.
     * The AMRMAction instantiations are accessible via the AMRMAction::ACTIONS array.
     */
    class AMRMAction : public daq::am::Action
    {

    private:

      /**
       * Initialize the action id values
       */
      void
      initValues();

    public:

      /*
       * Constructor that takes as argument the value of the DB action.
       */
      AMRMAction(const std::string& actionValue);

      virtual
      ~AMRMAction();

      /*
       * The index in ACTION_VALUES and ACTIONS arrays for all possible AMRM actions.
       */
      enum {
        ACTION_ID_ADMIN = 0,
        ACTION_ID_ASSIGN_ROLE,
        ACTION_ID_ENABLE_ROLE,
        ACTIONS_COUNT
      };

      /*
       * The string value of DB actions.
       */
      static const std::string ACTION_VALUES[ACTIONS_COUNT];

      /*
       * The AMRMAction instances for AMRM resource.
       * The ACTION_ID_ static members can be used to reference the AMRMAction objects in the ACTIONS array.
       */
      static const AMRMAction ACTIONS[ACTIONS_COUNT];

    };

  }

}

#endif
