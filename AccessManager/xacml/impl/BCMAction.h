#ifndef BCMACTION_H_
#define BCMACTION_H_

#include <AccessManager/xacml/Action.h>

namespace daq
{

namespace am
{

/**
 * The Action implementation for BMC Panel resource type.
 * The possible actions are "configure" and "update".
 */
class BCMAction : public daq::am::Action
{
	
private:
	
	/**
	 * Initialize the action id values
	 */
	void initValues();

public:

	/*
	 * Constructor that takes as argument the value of the BCM action.
	 * The action value string must to be retrieved with one of the getValueForAction____ methods. 
	 */
	BCMAction(const std::string& actionValue);
	virtual ~BCMAction();
	
	// return the value for action id in case the action is "configure" 
	static std::string getValueForActionCONFIGURE();
	
	// return the value for action id in case the action is "update" 
	static std::string getValueForActionUPDATE();
};

}

}

#endif /*BCMACTION_H_*/
