#ifndef AMRMRESOURCE_H_
#define AMRMRESOURCE_H_

#include <AccessManager/xacml/impl/AMRMAction.h>
#include <AccessManager/xacml/Resource.h>
#include <memory>

namespace daq
{

  namespace am
  {

    /**
     * The Resource class for RolesAdmin (Access Manager Roles Manager) resource type.
     * The AMRMResource objects can be obtained via the getInstance* methods.
     */
    class AMRMResource : public daq::am::Resource
    {

    private:

      /*
       * Private constructor.
       *  \param resourceId  argument specifies the type of DB operation (admin or role)
       *  \param name        the role name
       *  \param action      one of the following static action objects defined in this class
       */
      AMRMResource(const std::string& resourceId, const std::string& name, const AMRMAction& action);

      /*
       * Implementation of initValues from ABC
       */
      void
      initValues();


    public:

      virtual
      ~AMRMResource();

      /*
       * The index in OPERATION_TYPES array for all operation types on AMRM resource.
       */
      enum {
        OPERATION_TYPE_ID_ADMIN = 0,
        OPERATION_TYPE_ID_ROLE,
        OPERATION_TYPES_COUNT
      };

      /*
       * The string values of operation types.
       */
      static const std::string OPERATION_TYPES[OPERATION_TYPES_COUNT];

      /*
       * Get the AMRMResource object to send an authorization request for RolesAdmin admin operations.
       * The user MUST delete the pointer returned by this method after its usage.
       */
      static const AMRMResource *
      getInstanceForAdminOperations();

      /*
       * Get the AMRMResource object to send an authorization request for RolesAdmin admin operations.
       */
      static std::unique_ptr<const AMRMResource>
      getAutoptrInstanceForAdminOperations();

      /*
       * Get the AMRMResource object to send an authorization request for operations on a role.
       * The 'name' argument is the role name.
       * The 'ACTION_ID' argument must be one of the following ACTIOn_ID static members of this class:
       *  ACTION_ID_ASSIGN_ROLE
       *  ACTION_ID_ENABLE_ROLE
       * The user MUST delete the pointer returned by this method after its usage.
       */
      static const AMRMResource *
      getInstanceForRoleOperations(const std::string& name, const int &ACTION_ID);

      /*
       * Get the AMRMResource object to send an authorization request for operations in a directory.
       * The 'path' argument is the directory to be accessed.
       * The 'ACTION_ID' argument must be one of the following ACTIOn_ID static members of this class:
       *  ACTION_ID_ASSIGN_ROLE
       *  ACTION_ID_ENABLE_ROLE
       */
      static std::unique_ptr<const AMRMResource>
      getAutoptrInstanceForRoleOperations(const std::string& name, const int &ACTION_ID);

    };

  }

}

#endif
