#ifndef DBACTION_H_
#define DBACTION_H_

#include <AccessManager/xacml/Action.h>

namespace daq
{

namespace am
{

/**
 * The Action implementation for DB resource type.
 * There is nothing special with this class, it just extends the Action class. 
 * The DBAction instantiations are accessible via the DBResource::ACTIONS array. 
 */
class DBAction : public daq::am::Action
{
	
private:
		
	/**
	 * Initialize the action id values
	 */
	void initValues();

public:	
	
	/*
	 * Constructor that takes as argument the value of the DB action.
	 */
	DBAction(const std::string& actionValue);

	virtual ~DBAction();
	
};

}

}

#endif /*DBACTION_H_*/
