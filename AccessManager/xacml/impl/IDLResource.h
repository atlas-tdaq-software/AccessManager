#ifndef IDLRESOURCE_H_
#define IDLRESOURCE_H_

#include <AccessManager/xacml/impl/IDLAction.h>
#include <AccessManager/xacml/Resource.h>

namespace daq
{

namespace am
{
/**
 * The Resource implementation for IDL resource type.
 * The constructor takes as arguments the IDL interface name and the 
 * IDL method name for that interface.
 * The action attribute is assigned internally as an IDLAction object.
 */
class IDLResource : public Resource
{

protected:

	// the IDL specific action for all methods and interfaces
	static const IDLAction resourceTypeIDLAction;
	
	/*
	 * Implementation of initValues from ABC
	 */
	void initValues();

public:
	
	/**
	 * Constructor that takes as arguments the IDL interface name and the method of this interface
 	 * The action attribute is assigned internally as an IDLAction object.
	 */
	IDLResource(std::string IDLInterface, std::string IDLMethod);
	
	virtual ~IDLResource();

};

}

}

#endif /*IDLRESOURCE_H_*/
