#ifndef RUNCONTROLACTION_H_
#define RUNCONTROLACTION_H_

#include <AccessManager/xacml/Action.h>

namespace daq
{

namespace am
{

/**
 * The Action implementation for the Run Control resource type.
 * It has by default only one action id which is "exec_cmd".
 */
class RunControlAction : public Action
{
protected:
	
	/**
	 * Initialize the action id values
	 */
	void initValues();

public:
	RunControlAction();
	virtual ~RunControlAction();

	// return the value for action id in case the action is "exec_cmd" 
	static std::string getValueForActionEXEC_CMD();
};


}

}

#endif /*RUNCONTROLACTION_H_*/
