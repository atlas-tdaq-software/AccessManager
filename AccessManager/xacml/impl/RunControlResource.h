#ifndef RUNCONTROLRESOURCE_H_
#define RUNCONTROLRESOURCE_H_

#include <AccessManager/xacml/impl/RunControlAction.h>
#include <AccessManager/xacml/Resource.h>

namespace daq
{

namespace am
{

/**
 * The Resource implementation for the Run Control resource type.
 * The constructor takes as argument the command name attribute as returned by one of the getValueForCommandName__ methods
 * and optionally the partition name attribute 
 * The action attribute is assigned internally as an RunControlAction object.
 */
class RunControlResource : public daq::am::Resource
{

private:

	// the Run Control specific action for all the commands
	static const RunControlAction rcAction;
	/*
	 * Implementation of initValues from ABC
	 */
	void initValues();

public:

	// constructor that takes as parameter the command name attribute and optionally the partition name attribute
	RunControlResource(const std::string& commandName, const std::string& partitionName = "");
	virtual ~RunControlResource();

        /*
        * The index in ACTION_VALUES array for all possible Run Control actions.
        */
        enum {
          ACTION_ID_PUBLISH = 0,
          ACTION_ID_PUBLISHSTATS,
          ACTION_ID_PUBLISHSTATISTICS,
          ACTION_ID_USER,
          ACTION_ID_USERBROADCAST,
          ACTIONS_COUNT
        };

        /*
         * The string value of Run Control actions.
         */
        static const std::string ACTION_VALUES[ACTIONS_COUNT];
};

}

}

#endif /*RUNCONTROLRESOURCE_H_*/
