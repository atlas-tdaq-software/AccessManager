#ifndef TRIGGERCOMMANDERACTION_H_
#define TRIGGERCOMMANDERACTION_H_

#include <AccessManager/xacml/Action.h>

namespace daq
{

  namespace am
  {

    /**
     * The Action implementation for the Trigger Commander resource type.
     * It has by default only one action id which is "exec_cmd".
     */
    class TriggerCommanderAction : public Action
    {
    protected:

      /**
       * Initialize the action id values
       */
      void
      initValues();

    public:
      TriggerCommanderAction();

      virtual
      ~TriggerCommanderAction();

      // return the value for action id in case the action is "exec_cmd"
      static std::string
      getValueForActionEXEC_CMD();
    };

  }

}

#endif /*TRIGGERCOMMANDERACTION_H_*/
