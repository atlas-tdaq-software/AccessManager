#ifndef DBRESOURCE_H_
#define DBRESOURCE_H_

#include <AccessManager/xacml/impl/DBAction.h>
#include <AccessManager/xacml/Resource.h>
#include <memory>

namespace daq
{

  namespace am
  {

    /**
     * The Resource class for DataBase (DB) resource type.
     * The DBResource objects can be obtained via the getInstance* methods.
     */
    class DBResource : public daq::am::Resource
    {

    private:

      /*
       * Private constructor.
       * The 'resourceId' argument specifies the type of DB operation (admin, on file or directory)
       * The 'path' argument is the directory to be accessed.
       * The 'action' argument must be one of the following static action objects defined in this class:
       */
      DBResource(const std::string& resourceId, const std::string& path, const DBAction& action);

      /*
       * Implementation of initValues from ABC
       */
      void
      initValues();

    public:

      virtual
      ~DBResource();

      enum
      {
        OPERATION_TYPE_ID_ADMIN = 0,
        OPERATION_TYPE_ID_DIRECTORY,
        OPERATION_TYPE_ID_FILE,
        OPERATION_TYPES_COUNT
      };

      /*
       * The string values of operation types.
       */
      static const std::string OPERATION_TYPES[OPERATION_TYPES_COUNT];

      /*
       * The index in ACTION_VALUES and ACTIONS arrays for all possible DB actions.
       */
      enum
      {
        ACTION_ID_ADMIN = 0,
        ACTION_ID_CREATE_FILE,
        ACTION_ID_UPDATE_FILE,
        ACTION_ID_DELETE_FILE,
        ACTION_ID_CREATE_SUBDIR,
        ACTION_ID_DELETE_SUBDIR,
        ACTIONS_COUNT
      };

      /*
       * The string value of DB actions.
       */
      static const std::string ACTION_VALUES[ACTIONS_COUNT];

      /*
       * The DBAction instances for DB resource.
       * The ACTION_ID_ static members can be used to reference the DBAction objects in the ACTIONS array.
       */
      static const DBAction ACTIONS[ACTIONS_COUNT];

      /*
       * Get the DBResource object to send an authorization request for DB admin operations.
       * The user MUST delete the pointer returned by this method after its usage.
       */

      static const DBResource *
      getInstanceForAdminOperations();
      /*
       * Get the DBResource object to send an authorization request for DB admin operations.
       */

      static std::unique_ptr<const DBResource>
      getAutoptrInstanceForAdminOperations();

      /*
       * Get the DBResource object to send an authorization request for operations in a directory.
       * The 'path' argument is the directory to be accessed.
       * The 'ACTION_ID' argument must be one of the following ACTIOn_ID static members of this class:
       *  ACTION_ID_CREATE_FILE
       *  ACTION_ID_DELETE_FILE
       *  ACTION_ID_CREATE_SUBDIR
       *  ACTION_ID_DELETE_SUBDIR
       * The user MUST delete the pointer returned by this method after its usage.
       */

      static const DBResource *
      getInstanceForDirectoryOperations(const std::string& path, const int &ACTION_ID);

      /*
       * Get the DBResource object to send an authorization request for operations in a directory.
       * The 'path' argument is the directory to be accessed.
       * The 'ACTION_ID' argument must be one of the following ACTIOn_ID static members of this class:
       *  ACTION_ID_CREATE_FILE
       *  ACTION_ID_DELETE_FILE
       *  ACTION_ID_CREATE_SUBDIR
       *  ACTION_ID_DELETE_SUBDIR
       */
      static std::unique_ptr<const DBResource>
      getAutoptrInstanceForDirectoryOperations(const std::string& path, const int &ACTION_ID);

      /*
       * Get the DBResource object to send an authorization request for operations on a file.
       * The 'path' argument is the file to be accessed.
       * The 'ACTION_ID' argument must be one of the following ACTIOn_ID static members of this class:
       *  ACTION_ID_UPDATE_FILE
       * The user MUST delete the pointer returned by this method after its usage.
       */

      static const DBResource *
      getInstanceForFileOperations(const std::string& path, const int &ACTION_ID);
      /*
       * Get the DBResource object to send an authorization request for operations on a file.
       * The 'path' argument is the file to be accessed.
       * The 'ACTION_ID' argument must be one of the following ACTIOn_ID static members of this class:
       *  ACTION_ID_UPDATE_FILE
       */
      static std::unique_ptr<const DBResource>
      getAutoptrInstanceForFileOperations(const std::string& path, const int &ACTION_ID);

    };

  }

}

#endif /*DBRESOURCE_H_*/
