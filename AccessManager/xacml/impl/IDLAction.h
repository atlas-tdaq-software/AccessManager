#ifndef IDLACTION_H_
#define IDLACTION_H_

#include <AccessManager/xacml/Action.h>

namespace daq
{

namespace am
{

/**
 * The Action implementation for IDL resource type.
 * It has by default only one action id which is "call".
 */
class IDLAction : public Action
{
protected:
	
	/**
	 * Initialize the action id values
	 */
	void initValues();

public:
	IDLAction();
	virtual ~IDLAction();

	// return the value for action id in case the action is "call" 
	static std::string getValueForActionCALL();
};


}

}

#endif /*IDLACTION_H_*/
