#ifndef RMACTION_H_
#define RMACTION_H_

#include <AccessManager/xacml/Action.h>

namespace daq
{

namespace am
{

/**
 * The Action implementation for Resource Manager (RM) resource type.
 * The possible actions are: "any"
 */
class RMAction : public daq::am::Action
{
	
private:
	
	/**
	 * Initialize the action id values
	 */
	void initValues();

public:

	/*
	 * Constructor that takes as argument the value of the RM action.
	 * The action value string must to be retrieved with one of the getValueForAction____ methods. 
	 */
	RMAction(const std::string& actionValue);
	virtual ~RMAction();
	
	// return the value for action id in case the action is "any"
	static std::string getValueForActionANY();
	
};

}

}

#endif /*RMACTION_H_*/
