#ifndef BCMRESOURCE_H_
#define BCMRESOURCE_H_

#include <AccessManager/xacml/impl/BCMAction.h>
#include <AccessManager/xacml/Resource.h>
#include <memory>

namespace daq
{

namespace am
{

/**
 * The Resource implementation for BCM Panel resource type.
 * The constructor takes as argument the partition name and the.
 * The action attribute is assigned internally as an BCMAction object.
 */
class BCMResource : public daq::am::Resource
{

private:

	// constructor that takes as parameter the partition name
	BCMResource(const BCMAction& action);

	/*
	 * Implementation of initValues from ABC
	 */
	void initValues();

public:

	// the BCM possible actions:configure and update
	static const BCMAction ACTION_CONFIGURE;
	static const BCMAction ACTION_UPDATE;

	virtual ~BCMResource();

	/*
	 * Get the BCMResource object to send an authorization request for action CONFIGURE.
	 */
	static std::unique_ptr<const BCMResource> getInstanceForActionCONFIGURE();

	/*
	 * Get the BCMResource object to send an authorization request for action UPDATE.
	 */
	static std::unique_ptr<const BCMResource> getInstanceForActionUPDATE();
};

}

}

#endif /*BCMRESOURCE_H_*/
