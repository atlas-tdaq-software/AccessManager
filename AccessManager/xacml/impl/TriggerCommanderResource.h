#ifndef TRIGGERCOMMANDERRESOURCE_H_
#define TRIGGERCOMMANDERRESOURCE_H_

#include <AccessManager/xacml/impl/TriggerCommanderAction.h>
#include <AccessManager/xacml/Resource.h>

namespace daq
{

  namespace am
  {

    /**
     * The Resource implementation for the Trigger Commander resource type.
     * The constructor takes as argument the command name attribute as returned by one of the getValueForCommandName__ methods and optionally the partition name attribute.
     * The action attribute is assigned internally as an TriggerCommanderAction object.
     */
    class TriggerCommanderResource : public daq::am::Resource
    {

    private:

      // the Trigger Commander specific action for all the commands
      static const TriggerCommanderAction tcAction;

      /*
       * Implementation of initValues from ABC
       */
      void
      initValues();


    public:

      // constructor that takes as parameter the command name attribute and optionally the partition name attribute
      TriggerCommanderResource(const std::string& commandName, const std::string& partitionName = "");

      virtual
      ~TriggerCommanderResource();

      /*
      * The index in ACTION_VALUES array for all possible Trigger Commander actions.
      */
      enum {
        ACTION_ID_HOLD = 0,
        ACTION_ID_RESUME,
        ACTION_ID_SETPRESCALES,
        ACTION_ID_SETL1PRESCALES,
        ACTION_ID_SETHLTPRESCALES,
        ACTION_ID_SETBUNCHGROUP,
        ACTION_ID_SETPRESCALESANDBUNCHGROUP,
        ACTION_ID_SETLUMIBLOCKLEN,
        ACTION_ID_SETMINLUMIBLOCKLEN,
        ACTION_ID_INCREASELUMIBLOCK,
        ACTION_ID_SETCONDITIONSUPDATE,
        ACTIONS_COUNT
      };

      /*
       * The string value of Trigger Commander actions.
       */
      static const std::string ACTION_VALUES[ACTIONS_COUNT];

    };

  }

}

#endif /*TRIGGERCOMMANDERRESOURCE_H_*/
