#ifndef PMGRESOURCE_H_
#define PMGRESOURCE_H_

#include <AccessManager/xacml/impl/PMGAction.h>
#include <AccessManager/xacml/Resource.h>

namespace daq
{

namespace am
{

/**
 * The Resource implementation for PMG resource type.
 * The constructor takes as argument the process binary path. If not important for the request,
 * none can be provided
 * The action attribute is assigned internally as an PMGAction object.
 */
class PMGResource : public daq::am::Resource
{

private:

	// the PMG possible actions: start and terminate
	static const PMGAction ACTION_START;
	static const PMGAction ACTION_TERMINATE;

	/*
	 * Implementation of initValues from ABC
	 */
	void initValues();

public:

	// constructor for a resource with unspecified process details, but action only.
	PMGResource();

	// constructor that takes as parameter the process binary path,
	// the hostname where the process is running,
	// and the process arguments; additionally, it should provide a true/false value to the processOwnedByRequester
	PMGResource(const std::string& processBinaryPath,
				const std::string& processHostName 	= "",
				const std::string& processArguments 	= "",
				const bool processOwnedByRequester	= false,
				const std::string& partitionName 	= "");
	virtual ~PMGResource();

	// get the value for an anonymous process
	std::string getAnonymousProcessId();

	// set the action "start" for the current process
	void setStartAction();

	// set the action "terminate" for the current process
	void setTerminateAction();
};

}

}

#endif /*PMGRESOURCE_H_*/
