#ifndef IGUIRESOURCE_H_
#define IGUIRESOURCE_H_

#include <AccessManager/xacml/impl/IGUIAction.h>
#include <AccessManager/xacml/Resource.h>
#include <memory>

namespace daq
{

namespace am
{

/**
 * The Resource implementation for the IGUI resource type.
 * The constructor takes as argument the view mode attribute as returned by one of the getValueForViewMode__ methods
 * The action attribute is assigned internally as an IGUIAction object.
 */
class IGUIResource : public daq::am::Resource
{

private:

	// the IGUI specific action for all the view modes
	static const IGUIAction iguiAction;

	// constructor that takes as parameter the view mode id
	IGUIResource(const int viewModeId);

	/*
	 * Implementation of initValues from ABC
	 */
	void initValues();

public:

	/*
	 * The index in VIEW_MODES array for all view modes of IGUI resource.
	 */
        enum {
          VIEW_MODE_ID_DISPLAY  = 0,
          VIEW_MODE_ID_CONTROL,
          VIEW_MODE_ID_EXPERT,
          VIEW_MODES_COUNT
        };

	/*
	 * The string values of view modes.
	 */
	static const std::string VIEW_MODES[VIEW_MODES_COUNT];


	virtual ~IGUIResource();

	// return the value for view mode attribute "display"
	static std::unique_ptr<const IGUIResource> getInstanceForViewMode(const int viewModeId);

};

}

}

#endif /*IGUIRESOURCE_H_*/
