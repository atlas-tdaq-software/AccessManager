#ifndef RMRESOURCE_H_
#define RMRESOURCE_H_

#include <AccessManager/xacml/impl/RMAction.h>
#include <AccessManager/xacml/Resource.h>
#include <memory>

namespace daq
{

namespace am
{

/**
 * The Resource implementation for Resource Manager (RM) resource type.
 * The constructor takes as argument the partition name and the.
 * The action attribute is assigned internally as an RMAction object.
 */
class RMResource : public daq::am::Resource
{

private:

	// constructor that takes as parameter the partition name
	RMResource(const std::string& partitionName, const RMAction& action);

	/*
	 * Implementation of initValues from ABC
	 */
	void initValues();

public:

	// the RM possible actions: any
	static const RMAction ACTION_ANY;

	virtual ~RMResource();

	/*
	 * Get the RMResource object to send an authorization request for action ANY.
	 * The user MUST delete the pointer returned by this method after its usage.
	 */
	static const RMResource *getInstanceForActionANY(const std::string& partitionName);
	/*
	 * Get the RMResource object to send an authorization request for action ANY.
	 */
	static std::unique_ptr<const RMResource> getAutoptrInstanceForActionANY(const std::string& partitionName);
};

}

}

#endif /*RMRESOURCE_H_*/
