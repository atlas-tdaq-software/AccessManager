#ifndef OSACTION_H_
#define OSACTION_H_

#include <AccessManager/xacml/Action.h>

namespace daq
{

namespace am
{

/**
 * The Action implementation for the Operating System resource type.
 */
class OSAction : public daq::am::Action
{
	
private:
	
	/**
	 * Initialize the action id values
	 */
	void initValues();

public:

	/*
	 * Constructor that takes as argument the value of the OS action.
	 */
	OSAction(const std::string& actionValue);
	virtual ~OSAction();
	
};

}

}

#endif /*PMGACTION_H_*/
