#ifndef RRDBRESOURCE_H_
#define RRDBRESOURCE_H_

#include <AccessManager/xacml/impl/RDBAction.h>
#include <AccessManager/xacml/Resource.h>

namespace daq
{

namespace am
{

/**
 * The Resource implementation for the RRemote Database (RDB) resource type.
 * The constructor takes as argument the command name attribute as returned by one of the getValueForCommandName__ methods
 * and optionally the partition name attribute 
 * The action attribute is assigned internally as an RDBAction object.
 */
class RDBResource : public daq::am::Resource
{

private:

	// the Run Control specific action for all the commands
	static const RDBAction rcAction;
	/*
	 * Implementation of initValues from ABC
	 */
	void initValues();

public:

	// constructor that takes as parameter the command name attribute and optionally the partition name attribute
	RDBResource(const std::string& commandName, const std::string& partitionName = "");
	virtual ~RDBResource();

        /*
        * The index in ACTION_VALUES array for all possible RDB actions.
        */
        enum ActionID {
          ACTION_ID_OPEN = 0,
          ACTION_ID_CLOSE,
          ACTION_ID_UPDATE,
          ACTION_ID_RELOAD,
          ACTION_ID_OPEN_SESSION,
          ACTION_ID_LIST_SESSIONS,
          ACTIONS_COUNT
        };

        /*
         * The string value of Run Control actions.
         */
        static const std::string ACTION_VALUES[ACTIONS_COUNT];
};

}

}

#endif /*RRDBRESOURCE_H_*/
