#ifndef RESOURCE_H_
#define RESOURCE_H_

#include <AccessManager/xacml/AttributeIDsValues.h>
#include <AccessManager/xacml/Action.h>


namespace daq
{

namespace am
{

/**
 * Abstract class for a the resource object used to define rules, policies and policy sets.
 * The interface gives access to resource attributes required by the XACML standard.
 * @author mleahu
 * @version 1.0
 *
 */
class Resource: public AttributeIDsValues
{

protected:

	/**
	 * The possible resource types
	 */
	std::vector<std::string> types;

	/**
	 * The action for each resource identifier.
	 */
	std::map<std::string, const Action*> actions;


	/**
	 * The resource description
	 */
	std::string description;

	/*
	 * Method to add a new type
	 */
	virtual void addTypes(const std::string& newType);

	/*
	 * Method to set an action for a given identifier.
	 */
	virtual void addAction(const std::string& identifier, const Action* action);

public:
	Resource(const std::string& description);
	virtual ~Resource();

	/**
	 *  The URI identifier of the resource category.
	 *  In the RBAC model, this will be the target of the Permission Policy Set
	 * @return the name of resource type, or null if not available
	 */
	virtual const std::string getResourceCategory() const;

	/**
	 * The URI identifier of the resource id used in the definition of policies.
	 * In the RBAC model, this will be the target of the Permission Policy included in a PPS.
	 * @return resource's id
	 */
	virtual const std::string getResourceId() const;

	/**
	 * The resource types URI identifiers used in the rule definition.
	 * In the RBAC model, this will be the target of the rule included in a PP.
	 * @return resource's id
	 */
	virtual const std::vector<std::string>& getResourceTypes() const;

	/**
	 * The description of the resource used in the definition of rules and policies.
	 * @return resource's description
	 */
	virtual const std::string& getDescription() const;

	/**
	 * Get the data type for the given resource identifier.
	 * @param resourceIdentifier
	 * @return the data type in URI format
	 */
	virtual const std::string getDataType(const std::string& identifier) const;

	/**
	 * Get the match function id for the given resource identifier.
	 * @param resourceIdentifier
	 * @return the match function id in URI format
	 */
	virtual const std::string getMatchFunction(const std::string& identifier) const;

	/**
	 * Get the action for a given resource identifier
	 * @param identifier the resource identifier
	 * @return the reference to action object or throw exception if none
	 */
	virtual const Action* getAction(const std::string& identifier) const;
};

}

}

#endif /*RESOURCE_H_*/
