#ifndef TIMEATTRIBUTEPROXY_H_
#define TIMEATTRIBUTEPROXY_H_

#include <AccessManager/xacml/attr/AttributeProxy.h>
#include <AccessManager/xacml/attr/TimeAttribute.h>

namespace daq
{

namespace am
{

class TimeAttributeProxy : public daq::am::AttributeProxy
{
	AttributeValue* getInstance(const std::string& value) {
		return new TimeAttribute(value);
	}

	AttributeValue* getInstance(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root) {
		return new TimeAttribute(root);
	}
};

}

}

#endif /*TIMEATTRIBUTEPROXY_H_*/
