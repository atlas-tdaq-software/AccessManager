#ifndef ANYURIATTRIBUTEPROXY_H_
#define ANYURIATTRIBUTEPROXY_H_

#include <AccessManager/xacml/attr/AttributeProxy.h>
#include <AccessManager/xacml/attr/AnyURIAttribute.h>

namespace daq
{

namespace am
{

class AnyURIAttributeProxy : public daq::am::AttributeProxy
{
	AttributeValue* getInstance(const std::string& value) {
		return new AnyURIAttribute(value);
	}
	
	AttributeValue* getInstance(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root) {
		return new AnyURIAttribute(root);
	}
};

}

}

#endif /*ANYURIATTRIBUTEPROXY_H_*/
