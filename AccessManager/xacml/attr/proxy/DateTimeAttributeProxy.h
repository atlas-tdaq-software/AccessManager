#ifndef DATETIMEATTRIBUTEPROXY_H_
#define DATETIMEATTRIBUTEPROXY_H_

#include <AccessManager/xacml/attr/AttributeProxy.h>
#include <AccessManager/xacml/attr/DateTimeAttribute.h>

namespace daq
{

namespace am
{

class DateTimeAttributeProxy : public daq::am::AttributeProxy
{
	AttributeValue* getInstance(const std::string& value) {
		return new DateTimeAttribute(value);
	}

	AttributeValue* getInstance(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root) {
		return new DateTimeAttribute(root);
	}
};

}

}

#endif /*DATETIMEATTRIBUTEPROXY_H_*/
