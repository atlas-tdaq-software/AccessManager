#ifndef DATEATTRIBUTEPROXY_H_
#define DATEATTRIBUTEPROXY_H_

#include <AccessManager/xacml/attr/AttributeProxy.h>
#include <AccessManager/xacml/attr/DateAttribute.h>

namespace daq
{

namespace am
{

class DateAttributeProxy : public daq::am::AttributeProxy
{
	AttributeValue* getInstance(const std::string& value) {
		return new DateAttribute(value);
	}

	AttributeValue* getInstance(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root) {
		return new DateAttribute(root);
	}
};

}

}

#endif /*DATEATTRIBUTEPROXY_H_*/
