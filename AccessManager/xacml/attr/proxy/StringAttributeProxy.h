#ifndef STRINGATTRIBUTEPROXY_H_
#define STRINGATTRIBUTEPROXY_H_

#include <AccessManager/xacml/attr/AttributeProxy.h>
#include <AccessManager/xacml/attr/StringAttribute.h>

namespace daq
{

namespace am
{

class StringAttributeProxy : public daq::am::AttributeProxy
{
	AttributeValue* getInstance(const std::string& value) {
		return new StringAttribute(value);
	}

	AttributeValue* getInstance(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root) {
		return new StringAttribute(root);
	}
};

}

}

#endif /*STRINGATTRIBUTEPROXY_H_*/
