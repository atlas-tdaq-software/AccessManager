#ifndef ANYURIATTRIBUTE_H_
#define ANYURIATTRIBUTE_H_

#include "AttributeValue.h"
#include <AccessManager/util/URI.h>

namespace daq
{

namespace am
{

class AnyURIAttribute : public daq::am::AttributeValue
{
private:
	// implement the pure virtual method
	void initialize();

public:

	// the URI identifier
	static const std::string identifier;

	AnyURIAttribute(const std::string& value);
	AnyURIAttribute(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root);
	virtual ~AnyURIAttribute();

};

}

}

#endif /*ANYURIATTRIBUTE_H_*/
