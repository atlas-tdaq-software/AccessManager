#ifndef STRINGATTRIBUTE_H_
#define STRINGATTRIBUTE_H_

#include "AttributeValue.h"
#include <string>

namespace daq
{

namespace am
{

class StringAttribute : public daq::am::AttributeValue
{
private:	
	// implement the pure virtual method
	void initialize();

public:

	// the URI identifier
	static const std::string identifier;

	StringAttribute(const std::string& value);
	StringAttribute(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root);
	virtual ~StringAttribute();

};

}

}

#endif /*STRINGATTRIBUTE_H_*/
