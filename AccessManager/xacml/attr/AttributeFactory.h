#ifndef ATTRIBUTEFACTORY_H_
#define ATTRIBUTEFACTORY_H_

#include <AccessManager/xacml/attr/AttributeValue.h>
#include <AccessManager/xacml/attr/AttributeProxy.h>

#include <map>

namespace daq
{

namespace am
{
/*
 * Helper class to create AttributeValue derived objects based on the data type URI.
 * This is a singleton and after the use it SHOULD BE DELETED. So each class that access
 * the singleton at least once, should delete the instance in its destructor!
 */

class AttributeFactory
{

private:

	static AttributeFactory* singletonInstance;

	// the map of data types and their proxy
	std::map<std::string, AttributeProxy*> supportedDataTypes;

	// initialize the data types map
	void initializeDataTypes();

protected:
	AttributeFactory();

public:
	// get the singleton instance of this class
	static AttributeFactory* Instance();

	~AttributeFactory();

	/*
	 * Creates an AttributeValue object by instantianting the appropriate implementation
	 * based on the dataType.
	 * It throws and ers exception UndefinedAttributeValue if there is no implementation
	 * for given data type URI.
	 */
	AttributeValue* createValue(const std::string& dataType, const std::string& value);

	/*
	 * Creates an AttributeValue object by instantianting the appropriate implementation
	 * based on the dataType. If dataType not provided, it's trying to read from the
	 * DataType attributeof root DOM node. The value is read from the DOM Node.
	 * It throws and ers exception UndefinedAttributeValue if there is no implementation
	 * for given data type URI.
	 */
	AttributeValue* createValue(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root, const std::string& dataType = "");

	/*
	 * Creates an AttributeValue object that is a clone of given attributeValue object.
	 */
	AttributeValue* cloneValue(const AttributeValue* attributeValue);

};


}

}

#endif /*ATTRIBUTEFACTORY_H_*/
