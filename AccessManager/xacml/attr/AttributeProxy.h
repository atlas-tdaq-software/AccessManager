#ifndef ATTRIBUTEPROXY_H_
#define ATTRIBUTEPROXY_H_

#include <AccessManager/xacml/attr/AttributeValue.h>

namespace daq
{

namespace am
{
/**
 * Used by the <code>AttributeFactory</code> to create new attributes.
 * Typically a new proxy class is created which in turn knows how to create
 * a specific kind of attribute, and then this proxy class is installed in
 * the <code>AttributeFactory</code>.
 */

class AttributeProxy
{
	
public:

     virtual
     ~AttributeProxy()
     {
       ;
     }

    /**
     * Tries to create a new <code>AttributeValue</code> based on the given
     * String data.
     *
     * @param value the text form of some attribute data
     *
     * @return an <code>AttributeValue</code> representing the given data
     */
     virtual AttributeValue* getInstance(const std::string& value) = 0;

    /**
     * Tries to create a new <code>AttributeValue</code> based on the given
     * DOM Node to read the value from.
     *
     * @param root the DOM Node root
     *
     * @return an <code>AttributeValue</code> representing the given data
     */
     virtual AttributeValue* getInstance(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root) = 0;
};

}

}

#endif /*ATTRIBUTEPROXY_H_*/
