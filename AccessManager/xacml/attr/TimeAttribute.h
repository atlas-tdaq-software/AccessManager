#ifndef TIMEATTRIBUTE_H_
#define TIMEATTRIBUTE_H_

#include "AttributeValue.h"

namespace daq
{

namespace am
{

class TimeAttribute : public daq::am::AttributeValue
{
private:	
	// read the date and time information
	void initialize();

public:

	// the URI identifier
	static const std::string identifier;

	TimeAttribute(const std::string& value = "");
	TimeAttribute(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root);
	virtual ~TimeAttribute();

};

}

}

#endif /*TIMEATTRIBUTE_H_*/
