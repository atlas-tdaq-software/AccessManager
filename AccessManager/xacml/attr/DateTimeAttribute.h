#ifndef DATETIMEATTRIBUTE_H_
#define DATETIMEATTRIBUTE_H_

#include "AttributeValue.h"

namespace daq
{

namespace am
{

class DateTimeAttribute : public daq::am::AttributeValue
{
private:	
	// read the date and time information
	void initialize();

public:

	// the URI identifier
	static const std::string identifier;

	DateTimeAttribute(const std::string& value = "");
	DateTimeAttribute(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root);
	virtual ~DateTimeAttribute();
	
};

}

}

#endif /*DATETIMEATTRIBUTE_H_*/
