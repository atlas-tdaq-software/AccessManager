#ifndef DATEATTRIBUTE_H_
#define DATEATTRIBUTE_H_

#include "AttributeValue.h"

namespace daq
{

namespace am
{

class DateAttribute : public daq::am::AttributeValue
{
private:	
	// read the date and time information
	void initialize();

public:

	// the URI identifier
	static const std::string identifier;

	DateAttribute(const std::string& value = "");
	DateAttribute(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root);
	virtual ~DateAttribute();

};

}

}

#endif /*DATEATTRIBUTE_H_*/
