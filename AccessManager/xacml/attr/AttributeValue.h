#ifndef ATTRIBUTEVALUE_H_
#define ATTRIBUTEVALUE_H_

#include <AccessManager/util/URI.h>
#include <AccessManager/xacml/Indenter.h>

#include <xercesc/dom/DOMNode.hpp>

#include <string>

namespace daq
{

namespace am
{

/*
 * ABC for an attribute value; Provides methods to encode with XML tags and send it to an ostream.
 *
 */
class AttributeValue
{

private:
	// the type URI identifier
	std::string type;

protected:
	// the attribute value in string format ready to be encoded
	std::string strValue;

	// The pure virtual method
	// set or modify the strValue
	virtual void initialize() = 0;

public:

	AttributeValue(const std::string& type, const std::string& value = "");
	AttributeValue(const std::string& type, const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root);
	// the copy constructor
	AttributeValue(const AttributeValue& attributeValue);

	virtual ~AttributeValue();

	// return the data type of this attribute value
	virtual const std::string& getType() const;

	// returns a string representation of attribute value
	virtual const std::string& encode() const;


	// attaches XML tags to the attribute value
	std::string encodeWithTags(bool includeType) const;

	// sends the encoded value to the output stream with indentation
	void encode(std::ostream& os, Indenter& indt) const;

	// sends the encoded value to the output stream without indentation
	friend std::ostream& operator<<(std::ostream& os, const AttributeValue& attrv);

	// operator needed for std::set and other similar STL classes
	friend bool operator<(const AttributeValue& left, const AttributeValue& right);

};

}

}

#endif /*ATTRIBUTEVALUE_H_*/
