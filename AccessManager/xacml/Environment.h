#ifndef ENVIRONMENT_H_
#define ENVIRONMENT_H_

#include <AccessManager/xacml/AttributeIDsValues.h>

namespace daq
{

namespace am
{

class Environment: public AttributeIDsValues
{

public:

	Environment();
	virtual ~Environment();

	/**
	 * Get the URI indentifier for the Date attribute
	 * @return URI identifier
	 */
	const std::string getDate() const;

	/**
	 * Get the URI indentifier for the Time attribute
	 * @return URI identifier
	 */
	const std::string getTime() const;

	/**
	 * Get the data type for the given URI identifier
	 * @param identifier URI identifier
	 * @return the data type in URI format
	 */
	virtual const std::string getDataType(const std::string& identifier) const;

	/**
	 * Get the match function for the given URI identifier
	 * @param identifier URI identifier
	 * @return the match function in URI format
	 */
	virtual const std::string getMatchFunction(const std::string& identifier) const;


protected:

	// implementation of initValues from ABC
	void initValues();
};

}

}

#endif /*ENVIRONMENT_H_*/
