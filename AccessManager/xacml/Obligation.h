#ifndef OBLIGATION_H_
#define OBLIGATION_H_

#include <AccessManager/xacml/ctx/Attribute.h>

#include <xercesc/dom/DOMNode.hpp>

#include <AccessManager/util/URI.h>
#include <vector>

namespace daq
{

namespace am
{

/**
 * Represents the ObligationType XML type in XACML. This also stores all the
 * AttriubteAssignmentType XML types.
 */

class Obligation
{

private:
    // the obligation id
	std::string id;

    // effect to fulfill on, as defined in Result
    int fulfillOn;

    // the attribute assignments
    std::vector<Attribute> assignments;

public:

    /**
     * Constructor that takes all the data associated with an obligation.
     * The attribute assignment list contains <code>Attribute</code> objects,
     * but only the fields used by the AttributeAssignmentType are used.
     *
     * @param id the obligation's id
     * @param fulfillOn the effect denoting when to fulfill this obligation
     * @param assignments a <code>List</code> of <code>Attribute</code>s
     */
	Obligation(const std::string& id, const int& fulfillOn, const std::vector<Attribute>& assignments);
	virtual ~Obligation();

    /**
     * Creates an instance of <code>Obligation</code> based on the DOM root
     * node.
     *
     * @param root the DOM root of the ObligationType XML type
     *
     * @return an instance of an obligation
     *
     * @throws ParsingException if the structure isn't valid
     */
    static Obligation* getInstance(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root);


    /**
     * Returns the id of this obligation
     *
     * @return the id
     */
	inline const std::string& getId() const		{ return id;}

    /**
     * Returns effect that will cause this obligation to be included in a
     * response
     *
     * @return the fulfillOn effect
     */

	inline const int& getFulfillOn() const	{ return fulfillOn; }

    /**
     * Returns the attribute assignment data in this obligation. The
     * <code>List</code> contains objects of type <code>Attribute</code>
     * with only the correct attribute fields being used.
     *
     * @return the assignments
     */
	inline const std::vector<Attribute>& getAssignments() const	 { return assignments;}

    /**
     * Encodes this context into its XML representation and writes
     * this encoding to the given <code>OutputStream</code> with
     * indentation.
     *
     * @param output a stream into which the XML-encoded data is written
     * @param indenter an object that creates indentation strings
     */
	void encode(std::ostream& os, Indenter& indenter) const;

	// returns a string representation of attribute value
	std::string encode() const;

	// sends the encoded value to the output stream without indentation
	friend std::ostream& operator<<(std::ostream& os, const Obligation& reqCtx);

	// operator needed for std::set and other similar STL classes
	friend bool operator<(const Obligation& left, const Obligation& right);

};

}

}

#endif /*OBLIGATION_H_*/
