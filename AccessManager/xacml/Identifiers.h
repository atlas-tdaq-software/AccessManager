#ifndef IDENTIFIERS_H_
#define IDENTIFIERS_H_

#include <string>

namespace daq
{
  namespace am
  {
    /// The command name resource identifier.
    extern const std::string IDENTIFIER_RESOURCE_TYPE_COMMAND_NAME;

    /// The partition name resource identifier.
    extern const std::string IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME;

    /// The resource type name identifier.
    extern const std::string IDENTIFIER_RESOURCE_TYPE_NAME;

    /// The resource type identifier for the file or directory path.
    extern const std::string IDENTIFIER_RESOURCE_TYPE_PATH;

    /// The method resource identifier.
    extern const std::string IDENTIFIER_RESOURCE_TYPE_IDLMETHOD;

    /// The application resource identifier.
    extern const std::string IDENTIFIER_RESOURCE_TYPE_APPLICATION;

    /// The application arguments resource identifier.
    extern const std::string IDENTIFIER_RESOURCE_TYPE_ARGUMENTS;

    /// The hostname resource identifier
    extern const std::string IDENTIFIER_RESOURCE_TYPE_HOSTNAME;

    /// The owned-by-requester identifier with true and false allowed values.
    extern const std::string IDENTIFIER_RESOURCE_TYPE_OWNED_BY_REQUESTER;
  }
}

#endif
