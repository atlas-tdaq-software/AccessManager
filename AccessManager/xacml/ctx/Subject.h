#ifndef SUBJECT_H_
#define SUBJECT_H_

#include <AccessManager/xacml/ctx/Attribute.h>
#include <AccessManager/util/URI.h>
#include <set>

namespace daq
{

namespace am
{

/**
 * This class represents the collection of <code>Attribute</code>s associated
 * with a particular subject.
 */
class Subject
{
private:

    // the subject's category
    std::string category;

    // the attributes associated with the subject
    std::set<Attribute> attributes;


public:

	Subject(const std::set<Attribute>& attributes);
	Subject(const std::string& category, const std::set<Attribute>& attributes);

	virtual ~Subject();

	/**
	 * The access subject categories.
	 */

	/*
	 * This identifier indicates the system entity that initiated the access request.
	 * That is, the initial entity in a request chain.
	 * If subject category is not specified, this is the default value.
	 */
	static const std::string CATEGORY_ACCESS_SUBJECT;

	/*
	 * This identifier indicates the system entity that will receive
	 * the results of the request (used when it is distinct from the access-subject).
	 */
	static const std::string CATEGORY_RECIPIENT_SUBJECT;

	/*
	 * This identifier indicates a system entity through which the access request was passed.
	 * There may be more than one.  No means is provided to specify the order
	 * in which they passed the message.
	 */
	static const std::string CATEGORY_INTERMEDIARY_SUBJECT;

	/*
	 * This identifier indicates a system entity associated with a local or remote codebase
	 * that generated the request.  Corresponding subject attributes might include
	 * the URL from which it was loaded and/or the identity of the code-signer.
	 * There may be more than one.
	 * No means is provided to specify the order in which they processed the request.
	 */
	static const std::string CATEGORY_CODEBASE;

	/*
	 * This identifier indicates a system entity associated with the computer that initiated
	 * the access request.  An example would be an IPsec identity.
	 */
	static const std::string CATEGORY_REQUESTING_MACHINE;

    /**
     * <code>URI</code> form of the default subject category
     */
    static const std::string DEFAULT_CATEGORY;

    /**
     * Returns the category of this subject's attributes.
     *
     * @return the category
     */
	inline const std::string& getCategory() const {	return category;}

    /**
     * Returns the <code>Attribute</code>s associated with this subject.
     *
     * @return the immutable <code>Set</code> of <code>Attribute</code>s
     */
	inline const std::set<Attribute>* getAttributes() const {return &attributes;}

	// operator needed for std::set and other similar STL classes
	friend bool operator<(const Subject& left, const Subject& right);

};

}

}

#endif /*SUBJECT_H_*/
