#ifndef RESPONSECTX_H_
#define RESPONSECTX_H_

#include <AccessManager/xacml/ctx/Result.h>

#include <xercesc/dom/DOMNode.hpp>

#include <set>

namespace daq
{

namespace am
{

class ResponseCtx
{
	
private:

    // The set of Result objects returned by the PDP
	std::set<Result> results;

public:
    /**
     * Constructor that creates a new <code>ResponseCtx</code> with only a
     * single <code>Result</code> (a common case).
     *
     * @param result the single result in the response
     */
    ResponseCtx(const Result& result);
    
    /**
     * Constructor that creates a new <code>ResponseCtx</code> with a
     * <code>set</code> of <code>Result</code>s. The <code>set</code> must
     * be non-empty.
     *
     * @param results a <code>Set</code> of <code>Result</code> objects
     */
    ResponseCtx(const std::set<Result>& results);
    
	virtual ~ResponseCtx();

    /**
     * Creates a new instance of <code>ResponseCtx</code> based on the given
     * DOM root node. A <code>XACMLParsingException</code> is thrown if the DOM
     * root doesn't represent a valid ResponseType.
     *
     * @param root the DOM root of a ResponseType
     *
     * @return a new <code>ResponseCtx</code>
     *
     * @throws XACMLParsingException if the node is invalid,
     */
    static ResponseCtx* getInstance(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root);
    
    /**
     * Creates a new <code>ResponseCtx</code> by parsing XML from an
     * memory buffer. Note that this is a convenience method, and it will
     * not do schema validation by default. You should be parsing the data
     * yourself, and then providing the root node to the other
     * <code>getInstance</code> method.
     *
     * @param inputBuffer a buffer providing the XML data
     *
     * @return a new <code>ResponseCtx</code>
     *
     * @throws ParserException if there is an error parsing the input
     */
    static ResponseCtx* getInstance(const std::string& inputBuffer);
	
    /**
     * Get the set of <code>Result</code>s from this response.
     * 
     * @return a <code>set</code> of results
     */
    inline const std::set<Result>& getResults() const {return results;}
	
    /**
     * Encodes this context into its XML representation and writes
     * this encoding to the given <code>OutputStream</code> with
     * indentation.
     *
     * @param output a stream into which the XML-encoded data is written
     * @param indenter an object that creates indentation strings
     */
	void encode(std::ostream& os, Indenter& indenter) const;
	
	// returns a string representation of attribute value
	std::string encode() const;
		
	// sends the encoded value to the output stream without indentation
	friend std::ostream& operator<<(std::ostream& os, const ResponseCtx& reqCtx);
	
};

}

}

#endif /*RESPONSECTX_H_*/
