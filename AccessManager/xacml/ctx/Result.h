#ifndef RESULT_H_
#define RESULT_H_

#include <AccessManager/xacml/ctx/Status.h>
#include <AccessManager/xacml/Obligation.h>

#include <xercesc/dom/DOMNode.hpp>

#include <string>
#include <set>

namespace daq
{

namespace am
{

class Result
{

private:	
	// the decision effect
	int decision;
	
	// the status data
	const Status* status;
	
	// the resource identifier
	std::string resource;
	
	// the set of obligations which may be empty
	std::set<Obligation> obligations;

	// check if the decision is valid
    void checkDecision();
    
public:

	/**
	 * The decision to permit the request
	 */
	static const int DECISION_PERMIT = 0;
	
	/**
	 * The decision to deny the request
	 */
	static const int DECISION_DENY = 1;
	
	/**
	 * The decision that a decision about the request cannot be made
	 */
	static const int DECISION_INDETERMINATE = 2;
	
	/**
	 * The decision that nothing applied to us
	 */
	static const int DECISION_NOT_APPLICABLE = 3;
	
	/**
	 * The count of possible decisions.
	 */
	static const int DECISIONS_COUNT = 4;

	// string versions of the 4 Decision types used for encoding
	static const std::string DECISIONS[];
	                                            
    /**
     * Constructs a <code>Result</code> object with a resource identifier,
     * but default status data (OK). The resource being named must match
     * the resource (or a descendent of the resource in the case of a
     * hierarchical resource) from the associated request.
     *
     * @param decision the decision effect to include in this result. This
     *                 must be one of the four fields in this class.
     * @param resource a <code>String</code> naming the resource
     *
     * @throws IllegalArgumentException if decision is not valid
     */
    Result(const int& decision, const std::string& resource = "");
    
    /**
     * Constructs a <code>Result</code> object with default status data (OK),
     * and obligations, but no resource identifier.
     *
     * @param decision the decision effect to include in this result. This
     *                 must be one of the four fields in this class.
     * @param obligations the obligations the PEP must handle
     *
     * @throws IllegalArgumentException if decision is not valid
     */
    Result(const int& decision, const std::set<Obligation>& obligations);

    /**
     * Constructs a <code>Result</code> object with status data and a
     * resource identifier.
     *
     * @param decision the decision effect to include in this result. This
     *                 must be one of the four fields in this class.
     * @param status the <code>Status</code> to include in this result
     * @param resource a <code>String</code> naming the resource
     *
     * @throws IllegalArgumentException if decision is not valid
     */
    Result(const int& decision, const Status* status, const std::string& resource = "");

    /**
     * Constructs a <code>Result</code> object with status data and obligations
     * but without a resource identifier. Typically the decision is
     * DECISION_INDETERMINATE in this case, though that's not always true.
     *
     * @param decision the decision effect to include in this result. This
     *                 must be one of the four fields in this class.
     * @param status the <code>Status</code> to include in this result
     * @param obligations the obligations the PEP must handle
     *
     * @throws IllegalArgumentException if decision is not valid
     */
    Result(const int& decision, const Status* status, const std::set<Obligation>& obligations);


    /**
     * Constructs a <code>Result</code> object with a resource identifier,
     * and obligations, but default status data (OK). The resource being named
     * must match the resource (or a descendent of the resource in the case of
     * a hierarchical resource) from the associated request.
     *
     * @param decision the decision effect to include in this result. This
     *                 must be one of the four fields in this class.
     * @param resource a <code>String</code> naming the resource
     * @param obligations the obligations the PEP must handle
     *
     * @throws IllegalArgumentException if decision is not valid
     */
    Result(const int& decision, const std::string& resource, const std::set<Obligation>& obligations);

    /**
     * Constructs a <code>Result</code> object with status data, a
     * resource identifier, and obligations.
     *
     * @param decision the decision effect to include in this result. This
     *                 must be one of the four fields in this class.
     * @param status the <code>Status</code> to include in this result
     * @param resource a <code>String</code> naming the resource
     * @param obligations the obligations the PEP must handle
     *
     * @throws IllegalArgumentException if decision is not valid
     */
    Result(const int& decision, const Status* status, const std::string& resource,
                  const std::set<Obligation>& obligations);
    
    //copy constructor
    Result(const Result& result);
    
	virtual ~Result();
	
    /**
     * Creates a new instance of <code>Result</code> based on the given
     * DOM root node. A <code>XACMLParsingException</code> is thrown if the DOM
     * root doesn't represent a valid Result.
     *
     * @param root the DOM root of a Result
     *
     * @return a new <code>Result</code>
     *
     * @throws XACMLParsingException if the node is invalid,
     * 		   XACMLIllegalArgumentException if the root is NULL
     */
    static Result* getInstance(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root);

    /**
     * Returns the decision associated with this <code>Result</code>. This
     * will be one of the four <code>DECISION_*</code> fields in this class.
     *
     * @return the decision effect
     */
    inline const int& getDecision() const { return decision;}

    /**
     * Returns the status data included in this <code>Result</code>.
     * Typically this will be <code>STATUS_OK</code> except when the decision
     * is <code>INDETERMINATE</code>.
     *
     * @return status associated with this Result
     */
    inline const Status* getStatus() const {return status;}

    /**
     * Returns the resource to which this Result applies, or empty string if none
     * is specified.
     *
     * @return a resource identifier or empty string
     */
    inline const std::string& getResource() const { return resource;}

    /**
     * Sets the resource identifier if it has not already been set before.
     * The core code does not set the resource identifier, so this is useful
     * if you want to write wrapper code that needs this information.
     *
     * @param resource the resource identifier
     *
     * @return true if the resource identifier was set, false if it already
     *         had a value
     */
    inline bool setResource(const std::string& resource) {
        if (this->resource.size() > 0)
            return false;

        this->resource = resource;

        return true;
    }

    /**
     * Returns the set of obligations that the PEP must fulfill, which may
     * be empty.
     *
     * @return the set of obligations
     */
    inline const std::set<Obligation>& getObligations() const {return obligations;}

    /**
     * Adds an obligation to the set of obligations that the PEP must fulfill
     *
     * @param obligation the <code>Obligation</code> to add
     */
    inline void addObligation(const Obligation& obligation) { obligations.insert(obligation);}

    /**
     * Encodes this context into its XML representation and writes
     * this encoding to the given <code>OutputStream</code> with
     * indentation.
     *
     * @param output a stream into which the XML-encoded data is written
     * @param indenter an object that creates indentation strings
     */
	void encode(std::ostream& os, Indenter& indenter) const;
	
	// returns a string representation of attribute value
	std::string encode() const;
		
	// sends the encoded value to the output stream without indentation
	friend std::ostream& operator<<(std::ostream& os, const Result& reqCtx);

	// operator needed for std::set and other similar STL classes
	friend bool operator<(const Result& left, const Result& right);	
	
};

}

}

#endif /*RESULT_H_*/
