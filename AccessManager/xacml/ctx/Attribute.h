#ifndef ATTRIBUTE_H_
#define ATTRIBUTE_H_

#include <AccessManager/util/URI.h>
#include <AccessManager/xacml/attr/AttributeValue.h>
#include <AccessManager/xacml/attr/DateTimeAttribute.h>
#include <AccessManager/xacml/AttributeIDsValues.h>

namespace daq
{

namespace am
{

class Attribute
{
protected:

    // required meta-data attributes
    // attribute id
    std::string id;
    // attribute data type
    std::string type;

    // optional meta-data attributes
    std::string issuer;
    const DateTimeAttribute* issueInstant;

    // the single value associated with this attribute
    const AttributeValue* value;

	//initialize the member attributes
	void initialize(const std::string& id,
					const std::string& issuer,
					const DateTimeAttribute* issueInstant,
					const AttributeValue* value);

public:
    /**
     * Creates a new <code>Attribute</code> of the type specified in the
     * given <code>AttributeValue</code>.
     *
     * @param id the id of the attribute
     * @param issuer the attribute's issuer or "" if there is none
     * @param issueInstant the moment when the attribute was issued, or null
     *                     if it's unspecified.
     * 					   IT WILL BE DESTROYED IN THE ATTRIBUTE DESTRUCTOR.
     * @param value the actual value associated with the attribute meta-data
     * 					   IT WILL BE DESTROYED IN THE ATTRIBUTE DESTRUCTOR.
     */
	Attribute(const std::string& id, const std::string& issuer, const DateTimeAttribute* issueInstant, const AttributeValue* value);

    /**
     * Creates a new <code>Attribute</code> of the type specified in the
     * given <code>AttributeValue</code>.
     * The attribute issuers is "" and no issueInstant
     *
     * @param id the id of the attribute
     * @param value the actual value associated with the attribute meta-data
     * 					   IT WILL BE DESTROYED IN THE ATTRIBUTE DESTRUCTOR.
     */
	Attribute(const std::string& id, const AttributeValue* value);

    /**
     * Creates a new <code>Attribute</code> of the id provided as parameter and
     * the values retrieved from AttributeIDsValues object.
     * The attribute issuers is "" and no issueInstant.
     *
     * @param id the id of the attribute
     * @param AttributeIDsValue object containing the values for various ids
     */
	Attribute(const std::string& id, const AttributeIDsValues& idsValues);


	/**
	 * Copy constructor
	 */
	Attribute(const Attribute& attribute);

	virtual ~Attribute();

	// the getters
	inline const std::string& 			getId() const			{ return id;}
	inline const std::string& 			getType() const			{ return type;}
	inline const std::string&			getIssuer()	const 		{ return issuer;}
	inline const DateTimeAttribute*		getIssueInstant() const	{ return issueInstant;}
	inline const AttributeValue*		getValue() const		{ return value;}

	// returns a string representation of attribute value
	std::string encode() const;

	// sends the encoded value to the output stream with indentation
	void encode(std::ostream& os, Indenter& indt) const;

	// sends the encoded value to the output stream without indentation
	friend std::ostream& operator<<(std::ostream& os, const Attribute& attr);

	// operator needed for std::set and other similar STL classes
	friend bool operator<(const Attribute& left, const Attribute& right);
};

}

}

#endif /*ATTRIBUTE_H_*/
