#ifndef STATUSDETAIL_H_
#define STATUSDETAIL_H_

#include <AccessManager/xacml/ctx/Attribute.h>

#include <xercesc/dom/DOMNode.hpp>

#include <vector>
#include <string> 

namespace daq
{

namespace am
{

/**
 * This class represents the StatusDetailType in the context schema. Because
 * status detail is defined as a sequence of xs:any XML type, the data in
 * this class must be generic, and it is up to the application developer to
 * interpret the data appropriately.
 */
 
class StatusDetail
{

private:
    // the text version, if it's avilable already
    std::string detailText;
    
    //the root node
	const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode*	detailRoot;

	/**
     * Private helper routine that converts text into a node
     */
	XERCES_CPP_NAMESPACE_QUALIFIER DOMNode* textToNode(const std::string& encoded);

    /**
     * Private constructor that just sets the root node. This interface
     * is provided publically through the getInstance method.
     */
    StatusDetail(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode* root);
	
public:
    /**
     * Constructor that uses a <code>vector</code> of <code>Attribute</code>s
     * to define the status detail. This is a common form of detail data,
     * and can be used for things like providing the information included
     * with the missing-attribute status code.
     *
     * @param attributes a <code>vector</code> of <code>Attribute</code>s
     *
     */
    StatusDetail(const std::vector<Attribute>& attributes);

    /**
     * Constructor that takes the text-encoded form of the XML to use as
     * the status data. The encoded text will be wrapped with the
     * <code>StatusDetail</code> XML tag, and the resulting text must
     * be valid XML.
     *
     * @param encoded a <code>std::string</code> that encodes the
     *                status detail
     *
     */
    StatusDetail(const std::string& encoded = "");

	// copy constructor
    StatusDetail(const StatusDetail& statusDetail);
    
    /**
     * Creates an instance of a <code>StatusDetail</code> object based on
     * the given DOM root node. The node must be a valid StatusDetailType
     * root, or else a <code>ParsingException</code> is thrown.
     *
     * @param root the DOM root of the StatusDetailType XML type
     *
     * @return a new <code>StatusDetail</code> object
     *
     * @throws ParsingException if the root node is invalid
     */
    static StatusDetail* getInstance(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root);

	virtual ~StatusDetail();
	
    /**
     * Returns the StatusDetailType DOM root node. This may contain within
     * it any type of valid XML data, and it is up to the application writer
     * to handle the data accordingly. One common use of status data is to
     * include <code>Attribute</code>s, which can be created from their
     * root DOM nodes using their <code>getInstance</code> method.
     *
     * @return the DOM root for the StatusDetailType XML type
     */
    inline const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode* getDetail() const { return detailRoot; }

    /**
     * Returns the text-encoded version of this data, if possible. If the
     * <code>std::string</code> form constructor was used, this will just be the
     * original text wrapped with the StatusData tag. If the <code>std::vector</code>
     * form constructor was used, it will be the encoded attribute data.
     *
     * @return the encoded form of this data
     */
    inline const std::string& getEncoded() const { return detailText;}
	
	// operator needed for std::set and other similar STL classes
	friend bool operator<(const StatusDetail& left, const StatusDetail& right);	
};

}

}

#endif /*STATUSDETAIL_H_*/
