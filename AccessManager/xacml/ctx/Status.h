#ifndef STATUS_H_
#define STATUS_H_

#include <AccessManager/xacml/ctx/StatusDetail.h>

#include <xercesc/dom/DOMNode.hpp>

#include <string>
#include <vector>

namespace daq
{

namespace am
{

/**
 * Represents the status data that is included in a ResultType. By default,
 * the status is OK.
 */
 
class Status
{

private:
    // the status code
    std::vector<std::string> code;

    // the message
    std::string message;

    // the detail
    const StatusDetail* detail;

    // a single OK object we'll use most of the time
    static const Status okStatus;

	// check the code value and if detail is present
	void checkCode();
	
	/**
     * Encodes the object in XML
     */
    void encodeStatusCode(std::ostream& os, Indenter& indenter, std::vector<std::string>::const_iterator& cit) const;
	
public:

    /**
     * Standard identifier for the OK status
     */
    static const std::string STATUS_OK;

    /**
     * Standard identifier for the MissingAttribute status
     */
    static const std::string STATUS_MISSING_ATTRIBUTE;
    
    /**
     * Standard identifier for the SyntaxError status
     */
    static const std::string STATUS_SYNTAX_ERROR;

    /**
     * Standard identifier for the ProcessingError status
     */
    static const std::string STATUS_PROCESSING_ERROR;


    /**
     * Constructor that takes only one the status code.
     *
     * @param code a <code>string</code> codes, typically
     *             just one code, but this may contain any number of minor
     *             codes after the first item in the list, which is the major
     *             code
     */
    Status(const std::string& code);

    /**
     * Constructor that takes the status code, an optional message, and some
     * detail to include with the status. Note that the specification 
     * explicitly says that a status code of OK, SyntaxError or
     * ProcessingError may not appear with status detail, so an exception is
     * thrown if one of these status codes is used and detail is included.
     *
     * @param code a <code>vector</code> of <code>string</code> codes, typically
     *             just one code, but this may contain any number of minor
     *             codes after the first item in the list, which is the major
     *             code
     * @param message a message to include with the code, or null if there
     *                should be no message
     * @param detail the status detail to include, or null if there is no
     *               detail
     *
     * @throws XACMLIllegalArgumentException if detail is included for a status
     *                                  code that doesn't allow detail
     */
    Status(const std::vector<std::string>& code, const std::string& message = "", const StatusDetail* detail = NULL);
    
    // copy constructor
    Status(const Status& status); 
    
	virtual ~Status();
	
	
	
    /**
     * Creates a new instance of <code>Status</code> based on the given
     * DOM root node. A <code>ParsingException</code> is thrown if the DOM
     * root doesn't represent a valid StatusType.
     *
     * @param root the DOM root of a StatusType
     *
     * @return a new <code>Status</code>
     *
     * @throws ParsingException if the node is invalid
     */
    static Status* getInstance(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root);

//    /**
//     * Check if the status contain some information
//     *
//     * @return the status code
//     */
//    inline bool isEmpty() const { return code.size() == 0 || code[0].length() == 0;}

    /**
     * Returns the status code.
     *
     * @return the status code
     */
    inline const std::vector<std::string>& getCode() const { return code;}

    /**
     * Returns the status message or null if there is none.
     *
     * @return the status message or null
     */
    inline const std::string& getMessage() const { return message;}

    /**
     * Returns the status detail or null if there is none.
     *
     * @return a <code>StatusDetail</code> or null
     */
    inline const StatusDetail* getDetail() const { return detail; }

    /**
     * Gets a <code>Status</code> instance that has the OK status and no
     * other information. This is the default status data for all responses
     * except Indeterminate ones.
     *
     * @return an instance with <code>STATUS_OK</code>
     */
    static const Status& getOkInstance() {return okStatus; }
	
    /**
     * Encodes this context into its XML representation and writes
     * this encoding to the given <code>OutputStream</code> with
     * indentation.
     *
     * @param output a stream into which the XML-encoded data is written
     * @param indenter an object that creates indentation strings
     */
	void encode(std::ostream& os, Indenter& indenter) const;
	
	// returns a string representation of attribute value
	std::string encode() const;
		
	// sends the encoded value to the output stream without indentation
	friend std::ostream& operator<<(std::ostream& os, const Status& status);

	// operator needed for std::set and other similar STL classes
	friend bool operator<(const Status& left, const Status& right);	
};

}

}

#endif /*STATUS_H_*/
