#ifndef REQUESTCTX_H_
#define REQUESTCTX_H_

#include <AccessManager/xacml/ctx/Attribute.h>
#include <AccessManager/xacml/ctx/Subject.h>

#include <AccessManager/xacml/Indenter.h>

#include <set>
#include <string>
#include <ostream>

namespace daq
{

namespace am
{

/* Represents a request made to the PDP. This is the class that contains all
 * the data used to start a policy evaluation.
 */

class RequestCtx
{
	
private:
    // There must be at least one subject
    std::set<Subject> 	subjects;

    // There must be exactly one resource
    std::set<Attribute> resource;

    // There must be exactly one action
    std::set<Attribute> action;

    // There may be any number of environment attributes
    std::set<Attribute> environment;

    // The optional, generic resource content
    std::string resourceContent;

public:

    /**
     * Constructor that creates a <code>RequestCtx</code> from components.
     *
     * @param subjects a <code>Set</code> of <code>Subject</code>s
     * @param resource a <code>Set</code> of <code>Attribute</code>s
     * @param action a <code>Set</code> of <code>Attribute</code>s
     * @param environment a <code>Set</code> of environment attributes
     * @param resourceContent a text-encoded version of the content, suitable
     *                        for including in the RequestType, including the
     *                        root <code>RequestContent</code> node
     */
    RequestCtx(	const std::set<Subject>& 	subjects,
				const std::set<Attribute>& 	resource,
				const std::set<Attribute>& 	action,
				const std::set<Attribute>& 	environment,
				const std::string& 			resourceContent = "");
                      
	virtual ~RequestCtx();

    /**
     * Returns a <code>Set</code> containing <code>Subject</code> objects.
     *
     * @return the request's subject attributes
     */
	inline const std::set<Subject>& getSubjects() const {return subjects;}

    /**
     * Returns a <code>Set</code> containing <code>Attribute</code> objects.
     *
     * @return the request's resource attributes
     */
	inline const std::set<Attribute>& getResource() const {return resource;} 

    /**
     * Returns a <code>Set</code> containing <code>Attribute</code> objects.
     *
     * @return the request's action attributes
     */
	inline const std::set<Attribute>& getAction() const {return action;}

    /**
     * Returns a <code>Set</code> containing <code>Attribute</code> objects.
     *
     * @return the request's environment attributes
     */
	inline const std::set<Attribute>& getEnvironmentAttributes() const {return environment; }

	/**
	 * Encodes this context into its XML representation and writes
	 * this encoding to the given <code>OutputStream</code> with
	 * indentation.
	 *
	 * @param output a stream into which the XML-encoded data is written
	 * @param indenter an object that creates indentation strings
	 */
	void encode(std::ostream& os, Indenter& indenter) const;
	
	// returns a string representation of attribute value
	std::string encode() const;
		
	// sends the encoded value to the output stream without indentation
	friend std::ostream& operator<<(std::ostream& os, const RequestCtx& reqCtx);
	
};

}

}

#endif /*REQUESTCTX_H_*/
