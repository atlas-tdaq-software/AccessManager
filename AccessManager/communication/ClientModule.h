#ifndef CLIENTMODULE_H_
#define CLIENTMODULE_H_

#include "CommunicationStreams.h"

namespace daq
{

namespace am
{

/*
 * Interface for a client communication module.
 */
class ClientModule
{
public:
	/*
	 * Get the communication streams to be used for read/write
	 */
	virtual CommunicationStreams* getCommunicationStreams() const = 0;

	/*
	 * Close the communication channel.
	 */
	virtual void closeCommunication() = 0;
	
	virtual ~ClientModule();
};

}
}

#endif /*CLIENTMODULE_H_*/
