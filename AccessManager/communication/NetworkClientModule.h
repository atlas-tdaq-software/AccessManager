#ifndef NETWORKCCLIENTMODULE_H_
#define NETWORKCCLIENTMODULE_H_

#include <AccessManager/communication/ClientModule.h>
#include <AccessManager/communication/CommunicationStreams.h>
#include <AccessManager/communication/NetworkCommunicationStreams.h>

#include <string>

#include <sys/socket.h>
#include <netdb.h>


namespace daq
{
namespace am
{

class NetworkClientModule : public ClientModule
{

private:
	/*
	 * The server name the client is going to connect to
	 */
	std::string serverName;

	/*
	 * The server port the client is going to connect to
	 */
	unsigned short int serverPort;

	/*
	 * The structure with the server address information
	 */
	struct addrinfo serverInfo;

	/*
	 * The sockaddr variable with the memory already allocated to be used in the serverInfo structure
	 */
	struct sockaddr serverInfo_ai_addr;
	
	NetworkCommunicationStreams* communicationStream;

public:

	/*
	 * Initialize the object for network client connection module with the server.
	 * The parameters are the server name and port number
	 */
	NetworkClientModule(const std::string& serverName, const unsigned short int& serverPort);

	virtual ~NetworkClientModule();

	/*
	 * Tries to open the network connection with an optional timeout (in milliseconds) value strictly positive.
	 * throws NetworkError exception
	 */
	void initConnection(const unsigned long timeout = 0);

	/*
	 * Method from ClientModule interface
	 */
	inline CommunicationStreams* getCommunicationStreams() const {return communicationStream;}

	/*
	 * Get the server name
	 */
	inline const std::string& getServerName() const {return serverName;}

	/*
	 * Method from ClientModule interface
	 */
	void closeCommunication();

};

}
}

#endif /*NETWORKCLIENTMODULE_H_*/
