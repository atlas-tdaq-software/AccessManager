#ifndef COMMUNICATIONSTREAMS_H_
#define COMMUNICATIONSTREAMS_H_

#include <string>

namespace daq
{

namespace am
{

/*
 * Abstract Base Class to define the functionality of a set of communication streams
 * The methods to send and receive messages are defined here.
 */
class CommunicationStreams
{
protected:
	
	// the operations timeout values in milliseconds; both values of 0 means "no timeout"
	unsigned long recvTimeout;
	unsigned long sendTimeout;
	
public:
	// constructor that initialize the timeout values in ms for recv and send
	CommunicationStreams(const unsigned long& recvTimeout = 0, const unsigned long& sendTimeout = 0);
	virtual ~CommunicationStreams();
	
	// sends a message to the stream
	// throws an CommunicationStreamsError exception in case the send didn't succeed
	virtual void sendMessage(const std::string& message) = 0;
	
	// receives a message from the stream;
	// throws an CommunicationStreamsError exception in case the receive operation
	// didn't succeed due to some internal errors
	virtual const std::string receiveMessage() = 0;
	
	// checks if the communication stream is open and usable
	virtual bool isOpened() const = 0;
	
	// set the recv timeout value in miliseconds
	inline void setRecvTimeout(const unsigned long timeout) { this->recvTimeout = timeout;}
	
	// get the recv timeout value in miliseconds
	inline void setSendTimeout(const unsigned long timeout) { this->sendTimeout = timeout;}

	// get the recv timeout value in miliseconds
	inline unsigned long getRecvTimeout() const { return this->recvTimeout;}
	
	// get the recv timeout value in miliseconds
	inline unsigned long getSendTimeout() const { return this->sendTimeout;}
};

}

}

#endif /*COMMUNICATIONSTREAMS_H_*/
