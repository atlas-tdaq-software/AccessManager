#ifndef NETWORKCOMMUNICATIONSTREAMS_H_
#define NETWORKCOMMUNICATIONSTREAMS_H_

#include <AccessManager/communication/CommunicationStreams.h>

#include <string>
#include <sstream>

namespace daq
{

namespace am
{

#define BUFFER_SIZE		1024

/*
 * Use the poll function for timeout operations on socket
 */
#ifndef AM_CLIENT_NOT_POLL_SOCKET
#define AM_CLIENT_POLL_SOCKET
#endif

/*
 * Implementation of CommunicationStreams ABC for network communication
 */
class NetworkCommunicationStreams : public daq::am::CommunicationStreams
{
private:
	// the socket file descriptor
	int socketfd;

	// connection description used when printing logs or throwing exceptions
	std::string description;

	// last received message
	std::ostringstream strBuff;

	// internal buffer for receive operation
	char buffer[BUFFER_SIZE + 1];
	static const unsigned int bufferSize = BUFFER_SIZE;

	// receive characters from socket and throw exception if timeout occured or any other exception
	// return the number of characters read in the buffer
	int receiveTimeout(char* _buffer, unsigned int _bufferSize);

public:
	// constructor with connection description and socket file descriptor
	NetworkCommunicationStreams(const int& socketfd, const std::string& description);

	virtual ~NetworkCommunicationStreams();

	// sends a message to the stream
	// throws an CommunicationStreamsError exception in case the send didn't succeed
	void sendMessage(const std::string& message);

	// receives a message from the stream;
	// throws - CommunicationStreamsError exception in case the receive
	//			operation didn't succeed due to some internal errors
	//		  - CommunicationStreamsTimeout exception when the operation
	//			failed because of timeout
	const std::string receiveMessage();

	// checks if the communication stream is open and usable
	bool isOpened() const;

	// close the communication stream
	void close();

};

}
}
#endif /*NETWORKCOMMUNICATIONSTREAMS_H_*/
