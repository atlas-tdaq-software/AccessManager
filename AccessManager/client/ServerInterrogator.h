#ifndef SERVERINTERROGATOR_H_
#define SERVERINTERROGATOR_H_

#include <AccessManager/xacml/Resource.h>
#include <AccessManager/xacml/ctx/ResponseCtx.h>
#include <AccessManager/client/ClientConfig.h>
#include <AccessManager/client/RequestorInfo.h>
#include <AccessManager/client/RequestComposer.h>
#include <AccessManager/communication/ClientModule.h>

#include <string>

namespace daq
{

namespace am
{

class ServerInterrogator
{
private:

	// the client configuration object
	const std::unique_ptr<ClientConfig> clientCfg;

	// the current subject which is the access subject most of the time.
	// when another accessSubject is provided, the current subject is intermediary subject
	RequestorInfo	currentSubject;

	// the current AM server
	std::string currentAMServer;

	// the servers interrogated so far
	std::string amServerHistory;

	// the bit to say if the authorization has to be done
	bool authEnabled;

	// VARIABLES FOR SECONDARY AM SERVERS
	static const std::string MSG_SERVER_BUSY;
	// specify the use of secondary AM servers if found in response
	bool useSecondaryAMServers;

	// store the secondary am servers if found in the response separated by ","
	std::string secondaryAMServers;

	// store the error messages if a request could not be successfully honored
	std::string statusMessage;

	// helper method to get the next AM server name
	std::string getNextAMServer();

	// the basic functionality: answer to the question "is the access to the resource granted?"
	bool isAuthorizationGranted(RequestComposer& reqComp);

	// helper method to get the true/false decision from a response context
	// Throws:
	//		ServerProcessingIssue if the server has responded back with processing error
	bool getDecisionFromResponse(const ResponseCtx& responseCtx);

public:
	ServerInterrogator();
	virtual ~ServerInterrogator();

	// the basic functionality: answer to the question "is the access to the resource granted?"
	// the action to be performed on the resource is defined in the resource object
	// the request is made on behalf of the current user
	// the servers provided in the environment variables are checked one after the other until one of them responds
	// if a server responds with "server busy" message and provides a list of servers to be contacted, then this list
	// is used before continuing to iterate through the client list of am servers
	// Params:
	//	resource to request authorization for
	// Returns:
	//	if the authorization is granted return true, else return false
	// Throws:
	//	Exception						- the base class of all exception thrown by the AM C++ client API
	//	|
	//	|- CommunicationIssue			- the base class of all the exceptions concerning the communication problems
	//	|  |- CommunicationInit			- failure in communication initialization with the server
	//	|  |- NetworkError				- generic error when network problems are encountered
	//	|  |  |- NetworkConnectTimeout	- the network connection to the server has timeout
	//	|  |- CommunicationStreamsError	- the messages sending or receiving errors
	//	|     |- CommunicationStreamsTimeout	- the cause of a communication stream error is a timeout
	//	|
	//	|- XACMLIssue					- the base class of all the exceptions concerning XACML processing;
	//	|- ServerProcessingIssue		- the base class of all the exceptions cause by a server processing
	//									error found in the response from the server
	//
	bool isAuthorizationGranted(const Resource& resource);

	// the basic functionality: answer to the question "is the access to the resource granted?"
	// the action to be performed on the resource is defined in the resource object
	// the request is made on behalf of the access subject provided as parameter
	// the servers provided in the environment variables are checked one after the other until one of them responds
	// if a server responds with "server busy" message and provides a list of servers to be contacted, then this list
	// is used before continuing to iterate through the client list of am servers
	// Params:
	//	resource to request authorization for
	//	the subject to access the resource
	// Returns:
	//	if the authorization is granted return true, else return false
	// Throws:
	//	Exception						- the base class of all exception thrown by the AM C++ client API
	//	|
	//	|- CommunicationIssue			- the base class of all the exceptions concerning the communication problems
	//	|  |- CommunicationInit			- failure in communication initialization with the server
	//	|  |- NetworkError				- generic error when network problems are encountered
	//	|  |  |- NetworkConnectTimeout	- the network connection to the server has timeout
	//	|  |- CommunicationStreamsError	- the messages sending or receiving errors
	//	|     |- CommunicationStreamsTimeout	- the cause of a communication stream error is a timeout
	//	|
	//	|- XACMLIssue					- the base class of all the exceptions concerning XACML processing;
	//	|- ServerProcessingIssue		- the base class of all the exceptions cause by a server processing
	//									error found in the response from the server
	//
	bool isAuthorizationGranted(const Resource& resource, const RequestorInfo& accessSubject);

	 std::string pippo;

	// return the status message that may contain more details about the failure or success of
	// an authorization grant/deny
	const std::string getStatusMessage() const { return statusMessage;}
};

}

}

#endif /*SERVERINTERROGATOR_H_*/
