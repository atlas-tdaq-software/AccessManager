#ifndef REQUESTORINFO_H_
#define REQUESTORINFO_H_

#include <AccessManager/xacml/Subjects.h>
#include <sys/types.h>

namespace daq
{

  namespace am
  {

    /*
     * The user identifier to be used to get the username
     */
    enum USER_IDENTIFIER
    {
      REAL_ID, 		// real user id and group id
      EFFECTIVE_ID	// effective user id and group id
    };

    class RequestorInfo : public Subjects
    {

    private:
      /*
       * Requestor information: the username, the hostname and its ip address
       */

      /*
       * REAL USER ID and GROUP ID
       * The real UID (RUID) is the actual user identifier (UID) of the person who is running the program.
       * It is usually the same as the UID of the actual person who is logged into the computer,
       * sitting in front of the terminal (or workstation).
       */
      uid_t ruid;
      gid_t rgid;

      /*
       * EFFECTIVE USER ID and GROUP ID
       * The effective UID (EUID) identifies the actual privileges of the process that is running.
       */
      uid_t euid;
      gid_t egid;

      // what user identifier to be used: real or effective
      USER_IDENTIFIER userIdentifier;

      std::string username;
      std::string hostname;
      std::string ipaddress;

      /*
       * Set the values for user information.
       */
      void
      initValues();

    public:

      /*
       * Set the username. The machine name (hostname, ip address) is read from the current system.
       * If the username is not provided, the current effective user name is used.
       */
      RequestorInfo(const std::string& username = "");

      /*
       * Get the username based on user identifier (real or effective).
       * The machine name (hostname, ip address) is read from the current system.
       */
      RequestorInfo(const USER_IDENTIFIER& userIdentifier);

      /*
       * Set the username and the machine name (hostname). The ip address is automatically detected.
       */
      RequestorInfo(const std::string& username, const std::string& hostname);

      virtual
      ~RequestorInfo();

    };

  }

}

#endif /*REQUESTORINFO_H_*/
