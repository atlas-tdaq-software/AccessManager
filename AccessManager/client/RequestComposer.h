#ifndef REQUESTCOMPOSER_H_
#define REQUESTCOMPOSER_H_

#include <AccessManager/xacml/Subjects.h>
#include <AccessManager/xacml/Resource.h>
#include <AccessManager/xacml/ctx/RequestCtx.h>
#include <set>
#include <memory>

namespace daq
{

namespace am
{

/*
 * Get the AM specific entities and builds a request context ready to be sent to the AM server for
 * authorization.
 *
 */

class RequestComposer
{

private:

    std::set<Subject> 	subjects;
    std::set<Attribute> resource;
    std::set<Attribute> action;
    std::set<Attribute> environment;
    std::string resourceContent;

	// the request context object
	std::unique_ptr<RequestCtx> reqCtx;

	void addSubjects(const std::string& category, const Subjects& sbj);
	void initEnvironment();

	// add an action to request authorization for;
	// it will be used internally by addResource to get the action object for a resource identifier
	void addAction(const Resource& resource, const std::string& identifier);

public:
	/*
	 * Prepare the access request on behalf of 'accessSubject' by a possible intermediary subject.
	 */
	RequestComposer(const Subjects& accessSubject);
	RequestComposer(const Subjects& accessSubject, const Subjects& intermediarySubject);
	virtual ~RequestComposer();

	// add a resource to request access for
	void addResource(const Resource& resource);

	// set the resource to request access for
	void setResource(const Resource& resource);

	// build the request object
	void buildRequest();

	// returns a string representation of attribute value
	std::string encode() const;

	// sends the encoded value to the output stream without indentation
	friend std::ostream& operator<<(std::ostream& os, const RequestComposer& reqComp);

};

}

}

#endif /*REQUESTCOMPOSER_H_*/
