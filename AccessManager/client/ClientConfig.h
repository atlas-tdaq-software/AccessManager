#ifndef CLIENTCONFIG_H_
#define CLIENTCONFIG_H_

#include <string>
#include <map>

namespace daq
{

namespace am
{

/*
 * Provides client configuration settings like the AM server name and port number.
 */
class ClientConfig
{

private:

	// the environment variable that stores the AM server host
	static const std::string PARAMETER_AM_SERVER_HOST;
	// the environment variable that stores the AM server port
	static const std::string PARAMETER_AM_SERVER_PORT;

	// the environment variable that stores the flag enabled/disabled of AM authorization
	static const std::string PARAMETER_AM_AUTHORIZATION;
	static const std::string VALUE_AM_AUTHORIZATION_ENABLED;
	static const std::string VALUE_AM_AUTHORIZATION_DISABLED;
	static const std::string VALUE_AM_AUTHORIZATION_DEFAULT;

	// the timeout variables in milliseconds
	static const std::string PARAMETER_AM_CLIENT_CONNECT_TIMEOUT;
	static const std::string VALUE_AM_CLIENT_CONNECT_TIMEOUT_DEFAULT;
	static const std::string PARAMETER_AM_CLIENT_RECV_TIMEOUT;
	static const std::string VALUE_AM_CLIENT_RECV_TIMEOUT_DEFAULT;

	// the environment variable that specify if the API should use the secondary AM servers
	static const std::string PARAMETER_AM_CLIENT_USE_SECONDARY_SERVERS;
	static const std::string VALUE_AM_CLIENT_USE_SECONDARY_SERVERS;
	static const std::string VALUE_AM_CLIENT_DONT_USE_SECONDARY_SERVERS;
	static const std::string VALUE_AM_CLIENT_USE_SECONDARY_SERVERS_DEFAULT;

	// the environment variables to be read;
	// first string is parameter name, the second is the value
	std::map<std::string, std::string> parameters;

	// the am server hosts separated by ','
	std::string amServerHostsAvailable;

	// return a constant value from the map
	const std::string getConstantValueFromMap(const std::string& key) const;

public:

	ClientConfig();
	virtual ~ClientConfig();

	// read again the parameters value
	void refresh();

	// get all AM server host names in one string
	const std::string getAMServerHosts() const;

	// METHODS TO ITERATE THROUGH THE AM SERVER HOSTS IF THERE ARE MORE THAN ONE
	// get the first AM server host name;
	// the others are returned by the getNextAMSErverHost method
	const std::string getFirstAMServerHost();

	// get the next AM server host name;
	// returns an empty string if there are no more AM servers in the list
	const std::string getNextAMServerHost();

	// get the AM server port number
	unsigned long getAMServerPort() const;

	// check if the AM authorization is enabled
	bool isAMAuthorizationEnabled() const;

	// get the AM client connection timeout value in milliseconds
	unsigned long getAMClientConnectTimeout() const;

	// get the AM client receive timeout value in milliseconds
	unsigned long getAMClientReceiveTimeout() const;

	// check if the AM client APi should use the secondary AM servers
	bool useSecondaryAMServers() const;
};

}

}

#endif /*CLIENTCONFIG_H_*/
