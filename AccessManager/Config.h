#ifndef ACCESS_MANAGER_CONFIG_H_
#define ACCESS_MANAGER_CONFIG_H_

#include <ldap.h>

#include <list>
#include <map>
#include <set>
#include <string>

#include <ers/ers.h>

namespace daq {

  ERS_DECLARE_ISSUE(
    am,
    Exception,
    ,
  )

  ERS_DECLARE_ISSUE_BASE(
    am,
    CannotOpenRulesFile,
    Exception,
    "cannot open file \"" << file << '\"',
    ,
    ((std::string)file)
  )

  ERS_DECLARE_ISSUE_BASE(
    am,
    RulesFileSyntaxError,
    Exception,
    issue << " started at pos: " << pos << ", line: " << line << ", file: \"" << file << '\"',
    ,
    ((const char *)issue)
    ((unsigned long)pos)
    ((unsigned long)line)
    ((std::string)file)
  )

  ERS_DECLARE_ISSUE_BASE(
    am,
    RulesFileBadValue,
    Exception,
    type << " \'" << value << "\' started at pos: " << pos << ", line: " << line << ", file: \"" << file << '\"',
    ,
    ((const char *)type)
    ((std::string)value)
    ((unsigned long)pos)
    ((unsigned long)line)
    ((std::string)file)
  )

  ERS_DECLARE_ISSUE_BASE(
    am,
    LDAP_Error,
    Exception,
    function << ": " << reason,
    ,
    ((const char *)function)
    ((const char *)reason)
  )

  ERS_DECLARE_ISSUE_BASE(
    am,
    Bad_LDAP_AttributeData,
    Exception,
    "attribute " << attribute << " of " << item_type << ' ' << item_name << ": " << reason,
    ,
    ((const char *)attribute)
    ((const char *)item_type)
    ((const char *)item_name)
    ((const char *)reason)
  )

  namespace am {

    class Config;
    class Role;


    typedef std::list< std::map<std::string, std::string> * > RulesList;


    class Rule {

      friend class daq::am::Config;

      private:

        Rule(const std::string& name) : p_name(name) { ; }


      public:

          /** Get name of rule */

        const std::string& get_name() const { return p_name; }


          /** Get name of rule */

        const RulesList& get_rules() const { return p_data; }


      private:

        std::string p_name;
        RulesList p_data;

    };


    typedef std::map<std::string, Rule *> RulesMap;

    namespace ldap {

        // forward declaration of classes declared below Config class

      class User;
      class Sudo;
      class Role;

      typedef std::map<std::string, Role *> RolesMap;
      typedef std::map<std::string, User *> UsersMap;
      typedef std::map<std::string, Sudo *> SudosMap;

      typedef std::list<Role *> RolesList;
      typedef std::list<User *> UsersList;
      typedef std::list<Sudo *> SudosList;



      class Role {

        friend class daq::am::Config;
        friend class daq::am::Role;

        private:

          Role(const std::string& name) : p_name(name), p_assignable(false), p_sync_with_groups(false), p_role(0) { ; }


        public:

            /** Get name of role */

          const std::string& get_name() const { return p_name; }


            /** Get description of role */

          const std::string& get_description() const { return p_description; }


            /** Return true, if role is assignable */

          bool is_assignable() const { return p_assignable; }

          const RolesList& get_member_roles() const { return p_member_roles; }

          const UsersMap& get_users() const { return p_users; }

          const SudosList& get_sudo_commands() const { return p_sudo_commands; }

          const RulesMap& get_rules() const { return p_rules; }

            /**
             *  \brief Prints out details of role.
             */

          void print(
	    std::ostream& s,                 /*!< the output stream */
            const std::string& prefix = ""   /*!< optional shift output using prefix */
	  ) const noexcept;


        private:

          std::string p_name;
	  std::string p_description;
	  bool p_assignable;
	  bool p_sync_with_groups;
	  RolesList p_member_roles;
	  UsersMap p_users;
	  SudosList p_sudo_commands;
	  RulesMap p_rules;
	  mutable daq::am::Role * p_role;

      };


      class User {

        friend class daq::am::Config;

        private:

          User(const std::string& name) : p_name(name) { ; }


        public:

            /** Get name of user */

          const std::string& get_name() const { return p_name; }


            /** Get real name of user */

          const std::string& get_real_name() const { return p_real_name; }


            /** Get user roles */

          const RolesList& get_ldap_roles() const { return p_ldap_roles; }


            /** Get user sudo commands */

          const SudosList& get_sudo_commands() const { return p_sudo_commands; }


            /**
             *  \brief Prints out details of role.
             */

          void print(
	    std::ostream& s,                 /*!< the output stream */
            const std::string& prefix = ""   /*!< optional shift output using prefix */
	  ) const noexcept;


        private:

          std::string p_name;
          std::string p_real_name;
	  RolesList p_ldap_roles;
	  SudosList p_sudo_commands;
        
      };


      class Sudo {

        friend class daq::am::Config;
        friend class daq::am::Role;

        private:

          Sudo(const std::string& name) : p_name(name) { ; }


        public:

            /** Get name of sudo */

          const std::string& get_name() const { return p_name; }


            /** Get description of sudo */

          const std::string& get_description() const { return p_description; }


            /** Get run as */

          const std::string& get_run_as() const { return p_run_as; }
	  

            /** Get authenticate */

          bool get_authenticate() const { return p_authenticate; }


            /** Get users */

          const UsersMap& get_users() const { return p_users; }


            /** Get LDAP roles */

          const RolesMap& get_ldap_roles() const { return p_ldap_roles; }


            /** Get commands */

          const std::list<std::string>& get_commands() const { return p_commands; }


            /**
             *  \brief Prints out details of role.
             */

          void print(
	    std::ostream& s,                 /*!< the output stream */
            const std::string& prefix = ""   /*!< optional shift output using prefix */
	  ) const noexcept;


        private:

          std::string p_name;
	  std::list<std::string> p_commands;
	  bool p_authenticate;
	  std::string p_run_as;
	  std::string p_description;
	  UsersMap p_users;
	  RolesMap p_ldap_roles;
	  std::set<std::string> p_hosts;
        
      };

    }  // namespace ldap


    typedef std::map<std::string, Role *> RolesMap;
    typedef std::set<std::string> HostsSet;

    class Role {

      friend class Config;

      private:

        Role(const std::string& name, ldap::Role& ra, ldap::Role& re) : p_name(name), p_assigned_role(ra), p_enabled_role(re) { ; }


      public:

          /** Get name of role */

        const std::string& get_name() const noexcept { return p_name; }


          /** Get assigned role */

        const ldap::Role& get_assigned_role() const noexcept { return p_assigned_role; }


          /** Get enabled role */

        const ldap::Role& get_enabled_role() const noexcept { return p_enabled_role; }


          /** Get direct base roles */

        const RolesMap& get_direct_base_roles() const noexcept { return p_direct_base_roles; }


          /** Get all base roles */

        const RolesMap& get_all_base_roles() const noexcept { return p_all_base_roles; }


          /** Get sudos */

        const ldap::SudosMap& get_sudos() const noexcept { return p_sudo_commands; }
	

          /** Get login hosts */

	const HostsSet& get_login_hosts() const noexcept { return p_login_hosts; }


          /** Get rules */

	const RulesMap& get_rules() const noexcept { return p_rules; }


          /**
           *  \brief Prints out details of role.
           */

        void print(
          std::ostream& s,                 /*!< the output stream */
          const std::string& prefix = ""   /*!< optional shift output using prefix */
        ) const noexcept;


      private:

        std::string p_name;
        ldap::Role& p_assigned_role;
        ldap::Role& p_enabled_role;
	RolesMap p_direct_base_roles;
	RolesMap p_all_base_roles;
	HostsSet p_login_hosts;
	ldap::SudosMap p_sudo_commands;
	RulesMap p_rules;

    };


    class Config {

      public:

          // uri - Uniform Resource Identifier

        Config(const std::string& ldap_uri, const std::string& am_rules_file, bool read_sudo_config = true);

	~Config();

        void reset();

        const ldap::Role * get_role(const std::string& name) const;

        static std::string make_ldap_uri(const std::string& host, int port = 0);

        const ldap::RolesMap& get_ldap_roles() const noexcept { return p_ldap_roles; }

        const ldap::UsersMap& get_users() const noexcept { return p_users; }

        const ldap::SudosMap& get_sudos() const noexcept { return p_sudo_commands; }

        const RolesMap& get_roles() const noexcept { return p_roles; }
	
	const RulesMap& get_rules() const noexcept { return p_rules; }

          /**
           *  \brief Prints out details of configuration.
           */

        void print(std::ostream&) const noexcept;


      private:

        void clean_config();

	void read_ldap();

	void read_am_rules();
	
	void process_inheritance();

        void fill_all_base_roles(Role * role);


      private:

          // get existing object (role, rule, user, sudo), or create new

        ldap::Role * role(const std::string& name);
        ldap::User * user(const std::string& name);
        ldap::Sudo * sudo(const std::string& name);
        Rule * rule(const std::string& name);

        LDAP * p_ld;

        std::string p_ldap_uri;
	std::string p_rules_file;
        bool p_read_sudo_config;

        ldap::RolesMap p_ldap_roles;
        ldap::UsersMap p_users;
        ldap::SudosMap p_sudo_commands;
	RulesMap p_rules;

	RolesMap p_roles;

    };

  }

}

std::ostream& operator<<(std::ostream& s, const daq::am::Config & c);
std::ostream& operator<<(std::ostream& s, const daq::am::Role & c);

std::ostream& operator<<(std::ostream& s, const daq::am::ldap::Role & c);
std::ostream& operator<<(std::ostream& s, const daq::am::ldap::User & c);
std::ostream& operator<<(std::ostream& s, const daq::am::ldap::Sudo & c);


#endif
