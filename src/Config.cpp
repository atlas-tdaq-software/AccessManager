#include <errno.h>
#include <ldap.h>
#include <string.h>

#include <fstream>
#include <sstream>

#include <system/User.h>

#include <AccessManager/Config.h>

daq::am::ldap::Role *
daq::am::Config::role(const std::string& name)
{
  daq::am::ldap::Role *& r = p_ldap_roles[name];

  if (r == nullptr)
    r = new daq::am::ldap::Role(name);

  return r;
}


daq::am::ldap::User *
daq::am::Config::user(const std::string& name)
{
  daq::am::ldap::User *& u = p_users[name];

  if (u == nullptr)
    u = new daq::am::ldap::User(name);

  return u;
}


daq::am::ldap::Sudo *
daq::am::Config::sudo(const std::string& name)
{
  daq::am::ldap::Sudo *& s = p_sudo_commands[name];

  if (s == nullptr)
    s = new daq::am::ldap::Sudo(name);

  return s;
}


daq::am::Rule *
daq::am::Config::rule(const std::string& name)
{
  daq::am::Rule *& r = p_rules[name];

  if (r == nullptr)
    r = new daq::am::Rule(name);

  return r;
}



//////////

std::string
daq::am::Config::make_ldap_uri(const std::string& host, int port)
{
  std::ostringstream buf;
  if (!port)
    port = LDAP_PORT;
  buf << "ldap://" << host << ':' << port;
  return buf.str();
}


void
daq::am::Config::clean_config()
{
  for (const auto& i : p_ldap_roles)
    delete i.second;

  for (const auto& i : p_users)
    delete i.second;

  for (const auto& i : p_sudo_commands)
    delete i.second;
}


  // This class is used to call function cleaning LDAP connection

class LDAP_UnbindGuard
{
public:

  LDAP_UnbindGuard(LDAP * ld) :
      p_ld(ld)
  {
    ;
  }

  ~LDAP_UnbindGuard()
  {
    ldap_unbind_ext(p_ld, nullptr, nullptr);
  }

private:

  LDAP * p_ld;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class LDAP_Search
{

public:

  LDAP_Search(LDAP *ld, const char *base, int scope, const char *filter, char *attrs[], int attrsonly);

  ~LDAP_Search()
  {
    ldap_msgfree(p_res);
  }

  LDAPMessage *
  first()
  {
    return (p_msg = ldap_first_entry(p_ld, p_res));
  }

  LDAPMessage *
  next()
  {
    if (p_msg)
      {
        p_msg = ldap_next_entry(p_ld, p_msg);
      }

    return p_msg;
  }

private:

  LDAP * p_ld;
  LDAPMessage * p_res;
  LDAPMessage * p_msg;

};

LDAP_Search::LDAP_Search(LDAP *ld, const char *base, int scope, const char *filter, char *attrs[], int attrsonly) :
    p_ld(ld)
{
  int rc = ldap_search_ext_s(p_ld, base, scope, filter, attrs, attrsonly, NULL, NULL, NULL, LDAP_NO_LIMIT, &p_res);

  if (rc != LDAP_SUCCESS)
    {
      throw daq::am::LDAP_Error( ERS_HERE, "ldap_search_ext_s", ldap_err2string(rc));
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class LDAP_Result
{
public:

  LDAP_Result(LDAP * ld, LDAPMessage * msg);

  ~LDAP_Result();

  const std::string&
  name() const
  {
    return p_name;
  }

  const BerValue&
  get_bv() const
  {
    return bv;
  }

  BerValue*
  get_bvals() const
  {
    return bvals;
  }

  bool
  next();

private:

  LDAP * p_ld;
  LDAPMessage * p_msg;

  std::string p_name;

  BerValue bv, *bvals, **bvp;
  BerElement *ber;

};

LDAP_Result::LDAP_Result(LDAP * ld, LDAPMessage * msg) :
    p_ld(ld), p_msg(msg)
{
  if (ldap_msgtype(p_msg) == LDAP_RES_SEARCH_ENTRY)
    {
      bvp = &bvals;
      ldap_get_dn_ber(p_ld, p_msg, &ber, &bv);

      if (bv.bv_len > 3)
        {
          char * start_ptr = bv.bv_val + 3;
          char * end_ptr = strchrnul(start_ptr, ',');

          p_name.assign(start_ptr, end_ptr - start_ptr);

          return;
        }
    }

  ber = 0;  // no data
}

LDAP_Result::~LDAP_Result()
{
  if (ber)
    {
      ber_free(ber, 0);
    }
}

bool
LDAP_Result::next()
{
  return ((ldap_get_attribute_ber(p_ld, p_msg, ber, &bv, bvp) == LDAP_SUCCESS) && (bv.bv_val != nullptr));
}

daq::am::Config::Config(const std::string& ldap_uri, const std::string& rules_file, bool read_sudo_config) :
    p_ldap_uri(ldap_uri), p_rules_file(rules_file), p_read_sudo_config(read_sudo_config)
{
  ERS_DEBUG(1, "LDAP URI: \"" << p_ldap_uri << "\",\nRules File: \"" << p_rules_file << '\"');

  reset();
}

daq::am::Config::~Config()
{
  ERS_DEBUG( 1 , "Destroy daq::am::Config" );

  clean_config();
}

static BerValue&
get_one_value(BerValue * bvals, const char * type, const char * name, const char * attribute)
{
  if (bvals[0].bv_val == nullptr)
    {
      throw daq::am::Bad_LDAP_AttributeData( ERS_HERE, attribute, type, name, "empty array");
    }
  else if(bvals[1].bv_val != nullptr)
    {
      throw daq::am::Bad_LDAP_AttributeData( ERS_HERE, attribute, type, name, "array contains more than 1 value");
    }

  return bvals[0];
}

static std::string
get_any_value(BerValue * bvals, const char * type, const char * name, const char * attribute)
{
  if (bvals[0].bv_val == nullptr)
    {
      throw daq::am::Bad_LDAP_AttributeData( ERS_HERE, attribute, type, name, "empty array");
    }

  std::string out;

  for (int i=0; bvals[i].bv_val != nullptr; ++i)
    {
      if(i != 0)
        {
          out.append(1, '\n');
        }
      out.append(bvals[i].bv_val, bvals[i].bv_len);
    }

  return out;
}


void
daq::am::Config::read_ldap()
{
  // initialise LDAP

  ldap_initialize(&p_ld, p_ldap_uri.c_str());

  if (p_ld == 0)
    {
      char buf[1024];
      throw daq::am::LDAP_Error( ERS_HERE, "ldap_initialize", strerror_r(errno, buf, sizeof(buf)));
    }

  LDAP_UnbindGuard _ld_unbind_guard_(p_ld);  // call ldap_unbind_ext( p_ld, NULL, NULL ) on any return from this method

  int version = LDAP_VERSION3;

  if ((ldap_set_option(p_ld, LDAP_OPT_PROTOCOL_VERSION, &version) != LDAP_SUCCESS) || (ldap_set_option(p_ld, LDAP_OPT_REFERRALS, LDAP_OPT_ON) != LDAP_SUCCESS))
    {
      int err(0);
      ldap_get_option(p_ld, LDAP_OPT_RESULT_CODE, (void *) &err);
      throw daq::am::LDAP_Error( ERS_HERE, "ldap_set_option", ldap_err2string(err));
    }

    // bind to the server anonymously

    {
      struct berval passwd =
        { 0, nullptr };
      int rc = ldap_sasl_bind_s(p_ld, nullptr, LDAP_SASL_SIMPLE, &passwd, nullptr, nullptr, nullptr);

      if (rc != LDAP_SUCCESS)
        {
          throw daq::am::LDAP_Error( ERS_HERE, "ldap_sasl_bind_s", ldap_err2string(rc));
        }
    }


    // read description of roles

    {
      LDAP_Search roles(p_ld, "ou=netgroup,ou=atlas,o=cern,c=ch", LDAP_SCOPE_SUBTREE, "(objectClass=amRole)", nullptr, 0);

      for (LDAPMessage * msg = roles.first(); msg != 0; msg = roles.next())
        {
          LDAP_Result res(p_ld, msg);

          if (!res.name().empty())
            {
              daq::am::ldap::Role * r = role(res.name());

              while (res.next())
                {
                  const BerValue& bv(res.get_bv());
                  BerValue * bvals(res.get_bvals());

                  if (!strcmp(bv.bv_val, "cn"))
                    {
                      BerValue& v = get_one_value(bvals, "role", res.name().c_str(), bv.bv_val);
                      if (strcmp(v.bv_val, res.name().c_str()))
                        {
                          throw daq::am::Bad_LDAP_AttributeData( ERS_HERE, "cn", "role", res.name().c_str(), "value is not equal to name");
                        }
                    }
                  else if (!strcmp(bv.bv_val, "amRoleAssignable"))
                    {
                      BerValue& v = get_one_value(bvals, "role", res.name().c_str(), bv.bv_val);
                      r->p_assignable = !strcasecmp(v.bv_val, "true");
                    }
                  else if (!strcmp(bv.bv_val, "description"))
                    {
                      BerValue& v = get_one_value(bvals, "role", res.name().c_str(), bv.bv_val);
                      r->p_description.assign(v.bv_val, v.bv_len);
                    }
                  else if(!strcmp(bv.bv_val, "objectClass"))
                    {
                      ; // ignore unused attribute
                    }
                  else if (!strcmp(bv.bv_val, "nisNetgroupTriple"))
                    {
                      for (int i = 0; bvals[i].bv_val != nullptr; i++)
                        {
                          std::string user_name(bvals[i].bv_val + 2, bvals[i].bv_len - 4); // drop brackets and commas: "(,foo,)" => "foo"
                          daq::am::ldap::User * u = user(user_name);
                          r->p_users[user_name] = u;
                          u->p_ldap_roles.push_back(r);
                        }
                    }
                  else if (!strcmp(bv.bv_val, "memberNisNetgroup"))
                    {
                      for(int i = 0; bvals[i].bv_val != nullptr; i++)
                        {
                          std::string role_name(bvals[i].bv_val, bvals[i].bv_len);
                          r->p_member_roles.push_back(role(role_name));
                        }
                    }
                  else if (!strcmp(bv.bv_val, "amRoleSync2Group"))
                    {
                      BerValue& v = get_one_value(bvals, "role", res.name().c_str(), bv.bv_val);
                      r->p_sync_with_groups = !strcasecmp(v.bv_val, "TRUE");
                    }
                  else
                    {
                      std::cerr << "ignore " << res.get_bv().bv_val << " of role " << res.name() << std::endl;
                    }

                  ber_memfree(res.get_bvals());
                }
            }
        }
    }


    // read description of sudos

    {
      LDAP_Search roles(p_ld, "ou=SUDOers,ou=atlas,o=cern,c=ch", LDAP_SCOPE_SUBTREE, "(objectClass=sudoRole)", 0, 0);

      for (LDAPMessage * msg = roles.first(); msg != 0; msg = roles.next())
        {
          LDAP_Result res(p_ld, msg);

          if (!res.name().empty())
            {
              daq::am::ldap::Sudo * s = sudo(res.name());

              while (res.next())
                {
                  const BerValue& bv(res.get_bv());
                  BerValue * bvals(res.get_bvals());

                  if (!strcmp(bv.bv_val, "cn"))
                    {
                      BerValue& v = get_one_value(bvals, "sudo", res.name().c_str(), bv.bv_val);
                      if (strcmp(v.bv_val, res.name().c_str()))
                        {
                          throw daq::am::Bad_LDAP_AttributeData( ERS_HERE, "cn", "sudo", res.name().c_str(), "value is not equal to name");
                        }
                    }
                  else if (!strcmp(bv.bv_val, "sudoOption"))
                    {
                      try
                        {
                          BerValue& v = get_one_value(bvals, "sudo", res.name().c_str(), bv.bv_val);
                          s->p_authenticate = ( ( strcmp(v.bv_val, "!authenticate") == 0 ) ? false : true);
                        }
                      catch(daq::am::Bad_LDAP_AttributeData & ex)
                        {
                          std::cerr << "ignore: " << ex << std::endl;
                        }
                    }
                  else if (!strcmp(bv.bv_val, "description"))
                    {
                      s->p_description = get_any_value(bvals, "sudo", res.name().c_str(), bv.bv_val);
                    }
                  else if (!strcmp(bv.bv_val, "objectClass"))
                    {
                      ; // ignore unused attribute
                    }
                  else if (!strcmp(bv.bv_val, "sudoRunAs"))
                    {
                      BerValue& v = get_one_value(bvals, "sudo", res.name().c_str(), bv.bv_val);
                      s->p_run_as.assign(v.bv_val, v.bv_len);
                    }
                  else if (!strcmp(bv.bv_val, "sudoCommand"))
                    {
                      for(int i = 0; bvals[i].bv_val != nullptr; i++)
                        {
                          s->p_commands.push_back(std::string(bvals[i].bv_val, bvals[i].bv_len));
                        }
                    }
                  else if (!strcmp(bv.bv_val, "sudoUser"))
                    {
                      for (int i = 0; bvals[i].bv_val != nullptr; i++)
                        {
                          std::string name(bvals[i].bv_val, bvals[i].bv_len);
                          if (bvals[i].bv_len > 0)
                            {
                              if (bvals[i].bv_val[0] == '+')
                                {
                                  std::string role_name(bvals[i].bv_val + 1, bvals[i].bv_len - 1);
                                  ldap::RolesMap::iterator rx = p_ldap_roles.find(role_name);
                                  if (rx != p_ldap_roles.end())
                                    {
                                      s->p_ldap_roles[role_name] = rx->second;
                                      rx->second->p_sudo_commands.push_back(s);
                                    }
                                  else
                                    {
                                      std::cerr << "warning: cannot find role " << role_name << " for sudo " << res.name() << std::endl;
                                    }
                                }
                              else if (bvals[i].bv_val[0] == '%')
                                {
                                  std::cerr << "warning: ignore group " << bvals[i].bv_val << " for sudo " << res.name() << std::endl;
                                }
                              else
                                {
                                  std::string user_name(bvals[i].bv_val, bvals[i].bv_len);
                                  daq::am::ldap::User * u = user(user_name);
                                  s->p_users[user_name] = u;
                                  u->p_sudo_commands.push_back(s);
                                }
                            }
                        }
                    }
                  else if (!strcmp(bv.bv_val, "sudoHost"))
                    {
                      for (int i = 0; bvals[i].bv_val != nullptr; i++)
                        {
                          s->p_hosts.insert(std::string(bvals[i].bv_val, bvals[i].bv_len));
                        }
                    }
                  else
                    {
                      std::cerr << "ignore " << res.get_bv().bv_val << " of role " << res.name() << std::endl;
                    }

                  ber_memfree(res.get_bvals());
                }
            }
        }
    }

  // link LDAP roles

  for (auto& i : p_ldap_roles)
    {
      daq::am::ldap::Role * r = i.second;

      if (i.first.find("RA-") == 0)
        {
          std::string s(i.first, 3);

          std::string s2("RE-");
          s2 += s;

          ldap::RolesMap::iterator i2 = p_ldap_roles.find(s2);
          if (i2 == p_ldap_roles.end())
            {
              std::cerr << "ERROR: cannot find RE for " << s << std::endl;
              continue;
            }

          Role * role = new Role(s, *r, *i2->second);

          p_roles[role->get_name()] = role;

          role->p_assigned_role.p_role = role;
          role->p_enabled_role.p_role = role;

          // check "assignable" attribute

          if (role->p_assigned_role.p_assignable != role->p_enabled_role.p_assignable)
            {
              std::cerr << "ERROR: is assignable differs for " << role->p_assigned_role.get_name() << " and " << role->p_enabled_role.get_name() << std::endl;
            }
        }
      else if (i.first.find("RE-") != 0)
        {
          std::cerr << "ERROR: unexpected role " << i.first << std::endl;
        }
    }

  for (const auto& i : p_ldap_roles)
    {
      if (i.second->p_role == nullptr)
        {
          std::cerr << "ERROR: role " << i.first << " has no pair" << std::endl;
        }
    }


  // check consistency of inheritance hierarchy

  for (const auto& i : p_roles)
    {
      Role * role = i.second;

      daq::am::ldap::RolesList::const_iterator i1 = role->p_assigned_role.p_member_roles.begin();
      daq::am::ldap::RolesList::const_iterator i2 = role->p_enabled_role.p_member_roles.begin();

      while (true)
        {
          i1++;
          i2++;
          if ((i1 == role->p_assigned_role.p_member_roles.end() && i2 != role->p_enabled_role.p_member_roles.end()) ||
              (i1 != role->p_assigned_role.p_member_roles.end() && i2 == role->p_enabled_role.p_member_roles.end()))
            {
              if (i2 != role->p_enabled_role.p_member_roles.end())
                {
                  std::cerr << "ERROR: problem with inheritance hierarchy for " << role->p_assigned_role.get_name() << " and " << role->p_enabled_role.get_name() << ": number of member roles is different" << std::endl;
                }
              break;
            }

          if (i1 == role->p_assigned_role.p_member_roles.end())
            break; // normal exit

          if ((*i1)->p_role != (*i2)->p_role)
            {
              std::cerr << "ERROR: problem with inheritance hierarchy for " << role->p_assigned_role.get_name() << " and " << role->p_enabled_role.get_name() << ": order of member roles is different" << std::endl;
              break;
            }
        }
    }


  // fill direct inheritance

  for (const auto& i : p_roles)
    {
      Role * role = i.second;
      for (const auto& i2 : role->p_assigned_role.p_member_roles)
        {
          i2->p_role->p_direct_base_roles[role->p_name] = role;
        }
    }


  // fill real names of users

  for (auto& i : p_users)
    {
      System::User user(i.first);
      i.second->p_real_name = user.real_name();
    }
}


void
daq::am::Config::process_inheritance()
{

  // fill complete inheritance

  for (const auto& i : p_roles)
    {
      fill_all_base_roles(i.second);
    }

  // process sudos to determine sudo commands and allowed login hosts
  // process rules

  for (const auto& i : p_roles)
    {
      Role * role = i.second;

      Role * r = role;
      RolesMap::const_iterator j = role->p_all_base_roles.begin();

      while (true)
        {
          if (!r->p_assigned_role.p_sudo_commands.empty())
            {
              std::cerr << "Assigned role " << r->p_assigned_role.get_name() << " has sudo commands\n";
            }

          for (const auto& sudo : r->p_enabled_role.p_sudo_commands)
            {
              if (sudo->p_name.find("LOGIN-") == 0)
                {
                  for (const auto& hi : sudo->p_hosts)
                    {
                      role->p_login_hosts.insert(hi);
                    }
                }
              else
                {
                  role->p_sudo_commands[sudo->p_name] = sudo;
                }
            }

          for (const auto& si : r->p_enabled_role.p_rules)
            {
              role->p_rules[si.first] = si.second;
            }

          if (j != role->p_all_base_roles.end())
            {
              r = j->second;
              ++j;
            }
          else
            {
              break;
            }
        }
    }
}


void
daq::am::Config::read_am_rules()
{
  static const std::string __Rule__("Rule");
  static const std::string __Role__("Role");

  std::ifstream f(p_rules_file.c_str());

  if (!f)
    {
      throw daq::am::CannotOpenRulesFile( ERS_HERE, p_rules_file);
    }

  unsigned int lineno(0);
  std::string s;

  daq::am::Role * am_role = nullptr;
  daq::am::Rule * am_rule = nullptr;
  std::map<std::string, std::string> * rule_tokens = nullptr;

  while (std::getline(f, s))
    {
      lineno++;

      if (s.empty() || s[0] != '[')
        continue;

      am_role = 0;

      am_rule = 0;
      rule_tokens = 0;

      std::string::size_type end_idx(0);
      for (std::string::size_type idx = 0; idx != std::string::npos; idx = s.find_first_of('[', end_idx))
        {
          std::string::size_type idx2 = s.find_first_of('=', idx + 1);
          if (idx2 == std::string::npos)
            throw daq::am::RulesFileSyntaxError( ERS_HERE, "error in token", idx, lineno, p_rules_file);

          end_idx = s.find_first_of(']', idx2);
          if (end_idx == std::string::npos)
            throw daq::am::RulesFileSyntaxError( ERS_HERE, "error in token", idx, lineno, p_rules_file);

          std::string key = s.substr(idx + 1, idx2 - idx - 1);
          std::string value = s.substr(idx2 + 1, end_idx - idx2 - 1);

          if (key.empty() || value.empty())
            throw daq::am::RulesFileSyntaxError( ERS_HERE, "empty token", idx, lineno, p_rules_file);

          if (!am_role && !am_rule)
            {
              if (key == __Rule__)
                {
                  am_rule = rule(value);
                  rule_tokens = new std::map<std::string, std::string>();
                  am_rule->p_data.push_back(rule_tokens);
                }
              else if (key == __Role__)
                {
                  RolesMap::iterator itr = p_roles.find(value);
                  if (itr == p_roles.end())
                    throw daq::am::RulesFileBadValue( ERS_HERE, "unknown role", value, idx, lineno, p_rules_file);

                  am_role = itr->second;
                }
              else
                {
                  throw daq::am::RulesFileBadValue( ERS_HERE, "unexpected token", key, idx, lineno, p_rules_file);
                }

              continue;
            }

          if (am_rule)
            {
              std::string& r = (*rule_tokens)[key];

              if (r.empty())
                {
                  r = value;
                }
              else
                {
                  throw daq::am::RulesFileBadValue( ERS_HERE, "key is defined twice", key, idx, lineno, p_rules_file);
                }
            }
          else
            {
              if (key == "IncludeRule")
                {
                  RulesMap::iterator itr = p_rules.find(value);
                  if (itr == p_rules.end())
                    throw daq::am::RulesFileBadValue( ERS_HERE, "unknown rule", value, idx, lineno, p_rules_file);

                  am_role->p_enabled_role.p_rules[itr->first] = itr->second;
                }
              else
                {
                  throw daq::am::RulesFileBadValue( ERS_HERE, "unexpected token", key, idx, lineno, p_rules_file);
                }
            }
        }
    }
}


void
daq::am::Config::reset()
{
  clean_config();

  read_ldap();

  read_am_rules();

  process_inheritance();
}

void
daq::am::Config::fill_all_base_roles(Role * role)
{
  // return if there are no direct base roles or all base roles were already inited

  if (role->p_direct_base_roles.empty() || !role->p_all_base_roles.empty())
    return;

  for (const auto& i : role->p_direct_base_roles)
    {
      role->p_all_base_roles[i.second->p_name] = i.second;
      fill_all_base_roles(i.second);
      for (const auto& i2 : i.second->p_all_base_roles)
        {
          role->p_all_base_roles[i2.second->p_name] = i2.second;
        }
    }
}

#define PRINT_CONT(C, A, N)                                            \
{                                                                      \
  size_t num(C.size());                                                \
                                                                       \
  if (num == 0)                                                        \
    {                                                                  \
      s << prefix << "  there are no " << N << "s\n";                  \
    }                                                                  \
  else                                                                 \
    {                                                                  \
      if(num == 1)                                                     \
        {                                                              \
          s << prefix << "  there is 1 " << N << ":\n";                \
        }                                                              \
      else                                                             \
        {                                                              \
          s << prefix << "  there are " << num << ' ' << N << "s:\n";  \
        }                                                              \
                                                                       \
      for(const auto& i : C)                                           \
        {                                                              \
          s << prefix << "                     - " << A << std::endl;  \
        }                                                              \
    }                                                                  \
}

void
daq::am::Role::print(std::ostream& s, const std::string& prefix) const noexcept
{
  s.setf(std::ios::boolalpha);
  s << prefix << "Role \'" << p_name << "\':\n"
    << prefix << "  description\n"
    << prefix << "    role assigned:   " << p_assigned_role.p_description << std::endl
    << prefix << "    role enabled:    " << p_enabled_role.p_description << std::endl
    << prefix << "  is assignable:     " << std::boolalpha << p_assigned_role.p_assignable << std::endl
    << prefix << "  sync posix groups: " << std::boolalpha << p_assigned_role.p_sync_with_groups << " (role assigned), " << p_enabled_role.p_sync_with_groups << " (role enabled)" << std::endl;

  PRINT_CONT(p_assigned_role.p_member_roles, i->get_name(), "assigned derived role")
  PRINT_CONT(p_enabled_role.p_member_roles, i->get_name(), "enabled derived role")

  const size_t num_of_all_roles(p_all_base_roles.size());

  if (num_of_all_roles == 0)
    {
      s << prefix << "  there are no base roles\n";
    }
  else
    {
      if (num_of_all_roles == 1)
        {
          s << prefix << "  there is 1 base role";
        }
      else
        {
          s << prefix << "  there are " << num_of_all_roles << " base roles";
        }

      s << " (" << p_direct_base_roles.size() << " direct):\n";

      for (const auto& i : p_all_base_roles)
        {
          bool is_direct(p_direct_base_roles.find(i.first) != p_direct_base_roles.end());
          s << prefix << "                     - " << i.first << (is_direct ? " (*)" : "") << std::endl;
        }
    }


  const size_t num_of_assigned(p_assigned_role.p_users.size());

  if (num_of_assigned == 0)
    {
      s << prefix << "  there are no users\n";
    }
  else
    {
      if (num_of_assigned == 1)
        {
          s << prefix << "  there is 1 user";
        }
      else
        {
          s << prefix << "  there are " << num_of_assigned << " users";
        }

      s << " (" << p_enabled_role.p_users.size() << " enabled):\n";

      for (const auto& i : p_assigned_role.p_users)
        {
          bool is_enabled(p_enabled_role.p_users.find(i.first) != p_enabled_role.p_users.end());
          s << prefix << "                     - " << i.first << (is_enabled ? " (*)" : "") << std::endl;
        }
    }

  PRINT_CONT(p_login_hosts, i, "login host")

  const size_t num_of_sudos(p_sudo_commands.size());

  if (num_of_sudos == 0)
    {
      s << prefix << "  there are no sudos\n";
    }
  else
    {
      if (num_of_sudos == 1)
        {
          s << prefix << "  there is 1 sudo";
        }
      else
        {
          s << prefix << "  there are " << num_of_sudos << " sudos:\n";
        }

      for (daq::am::ldap::SudosMap::const_iterator i = p_sudo_commands.begin(); i != p_sudo_commands.end(); ++i)
        {
          daq::am::ldap::Sudo * sd = i->second;
          s << prefix << "                     - (" << sd->p_run_as << ") " << (sd->p_authenticate ? "" : "NO") << "PASSWD: " << sd->p_description << std::endl;
          for (const auto& j : sd->p_commands)
            {
              s << prefix << "                       * " << j << std::endl;
            }
        }
    }
}

void
daq::am::ldap::Role::print(std::ostream& s, const std::string& prefix) const noexcept
{
  s << prefix << "Role \'" << p_name << "\':\n" << prefix << "  description:       " << p_description << std::endl << prefix << "  is assignable:     " << p_assignable << std::endl;

  if (p_sync_with_groups)
    {
      s << prefix << "  sync posix groups: " << std::boolalpha << p_sync_with_groups << std::endl;
    }

  PRINT_CONT(p_member_roles, i->get_name(), "derived role")

  PRINT_CONT(p_users, i.first, "user")

  PRINT_CONT(p_sudo_commands, i->get_name(), "sudo command")
}

void
daq::am::ldap::User::print(std::ostream& s, const std::string& prefix) const noexcept
{
  s << prefix << "User \'" << p_name << "\' (" << p_real_name << "):\n";

  PRINT_CONT(p_ldap_roles, i->get_name(), "role")

  PRINT_CONT(p_sudo_commands, i->get_name(), "sudo command")
}

void
daq::am::ldap::Sudo::print(std::ostream& s, const std::string& prefix) const noexcept
{
  s << prefix << "Sudo \'" << p_name << "\':\n"
    << prefix << "  description:       " << p_description << std::endl;
  
  PRINT_CONT(p_commands, i, "command")

  s << prefix << "  authenticate:      " << std::boolalpha << p_authenticate << std::endl
    << prefix << "  run as:            " << p_run_as << std::endl;

  PRINT_CONT(p_users, i.second->get_name(), "user")

  PRINT_CONT(p_ldap_roles, i.second->get_name(), "role")

  PRINT_CONT(p_hosts, i, "host")
}

std::ostream&
operator<<(std::ostream& s, const daq::am::ldap::Role & c)
{
  c.print(s);
  return s;
}

std::ostream&
operator<<(std::ostream& s, const daq::am::ldap::User & u)
{
  u.print(s);
  return s;
}

std::ostream&
operator<<(std::ostream& s, const daq::am::ldap::Sudo & d)
{
  d.print(s);
  return s;
}

std::ostream&
operator<<(std::ostream& s, const daq::am::Role & c)
{
  c.print(s);
  return s;
}
