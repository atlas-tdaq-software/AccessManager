#include <fstream>
#include <iostream>
#include <filesystem>
#include <cstdlib>
#include <vector>
#include <string>
#include <mutex>
#include <cstring>

#include <ers/ers.h>

#include <AccessManager/client/ClientConfig.h>

// the default configuration file for Point-1 and tbed
std::vector<std::string> AM_CLIENT_CONFIGURATION_FILES =
  { "/sw/tdaq/AccessManager/cfg/client.cfg", "/dsk1/AccessManager/cfg/client.cfg" };

// try value of this process environment variable, if above configuration file does not exist
const char *AM_CLIENT_CONFIGURATION_FILE_ENV("TDAQ_AM_CONFIGURATION_FILE");

// the environment variable that stores the AM server host
const std::string daq::am::ClientConfig::PARAMETER_AM_SERVER_HOST("TDAQ_AM_SERVER_HOST");

// the environment variable that stores the AM server port
const std::string daq::am::ClientConfig::PARAMETER_AM_SERVER_PORT("TDAQ_AM_SERVER_PORT");

// the environment variable that stores the flag enabled/disabled of AM authorization
const std::string daq::am::ClientConfig::PARAMETER_AM_AUTHORIZATION("TDAQ_AM_AUTHORIZATION");
const std::string daq::am::ClientConfig::VALUE_AM_AUTHORIZATION_ENABLED("on");
const std::string daq::am::ClientConfig::VALUE_AM_AUTHORIZATION_DISABLED("off");
const std::string daq::am::ClientConfig::VALUE_AM_AUTHORIZATION_DEFAULT(ClientConfig::VALUE_AM_AUTHORIZATION_DISABLED);

// the timeout variables in milliseconds
const std::string daq::am::ClientConfig::PARAMETER_AM_CLIENT_CONNECT_TIMEOUT("TDAQ_AM_CLIENT_CONNECT_TIMEOUT");
const std::string daq::am::ClientConfig::PARAMETER_AM_CLIENT_RECV_TIMEOUT("TDAQ_AM_CLIENT_RECV_TIMEOUT");

const std::string daq::am::ClientConfig::PARAMETER_AM_CLIENT_USE_SECONDARY_SERVERS("TDAQ_AM_CLIENT_USE_SECONDARY_SERVERS");
const std::string daq::am::ClientConfig::VALUE_AM_CLIENT_DONT_USE_SECONDARY_SERVERS("false");
const std::string daq::am::ClientConfig::VALUE_AM_CLIENT_USE_SECONDARY_SERVERS("true");
const std::string daq::am::ClientConfig::VALUE_AM_CLIENT_USE_SECONDARY_SERVERS_DEFAULT(ClientConfig::VALUE_AM_CLIENT_DONT_USE_SECONDARY_SERVERS);

daq::am::ClientConfig::ClientConfig()
{
  parameters[PARAMETER_AM_SERVER_HOST] = "";
  parameters[PARAMETER_AM_SERVER_PORT] = "";
  parameters[PARAMETER_AM_AUTHORIZATION] = "";
  parameters[PARAMETER_AM_CLIENT_CONNECT_TIMEOUT] = "";
  parameters[PARAMETER_AM_CLIENT_RECV_TIMEOUT] = "";
  parameters[PARAMETER_AM_CLIENT_USE_SECONDARY_SERVERS] = "";

  refresh();
}

daq::am::ClientConfig::~ClientConfig()
{
}

namespace daq
{
  ERS_DECLARE_ISSUE(
    am,
    CannotAccessFile,
    "cannot access file " << path << ": " << reason,
    ((std::filesystem::path)path)
    ((std::string)reason)
  )
}


void
daq::am::ClientConfig::refresh()
{
  auto test_file = [] (const std::filesystem::path &name)
    {
      try
        {
          ERS_DEBUG(1, "try to access file " << name);

          if (std::filesystem::exists(name) == false)
            return false;

          return std::filesystem::is_regular_file(name);
        }
      catch (std::filesystem::filesystem_error &ex)
        {
          ers::error(CannotAccessFile(ERS_HERE, name, ex.code().message()));
          return false;
        }
    };

  // first try to detect which config file to use

  std::filesystem::path config_file_name;

  for (auto& f : AM_CLIENT_CONFIGURATION_FILES)
    {
      config_file_name = f;

      if (test_file(config_file_name))
        break;
      else
        config_file_name.clear();
    }

  if (config_file_name.empty())
    {
      if (const char *s = std::getenv(AM_CLIENT_CONFIGURATION_FILE_ENV))
        {
          config_file_name = s;

          if (test_file(config_file_name) == false)
            config_file_name.clear();
        }
    }

  if (!config_file_name.empty())
    {
      // try to read the variables from the configuration file
      std::string line;
      std::ifstream cfgFile;

      cfgFile.exceptions ( std::ostream::failbit | std::ostream::badbit );

      try
        {
          cfgFile.open(config_file_name);

          ERS_DEBUG(1, "Read configuration from file [" << config_file_name << "]");

          while (cfgFile.peek() != EOF && std::getline(cfgFile, line))
            {
              //std::getline(cfgFile, line);

              // Trim Leading Spaces
              std::string::size_type pos = line.find_first_not_of(" \t"); // Find the first character position after excluding leading blank spaces
              if (std::string::npos != pos)
                {
                  line = line.substr(pos);
                }
              // Trim trailing Spaces
              pos = line.find_last_not_of(" \t"); // Find the first character position from reverse after excluding trailing white spaces
              if (std::string::npos != pos)
                {
                  line = line.substr(0, pos + 1);
                }

              // check if the line is a comment
              if (line.find('#', 0) == 0)
                {
                  ERS_DEBUG(3, "Ignore comment from input file: " << line);
                  continue;
                }

              // the parameters in the file should be in this format 'VARIABLE_NAME=VALUE'
              pos = line.find("=");
              if (std::string::npos != pos)
                {
                  // get the key
                  std::string key = line.substr(0, pos);
                  // get the value
                  std::string value = line.substr(pos + 1);

                  //check if already added
                  std::map<std::string, std::string>::iterator pit = parameters.find(key);
                  if (pit != parameters.end())
                    {
                      if (pit->second.size() != 0)
                        {
                          ERS_DEBUG(2, "parameter[" << key << "] old value [" << pit->second << "] new value from file [" << value << "]");
                        }
                    }
                  else
                    {
                      ERS_DEBUG(2, "new parameter[" << key << "] with value from file [" << value << "]");
                    }

                  // set the new value for the key
                  parameters[key] = value;
                }
              else
                {
                  ERS_DEBUG(3, "Following line is not in the format 'VARIABLE_NAME=variable value':" << line);
                }
            }

          cfgFile.close();
        }
      catch(const std::system_error& ex)
        {
          char buffer[1024];
          buffer[0] = 0;
          ers::error(CannotAccessFile(ERS_HERE, config_file_name, strerror_r (errno, buffer, sizeof(buffer))));
        }
    }
  else
    {
      static std::once_flag flag;

      std::call_once(flag, []()
        {
          std::string cfiles;

          for (const auto& f : AM_CLIENT_CONFIGURATION_FILES)
            {
              if (!cfiles.empty())
                cfiles.append(", ");

              cfiles.push_back('\"');
              cfiles.append(f);
              cfiles.push_back('\"');
            }

          ERS_DEBUG(0, "There is no default configuration file [" << cfiles << "] or defined by " << AM_CLIENT_CONFIGURATION_FILE_ENV << " process environment variable");
        }
      );
    }

  // now read the parameters from the environment
  for (const auto &pit : parameters)
    if (!pit.second.empty())
      {
        ERS_DEBUG(3, "Variable " << pit.first << " will not be read from environment because has already the value [" << pit.second << "]");
      }
    else if (const char *envValue = std::getenv(pit.first.c_str()))
      {
        ERS_DEBUG(2, "Environment variable [" << (pit.first) << "] found!" << " Value [" << envValue << "]");
        parameters[(pit.first)] = envValue;
      }
    else
      {
        ERS_DEBUG(2, "Environment variable [" << (pit.first) << "] not found!");
      }

}

const std::string
daq::am::ClientConfig::getConstantValueFromMap(const std::string &key) const
{
  if (auto pit = parameters.find(key); pit != parameters.end())
    {
      ERS_DEBUG(2, "Parameter [" << key << "] found with value [" << pit->second << "]");
      return pit->second;
    }

  ERS_DEBUG(1, "Parameter [" << key << "] not found!");
  return "";
}

// get the AM server host name
const std::string
daq::am::ClientConfig::getAMServerHosts() const
{
  return getConstantValueFromMap(PARAMETER_AM_SERVER_HOST);
}

// METHODS TO ITERATE THROUGH THE AM SERVER HOSTS IF THERE ARE MORE THAN ONE
// get the first AM server host name;
// the others are returned by the getNextAMSErverHost method
const std::string
daq::am::ClientConfig::getFirstAMServerHost()
{
  this->amServerHostsAvailable = getAMServerHosts();
  return getNextAMServerHost();
}

// get the next AM server host name;
// returns an empty string if there are no more AM servers in the list
const std::string
daq::am::ClientConfig::getNextAMServerHost()
{
  std::string currentAMServer = "";
  if (this->amServerHostsAvailable.size() > 0)
    {
      // find the next separator or end of string
      std::string::size_type loc = amServerHostsAvailable.find(',', 0);
      if (loc != std::string::npos)
        {
          currentAMServer = amServerHostsAvailable.substr(0, loc);
          amServerHostsAvailable = amServerHostsAvailable.substr(loc + 1);
        }
      else
        {
          // found only one secondary server
          currentAMServer = amServerHostsAvailable;
          amServerHostsAvailable = "";
        }

      // Trim Both leading and trailing spaces
      std::string::size_type startpos = currentAMServer.find_first_not_of(" \t"); // Find the first character position after excluding leading blank spaces
      std::string::size_type endpos = currentAMServer.find_last_not_of(" \t"); // Find the first character position from reverse af
      // if all spaces or empty return an empty string
      if ((std::string::npos == startpos) || (std::string::npos == endpos))
        {
          currentAMServer = "";
        }
      else
        {
          currentAMServer = currentAMServer.substr(startpos, endpos - startpos + 1);
        }

      ERS_DEBUG(1, "Next AM server from the client configuration is: " << currentAMServer);
    }
  return currentAMServer;
}

// get the AM server port number
unsigned long
daq::am::ClientConfig::getAMServerPort() const
{
  return atol(getConstantValueFromMap(PARAMETER_AM_SERVER_PORT).c_str());
}

// get the AM server host name
bool
daq::am::ClientConfig::isAMAuthorizationEnabled() const
{
  std::string amEnabledValue = getConstantValueFromMap(PARAMETER_AM_AUTHORIZATION);

  if (amEnabledValue.length() > 0)
    {
      // convert it to lower case
      for (std::string::iterator sit = amEnabledValue.begin(); sit != amEnabledValue.end(); sit++)
        {
          *sit = std::tolower(*sit);
        }
    }
  else
    {
      amEnabledValue = VALUE_AM_AUTHORIZATION_DEFAULT;
      ERS_DEBUG(1, "Parameter [" << PARAMETER_AM_AUTHORIZATION << "] defaults to " << "[" << VALUE_AM_AUTHORIZATION_DEFAULT << "]");
    }

  //check its lowercase value
  return (amEnabledValue == VALUE_AM_AUTHORIZATION_ENABLED);
}

unsigned long
daq::am::ClientConfig::getAMClientConnectTimeout() const
{
  return std::atol(getConstantValueFromMap(PARAMETER_AM_CLIENT_CONNECT_TIMEOUT).c_str());
}

unsigned long
daq::am::ClientConfig::getAMClientReceiveTimeout() const
{
  return std::atol(getConstantValueFromMap(PARAMETER_AM_CLIENT_RECV_TIMEOUT).c_str());
}

// check if the AM client APi should use the secondary AM servers
bool
daq::am::ClientConfig::useSecondaryAMServers() const
{
  std::string amSecAMSValue = getConstantValueFromMap(PARAMETER_AM_CLIENT_USE_SECONDARY_SERVERS);

  if (!amSecAMSValue.empty())
    {
      // convert it to lower case
      for (auto& c : amSecAMSValue)
        c = std::tolower(c);
    }
  else
    {
      amSecAMSValue = VALUE_AM_CLIENT_USE_SECONDARY_SERVERS_DEFAULT;
      ERS_DEBUG(1, "Parameter [" << PARAMETER_AM_CLIENT_USE_SECONDARY_SERVERS << "] defaults to " << "[" << VALUE_AM_CLIENT_USE_SECONDARY_SERVERS_DEFAULT << "]");
    }

  //check its lowercase value
  return (amSecAMSValue == VALUE_AM_CLIENT_USE_SECONDARY_SERVERS);
}

