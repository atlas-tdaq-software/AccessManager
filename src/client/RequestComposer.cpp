#include <AccessManager/client/RequestComposer.h>

#include <AccessManager/util/ErsIssues.h>
#include <AccessManager/xacml/Environment.h>


namespace daq
{

namespace am
{


void RequestComposer::addSubjects(const std::string& category, const Subjects& sbj){

	// don't attach issueInstant like in the java implementation. Environment section will contain the date and time
	try{
		// build the attributes
		Attribute attrId(	sbj.getSubjectId(), 	sbj);
		Attribute attrDNS(	sbj.getDNSName(), 		sbj);
		Attribute attrIP(	sbj.getIpAddress(), 	sbj);

		// construct a set with those attributes
		std::set<Attribute> attrs;
		attrs.insert(attrId);
		attrs.insert(attrDNS);
		attrs.insert(attrIP);

		// create a subject object with the set of attributes
		Subject subject(category, attrs);

		// insert the subject in the set
		this->subjects.insert(subject);
	} catch (XACMLIssue & ex) {
		std::string message = "Can not initialize subject category [" + category + "]";
		ERS_INFO( message );
		ers::error(ex);
	}
}

void RequestComposer::initEnvironment(){

	try{
		this->environment.clear();
//		Environment env;
//		Attribute attrTime(env.getTime(), env);
//		Attribute attrDate(env.getDate(), env);
//		this->environment.insert(attrTime);
//		this->environment.insert(attrDate);
	} catch (XACMLIssue & ex) {
		ERS_INFO("Can not initialize the environment");
		ers::error(ex);
	}
}


void RequestComposer::addAction(const Resource& resource, const std::string& identifier){
	// check if there is an action for this resource identifier
	try{
		const Action* resAction = resource.getAction(identifier);
		Attribute resActionAttr(resAction->getActionId(), *resAction);

		// add this attribute to the action set
		this->action.insert(resActionAttr);

	}catch(XACMLMissingAction& ex){
		// the error is already printed as DEBUG by the resource.getAction method
		//ERS_DEBUG(3, "Missing action for resource identifier [" << identifier << "]");
	}
}


RequestComposer::RequestComposer(const Subjects& accessSubject, const Subjects& intermediarySubject):
	resourceContent(""), reqCtx(nullptr){

	addSubjects(Subject::CATEGORY_ACCESS_SUBJECT, accessSubject);
	addSubjects(Subject::CATEGORY_INTERMEDIARY_SUBJECT, intermediarySubject);
}

RequestComposer::RequestComposer(const Subjects& accessSubject):
	resourceContent(""), reqCtx(nullptr){

	addSubjects(Subject::CATEGORY_ACCESS_SUBJECT, accessSubject);
}

RequestComposer::~RequestComposer(){

}


// add a resource to request access for. Set also the action for all the resource identifiers
void RequestComposer::addResource(const Resource& res){
	try{

		Attribute resCategory(res.getResourceCategory(), res);
		this->resource.insert(resCategory);
		addAction(res, res.getResourceCategory());

		Attribute resId(res.getResourceId(), res);
		this->resource.insert(resId);
		addAction(res, res.getResourceId());

		// add the other resource types
		const std::vector<std::string>& vtypes = res.getResourceTypes();
		for(std::vector<std::string>::const_iterator vit = vtypes.begin(); vit != vtypes.end(); vit++){

			const std::string& resTypeId = *vit;
			Attribute resType(resTypeId, res);
			this->resource.insert(resType);
			addAction(res, resTypeId);
		}

	} catch (XACMLIssue & ex) {
		ERS_INFO("Can not add the resource:" << std::endl << res << "The error is:");
		ers::error(ex);
	}

}

// set the resource to request access for
void RequestComposer::setResource(const Resource& res){
	this->resource.clear();
	this->action.clear();
	addResource(res);
}

void RequestComposer::buildRequest(){
	initEnvironment();
	reqCtx.reset(new RequestCtx(subjects, resource, action, environment, resourceContent));
}


// returns a string representation of XML formatted request
std::string RequestComposer::encode() const {
	if (reqCtx.get() == NULL) {
		return "NO REQUEST CONTEXT AVAILABLE";
	} else {
		return (reqCtx->encode());
	}
}

// sends the encoded value to the output stream without indentation
std::ostream& operator<<(std::ostream& os, const RequestComposer& reqComp){
	if (reqComp.reqCtx.get() == NULL) {
		os << "NO REQUEST CONTEXT AVAILABLE";
	} else {
		os << (*(reqComp.reqCtx));
	}
	return os;
}

}

}
