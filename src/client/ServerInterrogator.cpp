#include <AccessManager/client/ServerInterrogator.h>

#include <AccessManager/communication/NetworkClientModule.h>
#include <AccessManager/client/RequestComposer.h>
#include <AccessManager/util/ErsIssues.h>

#include <AccessManager/xacml/ctx/Result.h>
#include <AccessManager/xacml/ctx/Status.h>

namespace daq
{

namespace am
{

const std::string ServerInterrogator::MSG_SERVER_BUSY("SERVER_BUSY");

ServerInterrogator::ServerInterrogator(): clientCfg(new ClientConfig()) {
	this->secondaryAMServers = "";
	this->currentAMServer = "";
	this->authEnabled = this->clientCfg->isAMAuthorizationEnabled();
	this->useSecondaryAMServers = this->clientCfg->useSecondaryAMServers();
}

ServerInterrogator::~ServerInterrogator(){
}


// request on behalf of current user
bool ServerInterrogator::isAuthorizationGranted(const Resource& resource){

// Commented out to create a patched version for preseries
/*	ERS_DEBUG(1, "The resource is: " << resource.getDescription());
	// if AM is off and the resource is DB (OKS) the permission is denied
	if (!this->authEnabled and resource.getDescription() == "DB Resource") {
		statusMessage = "AM Authorization is DISABLED AND resource is DB (OKS) so the permission is DENIED!";
		ERS_DEBUG(1, statusMessage);
	return false;
	}
*/

	// the access Subject is the same as current Subject
	RequestComposer rc(currentSubject);
	rc.setResource(resource);
	return isAuthorizationGranted(rc);
}

bool ServerInterrogator::isAuthorizationGranted(const Resource& resource, const RequestorInfo& accessSubject){

// Commented out to create a patched version for preseries
/*	ERS_DEBUG(1, "The resource is: " << resource.getDescription());
	// if AM is off and the resource is DB (OKS) the permission is denied
	if (!this->authEnabled and resource.getDescription() == "DB Resource") {
		statusMessage = "AM Authorization is DISABLED AND resource is DB (OKS) so the permission is DENIED!";
		ERS_DEBUG(1, statusMessage);
	return false;
	}
*/

	// the current subject is the intermediary subject
	RequestComposer rc(accessSubject, currentSubject);
	rc.setResource(resource);
	return isAuthorizationGranted(rc);
}

// helper method to get the next AM server name
std::string ServerInterrogator::getNextAMServer() {
	std::string nextAMServer = "";
	if (useSecondaryAMServers && secondaryAMServers.length() > 0) {
		// set the am server to the first one found in the list of secondary servers
		std::string::size_type loc = secondaryAMServers.find(',', 0);
		if (loc != std::string::npos) {
		   // found more secondary servers, so get the first one
			nextAMServer = secondaryAMServers.substr(0, loc);
			//shift the list to the loc
			secondaryAMServers = secondaryAMServers.substr(loc + 1);
		} else {
		   // found only one secondary server
		   nextAMServer = secondaryAMServers;
		   secondaryAMServers = "";
		}

		// Trim Both leading and trailing spaces
		std::string::size_type startpos = nextAMServer.find_first_not_of(" \t"); // Find the first character position after excluding leading blank spaces
		std::string::size_type endpos = nextAMServer.find_last_not_of(" \t"); // Find the first character position from reverse af
		// if all spaces or empty return an empty string
		if(( std::string::npos == startpos ) || ( std::string::npos == endpos)){
			nextAMServer = "";
		} else {
			nextAMServer = nextAMServer.substr( startpos, endpos-startpos+1 );
		}

		ERS_INFO("Try next AM server from server_busy list [" << nextAMServer << "]");
		amServerHistory += "(SB)";
	} else {
		nextAMServer = clientCfg->getNextAMServerHost();
		if (nextAMServer.length() > 0 ) {
			ERS_INFO("Try next AM server from client configuration [" << nextAMServer << "]");
		}
	}
	return nextAMServer;
}

bool ServerInterrogator::isAuthorizationGranted(RequestComposer& reqComp){
	// respond with "true" when the authorization functionality is disabled
	if ( ! this->authEnabled){
		statusMessage = "Authorization is DISABLED!";
		ERS_DEBUG(1, statusMessage);
		return true;
	}

	amServerHistory = "AMServerHistory=";
	bool decision = false;
	currentAMServer = clientCfg->getFirstAMServerHost();

	if (currentAMServer.length() <= 0) {
		ERS_INFO("No AM server specified!");
	}

	while (currentAMServer.length() > 0) {

		std::unique_ptr<ClientModule> clientCommModule;

		/*
		 * try to connect to the AM server
		 */
		bool amServerHistoryUpdated = false;
		try{

			std::unique_ptr<NetworkClientModule> ncm( new NetworkClientModule(currentAMServer, clientCfg->getAMServerPort()) );
			amServerHistory = amServerHistory + ncm->getServerName() + ",";
			amServerHistoryUpdated = true;

			ncm->initConnection(clientCfg->getAMClientConnectTimeout());

			// transfer the NetworkClientModule object from ncm unique_ptr to clientCommModule unique_ptr
			clientCommModule = std::move(ncm);

		} catch(daq::am::NetworkError& ex){

			if (! amServerHistoryUpdated) {
				amServerHistory = amServerHistory + currentAMServer + "(IP_UNKNOWN),";
			}

			statusMessage = "[" + amServerHistory + "] Failed to initiate the communication with the server!";
			ERS_INFO("Failed to contact the AM server [" << currentAMServer << "]:" << ex.what());
			currentAMServer = getNextAMServer();
			if (currentAMServer.length() > 0) {
				// try again to connect to the server
				continue;
			}

			// if there are no more servers to try, then stop the interrogation and throw the exception
			throw ex;
		}

		// get the communication streams
		CommunicationStreams* cs = clientCommModule->getCommunicationStreams();

		unsigned long receiveTimeout = clientCfg->getAMClientReceiveTimeout();
		if ( receiveTimeout > 0) {
			cs->setRecvTimeout(receiveTimeout);
		}

		// build and send the request message
		std::string strRequest = "";
		try{
			reqComp.buildRequest();
			strRequest = reqComp.encode();
			cs->sendMessage(strRequest);
		} catch (...) {
			statusMessage = "[" + amServerHistory + "] Can not send the message!";
			throw;
		}

		// receive the response message
		std::string strResponse = "";
		try{
			strResponse = cs->receiveMessage();
		} catch (daq::am::CommunicationStreamsTimeout& ex) {

			statusMessage = "[" + amServerHistory + "] Timeout while receiving the answer";
			ERS_INFO("Failed to receive message from the AM server [" << currentAMServer << "]:" << ex.what());
			currentAMServer = getNextAMServer();
			if (currentAMServer.length() > 0) {
				// try again to connect to the server
				continue;
			}
			throw;
		} catch (...) {

			statusMessage = "[" + amServerHistory + "] Can not receive the message";
			ERS_INFO("Failed to receive message from the AM server [" << currentAMServer << "]!");
			currentAMServer = getNextAMServer();
			if (currentAMServer.length() > 0) {
				// try again to connect to the server
				continue;
			}
			throw;
		}

                try {
		        const std::unique_ptr<ResponseCtx> responseCtx( ResponseCtx::getInstance(strResponse) );
		        // the decision identification method may throw a ServerProcessingIssue exception
			decision = getDecisionFromResponse(*responseCtx);
                } catch (XACMLParsingException& ex) {
                        statusMessage += "[request={" + strRequest + "} responce={" + strResponse + "} error: " + ex.what() + "]";
                        throw;
		} catch (...) {
			statusMessage += "[request={" + strRequest + "} responce={" + strResponse + "]";
			throw;
		}

		currentAMServer = "";
		if (useSecondaryAMServers && decision == false && secondaryAMServers.length() > 0) {
			ERS_DEBUG(0, "Received secondary servers: [" << secondaryAMServers << "]" );
			currentAMServer = getNextAMServer();
		}
	}

	return decision;
}

bool ServerInterrogator::getDecisionFromResponse(const ResponseCtx& responseCtx){
	bool decision = true;
	int decisionId = -1;
	statusMessage = "[" + amServerHistory + "]";
	secondaryAMServers = "";

	const std::set<Result>& results = responseCtx.getResults();
	for(std::set<Result>::const_iterator rit = results.begin(); rit != results.end(); rit++){
		decisionId = rit->getDecision();
		ERS_DEBUG(1, "Found decision in result: " << Result::DECISIONS[decisionId]);
		if (decisionId == Result::DECISION_PERMIT){
			decision = decision && true;
		} else{
			decision = decision && false;
		}

		const Status* status = rit->getStatus();
		statusMessage += "[DECISION " + Result::DECISIONS[decisionId];
		if (status){
			const std::vector<std::string>& code = status->getCode();
			// take only the first code
			if (code.size() > 0) {
				if (code[0] == Status::STATUS_OK) {
					ERS_DEBUG(1, "Decision status is OK: " << code[0]);
				} else {
					ERS_DEBUG(1, "Decision status is not OK: " << code[0]);
				}

				if (code[0] == Status::STATUS_MISSING_ATTRIBUTE) {
					statusMessage += " with status [missing attribute:" + status->getMessage() + "]";
					ServerProcessingIssue issue(ERS_HERE, statusMessage);
					throw issue;
				} else if (code[0] == Status::STATUS_PROCESSING_ERROR) {
					statusMessage += " with status [processing error:" + status->getMessage() + "]";
					ServerProcessingIssue issue(ERS_HERE, statusMessage);
					throw issue;
				} else if (code[0] == Status::STATUS_SYNTAX_ERROR) {
					statusMessage += " with status [syntax error:" + status->getMessage() + "]";
					ServerProcessingIssue issue(ERS_HERE, statusMessage);
					throw issue;
				}
			}
		}

		// check if the decision is Indeterminate, status OK and SERVER_BUSY received
		if (decisionId == Result::DECISION_INDETERMINATE) {
			if (status) {
				const std::vector<std::string>& code = status->getCode();
				// take only the first code and make sure is OK
				if (code.size() && code[0] == Status::STATUS_OK) {
					const std::string& _statusMessage = status->getMessage();
					std::string::size_type loc = _statusMessage.find(MSG_SERVER_BUSY, 0);
					if (loc != std::string::npos) {
						ERS_DEBUG(0, "Received decision INDETERMINATE with status OK because:" << _statusMessage);
						statusMessage += " because:" + MSG_SERVER_BUSY;
						// found the SERVER_BUSY message, now get the secondary servers
						if (useSecondaryAMServers) {
							std::string::size_type startSecAMSrv = _statusMessage.find('[', 0);
							std::string::size_type endSecAMSrv = _statusMessage.find(']', 0);
							std::string::size_type pos = endSecAMSrv - startSecAMSrv - 1;
							if (pos > 0) {
								secondaryAMServers = _statusMessage.substr(startSecAMSrv + 1, pos);
								ERS_INFO(currentAMServer << " sent " << MSG_SERVER_BUSY << " with secondary servers [" << secondaryAMServers << "]");
							}
						}
				   } else {
						statusMessage += " because " + status->getMessage();
				   }
				}
			}
		}

		statusMessage += "] ";
	}

	return decision;
}

}

}
