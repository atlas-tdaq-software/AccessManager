#include <unistd.h>
#include <sys/types.h>

#include <system/User.h>
#include <system/Host.h>

#include <ers/ers.h>

#include <AccessManager/client/RequestorInfo.h>

namespace daq
{

  namespace am
  {

    RequestorInfo::RequestorInfo(const std::string& username) :
        Subjects(), userIdentifier(EFFECTIVE_ID), username(username), hostname(""), ipaddress("")
    {

      initValues();

    }

    RequestorInfo::RequestorInfo(const USER_IDENTIFIER& userIdentifier) :
        Subjects(), userIdentifier(userIdentifier), username(""), hostname(""), ipaddress("")
    {

      initValues();

    }

    RequestorInfo::RequestorInfo(const std::string& username, const std::string& hostname) :
        Subjects(), userIdentifier(EFFECTIVE_ID), username(username), hostname(hostname), ipaddress("")
    {

      initValues();

    }

    RequestorInfo::~RequestorInfo()
    {
    }

    void
    RequestorInfo::initValues()
    {

      // initialize the uid an euid
      ruid = getuid();
      rgid = getgid();
      euid = geteuid();
      egid = getegid();
      ERS_DEBUG(2, "Real      UID=" << ruid << " GID=" << rgid);
      ERS_DEBUG(2, "Effective UID=" << euid << " GID=" << egid);

      if (username.empty() == true)
        {
          // get the username from the system
          if (userIdentifier == EFFECTIVE_ID)
            {
              ERS_DEBUG(2, "Initializing the username using the effective uid");
              username.assign(System::User(euid).name_safe());
            }
          else if (userIdentifier == REAL_ID)
            {
              ERS_DEBUG(2, "Initializing the username using the real uid");
              username.assign(System::User(ruid).name_safe());
            }
          else
            {
              ERS_DEBUG(1, "Invalid user identifier:" << userIdentifier);
            }

          if (username.empty() == false)
            {
              ERS_DEBUG(2, "Username identified as [" << username <<"]");
            }
          else
            {
              ERS_DEBUG(1, "Username could not be identified");
            }
        }
      addValue(Subjects::getSubjectId(), username);

      if (hostname.empty() == true)
        {
          hostname.assign(System::LocalHost::instance()->full_name());
          ERS_DEBUG(2, "Hostname identified as [" << hostname << "]");
        }
      addValue(Subjects::getDNSName(), hostname);

      if (ipaddress.empty() == true)
        {
          // get the ip address
          ipaddress.assign(System::LocalHost::instance()->ip_string());
          ERS_DEBUG(2, "IP address identified as [" << ipaddress << "]");
        }
      addValue(Subjects::getIpAddress(), ipaddress);

    }

  }

}
