#include <ctype.h>

#include <algorithm>
#include <fstream>

#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>

#include <boost/program_options.hpp>

#include <AccessManager/Config.h>

static char
cvt_symbol(char c)
{
  if (c == '\n')
    return ' ';
  else if (iscntrl(c))
    return ' ';
  else if (!isascii(c))
    return ' ';
  else if (!isprint(c))
    return ' ';
  return c;
}

static std::string
cvt_string(const std::string& in)
{
  std::string s(in);
  std::transform(s.begin(), s.end(), s.begin(), cvt_symbol);
  return s;
}

int
main(int argc, char **argv)
{
  std::string ldap_uri("");
  std::string am_rules_file("");
  std::string json_file("");

  try
    {
      boost::program_options::options_description desc("Utility to produce JSON file used by Access Manager Roles Viewer.");

      desc.add_options()
        ( "help,h"                                                                                   , "Print help message"                )
        ( "ldap-uri,u"      , boost::program_options::value<std::string>(&ldap_uri)->required()      , "LDAP URI"                          )
        ( "am-rules-file,f" , boost::program_options::value<std::string>(&am_rules_file)->required() , "Name of Access Manager rules file" )
        ( "json-file,o"     , boost::program_options::value<std::string>(&json_file)->required()     , "Name of output JSON file"          )
      ;

      boost::program_options::variables_map vm;
      boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);

      if (vm.count("help"))
        {
          std::cout << desc << std::endl;
          return 0;
        }

      boost::program_options::notify(vm);
    }
  catch (std::exception& ex)
    {
      std::cerr << "Parsing of command line parameters failed: " << ex.what() << std::endl;
      return 1;
    }


  try
    {

      // Create output file

      std::ofstream f(json_file.c_str());
      f.exceptions(std::ostream::failbit | std::ostream::badbit);

      if (!f)
        {
          std::cerr << "Cannot create output file \"" << json_file << '\"' << std::endl;
          return 2;
        }

      // Read AM configuration
      daq::am::Config x(ldap_uri, am_rules_file);

      // Get timestamp
//      boost::posix_time::ptime now = boost::posix_time::second_clock::universal_time();

      // build maps of roles, sudos, users, rules and hosts used by Access Manager
      std::map<std::string, int> roles, sudos, hosts, users, rules;

      daq::am::ldap::SudosMap used_sudos;
      daq::am::RulesMap used_rules;

        {
          std::set<std::string> used_hosts;
          std::set<std::string> assigned_users;

          for (const auto& i : x.get_roles())
            {
              daq::am::Role * r = i.second;

              for (const auto& j : r->get_sudos())
                used_sudos[j.first] = j.second;

              for (const auto& j : r->get_login_hosts())
                used_hosts.insert(j);

              for (const auto& j : r->get_assigned_role().get_users())
                assigned_users.insert(j.first);

              for (const auto& j : r->get_enabled_role().get_rules())
                used_rules[j.first] = j.second;
            }

          int idx = 0;
          for (const auto& i : x.get_roles())
            roles[i.second->get_name()] = idx++;

          idx = 0;
          for (const auto& i : used_rules)
            rules[i.second->get_name()] = idx++;

          idx = 0;
          for (const auto& i : used_sudos)
            sudos[i.second->get_name()] = idx++;

          idx = 0;
          for (const auto& i : used_hosts)
            hosts[i] = idx++;

          idx = 0;
          for (const auto& i : assigned_users)
            users[i] = idx++;
        }

      f << "{\n"
          " \"last-updated\": \"" << boost::posix_time::to_simple_string(boost::posix_time::second_clock::universal_time()) << " (UTC)\",\n"
          " \"roles\": [\n";

      if (!x.get_roles().empty())
        for (auto i = x.get_roles().begin(); true;)
          {
            const daq::am::Role * r = i->second;
            f << "  { \"n\": \"" << r->get_name() << '\"';
            if (r->get_assigned_role().is_assignable() == false)
              f << ", \"a\": false";
            const daq::am::RolesMap& base_roles = r->get_direct_base_roles();
            if (base_roles.size() != 0)
              {
                f << ", \"r\": [";
                for (auto j = base_roles.begin(); j != base_roles.end(); ++j)
                  {
                    if (j != base_roles.begin())
                      f << ", ";
                    f << roles[j->first];
                  }
                f << "]";
              }

            std::list<int> derived_sudos_list, direct_sudos_list;

            const daq::am::ldap::SudosMap& all_sudos = r->get_sudos();
            const daq::am::ldap::SudosList& direct_sudos = r->get_enabled_role().get_sudo_commands();

            for (const auto& j : all_sudos)
              {
                int idx = sudos[j.first];

                bool is_direct_sudo = false;

                for (const auto& x : direct_sudos)
                  {
                    if (x->get_name() == j.first)
                      {
                        is_direct_sudo = true;
                        break;
                      }
                  }

                if (is_direct_sudo == false)
                  derived_sudos_list.push_back(idx);
                else
                  direct_sudos_list.push_back(idx);
              }

            if (derived_sudos_list.size() != 0)
              {
                f << ", \"s\": [";
                for (auto j = derived_sudos_list.begin(); j != derived_sudos_list.end(); ++j)
                  {
                    if (j != derived_sudos_list.begin())
                      f << ", ";
                    f << *j;
                  }
                f << "]";
              }

            if (direct_sudos_list.size() != 0)
              {
                f << ", \"S\": [";
                for (auto j = direct_sudos_list.begin(); j != direct_sudos_list.end(); ++j)
                  {
                    if (j != direct_sudos_list.begin())
                      f << ", ";
                    f << *j;
                  }
                f << "]";
              }

            const daq::am::HostsSet& login_hosts = r->get_login_hosts();
            if (login_hosts.size() != 0)
              {
                f << ", \"h\": [";
                for (auto j = login_hosts.begin(); j != login_hosts.end(); ++j)
                  {
                    if (j != login_hosts.begin())
                      f << ", ";
                    f << hosts[*j];
                  }
                f << "]";
              }

            const daq::am::ldap::UsersMap& assigned_users = r->get_assigned_role().get_users();
            const daq::am::ldap::UsersMap& enabled_users = r->get_enabled_role().get_users();

            if (assigned_users.size() != 0)
              {
                if (assigned_users.size() > enabled_users.size())
                  {
                    f << ", \"u\": [";

                    bool is_first = true;
                    for (const auto& j : assigned_users)
                      {
                        if (enabled_users.find(j.first) == enabled_users.end())
                          {
                            if (!is_first)
                              f << ", ";
                            else
                              is_first = false;
                            f << users[j.first];
                          }
                      }
                    f << "]";
                  }

                if (!enabled_users.empty())
                  {
                    f << ", \"U\": [";

                    bool is_first = true;
                    for (const auto& j : enabled_users)
                      {
                        if (!is_first)
                          f << ", ";
                        else
                          is_first = false;
                        f << users[j.first];
                      }
                    f << "]";
                  }
              }

            const daq::am::RulesMap& direct_rules = r->get_enabled_role().get_rules();
            const daq::am::RulesMap& all_rules = r->get_rules();

            if (all_rules.size() != 0)
              {
                if (all_rules.size() > direct_rules.size())
                  {
                    f << ", \"l\": [";

                    bool is_first = true;
                    for (const auto& j : all_rules)
                      {
                        if (direct_rules.find(j.first) == direct_rules.end())
                          {
                            if (!is_first)
                              f << ", ";
                            else
                              is_first = false;
                            f << rules[j.first];
                          }
                      }
                    f << "]";
                  }

                if (!direct_rules.empty())
                  {
                    f << ", \"L\": [";

                    bool is_first = true;
                    for (const auto& j : direct_rules)
                      {
                        if (!is_first)
                          f << ", ";
                        else
                          is_first = false;
                        f << rules[j.first];
                      }
                    f << "]";
                  }
              }

            ++i;

            if (i != x.get_roles().end())
              {
                f << " },\n";
              }
            else
              {
                f << " }\n";
                break;
              }
          }

      f << " ],\n"
          " \"sudos\": [\n";

      if (!used_sudos.empty())
        for (auto i = used_sudos.begin(); true;)
          {
            const daq::am::ldap::Sudo * s = i->second;
            f << "  { \"n\": \"" << s->get_name() << "\", \"d\": \"" << cvt_string(s->get_description()) << "\", \"r\": \"" << s->get_run_as() << '\"';
            if (s->get_authenticate() == false)
              f << ", \"a\": false";

            const std::list<std::string>& commands = s->get_commands();
            if (commands.size() != 0)
              {
                f << ", \"c\": [";
                for (auto j = commands.begin(); j != commands.end(); ++j)
                  {
                    if (j != commands.begin())
                      f << ", ";
                    f << '\"' << *j << '\"';
                  }
                f << "]";
              }

            ++i;

            if (i != used_sudos.end())
              {
                f << " },\n";
              }
            else
              {
                f << " }\n";
                break;
              }
          }

      f << " ],\n"
          " \"hosts\": [\n";

      if (!hosts.empty())
        for (auto i = hosts.begin(); true;)
          {
            f << "  \"" << i->first << '\"';

            ++i;

            if (i != hosts.end())
              {
                f << ",\n";
              }
            else
              {
                f << "\n";
                break;
              }
          }

      f << " ],\n"
          " \"users\": [\n";

      if (!users.empty())
        for (auto i = users.begin(); true;)
          {
            x.get_users().find(i->first);
            const auto& i2 = x.get_users().find(i->first);
            if (i2 != x.get_users().end())
              {
                const daq::am::ldap::User * user = i2->second;

                f << "  \"" << user->get_name() << " - " << user->get_real_name() << '\"';

                ++i;

                if (i != users.end())
                  {
                    f << ",\n";
                  }
                else
                  {
                    f << "\n";
                    break;
                  }
              }
          }

      f << " ],\n"
          " \"rules\": [\n";

      if (!used_rules.empty())
        for (auto i = used_rules.begin(); true;)
          {
            const daq::am::Rule * r = i->second;
            f << "  { \"n\": \"" << r->get_name() << "\", \"c\": [\n";
            const daq::am::RulesList& items = r->get_rules();
            daq::am::RulesList::const_iterator j = items.begin();

            while (true)
              {
                f << "   { ";

                std::map<std::string, std::string> * m = *j;

                for (auto d = m->begin(); d != m->end(); ++d)
                  {
                    if (d != m->begin())
                      f << ", ";
                    f << "\"" << d->first << "\": \"";
                    const std::string& val(d->second);
                    if (val.find_first_of("{regexp}") == 0)
                      {
                        for (unsigned int n = 0; n < val.size(); ++n)
                          {
                            char c(val[n]);
                            if (c == '\\')
                              f << "\\\\";
                            else if (c == '\"')
                              f << "\"";
                            else
                              f << c;
                          }
                      }
                    else
                      {
                        f << d->second;
                      }
                    f << '\"';
                  }

                ++j;

                if (j == items.end())
                  {
                    f << " }\n";
                    break;
                  }
                else
                  {
                    f << " },\n";
                  }

              }

            ++i;

            if (i != used_rules.end())
              {
                f << "  ] },\n";
              }
            else
              {
                f << "  ] }\n";
                break;
              }
          }

      f << " ]\n"
          "}\n";

      f.close();

    }
  catch (daq::am::Exception& ex)
    {
      std::cerr << "Caught daq::am exception " << ex << std::endl;
      return 1;
    }
  catch (std::exception& ex)
    {
      std::cerr << "Caught std exception " << ex.what() << std::endl;
      return 2;
    }

  return 0;
}
