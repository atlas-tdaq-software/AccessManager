#include <AccessManager/communication/NetworkCommunicationStreams.h>

#include <AccessManager/util/ErsIssues.h>

#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#ifdef AM_CLIENT_POLL_SOCKET
#include <sys/poll.h>
#endif
#include <netinet/in.h>
#include <errno.h>

namespace daq
{

namespace am
{

#define RECV_TIMEOUT_SECONDS						6
#define RECV_TIMEOUT_MILISECONDS					0

#define SEND_TIMEOUT_SECONDS						6
#define SEND_TIMEOUT_MILISECONDS					0

NetworkCommunicationStreams::NetworkCommunicationStreams(const int& socketfd, const std::string& description):
	CommunicationStreams(
		RECV_TIMEOUT_SECONDS * 1000 + RECV_TIMEOUT_MILISECONDS,
		SEND_TIMEOUT_SECONDS * 1000 + SEND_TIMEOUT_MILISECONDS),
	socketfd(socketfd), description(description) {
}

NetworkCommunicationStreams::~NetworkCommunicationStreams(){
	this->close();
}

// sends a message to the stream
// throws an CommunicationStreamsError exception in case the send didn't succeed
void NetworkCommunicationStreams::sendMessage(const std::string& message){

	 //check if the communication stream is opened
	 if (! isOpened()){
		throw CommunicationStreamsError(ERS_HERE, "stream closed!");
	 }

	//send the message
	ERS_DEBUG(1, "Sending message of length " << message.length() << " [" << message.c_str() <<"]");
	const char *buffer = message.c_str();
	unsigned long remainingBytes = message.length();
	long bytesSent = 0;

	while(remainingBytes > 0) {
		bytesSent = send(this->socketfd, buffer, remainingBytes, 0);
		if (bytesSent > 0) {
			remainingBytes -= bytesSent;
			buffer +=bytesSent;
			ERS_DEBUG(3, "sent bytes: " << bytesSent);
		} else if (bytesSent == 0) {
			ERS_DEBUG(1, "socket send returned 0! ");
		} else {
                        std::ostringstream text;
                        text << "failed sending " << remainingBytes << " bytes because: " << strerror(errno);
			throw CommunicationStreamsError(ERS_HERE, text.str());
		}
	}

	// shutdown the output stream
	::shutdown(this->socketfd, SHUT_WR);
}

// receives a message from the stream;
// throws - CommunicationStreamsError exception in case the receive
//			operation didn't succeed due to some internal errors
//		  - CommunicationStreamsTimeout exception when the operation
//			failed because of timeout
const std::string NetworkCommunicationStreams::receiveMessage(){

	 //check if the communication stream is opened
	 if (! isOpened()){
		throw CommunicationStreamsError(ERS_HERE, "stream closed!");
	 }

	// initialize the strBuff
	this->strBuff.str("");
	bool endLoop = false;

	int numbytes = 0;
	// the algorithm is: receive in the string buffer until either the stream is closed or the
	// number of bytes to receive exceeds the maximum limit
	while (!endLoop){
		numbytes = receiveTimeout(this->buffer, this->bufferSize);
		if (numbytes > 0) {
			this->buffer[numbytes] = '\0'; // set the end of string
			this->strBuff << this->buffer;
			ERS_DEBUG(10, "Received bytes:" << numbytes);
		}
		if (numbytes == 0) {
			ERS_DEBUG(2, "recv returned 0: pear connection closed!");
			ERS_DEBUG(3, "Message received so far [" << this->strBuff.str() << "]");
			endLoop = true;
		}

		// the check for returned code -1 is done in the receiveTimeout method
	}

	// shutdown the input stream
	::shutdown(this->socketfd, SHUT_RD);


	const std::string& messageReceived = this->strBuff.str();
	ERS_DEBUG(1, "Received message: length[" << messageReceived.length() << "] content[" << messageReceived << "]");
	return messageReceived;
}

// receive characters from socket and throw exception if timeout occurred or any other exception
// return the number of characters read in the buffer
int NetworkCommunicationStreams::receiveTimeout(char* _buffer, unsigned int _bufferSize){

	//check if needed to set the timeout
	if (this->recvTimeout != 0){

		//OBSERVATIONS:

		/* select() on sockets will fail in programs that need more than FD_SETSIZE file descriptors
		 * if one of the sockets being selected on happens to have a value of FD_SETSIZE or higher.
		 * Glibc fixes FD_SETSIZE at 1024, and does not allow this limit to be changed.
		 * poll() does not suffer from this limitation.
		*/

#ifdef AM_CLIENT_POLL_SOCKET
		/*
		 * Use the poll function for timeout
		 */
		ERS_DEBUG(2, "Poll the socket for timeout!");

	    int retcode;
	    struct pollfd ufds[1];

	    // set up the file descriptor set
		ufds[0].fd = this->socketfd;
		ufds[0].events = POLLIN | POLLPRI; // check for normal or out-of-band


	    // wait until timeout or data received
	    retcode = poll(ufds, 1, this->recvTimeout);
	    if (retcode == 0){
	    	// timeout occurred
			CommunicationStreamsTimeout issue(ERS_HERE, "socket receive", RECV_TIMEOUT_SECONDS, RECV_TIMEOUT_MILISECONDS);
			throw issue;
	    } else if (retcode == -1){
	    	// error
	    	std::string message = "poll error:";
	    	message += strerror(errno);
			throw CommunicationStreamsError(ERS_HERE, message);
		} else if ( ufds[0].revents & POLLHUP ) {
			std::string message = "poll error due to the remote side of the connection hung up";
			ERS_DEBUG(3, message);
			//throw CommunicationStreamsError(ERS_HERE, message);
		} else if ( ufds[0].revents & POLLERR ) {
			std::string message = "poll error:";
			message += strerror(errno);
			throw CommunicationStreamsError(ERS_HERE, message);
		} else if ( ufds[0].revents & POLLNVAL ) {
			std::string message = "poll error due to socket fd:";
			message += strerror(errno);
			throw CommunicationStreamsError(ERS_HERE, message);
	    } else if (! (ufds[0].revents & POLLIN || ufds[0].revents & POLLPRI) ) {
			std::string message = "poll event issue: poll OK, but no POLLIN or POLLPRI event returned! ";
			message += strerror(errno);
			throw CommunicationStreamsError(ERS_HERE, message);
	    }
#else
		/*
		 * Use the select function for timeout
		 */
		ERS_DEBUG(2, "Select on the socket for timeout!");

	 	fd_set fds;
	    int retcode;
	    struct timeval tv;

	    // set up the file descriptor set
	    FD_ZERO(&fds);
	    FD_SET(this->socketfd, &fds);

	    // set up the struct timeval for the timeout
	    tv.tv_sec = this->recvTimeout / 1000;
	    tv.tv_usec = (this->recvTimeout % 1000) * 1000;	// this value is microseconds


	    // wait until timeout or data received
	    retcode = select(this->socketfd + 1, &fds, NULL, NULL, &tv);
	    if (retcode == 0){
	    	// timeout occured
			throw CommunicationStreamsTimeout(ERS_HERE, "socket receive select", this->recvTimeout / 1000 , this->recvTimeout % 1000);
	    }

	    if (retcode == -1){
	    	// error
	    	std::string message = "select error:";
	    	message += strerror(errno);
			throw CommunicationStreamsError(ERS_HERE, message);
	    }
#endif

	}

    // data must be here, so do a normal recv()
    int numbytes = recv(this->socketfd, _buffer, _bufferSize, 0);
    if (numbytes < 0){
    	if (errno == ETIMEDOUT){
	    	// timeout occured
			throw CommunicationStreamsTimeout(ERS_HERE, "socket receive", this->recvTimeout / 1000 , this->recvTimeout % 1000);
    	} else {
			std::string message = "failed receiving because: ";
			message += strerror(errno);
			throw CommunicationStreamsError(ERS_HERE, message);
    	}
	} else {
    	return numbytes;
    }
}

// checks if the communication stream is open and usable
bool NetworkCommunicationStreams::isOpened() const {
	return this->socketfd > 0;
}

// close the communication stream
void NetworkCommunicationStreams::close(){
	if (this->socketfd > 0){
		//close the socket
		::close(this->socketfd);
		this->socketfd = 0;
		ERS_DEBUG(1, "Connection [ " << description << "] closed");
	} else{
		ERS_DEBUG(3, "Socket already closed!");
	}
}

}
}
