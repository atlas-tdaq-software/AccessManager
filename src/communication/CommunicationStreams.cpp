#include <AccessManager/communication/CommunicationStreams.h>

namespace daq
{

namespace am
{

CommunicationStreams::CommunicationStreams(const unsigned long& recvTimeout, const unsigned long& sendTimeout):
	recvTimeout(recvTimeout), sendTimeout(sendTimeout){
}

CommunicationStreams::~CommunicationStreams(){
}

}

}
