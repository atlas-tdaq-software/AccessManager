#include <AccessManager/communication/NetworkClientModule.h>

#include <AccessManager/communication/NetworkCommunicationStreams.h>

#include <AccessManager/util/ErsIssues.h>

#ifdef AM_CLIENT_POLL_SOCKET
#include <sys/poll.h>
#endif
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <iostream>
#include <sstream>


namespace daq
{

namespace am
{

#define CONNECT_TIMEOUT_SECONDS						2
#define CONNECT_TIMEOUT_MILISECONDS					0

NetworkClientModule::NetworkClientModule(const std::string& serverName, const unsigned short int& serverPort)
	: serverName(serverName), serverPort(serverPort), communicationStream(NULL) {

	ERS_DEBUG(3, "Information retrieved from constructor arguments: " <<
			"serverName[" << this->serverName << "] " <<
			"serverPort[" << this->serverPort << "]");

	//	struct addrinfo {
	//			 int ai_flags;           /* input flags */
	//			 int ai_family;          /* protocol family for socket */
	//			 int ai_socktype;        /* socket type */
	//			 int ai_protocol;        /* protocol for socket */
	//			 socklen_t ai_addrlen;   /* length of socket-address */
	//			 struct sockaddr *ai_addr; /* socket-address for socket */
	//			 char *ai_canonname;     /* canonical name for service location */
	//			 struct addrinfo *ai_next; /* pointer to next in list */
	//	 };
	/*	This structure can be used to provide hints concerning the type of socket
	     that the caller supports or wishes to use.  The caller can supply the
	     following structure elements in hints:

	     ai_family      The protocol family that should be used.  When ai_family
	                    is set to PF_UNSPEC, it means the caller will accept any
	                    protocol family supported by the operating system.

	     ai_socktype    Denotes the type of socket that is wanted: SOCK_STREAM,
	                    SOCK_DGRAM, or SOCK_RAW.  When ai_socktype is zero the
	                    caller will accept any socket type.

	     ai_protocol    Indicates which transport protocol is desired, IPPROTO_UDP
	                    or IPPROTO_TCP.  If ai_protocol is zero the caller will
	                    accept any protocol.

	     ai_flags       ai_flags is formed by OR'ing the following values:

	                    AI_CANONNAME    If the AI_CANONNAME bit is set, a success-
	                                    ful call to getaddrinfo() will return a
	                                    NUL-terminated string containing the
	                                    canonical name of the specified hostname
	                                    in the ai_canonname element of the first
	                                    addrinfo structure returned.

	                    AI_NUMERICHOST  If the AI_NUMERICHOST bit is set, it indi-
	                                    cates that nodename should be treated as a
	                                    numeric string defining an IPv4 or IPv6
	                                    address and no name resolution should be
	                                    attempted.

	                    AI_PASSIVE      If the AI_PASSIVE bit is set it indicates
	                                    that the returned socket address structure
	                                    is intended for use in a call to bind(2).
	                                    In this case, if the nodename argument is
	                                    the null pointer, then the IP address por-
	                                    tion of the socket address structure will
	                                    be set to INADDR_ANY for an IPv4 address
	                                    or IN6ADDR_ANY_INIT for an IPv6 address.

	                                    If the AI_PASSIVE bit is not set, the
	                                    returned socket address structure will be
	                                    ready for use in a call to connect(2) for
	                                    a connection-oriented protocol or
	                                    connect(2), sendto(2), or sendmsg(2) if a
	                                    connectionless protocol was chosen.  The
	                                    IP address portion of the socket address
	                                    structure will be set to the loopback
	                                    address if nodename is the null pointer
	                                    and AI_PASSIVE is not set.

	     All other elements of the addrinfo structure passed via hints must be
	     zero or the null pointer.

	     If hints is the null pointer, getaddrinfo() behaves as if the caller pro-
	     vided a struct addrinfo with ai_family set to PF_UNSPEC and all other
	     elements set to zero or NULL.
	*/

		struct addrinfo hints;
		struct addrinfo *res;


		std::stringstream service;
		int err;

		memset(&hints, 0, sizeof(hints));
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_family = AF_INET;

		service << serverPort;

		if ((err = getaddrinfo(serverName.c_str(), service.str().c_str(), &hints, &res)) != 0) {
			throw NetworkError(ERS_HERE, serverName.c_str(), serverPort, gai_strerror(err));
		}

		this->serverInfo.ai_flags 		= res->ai_flags;
		this->serverInfo.ai_family 		= res->ai_family;
		this->serverInfo.ai_socktype 	= res->ai_socktype;
		this->serverInfo.ai_protocol 	= res->ai_protocol;
		this->serverInfo.ai_addrlen 	= res->ai_addrlen;
		this->serverInfo.ai_addr 		= &(this->serverInfo_ai_addr);
		memcpy(this->serverInfo.ai_addr, res->ai_addr, res->ai_addrlen);
		this->serverInfo.ai_canonname 	= NULL;
		this->serverInfo.ai_next 		= NULL;

		this->serverName = "" + serverName +
				"(" + inet_ntoa(((struct sockaddr_in*)this->serverInfo.ai_addr)->sin_addr) + ")";

		ERS_DEBUG(3, "Information retrieved from first addrinfo structure: " <<
				"serverName [" << this->serverName << "]");

		freeaddrinfo(res);
}

/*
 * The destructor
 */
NetworkClientModule::~NetworkClientModule(){
	this->closeCommunication();
}

/*
 * Tries to open the network connection with the provided timeout value in milliseconds.
 */
void NetworkClientModule::initConnection(const unsigned long timeout){

	if (communicationStream != NULL) {
		throw NetworkError(ERS_HERE, serverName, serverPort, "Connection already initialized!");
	}

	int socketfd = -1;

	// initialize the socket
    if ((socketfd = socket(serverInfo.ai_family, serverInfo.ai_socktype, serverInfo.ai_protocol)) == -1) {
		throw NetworkError(ERS_HERE, serverName.c_str(), serverPort, strerror(errno) );
    }

	/* Disable the Nagle (TCP No Delay) algorithm */
	int flagNoDelay;
	socklen_t lFlagNoDelay = sizeof(flagNoDelay);
	flagNoDelay = 1;

	if (setsockopt(socketfd, IPPROTO_TCP, TCP_NODELAY, (void*)&flagNoDelay, lFlagNoDelay) == -1 ) {
		ERS_DEBUG(0, "Can not set the flag NoDelay");
	}

	// set the NON BLOCKING mode for connect with timeout
	int flags;
	if ((flags = fcntl(socketfd, F_GETFL)) < 0){
		ERS_DEBUG(1, "Can not get the status flags for the socket descriptor");
	} else {
		flags |= O_NONBLOCK;
		if (fcntl(socketfd, F_SETFL, flags) == -1){
			ERS_DEBUG(1, "Can not set the non blocking mode for the socket descriptor");
		}
	}
	unsigned int timeoutSeconds = CONNECT_TIMEOUT_SECONDS;
	unsigned int timeoutMiliseconds = CONNECT_TIMEOUT_MILISECONDS;

	// check if the provided parameter has a valid value
	if (timeout > 0 ){
		timeoutSeconds = 0;
		timeoutMiliseconds = timeout;
	}

	// try to connect with timeout
    if (connect(socketfd, serverInfo.ai_addr, serverInfo.ai_addrlen) == -1) {

    	if (errno == EINPROGRESS){

#ifdef AM_CLIENT_POLL_SOCKET
			/*
			 * Use the poll function for timeout
			 */
			ERS_DEBUG(2, "Poll the socket for connect timeout!");

			int retcode;
			struct pollfd ufds[1];

			// set up the file descriptor set
			ufds[0].fd = socketfd;
			ufds[0].events = POLLOUT; // check for normal or out-of-band


			// wait until timeout or data received
			retcode = poll(ufds, 1, timeoutSeconds * 1000 + timeoutMiliseconds);
			if (retcode == 0){
				// timeout occurred
				throw NetworkConnectTimeout(ERS_HERE,
					serverName.c_str(),
					serverPort,
					"",
					timeoutSeconds,
					timeoutMiliseconds);
			} else if (retcode == -1){
				// error
				std::string message = "connect poll error:";
				message += strerror(errno);
				throw NetworkError(ERS_HERE, serverName.c_str(), serverPort, message.c_str());
			} else if ( ufds[0].revents & POLLHUP ) {
				std::string message = "connect poll error(the destination port is not open)";
				throw NetworkError(ERS_HERE, serverName.c_str(), serverPort, message.c_str());
			} else if ( ufds[0].revents & POLLERR ) {
				std::string message = "socket connect poll error";
				message += strerror(errno);
				throw NetworkError(ERS_HERE, serverName.c_str(), serverPort, message.c_str());
			} else if ( ufds[0].revents & POLLNVAL ) {
				std::string message = "connect poll error due to socket fd:";
				message += strerror(errno);
				throw NetworkError(ERS_HERE, serverName.c_str(), serverPort, message.c_str());
			} else if (! (ufds[0].revents & POLLOUT) ) {
				std::string message = "poll event issue: poll OK, but no POLLOUT/HUP/ERR/NVAL event returned!";
				message += strerror(errno);
				throw NetworkError(ERS_HERE, serverName.c_str(), serverPort, message.c_str());
			}
#else
			/*
			 * Use the select function for timeout
			 */
			ERS_DEBUG(2, "Select on the socket for connect timeout!");

    		// let's wait the connection to be done
		 	fd_set fds;
		    int retcode;
		    struct timeval tv;

		    // set up the file descriptor set
		    FD_ZERO(&fds);
		    FD_SET(socketfd, &fds);

		    // set up the struct timeval for the timeout
		    tv.tv_sec 	= timeoutSeconds;
		    tv.tv_usec 	= timeoutMiliseconds * 1000;	// this value is microseconds

		    // wait until timeout or write operation can be performed
		    retcode = select(socketfd + 1, NULL, &fds, NULL, &tv);
		    if (retcode < 0 && errno != EINTR){
		    	// error
		    	std::string message = "error connecting:";
		    	message += strerror(errno);
				throw NetworkError(ERS_HERE, serverName.c_str(), serverPort, message.c_str());
		    } else if (retcode > 0){
		    	// success; the socket is selected for write
		    	int valopt;
		    	socklen_t lopt = sizeof(valopt);
		    	if (getsockopt(socketfd, SOL_SOCKET, SO_ERROR, (void*)&valopt, &lopt) < 0){
			    	std::string message = "error in getsockopt:";
			    	message += strerror(errno);
					throw NetworkError(ERS_HERE, serverName.c_str(), serverPort, message.c_str());
		    	}

		    	if (valopt != 0){
			    	std::string message = "error in connection:";
			    	message += strerror(valopt);
					throw NetworkError(ERS_HERE, serverName.c_str(), serverPort, message.c_str());
		    	}

		    } else{
		    	// timeout occured
				throw NetworkConnectTimeout(ERS_HERE,
					serverName.c_str(),
					serverPort,
					"",
					timeoutSeconds,
					timeoutMiliseconds);
		    }
#endif

    	} else {
    		// error on connect
			throw NetworkError(ERS_HERE, serverName.c_str(), serverPort, strerror(errno) );
    	}
    }

	// UNset the NON BLOCKING mode for connect with timeout
	if ((flags = fcntl(socketfd, F_GETFL)) < 0){
		ERS_DEBUG(1, "Can not get the status flags for the socket descriptor");
	} else {
		flags &= (~O_NONBLOCK); // clear the bit
		if (fcntl(socketfd, F_SETFL, flags) == -1){
			ERS_DEBUG(1, "Can not unset the non blocking mode for the socket descriptor");
		}
	}

    std::ostringstream description;
    description << "[" << serverName << ":" << serverPort << "]";
    communicationStream = new NetworkCommunicationStreams(socketfd, description.str());

	ERS_DEBUG(1, "Successfully connected to server [" << serverName << ":" << serverPort << "]");
}

/*
 * Method from ClientModule interface
 */
void NetworkClientModule::closeCommunication(){
	// delete the network socket
	delete communicationStream;
	communicationStream = NULL;
}

}
}
