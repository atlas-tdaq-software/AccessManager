#include <AccessManager/xacml/ctx/Attribute.h>

#include <AccessManager/xacml/attr/AttributeFactory.h>
#include <AccessManager/util/ErsIssues.h>

namespace daq
{

namespace am
{

Attribute::Attribute(
		const std::string& id,
		const std::string& issuer,
		const DateTimeAttribute* issueInstant,
		const AttributeValue* value){

	initialize(id, issuer, issueInstant, value);
}


Attribute::Attribute(
		const std::string& id,
		const AttributeValue* value){

	initialize(id, "", NULL, value);
}


Attribute::Attribute(
		const std::string& id,
		const AttributeIDsValues& idsValues){

	try{
	initialize(	id, "", NULL,
				AttributeFactory::Instance()->createValue(idsValues.getDataType(id), idsValues.getValue(id)));
	} catch (XACMLIssue & ex) {
		ERS_INFO("Can not build attribute with id [" << id << ']');
		throw ex;
	}
}

Attribute::Attribute(const Attribute& attribute):
	id(attribute.id),
	type(attribute.type),
	issuer(attribute.issuer),
	issueInstant((const DateTimeAttribute*)AttributeFactory::Instance()->cloneValue(attribute.issueInstant)),
	value(AttributeFactory::Instance()->cloneValue(attribute.value)) {

}

Attribute::~Attribute(){
	delete this->issueInstant;
	delete this->value;
}

//initialize the member attributes
void Attribute::initialize(
		const std::string& id,
		const std::string& issuer,
		const DateTimeAttribute* issueInstant,
		const AttributeValue* value){

	this->id = id;
	this->issuer = issuer;
	this->issueInstant = issueInstant;
	this->value = value;

	if (value != NULL){
		type = value->getType();
	} else {
		ERS_INFO("The attribute has no value!");
		type = "";
	}
}

// returns a string representation of attribute value
std::string Attribute::encode() const{
	std::string encoded = "<Attribute AttributeId=\"" + id + "\" " +
	    "DataType=\"" + type + '\"';

	if (!issuer.empty())
	    encoded += " Issuer=\"" + issuer + '\"';

	if (issueInstant != NULL)
	    encoded += " IssueInstant=\"" + issueInstant->encode() + '\"';

	encoded += '>';
	if (value != NULL)
		encoded += value->encodeWithTags(false);

	encoded += "</Attribute>";

	return encoded;
}

// sends the encoded value to the output stream with indentation
void Attribute::encode(std::ostream& os, Indenter& indt) const{
	os << indt << encode() << std::endl;
}

// sends the encoded value to the output stream without indentation
std::ostream& operator<<(std::ostream& os, const Attribute& attr){
	Indenter indt(0);
	attr.encode(os, indt);
	return os;
}

// operator needed for std::set and other similar STL classes
bool operator<(const Attribute& left, const Attribute& right){
	return left.id < right.id || left.value < right.value;
}



}

}
