#include <AccessManager/xacml/ctx/StatusDetail.h>

#include <AccessManager/util/XMLParsing.h>

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOMException.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>

namespace daq
{

namespace am
{

StatusDetail::StatusDetail(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode* root): detailText("") {
	detailRoot = root->cloneNode(true);
}


StatusDetail::StatusDetail(const std::vector<Attribute>& attributes){
	detailText = "<StatusDetail>\n";
	for(std::vector<Attribute>::const_iterator ait = attributes.begin(); ait != attributes.end(); ait++){
		detailText += ait->encode() + '\n';
	}
    detailText += "</StatusDetail>";
	detailRoot = textToNode(detailText);
}

StatusDetail::StatusDetail(const std::string& encoded){
	detailText = "<StatusDetail>\n" + encoded + "\n</StatusDetail>";
	detailRoot = textToNode(detailText);
}

StatusDetail::StatusDetail(const StatusDetail& sd):
	detailText(sd.detailText){
	this->detailRoot = ((sd.detailRoot == NULL) ? NULL : (sd.detailRoot)->cloneNode(true));
}

StatusDetail* StatusDetail::getInstance(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root){
	XERCES_CPP_NAMESPACE_USE

	ERS_DEBUG(3, "Preparing a StatusDetail instance from DOMNode");

	CREATE_DOM_ELEMENT(rootElement, &root);
    XMLCH_TO_STDSTRING(rootElement->getTagName(), elementName);

	if (elementName != "StatusDetail"){
		throw XACMLParsingException(ERS_HERE, "The node is [" + elementName + "]. Has to be StatusDetail!");
	}

    return new StatusDetail(&root);
}


XERCES_CPP_NAMESPACE_QUALIFIER DOMNode* StatusDetail::textToNode(const std::string& encoded){
	XERCES_CPP_NAMESPACE_USE

	std::string inputBuffer = "<?xml version=\"1.0\"?>\n" + encoded;

	try {
	    XMLPlatformUtils::Initialize();
	} catch (const XMLException& toCatch) {
            throw XACMLParsingException(ERS_HERE, make_exception_text("Error during initialization! :\n", toCatch.getMessage()));
	}

	XercesDOMParser parser;

	ErrorHandler* errHandler = (ErrorHandler*) new HandlerBase();
	parser.setErrorHandler(errHandler);

	// prepare the memory buffer input stream
	const char* memBufId = "XACML";
    MemBufInputSource memBufIS(
				        (const XMLByte*)inputBuffer.c_str()
				        , inputBuffer.size()
				        , memBufId
				        , false);

	try {
	    parser.parse(memBufIS);
	}
	catch (const XMLException& toCatch) {
            throw XACMLParsingException(ERS_HERE, make_exception_text("XMLException: ", toCatch.getMessage()));
	}
	catch (const DOMException& toCatch) {
            throw XACMLParsingException(ERS_HERE, make_exception_text("DOMException: ", toCatch.msg));
	}
        catch(const SAXException& toCatch) {
            throw XACMLParsingException(ERS_HERE, make_exception_text("SAXException: ", toCatch.getMessage()));
        }
	catch (...) {
	    throw XACMLParsingException(ERS_HERE, "unexpected exception!");
	}

    // get the DOM representation
    DOMNode *doc = parser.getDocument();
	DOMNode *newdoc = doc->cloneNode(true);

	delete errHandler;

	return newdoc;
}


StatusDetail::~StatusDetail(){
	delete detailRoot;
}
// operator needed for std::set and other similar STL classes
bool operator<(const StatusDetail& left, const StatusDetail& right){
	return left.detailText < right.detailText;
}

}

}
