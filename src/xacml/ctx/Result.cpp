#include <AccessManager/xacml/ctx/Result.h>

#include <AccessManager/util/XMLParsing.h>

#include <ostream>
#include <sstream>

namespace daq
{

namespace am
{

// string versions of the 4 Decision types used for encoding
const std::string Result::DECISIONS[] = { "Permit", "Deny",
		                                            "Indeterminate",
		                                            "NotApplicable" };

Result::Result(const int& decision, const std::string& resource):
			decision(decision), status(NULL), resource(resource) {
	checkDecision();
}

Result::Result(const int& decision, const Status* status, const std::string& resource):
			decision(decision), status(status), resource(resource){
	checkDecision();
}

Result::Result(const int& decision, const std::set<Obligation>& obligations):
			decision(decision), status(NULL), resource(""), obligations(obligations){
	checkDecision();
}

Result::Result(const int& decision, const Status* status, const std::set<Obligation>& obligations):
			decision(decision), status(status), resource(""), obligations(obligations){
	checkDecision();
}


Result::Result(const int& decision, const std::string& resource, const std::set<Obligation>& obligations):
			decision(decision), status(NULL), resource(resource), obligations(obligations){
	checkDecision();
}

Result::Result(const int& decision, const Status* status, const std::string& resource,
               const std::set<Obligation>& obligations):
			decision(decision), status(status), resource(resource), obligations(obligations){
	checkDecision();
}

Result::Result(const Result& result):
			decision(result.decision),
			resource(result.resource), obligations(result.obligations){
	this->status = ((result.status == NULL) ? NULL : new Status(*result.status));
}


Result::~Result(){
	delete status;
}

/**
 * Check if the decision is valid
 */
void Result::checkDecision(){
	// check that decision is valid
	if ((decision != DECISION_PERMIT) && (decision != DECISION_DENY) &&
	    (decision != DECISION_INDETERMINATE) &&
	    (decision != DECISION_NOT_APPLICABLE)){
		XACMLIllegalArgumentException issue(ERS_HERE, "invalid decision value");
		throw issue;
	}

}


Result* Result::getInstance(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root){
	XERCES_CPP_NAMESPACE_USE

    int ldecision = -1;
    Status* lstatus = NULL;
    std::set<Obligation> lobligations;

	ERS_DEBUG(3, "Preparing a Result instance from DOMNode");

	CREATE_DOM_ELEMENT(rootElement, &root);
    XMLCH_TO_STDSTRING(rootElement->getTagName(), elementName);

	if (elementName != "Result"){
		throw XACMLParsingException(ERS_HERE, "The node is [" + elementName + "]. Has to be Result!!");
	}



	// check for resource id attribute
	XMLCh *attr_ResourceId = XMLString::transcode("ResourceId");
	XMLCH_TO_STDSTRING(rootElement->getAttribute(attr_ResourceId), lresource);
	XMLString::release(&attr_ResourceId);
   	ERS_DEBUG(3, "Resource ID = [" << lresource << ']');

    // get the Decision (minOcc = 1, maxOcc = 1)
	GET_CHILDREN_WITH_TAG_AND_LIMITS(decisionElements, rootElement, "Decision", 1, 1);
	CREATE_DOM_ELEMENT(decisionElement, decisionElements->item(0));
	// have to get the text value of this attribute
	READ_TEXT_VALUE(decisionElement, decisionValue);
	ERS_DEBUG(3, "Found decision: " << decisionValue);
	for(int j=0; j < DECISIONS_COUNT; j++){
		if (decisionValue == DECISIONS[j]){
			ldecision = j;
			break;
		}
	}
    if (ldecision == -1){
    	ERS_DEBUG(2, "Unknown decision: " << decisionValue);
    	throw XACMLParsingException(ERS_HERE, "Unknown decision");
    }


    // get the Status (minOcc = 0, maxOcc = 1)
	GET_CHILDREN_WITH_TAG_AND_LIMITS(statusElements, rootElement, "Status", 0, 1);
	if (statusElements->getLength() == 1){
    	lstatus = Status::getInstance(*(statusElements->item(0)));
	}


    // get the Obligations  (minOcc = 0, maxOcc = 1)
	GET_CHILDREN_WITH_TAG_AND_LIMITS(obligationsElements, rootElement, "Obligations", 0, 1);
	if (obligationsElements->getLength() == 1){
		// get each Obligation tag in turn (minOcc = 1, maxOcc = unbounded)
		CREATE_DOM_ELEMENT(obligationsElement, obligationsElements->item(0));
		GET_CHILDREN_WITH_TAG_AND_LIMITS(obligationElements, obligationsElement, "Obligation", 1, MAX_OCC_UNBOUNDED);
		for(XMLSize_t idx = 0; idx < obligationElements->getLength(); idx++){
			Obligation* pobl = Obligation::getInstance(*(obligationElements->item(idx)));
			lobligations.insert(*pobl);
			delete pobl;
		}
	}

	ERS_DEBUG(3, "XML parsing done!");
	return new Result(ldecision, lstatus, lresource, lobligations);
}


void Result::encode(std::ostream& os, Indenter& indenter) const{

	// Prepare the indentation string
	std::string topIndent = indenter.makeString();
    ++indenter;
	std::string indent = indenter.makeString();
	++ indenter;

    // encode the starting tag
	if (resource.length() > 0){
	    os << topIndent << "<Result ResourceId=\"" << resource << "\">" << std::endl;
	} else {
	    os << topIndent << "<Result>" << std::endl;
	}

    // encode the decision
    os << indent << "<Decision>" << DECISIONS[decision] << "</Decision>" << std::endl;

    // encode the status
    if (status != NULL)
		status->encode(os, indenter);

    // encode the obligations
    if (obligations.size() != 0) {
	    os << indent << "<Obligations>" << std::endl;
		for(std::set<Obligation>::iterator oit = obligations.begin(); oit != obligations.end(); oit++){
			oit->encode(os, indenter);
		}
	    os << indent << "</Obligations>" << std::endl;
    }

	--indenter;
    --indenter;

    os << topIndent << "</Result>" << std::endl;
}

// returns a string representation of XML formatted request
std::string Result::encode() const{
	std::ostringstream xmlstream;
	Indenter indt(0);
	encode(xmlstream, indt);
	return xmlstream.str();
}

// sends the encoded value to the output stream without indentation
std::ostream& operator<<(std::ostream& os, const Result& res){
	Indenter indt;
	res.encode(os, indt);
	return os;
}

// operator needed for std::set and other similar STL classes
bool operator<(const Result& left, const Result& right){
	return 	left.decision 	< right.decision 	||
			left.status 	< right.status 		||
			left.resource	< right.resource;
}


}

}
