#include <AccessManager/xacml/ctx/RequestCtx.h>

#include <sstream>

namespace daq
{

namespace am
{

RequestCtx::RequestCtx(	const std::set<Subject>& 	subjects,
						const std::set<Attribute>& 	resource,
						const std::set<Attribute>& 	action,
						const std::set<Attribute>& 	environment,
						const std::string& 			resourceContent):
						subjects(subjects),
						resource(resource),
						action(action),
						environment(environment),
						resourceContent(resourceContent) {
	// nothing else to do yet
}

RequestCtx::~RequestCtx()
{
}

// helper macro to encode the attributes set
#define ENCODE_ATTRIBUTES_SET(attributes, ostream, indenter) \
	for(std::set<Attribute>::iterator it = (attributes)->begin(); it != (attributes)->end(); it++) \
		it->encode( (ostream), (indenter));

/**
 * Encodes this context into its XML representation and writes
 * this encoding to the given <code>OutputStream</code> with
 * indentation.
 *
 * @param output a stream into which the XML-encoded data is written
 * @param indenter an object that creates indentation strings
 */
void RequestCtx::encode(std::ostream& os, Indenter& indenter) const{
	
	// write the XML prolog
	os << "<?xml version=\"1.0\"?>" << std::endl;
	
	// Prepare the indentation string
	std::string topIndent = indenter.makeString();
    os << topIndent << "<Request>" << std::endl;

    // go in one more for next-level elements...
    ++indenter;
    std::string indent = indenter.makeString();

    // ...and go in again for everything else
    ++indenter;

    // first off, go through all SUBJECTS
    for(std::set<Subject>::iterator subject = subjects.begin(); subject != subjects.end(); subject++){
    	os << indent << "<Subject SubjectCategory=\""
           << subject->getCategory() + '\"';
        const std::set<Attribute>* sattrs = subject->getAttributes();

        if (sattrs != NULL && sattrs->size() > 0 ) {
            // there's content, so fill it in
        	os << '>' << std::endl;
        	ENCODE_ATTRIBUTES_SET(sattrs, os, indenter);
        	os << indent << "</Subject>" << std::endl;
        } else {
            // there's nothing in this Subject, so just close the tag
        	os << "/>" << std::endl;
        } 
    }
    

    // next do the resource
    if ((resource.size() != 0) || (resourceContent.length() != 0)) {
        os << indent << "<Resource>" << std::endl;
        if (resourceContent.length() != 0)
            os << indenter << "<ResourceContent>" << resourceContent << "</ResourceContent>" << std::endl;
    	ENCODE_ATTRIBUTES_SET(&resource, os, indenter);
        os << indent << "</Resource>" << std::endl;
    } else {
        os << indent << "<Resource/>" << std::endl;
    }

    // now the action
    if (action.size() != 0) {
        os << indent << "<Action>"  << std::endl;
    	ENCODE_ATTRIBUTES_SET(&action, os, indenter);
        os << indent << "</Action>" << std::endl;
    } else {
        os << indent << "<Action/>" << std::endl;
    }

    // finally the environment, if there are any attrs
    if (environment.size() != 0) {
        os << indent << "<Environment>" << std::endl;
    	ENCODE_ATTRIBUTES_SET(&environment, os, indenter);
        os << indent << "</Environment>" << std::endl;
    } else {
    	os << indent << "<Environment/>" << std::endl;
    }

    // we're back to the top
    --indenter;
    --indenter;
    
    os << topIndent << "</Request>";
	
}

// returns a string representation of XML formatted request
std::string RequestCtx::encode() const{
	std::ostringstream xmlstream;
	Indenter indt(0);
	encode(xmlstream, indt);
	return xmlstream.str();
}

// sends the encoded value to the output stream without indentation
std::ostream& operator<<(std::ostream& os, const RequestCtx& reqCtx){
	Indenter indt;
	reqCtx.encode(os, indt);
	return os;	
}


}

}
