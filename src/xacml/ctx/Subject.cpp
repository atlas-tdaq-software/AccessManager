#include <AccessManager/xacml/ctx/Subject.h>

namespace daq
{

namespace am
{

/**
 * The access subject categories.
 */

const std::string Subject::CATEGORY_ACCESS_SUBJECT("urn:oasis:names:tc:xacml:1.0:subject-category:access-subject");
const std::string Subject::CATEGORY_RECIPIENT_SUBJECT("urn:oasis:names:tc:xacml:1.0:subject-category:recipient-subject");
const std::string Subject::CATEGORY_INTERMEDIARY_SUBJECT("urn:oasis:names:tc:xacml:1.0:subject-category:intermediary-subject");
const std::string Subject::CATEGORY_CODEBASE("urn:oasis:names:tc:xacml:1.0:subject-category:codebase");
const std::string Subject::CATEGORY_REQUESTING_MACHINE("urn:oasis:names:tc:xacml:1.0:subject-category:requesting-machine");
const std::string Subject::DEFAULT_CATEGORY(Subject::CATEGORY_ACCESS_SUBJECT);


Subject::Subject(const std::set<Attribute>& attributes): attributes(attributes) {
	this->category = DEFAULT_CATEGORY;
}

Subject::Subject(const std::string& category, const std::set<Attribute>& attributes):
		category(category), attributes(attributes) { }

Subject::~Subject(){
}

// operator needed for std::set and other similar STL classes
bool operator<(const Subject& left, const Subject& right){
	return left.category < right.category;
}


}

}
