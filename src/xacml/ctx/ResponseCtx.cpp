#include <boost/thread/mutex.hpp>

#include <AccessManager/xacml/ctx/ResponseCtx.h>

#include <AccessManager/util/XMLParsing.h>

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/dom/DOMException.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/OutOfMemoryException.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>

#include <sstream>


namespace daq
{

namespace am
{

std::string make_exception_text(const char * prefix, const XMLCh * what)
{
  std::string s(prefix);
  char * message = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(what);
  s += message;
  XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&message);
  return s;
}

std::string make_exception_text(const char * prefix, const XMLCh * what, const std::string& xml) {
  std::string s(make_exception_text(prefix, what));
  s += " xml-string: \"";
  s += xml;
  s += "\".";
  return s;
}

ResponseCtx::ResponseCtx(const Result& result) {
    results.insert(result);
}

ResponseCtx::ResponseCtx(const std::set<Result>& results): results(results) {
}

ResponseCtx::~ResponseCtx(){
}

ResponseCtx* ResponseCtx::getInstance(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root){
	XERCES_CPP_NAMESPACE_USE

	std::set<Result> lresults;

	ERS_DEBUG(3, "Preparing a ResponseCtx instance from DOMNode");

	CREATE_DOM_ELEMENT(rootElement, &root);
    XMLCH_TO_STDSTRING(rootElement->getTagName(), elementName);

	if (elementName != "Response"){
		throw XACMLParsingException(ERS_HERE, "The node is [" + elementName + "]. Has to be <Response>!!");
	}

	GET_CHILDREN_WITH_TAG_AND_LIMITS(resultElements, rootElement, "Result", 1, MAX_OCC_UNBOUNDED);
	for(XMLSize_t ridx = 0; ridx < resultElements->getLength(); ridx++){
    	try{
        	Result* result = Result::getInstance(*(resultElements->item(ridx)));
        	lresults.insert(*result);
        	delete result;
    	} catch (XACMLParsingException &ex) {
    		ers::error(ex);
    	}
	}

	if (lresults.size() == 0){
		throw XACMLParsingException(ERS_HERE, "must have at least one Result");
	}

	ERS_DEBUG(3, "XML parsing done!");

	return new ResponseCtx(lresults);
}



ResponseCtx* ResponseCtx::getInstance(const std::string& inputBuffer){
	XERCES_CPP_NAMESPACE_USE

	ERS_DEBUG(3, "Preparing a ResponseCtx instance from input buffer:\n" << inputBuffer);

	try {
            static boost::mutex s_mutex;
            boost::mutex::scoped_lock lock(s_mutex);
	    XMLPlatformUtils::Initialize();
	}

	catch (const XMLException& toCatch) {
	    throw XACMLParsingException(ERS_HERE, make_exception_text("Error during initialization! :\n", toCatch.getMessage()));
	}

	XercesDOMParser parser;

	ErrorHandler* errHandler = (ErrorHandler*) new HandlerBase();
	parser.setErrorHandler(errHandler);

	// prepare the memory buffer input stream
	const char* memBufId = "XACML";
    MemBufInputSource memBufIS(
				        (const XMLByte*)inputBuffer.c_str()
				        , inputBuffer.size()
				        , memBufId
				        , false);

	try {
	    parser.parse(memBufIS);
	}
	catch (const XMLException& toCatch) {
            throw XACMLParsingException(ERS_HERE, make_exception_text("XMLException: ", toCatch.getMessage()), inputBuffer);
	}
	catch (const DOMException& toCatch) {
            throw XACMLParsingException(ERS_HERE, make_exception_text("DOMException: ", toCatch.msg), inputBuffer);
	}
        catch(const SAXException& toCatch) {
            throw XACMLParsingException(ERS_HERE, make_exception_text("SAXException: ", toCatch.getMessage()), inputBuffer);
        }
        catch(const OutOfMemoryException&) {
            throw XACMLParsingException(ERS_HERE, "OutOfMemoryException");
        }
	catch(std::exception& e){
            throw XACMLParsingException(ERS_HERE, "std exception", e);
	}
	catch (...) {
	    throw XACMLParsingException(ERS_HERE, "unexpected exception!");
	}

    // get the DOM representation
    DOMDocument *doc = parser.getDocument();
	DOMElement* element = doc->getDocumentElement();
	if (element == NULL){
	    throw XACMLParsingException(ERS_HERE, "No <Response> tag name found!");
	}

	// build a new responce ctx object
	ResponseCtx* responseCtx = ResponseCtx::getInstance(*element);

	delete errHandler;

	return responseCtx;
}

void ResponseCtx::encode(std::ostream& os, Indenter& indenter) const{

	// Prepare the indentation string
	std::string topIndent = indenter.makeString();
    os << topIndent << "<Response>" << std::endl;

    // ...and go in again for everything else
    ++indenter;

	for(std::set<Result>::iterator rit = results.begin(); rit != results.end(); rit++){
		rit->encode(os, indenter);
	}

    --indenter;

    os << topIndent << "</Response>" << std::endl;
}

// returns a string representation of XML formatted request
std::string ResponseCtx::encode() const{
	std::ostringstream xmlstream;
	Indenter indt(0);
	encode(xmlstream, indt);
	return xmlstream.str();
}

// sends the encoded value to the output stream without indentation
std::ostream& operator<<(std::ostream& os, const ResponseCtx& resCtx){
	Indenter indt;
	resCtx.encode(os, indt);
	return os;
}


}

}
