#include <AccessManager/xacml/ctx/Status.h>

#include <AccessManager/util/XMLParsing.h>

namespace daq
{

namespace am
{

/**
 * Standard identifier for the OK status
 */
const std::string Status::STATUS_OK("urn:oasis:names:tc:xacml:1.0:status:ok");

/**
 * Standard identifier for the MissingAttribute status
 */
const std::string Status::STATUS_MISSING_ATTRIBUTE("urn:oasis:names:tc:xacml:1.0:status:missing-attribute");

/**
 * Standard identifier for the SyntaxError status
 */
const std::string Status::STATUS_SYNTAX_ERROR("urn:oasis:names:tc:xacml:1.0:status:syntax-error");

/**
 * Standard identifier for the ProcessingError status
 */
const std::string Status::STATUS_PROCESSING_ERROR("urn:oasis:names:tc:xacml:1.0:status:processing-error");

// a single OK object we'll use most of the time
const Status Status::okStatus(Status::STATUS_OK);


Status::Status(const std::string& code):
	message(""), detail(NULL) {
	this->code.push_back(code);
	checkCode();
}

Status::Status(const std::vector<std::string>& code, const std::string& message, const StatusDetail* detail):
	code(code), message(message), detail(detail){
	checkCode();
}

Status::Status(const Status& status):
	code(status.code), message(status.message){
	this->detail = ((status.detail == NULL) ? NULL : new StatusDetail(*status.detail));
}

Status::~Status(){
	delete detail;
}

// check the code value and if detail is present
void Status::checkCode(){
	if (this->detail != NULL && this->code.size() > 0){
	    std::string c = code[0];
	    if (c == STATUS_OK || c == STATUS_SYNTAX_ERROR || c == STATUS_PROCESSING_ERROR){
	        XACMLIllegalArgumentException issue(ERS_HERE, "status detail cannot be included with " + c);
			throw issue;
	    }
	}
}


Status* Status::getInstance(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root){
	XERCES_CPP_NAMESPACE_USE

    std::vector<std::string> 	lcode;
    std::string 				lmessage = "";
    StatusDetail* 				ldetail = NULL;

	ERS_DEBUG(3, "Preparing a Status instance from DOMNode");

	CREATE_DOM_ELEMENT(rootElement, &root);
    XMLCH_TO_STDSTRING(rootElement->getTagName(), elementName);

	if (elementName != "Status"){
		throw XACMLParsingException(ERS_HERE, "The node is [" + elementName + "]. Has to be Status!!");
	}

    // get the StatusCode (minOcc = 1, maxOcc = 1), but use MAX_OCC_UNBOUND for the sub codes that are children
	GET_CHILDREN_WITH_TAG_AND_LIMITS(codeElements, rootElement, "StatusCode", 1, MAX_OCC_UNBOUNDED);
	for(XMLSize_t idx = 0; idx < codeElements->getLength(); idx++){
		CREATE_DOM_ELEMENT(codeElement, codeElements->item(idx));
		// get the value attribute
		XMLCh *attr_Value = XMLString::transcode("Value");
		XMLCH_TO_STDSTRING(codeElement->getAttribute(attr_Value), codeValue);
		XMLString::release(&attr_Value);
		// add this code to the vector
		lcode.push_back(codeValue);
		ERS_DEBUG(3, "Read code value [" << codeValue << ']');
	}

    // get the StatusMessage (minOcc = 0, maxOcc = 1)
	GET_CHILDREN_WITH_TAG_AND_LIMITS(messageElements, rootElement, "StatusMessage", 0, 1);
	if (messageElements->getLength() == 1){
		CREATE_DOM_ELEMENT(messageElement, messageElements->item(0));
		// the text is in a DOM Text
		READ_TEXT_VALUE(messageElement, messageValue);
		lmessage = messageValue;
		ERS_DEBUG(3, "Read message value [" << messageValue << ']');
	}

    // get the StatusDetail (minOcc = 0, maxOcc = 1)
	GET_CHILDREN_WITH_TAG_AND_LIMITS(detailElements, rootElement, "StatusDetail", 0, 1);
	if (detailElements->getLength() == 1){
		CREATE_DOM_ELEMENT(detailElement, detailElements->item(0));
		ERS_DEBUG(3, "Read one status detail ... creating the instance ...");
		ldetail = StatusDetail::getInstance(*detailElement);
	}

	ERS_DEBUG(3, "XML parsing done!");
    return new Status(lcode, lmessage, ldetail);
}



void Status::encode(std::ostream& os, Indenter& indenter) const{

	// Prepare the indentation string
	std::string topIndent = indenter.makeString();
    ++indenter;
	std::string indent = indenter.makeString();

    os << topIndent << "<Status>" << std::endl;

	//encode status code
	std::vector<std::string>::const_iterator cit = code.begin();
	encodeStatusCode(os, indenter, cit);

	//encode the message
	if (message.length() > 0){
    	os << indent << "<StatusMessage>" << message << "</StatusMessage>" << std::endl;
	}

	// encode the status detail
	if (detail != NULL){
		os << indent << detail->getEncoded() << std::endl;
	}


    --indenter;
    os << topIndent << "</Status>" << std::endl;
}

void Status::encodeStatusCode(std::ostream& os,
		 Indenter& indenter,
		 std::vector<std::string>::const_iterator& cit) const{

	std::string indent = indenter.makeString();
	const std::string& scode = *cit;

	if ( (cit + 1) != this->code.end() ){
	    os << indent << "<StatusCode Value=\"" << scode << "\">" << std::endl;
		++indenter;
		std::vector<std::string>::const_iterator cit1 = cit + 1;
		encodeStatusCode(os, indenter, cit1);
		--indenter;
	    os << indent << "</StatusCode>" << std::endl;
	} else {
	    os << indent << "<StatusCode Value=\"" << scode << "\"/>" << std::endl;
	}

}


// returns a string representation of XML formatted request
std::string Status::encode() const{
	std::ostringstream xmlstream;
	Indenter indt(0);
	encode(xmlstream, indt);
	return xmlstream.str();
}

// sends the encoded value to the output stream without indentation
std::ostream& operator<<(std::ostream& os, const Status& status){
	Indenter indt;
	status.encode(os, indt);
	return os;
}

// operator needed for std::set and other similar STL classes
bool operator<(const Status& left, const Status& right){
	return 	left.code 	< right.code 	||
			left.message< right.message	||
			left.detail	< right.detail;
}


}

}
