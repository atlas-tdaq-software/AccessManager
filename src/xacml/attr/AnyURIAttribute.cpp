#include <AccessManager/xacml/attr/AnyURIAttribute.h>

namespace daq
{

namespace am
{
// the URI identifier
const std::string AnyURIAttribute::identifier("http://www.w3.org/2001/XMLSchema#anyURI");

AnyURIAttribute::AnyURIAttribute(const std::string& value):
		AttributeValue(identifier, value){
}

AnyURIAttribute::AnyURIAttribute(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root):
		AttributeValue(identifier, root){
}

AnyURIAttribute::~AnyURIAttribute(){
}

void AnyURIAttribute::initialize(){
}

}

}
