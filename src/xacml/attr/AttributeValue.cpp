#include <AccessManager/xacml/attr/AttributeValue.h>

#include <AccessManager/util/XMLParsing.h>

#define INVALID_XML_CHAR_REPLACEMENT	"_"

namespace daq
{

namespace am
{
AttributeValue::AttributeValue(const std::string& type, const std::string& value):
	type(type), strValue(value){

	/**
	 * Ensures that the value string has only
	 * valid XML unicode characters as specified by the
	 * XML 1.0 standard. For reference, please see
	 * <a href="http://www.w3.org/TR/2000/REC-xml-20001006#NT-Char">the
	 * standard</a>.
	 *
	 */

	/* The following are the character ranges for low-order non-printable
	 * ASCII characters that are rejected by MSXML versions 3.0 and later:
	 * 		#x0 - #x8 (ASCII 0 - 8)
	 * 		#xB - #xC (ASCII 11 - 12)
	 * 		#xE - #x1F (ASCII 14 - 31)
	 */

	for (unsigned int i = 0; i < strValue.length(); i++) {
		char current = strValue.at(i);
		if (((current >= 0x0) && (current <= 0x8)) ||
			((current >= 0xB) && (current <= 0xC)) ||
			((current >= 0xE) && (current <= 0x1F))) {
        	ERS_DEBUG(2, "Replace character with code " << (int)current << " at position " << i << \
        			" with character " << INVALID_XML_CHAR_REPLACEMENT);
			strValue.replace(i, 1, INVALID_XML_CHAR_REPLACEMENT);
		}
	}

}

AttributeValue::AttributeValue(const std::string& type, const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root):
		type(type){

	XERCES_CPP_NAMESPACE_USE

	ERS_DEBUG(3, "Preparing an AttributeValue instance from DOMNode");
	CREATE_DOM_ELEMENT(rootElement, &root);

	// get the data type if the provided argument is empty
    if(this->type.length() == 0){
    	ERS_DEBUG(2, "DataType argument is empty, trying to read it from node attributes");
		XMLCH_TO_STDSTRING(rootElement->getAttribute(XMLString::transcode("DataType")), dataType);
        if (dataType.length() != 0){
        	this->type = dataType;
        } else {
        	ERS_DEBUG(2, "DataType could not be identified!");
        }
	}

    // get the attribute value
	READ_TEXT_VALUE(rootElement, strAttributeValue);
    this->strValue = strAttributeValue;

	ERS_DEBUG(3, "Prepared an AttributeValue instance from DOMNode with:" << \
				 "DataType=[" << this->type << "] value=[" << this->strValue << ']');
}

AttributeValue::AttributeValue(const AttributeValue& attributeValue):
	type(attributeValue.type), strValue(attributeValue.strValue){
}

AttributeValue::~AttributeValue(){

}

// return the data type of this attribute value
const std::string& AttributeValue::getType() const {
	return type;
}

// returns a string representation of attribute value
const std::string& AttributeValue::encode() const {
	return strValue;
}

// attaches XML tags to the attribute value
std::string AttributeValue::encodeWithTags(bool includeType = true) const{
	if (includeType)
	    return "<AttributeValue DataType=\"" + type + "\">" + encode() + "</AttributeValue>";
	else
	    return "<AttributeValue>" + encode() + "</AttributeValue>";
}

// sends the encoded value to the output stream with indentation
void AttributeValue::encode(std::ostream& os, Indenter& indt) const{
	os << indt << encodeWithTags() << std::endl;
}

// sends the encoded value to the output stream
std::ostream& operator<<(std::ostream& os, const AttributeValue& attrv){
	Indenter indt(0);
	attrv.encode(os, indt);
	return os;
}


// operator needed for std::set and other similar STL classes
bool operator<(const AttributeValue& left, const AttributeValue& right){
	return left.encode() < right.encode();
}


}

}
