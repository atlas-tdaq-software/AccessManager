#include <AccessManager/xacml/attr/TimeAttribute.h>

#include <boost/date_time/posix_time/posix_time.hpp>
#include <sstream>

namespace daq
{

namespace am
{

// the URI identifier
const std::string TimeAttribute::identifier("http://www.w3.org/2001/XMLSchema#time");

TimeAttribute::TimeAttribute(const std::string& value):
		AttributeValue(identifier, value) {
	
	if (this->strValue == "")		
		initialize();
	
}

TimeAttribute::TimeAttribute(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root):
		AttributeValue(identifier, root){
}

TimeAttribute::~TimeAttribute()
{
}

void TimeAttribute::initialize(){
	using namespace boost::posix_time;
	using namespace std;
	ptime	universal_time 	= second_clock::universal_time();
	ptime	local_time 		= second_clock::local_time();
	
	// compute the timezone
	time_duration tz = local_time - universal_time;
	
	ostringstream vs;
  	vs << to_simple_string(local_time.time_of_day()) 
  	   << internal << showpos << setw(3) << setfill('0') 
  	   << tz.hours() << ":00";
  	   
  	this->strValue = vs.str();	
}

}

}
