#include <AccessManager/xacml/attr/StringAttribute.h>

namespace daq
{

namespace am
{
	
// the URI identifier
const std::string StringAttribute::identifier("http://www.w3.org/2001/XMLSchema#string");

StringAttribute::StringAttribute(const std::string& value): 
		AttributeValue(identifier, value){
}

StringAttribute::StringAttribute(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root):
		AttributeValue(identifier, root){
}

StringAttribute::~StringAttribute(){
	
}

void StringAttribute::initialize(){
}

}

}
