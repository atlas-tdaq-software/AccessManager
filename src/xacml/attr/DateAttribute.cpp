#include <AccessManager/xacml/attr/DateAttribute.h>

#include <boost/date_time/posix_time/posix_time.hpp>
#include <sstream>

namespace daq
{

namespace am
{

// the URI identifier
const std::string DateAttribute::identifier("http://www.w3.org/2001/XMLSchema#date");

DateAttribute::DateAttribute(const std::string& value):
		AttributeValue(identifier, value) {
	
	if (this->strValue == "")		
		initialize();
	
}

DateAttribute::DateAttribute(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root):
		AttributeValue(identifier, root){
}

DateAttribute::~DateAttribute()
{
}

void DateAttribute::initialize(){
	using namespace boost::posix_time;
	using namespace std;
	ptime	universal_time 	= second_clock::universal_time();
	ptime	local_time 		= second_clock::local_time();
	
	// compute the timezone
	time_duration tz = local_time - universal_time;
		
	ostringstream vs;
  	vs << to_iso_extended_string(local_time.date())
 	   << internal << showpos << setw(3) << setfill('0') 
  	   << tz.hours() << ":00";
  	     	   
  	this->strValue = vs.str();	
}


}

}
