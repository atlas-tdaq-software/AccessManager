#include <AccessManager/xacml/attr/AttributeFactory.h>

#include <AccessManager/xacml/attr/proxy/AnyURIAttributeProxy.h>
#include <AccessManager/xacml/attr/proxy/DateAttributeProxy.h>
#include <AccessManager/xacml/attr/proxy/DateTimeAttributeProxy.h>
#include <AccessManager/xacml/attr/proxy/StringAttributeProxy.h>
#include <AccessManager/xacml/attr/proxy/TimeAttributeProxy.h>

#include <AccessManager/util/XMLParsing.h>

namespace daq
{

namespace am
{

//singleton private member
AttributeFactory* AttributeFactory::singletonInstance = 0; // initialize pointer

//std::map<std::string, AttributeProxy*> AttributeFactory::supportedDataTypes;


// the sole instance retriever
AttributeFactory* AttributeFactory::Instance(){
	if (singletonInstance == 0){
		singletonInstance = new AttributeFactory();
	}
	return singletonInstance;
}

// the constructor
AttributeFactory::AttributeFactory(){
	initializeDataTypes();
}

AttributeFactory::~AttributeFactory(){
	// free the map
	std::map<std::string, AttributeProxy*>::iterator it;
	for (it = supportedDataTypes.begin(); it != supportedDataTypes.end(); it++){
		delete it->second;
	}
	singletonInstance = 0;
}


void AttributeFactory::initializeDataTypes(){
	supportedDataTypes[AnyURIAttribute::identifier] 	= new AnyURIAttributeProxy();
	supportedDataTypes[DateAttribute::identifier] 		= new DateAttributeProxy();
	supportedDataTypes[DateTimeAttribute::identifier] 	= new DateTimeAttributeProxy();
	supportedDataTypes[StringAttribute::identifier] 	= new StringAttributeProxy();
	supportedDataTypes[TimeAttribute::identifier] 		= new TimeAttributeProxy();
}

AttributeValue* AttributeFactory::createValue(const std::string& dataType, const std::string& value) {
	if (supportedDataTypes.find(dataType) != supportedDataTypes.end()){
		return supportedDataTypes[dataType]->getInstance(value);
	} else {
		ERS_DEBUG(1, "Undefined attribute value implementation for data type ["
					<< dataType << ']');
		XACMLUndefinedAttributeValue issue(ERS_HERE, dataType);
		throw issue;
	}
}

AttributeValue* AttributeFactory::createValue(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root, const std::string& dataType) {
	XERCES_CPP_NAMESPACE_USE

	std::string dt = dataType;

	ERS_DEBUG(3, "Preparing to create an AttributeValue instance from DOMNode");
	CREATE_DOM_ELEMENT(rootElement, &root);

	// get the data type if the provided argument is empty
    if(dt.length() == 0){
    	ERS_DEBUG(2, "DataType argument is empty, trying to read it from node attributes");
		XMLCH_TO_STDSTRING(rootElement->getAttribute(XMLString::transcode("DataType")), dataTypeValue);
        if (dataTypeValue.length() != 0){
        	dt = dataTypeValue;
        } else {
        	ERS_DEBUG(2, "DataType could not be identified!");
        }
	}

	// if it didn't succeed
    if (dt.length() == 0){
		XACMLParsingException issue(ERS_HERE, "DataType not found");
		throw issue;
    }

	if (supportedDataTypes.find(dt) != supportedDataTypes.end()){
		return supportedDataTypes[dt]->getInstance(root);
	} else {
		ERS_DEBUG(2, "Undefined attribute value implementation for data type ["
					<< dataType << ']');
		XACMLUndefinedAttributeValue issue(ERS_HERE, dataType);
		throw issue;
	}
}

AttributeValue* AttributeFactory::cloneValue(const AttributeValue* attributeValue){
	return (attributeValue == NULL) ? NULL : createValue(attributeValue->getType(), attributeValue->encode());
}


}
}
