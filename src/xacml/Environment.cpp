#include <AccessManager/xacml/Environment.h>

#include <AccessManager/xacml/ctx/Attribute.h>
#include <AccessManager/xacml/attr/AttributeFactory.h>

namespace daq
{

namespace am
{


Environment::Environment():AttributeIDsValues()
{
	initValues();
}

Environment::~Environment()
{
}


 /**
 * Environment identifiers
 */
#define IDENTIFIER_DATE		"urn:oasis:names:tc:xacml:1.0:environment:date"
#define IDENTIFIER_TIME		"urn:oasis:names:tc:xacml:1.0:environment:time"

/**
 * The data type specified in URI format
 */
#define DATA_TYPE_DATE		"http://www.w3.org/2001/XMLSchema#date"
#define DATA_TYPE_TIME		"http://www.w3.org/2001/XMLSchema#time"
#define DEFAULT_DATA_TYPE	DATA_TYPE_DATE

/**
 * The default match function.
 */
#define DEFAULT_MATCH_FUNCTION	"urn:oasis:names:tc:xacml:1.0:function:UNKNOWN"

/**
 * Get the URI indentifier for the date attribute
 * @return URI identifier
 */
const std::string Environment::getDate() const{
	return IDENTIFIER_DATE;
}

/**
 * Get the URI indentifier for the time attribute
 * @return URI identifier
 */
const std::string Environment::getTime() const{
	return IDENTIFIER_TIME;
}

/**
 * Get the data type for the given URI identifier
 * @param identifier URI identifier
 * @return the data type in URI format
 */
const std::string Environment::getDataType(const std::string& identifier) const {
	if (IDENTIFIER_DATE == identifier){
		return DATA_TYPE_DATE;
	} else if (IDENTIFIER_TIME == identifier){
		return DATA_TYPE_TIME;
	} else {
		return DEFAULT_DATA_TYPE;
	}
}

/**
 * Get the match function for the given URI identifier
 * @param identifier URI identifier
 * @return the match function in URI format
 */
const std::string Environment::getMatchFunction(const std::string& identifier) const {
	if (IDENTIFIER_DATE == identifier){
		return DEFAULT_MATCH_FUNCTION;
	} else if (IDENTIFIER_TIME == identifier){
		return DEFAULT_MATCH_FUNCTION;
	} else {
		return DEFAULT_MATCH_FUNCTION;
	}
}

void Environment::initValues(){
	//add 2 attributes: one for time and another for date
	// the values are retrieved from 2 instances of time and date attributes
	const std::string& timeId = this->getTime();
	const std::string& dateId = this->getDate();

	AttributeValue* attrvTime = AttributeFactory::Instance()->createValue(this->getDataType(timeId), "");
	AttributeValue* attrvDate = AttributeFactory::Instance()->createValue(this->getDataType(dateId), "");

	addValue(timeId, attrvTime->encode());
	addValue(dateId, attrvDate->encode());

	delete attrvTime;
	delete attrvDate;
}

}

}
