#include <AccessManager/xacml/Subjects.h>

namespace daq
{

namespace am
{

Subjects::Subjects(): AttributeIDsValues()
{
}

Subjects::~Subjects()
{
}

/**
 * Subjects identifiers
 */
#define IDENTIFIER_SUBJECT_ID	"urn:oasis:names:tc:xacml:1.0:subject:subject-id"
#define IDENTIFIER_DNS_NAME		"urn:oasis:names:tc:xacml:1.0:subject:authn-locality:dns-name"
#define IDENTIFIER_IP_ADDRESS	"urn:oasis:names:tc:xacml:1.0:subject:authn-locality:ip-address"

/**
 * The data type specified in URI format
 */
#define DEFAULT_DATA_TYPE		"http://www.w3.org/2001/XMLSchema#string"

/**
 * The default match function.
 */
#define DEFAULT_MATCH_FUNCTION	"urn:oasis:names:tc:xacml:1.0:function:string-equal"


/**
 * Get the URI indentifier for the Subject Id attribute
 * @return URI identifier
 */
const std::string Subjects::getSubjectId() const {
	return IDENTIFIER_SUBJECT_ID;
}

/**
 * Get the URI indentifier for the DNS name attribute
 * @return URI identifier
 */
const std::string Subjects::getDNSName() const{
	return IDENTIFIER_DNS_NAME;
}

/**
 * Get the URI indentifier for the IP address attribute
 * @return URI identifier
 */
const std::string Subjects::getIpAddress() const{
	return IDENTIFIER_IP_ADDRESS;
}

/**
 * Get the data type for the given URI identifier
 * @param identifier URI identifier
 * @return the data type in URI format
 */
const std::string Subjects::getDataType(const std::string& identifier) const {
	if (identifier == getSubjectId()){
		return DEFAULT_DATA_TYPE;
//	} else if (identifier == getDNSName()){
//		return DEFAULT_DATA_TYPE;
//	} else if (identifier == getIpAddress()()){
//		return DEFAULT_DATA_TYPE;
	}
	return DEFAULT_DATA_TYPE;
}

/**
 * Get the match function for the given URI identifier
 * @param identifier URI identifier
 * @return the match function in URI format
 */
const std::string Subjects::getMatchFunction(const std::string& identifier) const {
	if (identifier == getSubjectId()){
		return DEFAULT_MATCH_FUNCTION;
//	} else if (identifier == getDNSName()){
//		return DEFAULT_MATCH_FUNCTION;
//	} else if (identifier == getIpAddress()()){
//		return DEFAULT_MATCH_FUNCTION;
	}
	return DEFAULT_MATCH_FUNCTION;
}

}
}
