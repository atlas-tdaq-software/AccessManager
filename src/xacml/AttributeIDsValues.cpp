#include <AccessManager/xacml/AttributeIDsValues.h>

#include <AccessManager/util/ErsIssues.h>

namespace daq
{

namespace am
{

AttributeIDsValues::AttributeIDsValues() {
}

AttributeIDsValues::~AttributeIDsValues(){
}


/**
 * Get the value of an attribute based on the attribute URI identifier.
 * If there are more values available, only the first one is returned.
 * @param identifier attribute identifier
 * @return the string value, null if none
 */
const std::string& AttributeIDsValues::getValue(const std::string& identifier) const{
	try {
		const std::vector<std::string>& v = getValues(identifier);
		if (v.size() > 0){
			return v.at(0);
		} else {
			ERS_DEBUG(2, "Value not found for identifier [" << identifier << "]");
			XACMLMissingAttributeValue issue(ERS_HERE, identifier);
			throw issue;
		}
	} catch (XACMLIssue & ex) {
		throw ex;
	}

}

/**
 * Get the values of an action identifier
 * @param identifier the identifier in URI format
 * @return a vector of string values
 */
const std::vector<std::string>& AttributeIDsValues::getValues(const std::string& identifier) const{
	std::map<std::string, std::vector<std::string> >::const_iterator cit = values.find(identifier);
	if (cit != values.end()){
		return cit->second;
	} else {
		ERS_DEBUG(2, "Values not found for identifier [" << identifier << "]");
		XACMLMissingAttributeValue issue(ERS_HERE, identifier);
		throw issue;
	}
}

/*
 * Method to associate one more value to the identifier
 */
void AttributeIDsValues::addValue(const std::string& identifier, const std::string& value){
	std::vector<std::string>& v = values[identifier];

	// check if the value is already assigned to the identifier
	std::vector<std::string>::const_iterator vit = v.begin();
	while (vit != v.end() && (*vit != value)) {
		vit++;
	}

	if (vit == v.end()){
		v.push_back(value);
		ERS_DEBUG(3, "Value [" << value << "] added to identifier [" << identifier << "]");
	}else {
		ERS_DEBUG(2, "Value [" << value << "] already added to identifier [" << identifier << "]");
	}
}

// sends the uri ids and their values to ostream
std::ostream& operator<<(std::ostream& os, const AttributeIDsValues& attrIV){
	std::map<std::string, std::vector<std::string> >::const_iterator kit; // key iterator
	std::vector<std::string>::const_iterator vit; // value iterator

	for( kit = attrIV.values.begin(); kit != attrIV.values.end(); kit++){

		os << "Identifier [" << kit->first << "]=(";

		std::vector<std::string> v = kit->second;
		for(vit = v.begin(); vit != v.end(); vit++){
			os << " \"" << *vit << "\" ";
		}

		os << ")" << std::endl;
	}
	return os;
}

}

}
