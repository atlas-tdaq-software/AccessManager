#include <stdexcept>
#include <AccessManager/xacml/Action.h>

namespace daq
{

namespace am
{

Action::Action(): AttributeIDsValues()
{
}

Action::~Action()
{
}

/**
 * Actions identifiers
 */
#define __IDENTIFIER_ACTION_ID__		"urn:oasis:names:tc:xacml:1.0:action:action-id"

/**
 * The data type specified in URI format
 */
#define __DEFAULT_DATA_TYPE__			"http://www.w3.org/2001/XMLSchema#string"

/**
 * The default match function.
 */
#define __DEFAULT_MATCH_FUNCTION__		"urn:oasis:names:tc:xacml:1.0:function:string-equal"



/**
 * Get the action URI identifier
 * @return the URI identifier
 */
const std::string Action::getActionId() const {
	return __IDENTIFIER_ACTION_ID__;
}

/**
 * Get the data type for the given URI identifier
 * @param identifier URI identifier
 * @return the data type in URI format
 */
const std::string Action::getDataType(const std::string& identifier) const {
	if (identifier == getActionId()){
		return __DEFAULT_DATA_TYPE__;
	}
	return __DEFAULT_DATA_TYPE__;
}

/**
 * Get the match function for the given URI identifier
 * @param identifier URI identifier
 * @return the match function in URI format
 */
const std::string Action::getMatchFunction(const std::string& identifier) const {
	if (identifier == getActionId()){
		return __DEFAULT_MATCH_FUNCTION__;
	}
	return __DEFAULT_MATCH_FUNCTION__;
}

}

}
