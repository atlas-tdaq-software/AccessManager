#include <AccessManager/xacml/Identifiers.h>

namespace daq
{
  namespace am
  {
    static std::string
    make_resource_type_identifier(const char *id)
    {
      return std::string("urn:oasis:names:tc:xacml:1.0:resource:resource-type:") + id;
    }

    const std::string IDENTIFIER_RESOURCE_TYPE_COMMAND_NAME = make_resource_type_identifier("command");
    const std::string IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME = make_resource_type_identifier("partition");
    const std::string IDENTIFIER_RESOURCE_TYPE_NAME = make_resource_type_identifier("name");
    const std::string IDENTIFIER_RESOURCE_TYPE_PATH = make_resource_type_identifier("path");
    const std::string IDENTIFIER_RESOURCE_TYPE_IDLMETHOD = make_resource_type_identifier("method");
    const std::string IDENTIFIER_RESOURCE_TYPE_APPLICATION = make_resource_type_identifier("application");
    const std::string IDENTIFIER_RESOURCE_TYPE_ARGUMENTS = make_resource_type_identifier("arguments");
    const std::string IDENTIFIER_RESOURCE_TYPE_HOSTNAME = make_resource_type_identifier("hostname");
    const std::string IDENTIFIER_RESOURCE_TYPE_OWNED_BY_REQUESTER = make_resource_type_identifier("owned-by-requester");
  }
}
