#include <AccessManager/xacml/Indenter.h>

namespace daq
{

namespace am
{

Indenter::Indenter(int width):
	width(width) { 
	depth = 0; spaces = "";
}
	
// prefix increment operator
const Indenter& operator++(Indenter& indt)	{
	 indt.depth += indt.width;
	 return indt;
}

//prefix decrement operator
const Indenter& operator--(Indenter& indt)	{
	indt.depth -= indt.width;
	indt.depth = (indt.depth < 0) ? 0 : indt.depth;
	return indt;
}

std::string Indenter::makeString() { 
	spaces.assign(depth, ' '); 
	return spaces;
}

std::ostream& operator<<(std::ostream& os, const Indenter& indt) {
	std::string spaces(indt.depth, ' ');
	os << spaces;
	return os;
 }  


}
}
