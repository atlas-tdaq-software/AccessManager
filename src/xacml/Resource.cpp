#include <stdexcept>
#include <AccessManager/xacml/Resource.h>

#include <AccessManager/util/ErsIssues.h>

namespace daq
{

namespace am
{

/**
 * Resources identifiers
 */

#define IDENTIFIER_RESOURCE_CATEGORY 	"urn:oasis:names:tc:xacml:1.0:resource:resource-category"
#define IDENTIFIER_RESOURCE_ID			"urn:oasis:names:tc:xacml:1.0:resource:resource-id"
#define IDENTIFIER_RESOURCE_TYPE		"urn:oasis:names:tc:xacml:1.0:resource:resource-type"
#define IDENTIFIER_RESOURCE_LOCATION	"urn:oasis:names:tc:xacml:1.0:resource:resource-location"
#define DEFAULT_DATA_TYPE				"http://www.w3.org/2001/XMLSchema#string"
#define DEFAULT_MATCH_FUNCTION			"urn:oasis:names:tc:xacml:1.0:function:string-equal"


Resource::Resource(const std::string& d): AttributeIDsValues(), description(d)
{
}

Resource::~Resource()
{
}

/**
 *  The URI identifier of the resource category.
 *  In the RBAC model, this will be the target of the Permission Policy Set
 * @return the name of resource type, or null if not available
 */
const std::string Resource::getResourceCategory() const
{
	return IDENTIFIER_RESOURCE_CATEGORY;
}

/**
 * The URI identifier of the resource id used in the definition of policies.
 * In the RBAC model, this will be the target of the Permission Policy included in a PPS.
 * @return resource's id
 */
const std::string Resource::getResourceId() const
{
	return IDENTIFIER_RESOURCE_ID;
}

/**
 * The resource types URI identifiers used in the rule definition.
 * In the RBAC model, this will be the target of the rule included in a PP.
 * @return resource's id
 */
const std::vector<std::string>& Resource::getResourceTypes() const
{
	return this->types;
}

/**
 * The description of the resource used in the definition of rules and policies.
 * @return resource's description
 */
const std::string& Resource::getDescription() const
{
	return description;
}

/**
 * Get the data type for the given resource identifier.
 * @param resourceIdentifier
 * @return the data type in URI format
 */
const std::string Resource::getDataType(const std::string& identifier) const
{
	if (identifier == getResourceCategory()){
		return DEFAULT_DATA_TYPE;
//	} else if (identifier == getResourceId()){
//		return DEFAULT_DATA_TYPE;
	}
	return DEFAULT_DATA_TYPE;
}

/**
 * Get the match function id for the given resource identifier.
 * @param resourceIdentifier
 * @return the match function id in URI format
 */
const std::string Resource::getMatchFunction(const std::string& identifier) const
{
	if (identifier == getResourceCategory()){
		return DEFAULT_MATCH_FUNCTION;
//	} else if (identifier == getResourceId()){
//		return DEFAULT_MATCH_FUNCTION;
	}
	return DEFAULT_MATCH_FUNCTION;
}

/**
 * Get the action for a given resource identifier
 * @param identifier the resource identifier
 * @return the reference to action object or throw exception if none
 */
const Action* Resource::getAction(const std::string& identifier) const
{
	std::map<std::string, const Action*>::const_iterator ait = actions.find(identifier);
	if (ait != actions.end()){
		return ait->second;
	} else{
		ERS_DEBUG(2, "Action not found for identifier [" << identifier << "]");
		XACMLMissingAction issue(ERS_HERE, identifier);
		throw issue;
	}
}

/*
 * Method to add a new type
 */
void Resource::addTypes(const std::string& newType){

	// check if the type has been already added
	std::vector<std::string>::iterator vit = types.begin();
	while (vit != types.end() && (*vit != newType)) {
		vit++;
	}

	if (vit == types.end()){
		types.push_back(newType);
	}else {
		ERS_DEBUG(3, "Type [" << newType << "] already added to types vector ");
	}
}

/*
 * Method to set an action for a given identifier.
 */
void Resource::addAction(const std::string& identifier, const Action* action){
	if (actions.find(identifier) != actions.end()){
		ERS_DEBUG(2, "Identifier [" << identifier << " has already assigned an action.It will be overridden!!");
	}
	actions[identifier] = action;
}


}

}
