#include <AccessManager/xacml/impl/BCMAction.h>

namespace daq
{
  namespace am
  {

    BCMAction::BCMAction(const std::string &actionValue) :
        Action()
    {
      initValues();
      addValue(Action::getActionId(), actionValue);
    }

    BCMAction::~BCMAction()
    {
    }

    void
    BCMAction::initValues()
    {
    }

    std::string
    BCMAction::getValueForActionCONFIGURE()
    {
      return "configure";
    }

    std::string
    BCMAction::getValueForActionUPDATE()
    {
      return "update";
    }

  }
}
