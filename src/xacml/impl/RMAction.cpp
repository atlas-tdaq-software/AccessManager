#include <AccessManager/xacml/impl/RMAction.h>

namespace daq
{

namespace am
{

RMAction::RMAction(const std::string& actionValue): Action() {
	initValues();
	addValue(Action::getActionId(), actionValue);
}

RMAction::~RMAction(){
	
}

/*
 * Initialize the action id values
 */
void RMAction::initValues()
{
	// do nothing, but has to be here because is defined in the ABC
}

// return the value for action id in case the action is "any"
std::string RMAction::getValueForActionANY() {
	return "any";
}

}

}
