#include <AccessManager/xacml/impl/IGUIResource.h>

namespace daq
{

namespace am
{

// the IGUI acction
const IGUIAction IGUIResource::iguiAction;


/*
 * The string values of operation types.
 */
    const std::string IGUIResource::VIEW_MODES[VIEW_MODES_COUNT] =
      { "display", "control", "expert" };

IGUIResource::IGUIResource(const int viewModeId):
	Resource("IGUI Resource"){

	//TYPES INITIALIZATION
	// no needed

	//VALUES INITIALIZATION
	initValues();

	// set the view mode as value for resource identifier
	addValue(getResourceId(), IGUIResource::VIEW_MODES[viewModeId]);

	//ACTIONS INITIALIZATION
	//initialize the actions for the resource id
	addAction(getResourceId(), &iguiAction);
}

IGUIResource::~IGUIResource(){

}

void IGUIResource::initValues(){
	addValue(getResourceCategory(), "igui");
}

std::unique_ptr<const IGUIResource> IGUIResource::getInstanceForViewMode(const int viewModeId) {
	if (viewModeId >=0 && viewModeId < IGUIResource::VIEW_MODES_COUNT) {
		return std::unique_ptr<const IGUIResource>(new IGUIResource(viewModeId));
	} else {
		return std::unique_ptr<const IGUIResource>(nullptr);
	}

}

}

}
