#include <AccessManager/xacml/Identifiers.h>
#include <AccessManager/xacml/impl/RunControlResource.h>

namespace daq
{

  namespace am
  {

    // the Run Control action
    const RunControlAction RunControlResource::rcAction;

    RunControlResource::RunControlResource(const std::string& commandName, const std::string& partitionName) :
        Resource("Run Control Resource")
    {

      //TYPES INITIALIZATION
      // add the command name and partition name
      // ... later when the values are checked

      //VALUES INITIALIZATION
      initValues();

      if (commandName.length() > 0)
        {
          addTypes(IDENTIFIER_RESOURCE_TYPE_COMMAND_NAME);
          addValue(IDENTIFIER_RESOURCE_TYPE_COMMAND_NAME, commandName);
        }

      if (partitionName.length() > 0)
        {
          addTypes(IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME);
          addValue(IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME, partitionName);
        }

      //ACTIONS INITIALIZATION
      //initialize the actions for the resource id
      addAction(getResourceId(), &rcAction);
    }

    RunControlResource::~RunControlResource()
    {
    }

    /*
     * Populate the list and maps from parent class with known values;
     */
    void
    RunControlResource::initValues()
    {
      // set the Run Control category value
      addValue(getResourceCategory(), "RunControl");
      addValue(getResourceId(), "rc");
    }

    /*
     * The string value of TriggerCommander actions.
     */
    const std::string RunControlResource::ACTION_VALUES[ACTIONS_COUNT] =
      { "PUBLISH", "PUBLISHSTATS", "PUBLISHSTATISTICS", "USER", "USERBROADCAST" };

  }

}
