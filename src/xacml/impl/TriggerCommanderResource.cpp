#include <AccessManager/xacml/Identifiers.h>
#include <AccessManager/xacml/impl/TriggerCommanderResource.h>


namespace daq
{

  namespace am
  {

    // the Trigger Commander action
    const TriggerCommanderAction TriggerCommanderResource::tcAction;

    TriggerCommanderResource::TriggerCommanderResource(const std::string& commandName, const std::string& partitionName) :
        Resource("Trigger Commander Resource")
    {

      //TYPES INITIALIZATION
      // add the command name and partition name
      // ... later when the values are checked

      //VALUES INITIALIZATION
      initValues();

      if (commandName.length() > 0)
        {
          addTypes(IDENTIFIER_RESOURCE_TYPE_COMMAND_NAME);
          addValue(IDENTIFIER_RESOURCE_TYPE_COMMAND_NAME, commandName);
        }

      if (partitionName.length() > 0)
        {
          addTypes(IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME);
          addValue(IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME, partitionName);
        }

      //ACTIONS INITIALIZATION
      //initialize the actions for the resource id
      addAction(getResourceId(), &tcAction);

    }

    TriggerCommanderResource::~TriggerCommanderResource()
    {

    }

    /*
     * Populate the list and maps from parent class with known values;
     */
    void
    TriggerCommanderResource::initValues()
    {
      // set the Run Control category value
      addValue(getResourceCategory(), "TriggerCommander");
      addValue(getResourceId(), "tc");
    }

    /*
     * The string value of TriggerCommander actions.
     */
    const std::string TriggerCommanderResource::ACTION_VALUES[ACTIONS_COUNT] =
      { "HOLD", "RESUME", "SETPRESCALES", "SETL1PRESCALES", "SETHLTPRESCALES", "SETBUNCHGROUP", "SETPRESCALESANDBUNCHGROUP", "SETLUMIBLOCKLEN", "SETMINLUMIBLOCKLEN", "INCREASELUMIBLOCK", "SETCONDITIONSUPDATE" };

  }

}
