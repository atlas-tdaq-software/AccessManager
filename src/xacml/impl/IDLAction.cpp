#include <AccessManager/xacml/impl/IDLAction.h>

namespace daq
{

namespace am
{

IDLAction::IDLAction(): Action()
{
	initValues();	
}

IDLAction::~IDLAction()
{
}

/*
 * Initialize the action id values
 */
void IDLAction::initValues()
{
	addValue(Action::getActionId(), getValueForActionCALL());
}

// return the value for action id
std::string IDLAction::getValueForActionCALL() {
	return "call";
}


}
}
