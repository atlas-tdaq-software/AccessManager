#include <AccessManager/xacml/Identifiers.h>
#include <AccessManager/xacml/impl/IDLResource.h>

namespace daq
{

namespace am
{


// the IDL specific action for all methods and interfaces
const IDLAction IDLResource::resourceTypeIDLAction;

/*
 * The constructor takes as arguments the name of IDL interface and the method name
 */
IDLResource::IDLResource(std::string IDLInterface, std::string IDLMethod):
	Resource("IDL Resource") {
			
	//TYPES INITIALIZATION
	// add the idl method resource type
	addTypes(IDENTIFIER_RESOURCE_TYPE_IDLMETHOD);

	//VALUES INITIALIZATION
	initValues();
	// set IDL interface name as value for resource identifier
	addValue(getResourceId(), IDLInterface);
	// set IDL method name as value for IDL method name resource identifier
	addValue(IDENTIFIER_RESOURCE_TYPE_IDLMETHOD, IDLMethod);
	
	//ACTIONS INITIALIZATION
	//initialize the actions for the idl method resource identifier
	addAction(IDENTIFIER_RESOURCE_TYPE_IDLMETHOD, &resourceTypeIDLAction);	
}

IDLResource::~IDLResource(){
	
}

/*
 * Populate the list and maps from parent class with known values;
 */
void IDLResource::initValues(){
	// set the IDL category value
	addValue(getResourceCategory(), "idl");
}


}

}
