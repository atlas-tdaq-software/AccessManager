#include <AccessManager/xacml/impl/IGUIAction.h>

namespace daq
{

namespace am
{

IGUIAction::IGUIAction(): Action()
{
	initValues();	
}

IGUIAction::~IGUIAction()
{
}

/*
 * Initialize the action id values
 */
void IGUIAction::initValues()
{
	addValue(Action::getActionId(), getValueForActionVIEW());
}

// return the value for action id 
std::string IGUIAction::getValueForActionVIEW() {
	return "view";
}


}
}
