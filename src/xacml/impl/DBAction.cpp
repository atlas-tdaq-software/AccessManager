#include <AccessManager/xacml/impl/DBAction.h>

namespace daq
{
  namespace am
  {

    DBAction::DBAction(const std::string &actionValue) :
        Action()
    {
      initValues();
      addValue(Action::getActionId(), actionValue);
    }

    DBAction::~DBAction()
    {
    }

    /*
     * Initialize the action id values
     */
    void
    DBAction::initValues()
    {
    }

  }
}
