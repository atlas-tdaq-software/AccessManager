#include <AccessManager/xacml/Identifiers.h>
#include <AccessManager/xacml/impl/DBResource.h>


namespace daq
{

  namespace am
  {

    /*
     * The string values of operation types.
     */
    const std::string DBResource::OPERATION_TYPES[OPERATION_TYPES_COUNT] =
      { "admin", "directory", "file" };

    /*
     * The string value of DB actions.
     */
    const std::string DBResource::ACTION_VALUES[ACTIONS_COUNT] =
      { "admin", "create_file", "update_file", "delete_file", "create_subdir", "delete_subdir" };

    /*
     * The DBAction instances for DB resource.
     */
    const DBAction DBResource::ACTIONS[6] = {
      DBAction(ACTION_VALUES[ACTION_ID_ADMIN]),
      DBAction(ACTION_VALUES[ACTION_ID_CREATE_FILE]),
      DBAction(ACTION_VALUES[ACTION_ID_UPDATE_FILE]),
      DBAction(ACTION_VALUES[ACTION_ID_DELETE_FILE]),
      DBAction(ACTION_VALUES[ACTION_ID_CREATE_SUBDIR]),
      DBAction(ACTION_VALUES[ACTION_ID_DELETE_SUBDIR]) };

    DBResource::DBResource(const std::string& resourceId, const std::string& path, const DBAction& action) :
        Resource("DB Resource")
    {
      initValues();
      addValue(getResourceId(), resourceId);

      if (path.length() > 0)
        {
          addTypes(IDENTIFIER_RESOURCE_TYPE_PATH);
          addValue(IDENTIFIER_RESOURCE_TYPE_PATH, path);
        }

      addAction(getResourceId(), &action);
    }

    void
    DBResource::initValues()
    {
      addValue(getResourceCategory(), "DataBase");
    }

    DBResource::~DBResource()
    {
    }

    const DBResource *
    DBResource::getInstanceForAdminOperations()
    {
      return new DBResource(OPERATION_TYPES[OPERATION_TYPE_ID_ADMIN], "", ACTIONS[ACTION_ID_ADMIN]);
    }

    std::unique_ptr<const DBResource>
    DBResource::getAutoptrInstanceForAdminOperations()
    {
      return std::unique_ptr<const DBResource>(DBResource::getInstanceForAdminOperations());
    }

    const DBResource *
    DBResource::getInstanceForDirectoryOperations(const std::string& path, const int &ACTION_ID)
    {
      if (ACTION_ID >= 0 && ACTION_ID < DBResource::ACTIONS_COUNT)
        {
          return new DBResource(OPERATION_TYPES[OPERATION_TYPE_ID_DIRECTORY], path, ACTIONS[ACTION_ID]);
        }
      else
        {
          return nullptr;
        }
    }

    std::unique_ptr<const DBResource>
    DBResource::getAutoptrInstanceForDirectoryOperations(const std::string& path, const int &ACTION_ID)
    {
      return std::unique_ptr<const DBResource>(DBResource::getInstanceForDirectoryOperations(path, ACTION_ID));
    }

    const DBResource *
    DBResource::getInstanceForFileOperations(const std::string& path, const int &ACTION_ID)
    {
      if (ACTION_ID >= 0 && ACTION_ID < DBResource::ACTIONS_COUNT)
        {
          return new DBResource(OPERATION_TYPES[OPERATION_TYPE_ID_FILE], path, ACTIONS[ACTION_ID]);
        }
      else
        {
          return nullptr;
        }
    }

    std::unique_ptr<const DBResource>
    DBResource::getAutoptrInstanceForFileOperations(const std::string& path, const int &ACTION_ID)
    {
      return std::unique_ptr<const DBResource>(DBResource::getInstanceForFileOperations(path, ACTION_ID));
    }

  }

}
