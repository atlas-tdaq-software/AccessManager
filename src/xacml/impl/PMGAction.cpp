#include <AccessManager/xacml/impl/PMGAction.h>

namespace daq
{
  namespace am
  {

    PMGAction::PMGAction(const std::string &actionValue) :
        Action()
    {
      initValues();
      addValue(Action::getActionId(), actionValue);
    }

    PMGAction::~PMGAction()
    {

    }

    void
    PMGAction::initValues()
    {
    }

    std::string
    PMGAction::getValueForActionSTART()
    {
      return "start";
    }

    std::string
    PMGAction::getValueForActionTERMINATE()
    {
      return "terminate";
    }

  }
}
