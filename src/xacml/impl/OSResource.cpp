#include <AccessManager/xacml/Identifiers.h>
#include <AccessManager/xacml/impl/OSResource.h>

namespace daq
{

namespace am
{

OSResource::OSResource(	const std::string& resourceLocation,
			const std::string& application,
			const std::string& arguments):
	Resource("OS Resource"){

	//TYPES INITIALIZATION
	// add the "application" and "arguments" method resource type
	// ... later, in the values initialization

	//VALUES INITIALIZATION
	initValues();
	// set the resource location as value for resource identifier
	addValue(getResourceId(), resourceLocation);
	// the application and arguments resource types
	addTypes(IDENTIFIER_RESOURCE_TYPE_APPLICATION);
	addValue(IDENTIFIER_RESOURCE_TYPE_APPLICATION, application);

	if (arguments.length() > 0){
		addTypes(IDENTIFIER_RESOURCE_TYPE_ARGUMENTS);
		addValue(IDENTIFIER_RESOURCE_TYPE_ARGUMENTS, arguments);
	}

	//ACTIONS INITIALIZATION
	currentOSAction.reset(nullptr);
}

OSResource::~OSResource(){
}


void OSResource::setAction(const std::string& action){
	currentOSAction.reset(new OSAction(action));

	//initialize the actions for the resource id
	addAction(IDENTIFIER_RESOURCE_TYPE_APPLICATION, currentOSAction.get());
}

/*
 * Populate the list and maps from parent class with known values;
 */
void OSResource::initValues(){
	addValue(getResourceCategory(), "os");
}


}

}
