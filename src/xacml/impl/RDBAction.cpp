#include <AccessManager/xacml/impl/RDBAction.h>

#define VALUE_FOR_ACTION_EXEC_CMD       "exec_cmd"

namespace daq
{

  namespace am
  {

    RDBAction::RDBAction() :
        Action()
    {
      initValues();
    }

    RDBAction::~RDBAction()
    {
    }

    /*
     * Initialize the action id values
     */
    void
    RDBAction::initValues()
    {
      addValue(Action::getActionId(), VALUE_FOR_ACTION_EXEC_CMD);
    }

    // return the value for action id
    std::string
    RDBAction::getValueForActionEXEC_CMD()
    {
      return VALUE_FOR_ACTION_EXEC_CMD;
    }

  }
}
