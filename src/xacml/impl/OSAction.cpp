#include <AccessManager/xacml/impl/OSAction.h>

namespace daq
{

namespace am
{

OSAction::OSAction(const std::string& actionValue): Action() {
	initValues();
	addValue(Action::getActionId(), actionValue);
}

OSAction::~OSAction(){
	
}

/*
 * Initialize the action id values
 */
void OSAction::initValues()
{
	// do nothing, but has to be here because is defined in the ABC
}

}

}
