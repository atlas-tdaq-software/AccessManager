#include <AccessManager/xacml/impl/RunControlAction.h>

#define VALUE_FOR_ACTION_EXEC_CMD       "exec_cmd"

namespace daq
{
  namespace am
  {

    RunControlAction::RunControlAction() :
        Action()
    {
      initValues();
    }

    RunControlAction::~RunControlAction()
    {
    }

    /*
     * Initialize the action id values
     */
    void
    RunControlAction::initValues()
    {
      addValue(Action::getActionId(), VALUE_FOR_ACTION_EXEC_CMD);
    }

    // return the value for action id
    std::string
    RunControlAction::getValueForActionEXEC_CMD()
    {
      return VALUE_FOR_ACTION_EXEC_CMD;
    }

  }
}
