#include <AccessManager/xacml/Identifiers.h>
#include <AccessManager/xacml/impl/RDBResource.h>

namespace daq
{

  namespace am
  {

    // the RDB action
    const RDBAction RDBResource::rcAction;

    RDBResource::RDBResource(const std::string& commandName, const std::string& partitionName) :
        Resource("Run Control Resource")
    {

      //TYPES INITIALIZATION
      // add the command name and partition name
      // ... later when the values are checked

      //VALUES INITIALIZATION
      initValues();

      if (commandName.length() > 0)
        {
          addTypes(IDENTIFIER_RESOURCE_TYPE_COMMAND_NAME);
          addValue(IDENTIFIER_RESOURCE_TYPE_COMMAND_NAME, commandName);
        }

      if (partitionName.length() > 0)
        {
          addTypes(IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME);
          addValue(IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME, partitionName);
        }

      //ACTIONS INITIALIZATION
      //initialize the actions for the resource id
      addAction(getResourceId(), &rcAction);
    }

    RDBResource::~RDBResource()
    {

    }

    /*
     * Populate the list and maps from parent class with known values;
     */
    void
    RDBResource::initValues()
    {
      // set the Run Control category value
      addValue(getResourceCategory(), "RDB");
      addValue(getResourceId(), "rdb");
    }

    /*
     * The string value of TriggerCommander actions.
     */
    const std::string RDBResource::ACTION_VALUES[ACTIONS_COUNT] =
      { "OPEN", "CLOSE", "UPDATE", "RELOAD", "OPEN_SESSION", "LIST_SESSIONS" };

  }

}
