#include <AccessManager/xacml/Identifiers.h>
#include <AccessManager/xacml/impl/AMRMResource.h>

namespace daq
{
  namespace am
  {
    const std::string AMRMResource::OPERATION_TYPES[OPERATION_TYPES_COUNT] =
      { "admin", "role" };

    AMRMResource::AMRMResource(const std::string &resourceId, const std::string &name, const AMRMAction &action) :
        Resource("AMRM Resource")
    {
      initValues();
      addValue(getResourceId(), resourceId);

      if (name.length() > 0)
        {
          addTypes(IDENTIFIER_RESOURCE_TYPE_NAME);
          addValue(IDENTIFIER_RESOURCE_TYPE_NAME, name);
        }

      addAction(getResourceId(), &action);
    }

    void
    AMRMResource::initValues()
    {
      addValue(getResourceCategory(), "RolesAdmin");
    }

    AMRMResource::~AMRMResource()
    {
    }

    const AMRMResource*
    AMRMResource::getInstanceForAdminOperations()
    {
      return new AMRMResource(OPERATION_TYPES[OPERATION_TYPE_ID_ADMIN], "", AMRMAction::ACTIONS[AMRMAction::ACTION_ID_ADMIN]);
    }

    std::unique_ptr<const AMRMResource>
    AMRMResource::getAutoptrInstanceForAdminOperations()
    {
      return std::unique_ptr<const AMRMResource>(AMRMResource::getInstanceForAdminOperations());
    }

    const AMRMResource*
    AMRMResource::getInstanceForRoleOperations(const std::string &name, const int &ACTION_ID)
    {
      if (ACTION_ID >= 0 && ACTION_ID < AMRMAction::ACTIONS_COUNT)
        return new AMRMResource(OPERATION_TYPES[OPERATION_TYPE_ID_ROLE], name, AMRMAction::ACTIONS[ACTION_ID]);
      else
        return nullptr;
    }

    std::unique_ptr<const AMRMResource>
    AMRMResource::getAutoptrInstanceForRoleOperations(const std::string &name, const int &ACTION_ID)
    {
      return std::unique_ptr<const AMRMResource>(AMRMResource::getInstanceForRoleOperations(name, ACTION_ID));
    }

  }
}
