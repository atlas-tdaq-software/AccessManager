#include <iostream>
#include <string>
#include <AccessManager/xacml/impl/AMRMAction.h>

namespace daq
{
  namespace am
  {

    const std::string AMRMAction::ACTION_VALUES[ACTIONS_COUNT] =
      { "admin", "assign", "enable" };

    const AMRMAction AMRMAction::ACTIONS[ACTIONS_COUNT] = {
      AMRMAction(AMRMAction::ACTION_VALUES[AMRMAction::ACTION_ID_ADMIN]),
      AMRMAction(AMRMAction::ACTION_VALUES[AMRMAction::ACTION_ID_ASSIGN_ROLE]),
      AMRMAction(AMRMAction::ACTION_VALUES[AMRMAction::ACTION_ID_ENABLE_ROLE])
    };

    AMRMAction::AMRMAction(const std::string &actionValue) :
        Action()
    {
      initValues();
      addValue(Action::getActionId(), actionValue);
    }

    AMRMAction::~AMRMAction()
    {
    }

    void
    AMRMAction::initValues()
    {
    }

  }
}
