#include <AccessManager/xacml/Identifiers.h>
#include <AccessManager/xacml/impl/RMResource.h>

namespace daq
{

namespace am
{

// the RM possible actions: any
const RMAction RMResource::ACTION_ANY(RMAction::getValueForActionANY());

RMResource::RMResource(const std::string& partitionName, const RMAction& action):
	Resource("RM Resource"){

	//TYPES INITIALIZATION

	//VALUES INITIALIZATION
	initValues();

	// the partition name resource type
	if (partitionName.length() > 0){
		addTypes(IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME);
		addValue(IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME, partitionName);
	}

	//ACTIONS INITIALIZATION
	//initialize the actions for the resource id
	addAction(getResourceId(), &action);
}

/*
 * Populate the list and maps from parent class with known values;
 */
void RMResource::initValues(){
	// set the RM resource category value
	addValue(getResourceCategory(), "ResourceManager");
	// set the RM resource id value
	addValue(getResourceId(), "rm");
}

RMResource::~RMResource(){

}

const RMResource *RMResource::getInstanceForActionANY(const std::string& partitionName) {
	return new RMResource(partitionName, RMResource::ACTION_ANY);
}

std::unique_ptr<const RMResource> RMResource::getAutoptrInstanceForActionANY(const std::string& partitionName) {
	return std::unique_ptr<const RMResource>(RMResource::getInstanceForActionANY(partitionName));
}



}

}
