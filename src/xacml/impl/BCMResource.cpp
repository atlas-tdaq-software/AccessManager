#include <AccessManager/xacml/impl/BCMResource.h>

namespace daq
{
  namespace am
  {

    const BCMAction BCMResource::ACTION_CONFIGURE(BCMAction::getValueForActionCONFIGURE());
    const BCMAction BCMResource::ACTION_UPDATE(BCMAction::getValueForActionUPDATE());

    BCMResource::BCMResource(const BCMAction &action) :
        Resource("BCM Resource")
    {
      initValues();
      addAction(getResourceId(), &action);
    }

    void
    BCMResource::initValues()
    {
      addValue(getResourceCategory(), "BCM");
      addValue(getResourceId(), "bcm");
    }

    BCMResource::~BCMResource()
    {
    }

    std::unique_ptr<const BCMResource>
    BCMResource::getInstanceForActionCONFIGURE()
    {
      return std::unique_ptr<const BCMResource>(new BCMResource(BCMResource::ACTION_CONFIGURE));
    }

    std::unique_ptr<const BCMResource>
    BCMResource::getInstanceForActionUPDATE()
    {
      return std::unique_ptr<const BCMResource>(new BCMResource(BCMResource::ACTION_UPDATE));
    }

  }
}
