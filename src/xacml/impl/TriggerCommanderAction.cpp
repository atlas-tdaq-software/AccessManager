#include <AccessManager/xacml/impl/TriggerCommanderAction.h>

#define VALUE_FOR_ACTION_EXEC_CMD       "exec_cmd"

namespace daq
{

  namespace am
  {

    TriggerCommanderAction::TriggerCommanderAction() :
        Action()
    {
      initValues();
    }

    TriggerCommanderAction::~TriggerCommanderAction()
    {
    }

    /*
     * Initialize the action id values
     */
    void
    TriggerCommanderAction::initValues()
    {
      addValue(Action::getActionId(), VALUE_FOR_ACTION_EXEC_CMD);
    }

    // return the value for action id
    std::string
    TriggerCommanderAction::getValueForActionEXEC_CMD()
    {
      return VALUE_FOR_ACTION_EXEC_CMD;
    }

  }
}
