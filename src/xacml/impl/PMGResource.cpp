#include <AccessManager/xacml/Identifiers.h>
#include <AccessManager/xacml/impl/PMGResource.h>

namespace daq
{

namespace am
{

// the PMG possible actions: start and terminate
const PMGAction PMGResource::ACTION_START(PMGAction::getValueForActionSTART());
const PMGAction PMGResource::ACTION_TERMINATE(PMGAction::getValueForActionTERMINATE());


PMGResource::PMGResource():
	Resource("PMG Resource"){

	initValues();
	addValue(getResourceId(), getAnonymousProcessId());
}


PMGResource::PMGResource(	const std::string& processBinaryPath,
				const std::string& processHostName,
				const std::string& processArguments,
				const bool processOwnedByRequester,
				const std::string& partitionName):
	Resource("PMG Resource"){

	//TYPES INITIALIZATION
	// add the "hostname" and "arguments" method resource type
	// ... later, in the values initialization

	//VALUES INITIALIZATION
	initValues();
	// set the binary path as value for resource identifier
	addValue(getResourceId(), processBinaryPath);
	// the hostname and arguments resource types
	if (processHostName.length() > 0){
		addTypes(IDENTIFIER_RESOURCE_TYPE_HOSTNAME);
		addValue(IDENTIFIER_RESOURCE_TYPE_HOSTNAME, processHostName);
	}
	if (processArguments.length() > 0){
		addTypes(IDENTIFIER_RESOURCE_TYPE_ARGUMENTS);
		addValue(IDENTIFIER_RESOURCE_TYPE_ARGUMENTS, processArguments);
	}
        // the partition name resource type
        if (partitionName.length() > 0){
                addTypes(IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME);
                addValue(IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME, partitionName);
        }

	addTypes(IDENTIFIER_RESOURCE_TYPE_OWNED_BY_REQUESTER);
	addValue(IDENTIFIER_RESOURCE_TYPE_OWNED_BY_REQUESTER, (processOwnedByRequester)? "true": "false" );

	//ACTIONS INITIALIZATION
	//initialize the actions for the resource id
	// ...  done by the set methods
}

PMGResource::~PMGResource(){

}


// get the value for an anonymous process
std::string PMGResource::getAnonymousProcessId(){
	return "ANY:PROCESS";
}


void PMGResource::setStartAction(){
	//initialize the actions for the resource id
	addAction(getResourceId(), &ACTION_START);
}

void PMGResource::setTerminateAction(){
	//initialize the actions for the resource id
	addAction(getResourceId(), &ACTION_TERMINATE);
}

/*
 * Populate the list and maps from parent class with known values;
 */
void PMGResource::initValues(){
	// set the PMG category value
	addValue(getResourceCategory(), "pmg");
}


}

}
