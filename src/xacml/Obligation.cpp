#include <AccessManager/xacml/Obligation.h>

#include <AccessManager/xacml/attr/AttributeFactory.h>
#include <AccessManager/xacml/attr/AttributeValue.h>
#include <AccessManager/xacml/ctx/Result.h>

#include <AccessManager/util/XMLParsing.h>

#include <ostream>
#include <sstream>

namespace daq
{

namespace am
{

Obligation::Obligation(const std::string& id, const int& fulfillOn, const std::vector<Attribute>& assignments):
		id(id), fulfillOn(fulfillOn), assignments(assignments) {

}

Obligation::~Obligation(){

}

Obligation* Obligation::getInstance(const XERCES_CPP_NAMESPACE_QUALIFIER DOMNode& root){

	XERCES_CPP_NAMESPACE_USE

	std::string id = "";
	int fulfillOn = -1;
	std::vector<Attribute> assignments;

	ERS_DEBUG(3, "Preparing an Obligation instance from DOMNode");

	CREATE_DOM_ELEMENT(rootElement, &root);
    XMLCH_TO_STDSTRING(rootElement->getTagName(), elementName);

	if (elementName != "Obligation"){
		XACMLParsingException issue(ERS_HERE, "The node is [" + elementName + "]. Has to be Obligation!!");
		throw issue;
	}

	// check for obligation id attribute
	XMLCH_TO_STDSTRING(rootElement->getAttribute(XMLString::transcode("ObligationId")), _id);
   	ERS_DEBUG(3, "Obligation ID = [" << _id << "]");
   	if (_id.length() == 0){
		XACMLParsingException issue(ERS_HERE, "Error parsing required attribute: ObligationId");
		throw issue;
    }
	id = _id;

	// check for fulfillon attribute
	XMLCH_TO_STDSTRING(rootElement->getAttribute(XMLString::transcode("FulfillOn")), fulfillOnValue);
   	ERS_DEBUG(3, "Fulfill on = [" << fulfillOnValue << "]");
    if (fulfillOnValue == "Permit") {
        fulfillOn = Result::DECISION_PERMIT;
    } else if (fulfillOnValue == "Deny") {
        fulfillOn = Result::DECISION_DENY;
    } else {
        XACMLParsingException issue(ERS_HERE, "Invalid Effect type: " + fulfillOnValue);
		throw issue;
    }

    // the assignment attributes (minOcc = 0, maxOcc = unbounded)
	GET_CHILDREN_WITH_TAG_AND_LIMITS(assattrElements, rootElement, "AttributeAssignment", 0, MAX_OCC_UNBOUNDED);
	for(XMLSize_t idx = 0; idx < assattrElements->getLength(); idx++){
		CREATE_DOM_ELEMENT(assattrElement, assattrElements->item(idx));

		// get the attribute id
		XMLCH_TO_STDSTRING(assattrElement->getAttribute(XMLString::transcode("AttributeId")), attrId);

	    AttributeValue* attrValue = AttributeFactory::Instance()->createValue(*assattrElement);
	    Attribute attribute(attrId, attrValue);
	    assignments.push_back(attribute);
	}


    return new Obligation(id, fulfillOn, assignments);
}


void Obligation::encode(std::ostream& os, Indenter& indenter) const{

	// Prepare the indentation string
	std::string topIndent = indenter.makeString();
    ++indenter;
	std::string indent = indenter.makeString();
	++ indenter;

    // encode the starting tag
    os << topIndent << "<Obligation ObligationId=\"" << id
       << "\" FulfillOn=\"" << Result::DECISIONS[fulfillOn] << "\">" << std::endl;


	for(std::vector<Attribute>::const_iterator ait = assignments.begin(); ait != assignments.end(); ait++){
		const Attribute& attr = *ait;
		os << indent << "<AttributeAssignment "
		   << "AttributeId=\"" 	<< attr.getId() 	<< "\" "
		   << "DataType=\"" 	<< attr.getType()	<< "\">"
           << attr.getValue()
           << "</AttributeAssignment>" << std::endl;
	}

	--indenter;
    --indenter;

    os << topIndent << "</Obligation>" << std::endl;
}

// returns a string representation of XML formatted request
std::string Obligation::encode() const{
	std::ostringstream xmlstream;
	Indenter indt(0);
	encode(xmlstream, indt);
	return xmlstream.str();
}

// sends the encoded value to the output stream without indentation
std::ostream& operator<<(std::ostream& os, const Obligation& res){
	Indenter indt;
	res.encode(os, indt);
	return os;
}


// operator needed for std::set and other similar STL classes
bool operator<(const Obligation& left, const Obligation& right){
	return left.id < right.id || left.fulfillOn < right.fulfillOn || left.assignments < right.assignments;
}

}

}
