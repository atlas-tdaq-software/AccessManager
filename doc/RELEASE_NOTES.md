# AccessManager

## tdaq-11-02-00

Add `TDAQ_AM_CONFIGURATION_FILE` process environment variable to specify client configuration file. The `/sw/tdaq/AccessManager/cfg/client.cfg` file has a priority if exists.

Add `SETPRESCALESANDBUNCHGROUP` to the TriggerCommander interface commands.

Add RDB interface with `OPEN`, `CLOSE`, `UPDATE`, `RELOAD`, `OPEN_SESSION` and `LIST_SESSIONS` commands to be used by the RDB server.
