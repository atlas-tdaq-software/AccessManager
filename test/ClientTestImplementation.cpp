#include <test/ClientTestImplementation.h>

#include <AccessManager/util/ErsIssues.h>
#include <AccessManager/xacml/Resource.h>
#include <AccessManager/xacml/impl/BCMAction.h>
#include <AccessManager/xacml/impl/BCMResource.h>
#include <AccessManager/xacml/impl/DBAction.h>
#include <AccessManager/xacml/impl/DBResource.h>
#include <AccessManager/xacml/impl/AMRMResource.h>
#include <AccessManager/xacml/impl/IDLResource.h>
#include <AccessManager/xacml/impl/IGUIResource.h>
#include <AccessManager/xacml/impl/OSResource.h>
#include <AccessManager/xacml/impl/PMGResource.h>
#include <AccessManager/xacml/impl/RunControlResource.h>
#include <AccessManager/xacml/impl/TriggerCommanderResource.h>
#include <AccessManager/xacml/impl/RMResource.h>
#include <AccessManager/xacml/impl/RDBResource.h>
#include <AccessManager/client/RequestorInfo.h>
#include <AccessManager/client/ServerInterrogator.h>

#include <unistd.h>
#include <time.h>
#include <sys/time.h>

#define CLIENT_TEST_IMPLEMENTATION_VERSION		"$Revision$"


const std::string ClientTestImplementation::ENV_PARAMETER_SILENT("TDAQ_AM_CLIENT_TEST_SILENT");

// environment variable to specify the number of test iterations and the delay between iterations
const std::string ClientTestImplementation::ENV_PARAMETER_NUMBER_ITERATIONS("TDAQ_AM_CLIENT_TEST_NUMBER_ITERATIONS");
const std::string ClientTestImplementation::ENV_PARAMETER_DELAY_ITERATIONS("TDAQ_AM_CLIENT_TEST_DELAY_ITERATIONS");

//environment variables to specify the resource type and its attributes
const std::string ClientTestImplementation::ENV_PARAMETER_RESOURCE_TYPE("TDAQ_AM_CLIENT_TEST_REQ_RES_TYPE");
const std::string ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR1("TDAQ_AM_CLIENT_TEST_REQ_RES_ATTR1");
const std::string ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR2("TDAQ_AM_CLIENT_TEST_REQ_RES_ATTR2");
const std::string ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR3("TDAQ_AM_CLIENT_TEST_REQ_RES_ATTR3");
const std::string ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR4("TDAQ_AM_CLIENT_TEST_REQ_RES_ATTR4");
const std::string ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR5("TDAQ_AM_CLIENT_TEST_REQ_RES_ATTR5");

const std::string ClientTestImplementation::ENV_PARAMETER_RESOURCE_ACTION("TDAQ_AM_CLIENT_TEST_REQ_RES_ACTION");

//environment variables to explicitly provide the access subject username and hostname
const std::string ClientTestImplementation::ENV_PARAMETER_ACCESS_SUBJ_USERNAME("TDAQ_AM_CLIENT_TEST_ACCESS_SBJ_USERNAME");
const std::string ClientTestImplementation::ENV_PARAMETER_ACCESS_SUBJ_HOSTNAME("TDAQ_AM_CLIENT_TEST_ACCESS_SBJ_HOSTNAME");

/*
 *
 */
ClientTestImplementation::ClientTestImplementation(const std::string& _testId, std::ostream& _sout):
	testId(_testId),
	silent(false),
	nIterations(1),
	delayIterations(0),
	timing(true),
	averageExecutionTime(0.0),
	sout(_sout) {

	if ( ! testId.empty()) {
		testId = "(" + testId + ")";
	}
	// check the silent mode
	std::string silentMode;
	if (initFromEnv(silentMode, ClientTestImplementation::ENV_PARAMETER_SILENT)){
		// if the environment variable is set, the switch to silent mode
		silent = true;
	}

	// get the number of iterations from environment variable
	std::string sNIter;
	if (initFromEnv(sNIter, ClientTestImplementation::ENV_PARAMETER_NUMBER_ITERATIONS) && sNIter.size() > 0){
		std::istringstream myStream(sNIter);
		int i;
	  	if (myStream >> i)
	    	nIterations = i;
	  	else
	    	ERS_DEBUG(0, "Failed to convert to int the number of iterations from environment variable!" <<
	    			"(" << ClientTestImplementation::ENV_PARAMETER_NUMBER_ITERATIONS << "=" << sNIter << ")");
	}

	if (nIterations < 0){
		timing = false;
		nIterations = 1;
	}

	// get the delay between iterations
	// get the number of iterations from environment variable
	std::string sDelayIter;
	if (initFromEnv(sDelayIter, ClientTestImplementation::ENV_PARAMETER_DELAY_ITERATIONS) && sDelayIter.size() > 0){
		std::istringstream myStream(sDelayIter);
		useconds_t u;
	  	if (myStream >> u)
	    	delayIterations = u * 1000; // transform the milliseconds in microseconds
	  	else
	    	ERS_DEBUG(0, "Failed to convert to useconds_t the delay between iterations from environment variable!" <<
	    			"(" << ClientTestImplementation::ENV_PARAMETER_DELAY_ITERATIONS << "=" << sDelayIter << ")");
	}
}

ClientTestImplementation::~ClientTestImplementation(){

}


template<typename T, std::size_t N> std::string
print_actions_helper(T (&values)[N])
{
  std::string out;

  for(std::size_t idx = 0; idx < N; ++idx)
    {
      out.append(values[idx]);
      out.push_back(idx == (N-1) ? '\n' : '/');
    }

  return out;
}


/*
 *
 */
const std::string
ClientTestImplementation::printHelp() const
{
  using namespace std;
  std::stringstream help;

  help << "Client test implementation version: " << CLIENT_TEST_IMPLEMENTATION_VERSION << '\n'
    << "Environment variables needed to drive the test application:\n"
    << "  " << ClientTestImplementation::ENV_PARAMETER_RESOURCE_TYPE << " : " << " the resource type to be accessed.\n"
    << "        The valid values are: [pmg] [idl] [rc] [tc] [igui] [os] [rm] [db] [rdb] [amrm] [bcm]\n"
    << '\n'
    << "  " << ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR1 << " : " << " attribute #1 needed by the selected resource type:\n"
    << "        [pmg]-> process binary path\n"
    << "        [bcm]-> action to be performed: " <<
      daq::am::BCMAction::getValueForActionCONFIGURE() << '/' <<
      daq::am::BCMAction::getValueForActionUPDATE() << '\n'
    << "        [idl]-> IDL interface name\n"
    << "        [rc]-> command name: " << print_actions_helper(daq::am::RunControlResource::ACTION_VALUES)
    << "        [tc]-> command name: " << print_actions_helper(daq::am::TriggerCommanderResource::ACTION_VALUES)
    << "        [igui]-> view mode: " << print_actions_helper(daq::am::IGUIResource::VIEW_MODES)
    << "        [os]-> the resource location: crd, gateway\n"
    << "        [rm]-> the partition name\n"
    << "        [db]-> the resource id: " << print_actions_helper(daq::am::DBResource::OPERATION_TYPES)
    << "        [rdb]-> command name: " << print_actions_helper(daq::am::RDBResource::ACTION_VALUES)
    << "        [amrm]-> the resource id: " << print_actions_helper(daq::am::AMRMResource::OPERATION_TYPES)
    << '\n'
    << "  " << ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR2 << " : " << " attribute #2 needed by the selected resource type.\n"
    << "        [pmg]-> process host name\n"
    << "        [idl]-> IDL method\n"
    << "        [rc]-> partition name\n"
    << "        [tc]-> partition name\n"
    << "        [os]-> the application: for crd -> shell, for gateway -> login\n"
    << "        [db]-> the path to the file or directory\n"
    << "        [rdb]-> partition name\n"
    << "        [amrm]-> the name of the role\n"
    << '\n'
    << "  " << ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR3 << " : " << " attribute #3 needed by the selected resource type.\n"
    << "        [pmg]-> process arguments\n"
    << "        [os]-> the arguments\n"
    << '\n'
    << "  " << ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR4 << " : " << " attribute #4 needed by the selected resource type.\n"
    << "        [pmg]-> process owned by requester (true/false)\n"
    << '\n'
    << "  " << ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR5 << " : " << " attribute #5 needed by the selected resource type.\n"
    << "        [pmg]-> the partition name\n"
    << '\n'
    << "  " << ClientTestImplementation::ENV_PARAMETER_RESOURCE_ACTION << " : " << " the action for the resource\n"
    << "        [pmg]-> action to be performed: " <<
      daq::am::PMGAction::getValueForActionSTART() << '/' <<
      daq::am::PMGAction::getValueForActionTERMINATE() << '\n'
    << "        [idl]-> <not required>\n"
    << "        [rc]-> <not required>\n"
    << "        [tc]-> <not required>\n"
    << "        [igui]-> <not required>\n"
    << "        [os]->  the actions: for shell -> open, for login-> remote or inside\n"
    << "        [rm]-> the action: any\n"
    << "        [bcm]-> the action: configure, update\n"
    << "        [db]-> the action to perform on directory (" <<
      daq::am::DBResource::ACTION_VALUES[daq::am::DBResource::ACTION_ID_CREATE_FILE] << '/' <<
      daq::am::DBResource::ACTION_VALUES[daq::am::DBResource::ACTION_ID_UPDATE_FILE] << '/' <<
      daq::am::DBResource::ACTION_VALUES[daq::am::DBResource::ACTION_ID_DELETE_FILE] << '/' <<
      daq::am::DBResource::ACTION_VALUES[daq::am::DBResource::ACTION_ID_CREATE_SUBDIR] << '/' <<
      daq::am::DBResource::ACTION_VALUES[daq::am::DBResource::ACTION_ID_DELETE_SUBDIR]
    << ") or file (" <<
      daq::am::DBResource::ACTION_VALUES[daq::am::DBResource::ACTION_ID_UPDATE_FILE] << ")\n"
    << "        [rdb]-> <not required>\n"
    << "        [amrm]-> the action to perform on role (" <<
      daq::am::AMRMAction::ACTION_VALUES[daq::am::AMRMAction::ACTION_ID_ASSIGN_ROLE] << '/' <<
      daq::am::AMRMAction::ACTION_VALUES[daq::am::AMRMAction::ACTION_ID_ENABLE_ROLE] << ")\n"
    << '\n'
    << "  " << ClientTestImplementation::ENV_PARAMETER_ACCESS_SUBJ_USERNAME << " : " << " the username of the access subject if different from the current one\n"
    << '\n'
    << "  " << ClientTestImplementation::ENV_PARAMETER_ACCESS_SUBJ_HOSTNAME << " : " << " the hostname of the access subject if different from the current one\n"
    << '\n'
    << "  " << ClientTestImplementation::ENV_PARAMETER_NUMBER_ITERATIONS << " : " << " the number of test iterations.\n"
    << "        Default is [1]. If negative, then one iteration with no timing.\n"
    << '\n'
    << "  " << ClientTestImplementation::ENV_PARAMETER_DELAY_ITERATIONS << " : " << " the number of milliseconds to sleep between test iterations.\n"
    << "        Default is [0]\n"
    << '\n';
  return help.str();
}

// get the value of an environment variable and put it in the variable var
// return true if the variable has been found, if not false
bool ClientTestImplementation::initFromEnv(std::string &var, const std::string& envParam) const {
	char *envValue;
	var = "";
	if ((envValue = getenv(envParam.c_str())) != NULL){
		var = envValue;
		ERS_DEBUG(2, "Environment variable [" << envParam << "]=[" << var << "]");
		return true;
	} else {
		ERS_DEBUG(2, "Environment variable [" << envParam << "] not found!");
		return false;
	}
}

/*
 * Get a resource instant depending on the resource category
 */
std::unique_ptr<const daq::am::Resource>
ClientTestImplementation::getResourceInstance(const std::string& resourceType)
{

  std::unique_ptr<const daq::am::Resource> resource(nullptr);

  if (resourceType == "pmg")
    {
      // >>>> PMG resource type
      std::string action;
      initFromEnv(action, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ACTION);
      if (action != "start" && action != "terminate")
        {
          ERS_INFO("Invalid pmg action value [" << action << "]");
          return std::unique_ptr<const daq::am::Resource>(nullptr);
        }

      std::string processBinaryPath;
      initFromEnv(processBinaryPath, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR1);
      std::string processHostName;
      initFromEnv(processHostName, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR2);
      std::string processArguments;
      initFromEnv(processArguments, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR3);
      std::string processOwnedByReq;
      initFromEnv(processOwnedByReq, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR4);
      std::string partitionName;
      initFromEnv(partitionName, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR5);

      bool processOwnedByRequester = false;
      if (processOwnedByReq == "true")
        {
          processOwnedByRequester = true;
        }

      if (!silent)
        {
          sout << "[RESOURCE_TYPE=" << resourceType << "][ACTION=" << action << "][PARTITION=" << partitionName << "][BinaryPath=" << processBinaryPath << "] [HostName=" << processHostName << "] [ARGS=" << processArguments << "] [ProcessOwenedByRequester="
              << processOwnedByRequester << "]";
        }

      daq::am::PMGResource *pmgRes = new daq::am::PMGResource(processBinaryPath, processHostName, processArguments, processOwnedByRequester, partitionName);

      if (action == "start")
        {
          pmgRes->setStartAction();
        }
      else if (action == "terminate")
        {
          pmgRes->setTerminateAction();
        }

      resource.reset(pmgRes);

    }
  else if (resourceType == "idl")
    {
      // >>>> IDL resource type
      std::string idlInterface;
      initFromEnv(idlInterface, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR1);
      std::string idlMethod;
      initFromEnv(idlMethod, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR2);

      if (!silent)
        {
          sout << "[RESOURCE_TYPE=" << resourceType << "][IDL_INTERFACE=" << idlInterface << "][IDL_METHOD=" << idlMethod << "]";
        }

      resource.reset(new daq::am::IDLResource(idlInterface, idlMethod));

    }
  else if (resourceType == "rc")
    {
      // >>>> Run Control resource type
      std::string commandName;
      initFromEnv(commandName, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR1);
      std::string partitionName;
      initFromEnv(partitionName, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR2);

      if (!silent)
        {
          sout << "[RESOURCE_TYPE=" << resourceType << "][COMMAND=" << commandName << "][PARTITION=" << partitionName << "]";
        }

      resource.reset(new daq::am::RunControlResource(commandName, partitionName));

    }
  else if (resourceType == "tc")
    {
      // >>>> Trigger Commander resource type
      std::string commandName;
      initFromEnv(commandName, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR1);
      std::string partitionName;
      initFromEnv(partitionName, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR2);

      if (!silent)
        {
          sout << "[RESOURCE_TYPE=" << resourceType << "][COMMAND=" << commandName << "][PARTITION=" << partitionName << "]";
        }

      resource.reset(new daq::am::TriggerCommanderResource(commandName, partitionName));

    }
  else if (resourceType == "igui")
    {
      // >>>> Run Control resource type
      std::string viewMode;
      initFromEnv(viewMode, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR1);

      if (!silent)
        {
          sout << "[RESOURCE_TYPE=" << resourceType << "][VIEW_MODE=" << viewMode << "]";
        }

      int viewModeId = -1;
      for (int j = 0; j < daq::am::IGUIResource::VIEW_MODES_COUNT; j++)
        {
          if (daq::am::IGUIResource::VIEW_MODES[j] == viewMode)
            {
              viewModeId = j;
              break;
            }
        }
      if (viewModeId < 0)
        {
          ERS_INFO("Invalid view mode value [" << viewMode << "]");
          return resource;
        }

      resource = daq::am::IGUIResource::getInstanceForViewMode(viewModeId);

    }
  else if (resourceType == "os")
    {
      // >>>> Operating System resource type
      std::string resourceLocation;
      initFromEnv(resourceLocation, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR1);

      std::string application;
      initFromEnv(application, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR2);

      std::string arguments;
      initFromEnv(arguments, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR3);

      std::string action;
      initFromEnv(action, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ACTION);

      if (!silent)
        {
          sout << "[RESOURCE_TYPE=" << resourceType << "]\t" << "[RESOURCE_LOC=" << resourceLocation << "]\t" << "[APP=" << application << "]\t";
          if (arguments.length() > 0)
            {
              sout << "[ARG=" << arguments << "]\t";
            }
          sout << "[ACTION=" << action << "]" << std::endl;
        }

      daq::am::OSResource *osRes = new daq::am::OSResource(resourceLocation, application, arguments);
      osRes->setAction(action);

      resource.reset(osRes);

    }
  else if (resourceType == "rm")
    {
      // >>>> Resource Manager resource type
      std::string partitionName;
      initFromEnv(partitionName, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR1);

      std::string action;
      initFromEnv(action, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ACTION);

      if (!silent)
        {
          sout << "[RESOURCE_TYPE=" << resourceType << "][PARTITION=" << partitionName << "]ACTION=[" << action << "]";
        }

      if (daq::am::RMAction::getValueForActionANY() == action)
        {
          resource.reset(daq::am::RMResource::getInstanceForActionANY(partitionName));
        }
      else
        {
          ERS_INFO("Invalid rm action value [" << action << "]");
          return resource;
        }

    }
  else if (resourceType == "db")
    {
      // >>>> DataBase resource type
      std::string resourceId;
      initFromEnv(resourceId, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR1);

      std::string path;
      initFromEnv(path, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR2);

      std::string action;
      initFromEnv(action, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ACTION);

      if (!silent)
        {
          sout << "[RESOURCE_TYPE=" << resourceType << "][ResourceId=" << resourceId << "][PATH=" << path << "]ACTION=[" << action << "]";
        }

      int actionId = -1;
      if (resourceId != daq::am::DBResource::OPERATION_TYPES[daq::am::DBResource::OPERATION_TYPE_ID_ADMIN])
        {
          for (int j = 0; j < daq::am::DBResource::ACTIONS_COUNT; j++)
            {
              if (daq::am::DBResource::ACTION_VALUES[j] == action)
                {
                  actionId = j;
                  break;
                }
            }
          if (actionId < 0)
            {
              ERS_INFO("Invalid db action value [" << action << "]");
              return resource;
            }
        }

      if (resourceId == daq::am::DBResource::OPERATION_TYPES[daq::am::DBResource::OPERATION_TYPE_ID_ADMIN])
        {
          resource = daq::am::DBResource::getAutoptrInstanceForAdminOperations();
        }
      else if (resourceId == daq::am::DBResource::OPERATION_TYPES[daq::am::DBResource::OPERATION_TYPE_ID_DIRECTORY])
        {
          resource = daq::am::DBResource::getAutoptrInstanceForDirectoryOperations(path, actionId);
        }
      else if (resourceId == daq::am::DBResource::OPERATION_TYPES[daq::am::DBResource::OPERATION_TYPE_ID_FILE])
        {
          resource = daq::am::DBResource::getAutoptrInstanceForFileOperations(path, actionId);
        }

    }
  else if (resourceType == "rdb")
    {
      // >>>> Run Control resource type
      std::string commandName;
      initFromEnv(commandName, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR1);
      std::string partitionName;
      initFromEnv(partitionName, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR2);

      if (!silent)
        {
          sout << "[RESOURCE_TYPE=" << resourceType << "][COMMAND=" << commandName << "][PARTITION=" << partitionName << "]";
        }

      resource.reset(new daq::am::RDBResource(commandName, partitionName));

    }
  else if (resourceType == "amrm")
    {
      // >>>> DataBase resource type
      std::string resourceId;
      initFromEnv(resourceId, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR1);

      std::string name;
      initFromEnv(name, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ATTR2);

      std::string action;
      initFromEnv(action, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ACTION);

      if (!silent)
        {
          sout << "[RESOURCE_TYPE=" << resourceType << "][ResourceId=" << resourceId << "][NAME=" << name << "]ACTION=[" << action << "]";
        }

      int actionId = -1;
      if (resourceId != daq::am::AMRMResource::OPERATION_TYPES[daq::am::AMRMResource::OPERATION_TYPE_ID_ADMIN])
        {
          for (int j = 0; j < daq::am::AMRMAction::ACTIONS_COUNT; j++)
            {
              if (daq::am::AMRMAction::ACTION_VALUES[j] == action)
                {
                  actionId = j;
                  break;
                }
            }
          if (actionId < 0)
            {
              ERS_INFO("Invalid amrm action value [" << action << "]");
              return resource;
            }
        }

      if (resourceId == daq::am::AMRMResource::OPERATION_TYPES[daq::am::AMRMResource::OPERATION_TYPE_ID_ADMIN])
        {
          resource = daq::am::AMRMResource::getAutoptrInstanceForAdminOperations();
        }
      else if (resourceId == daq::am::AMRMResource::OPERATION_TYPES[daq::am::AMRMResource::OPERATION_TYPE_ID_ROLE])
        {
          resource = daq::am::AMRMResource::getAutoptrInstanceForRoleOperations(name, actionId);
        }
    }
  else if (resourceType == "bcm")
    {
      // >>>> Resource Manager resource type

      std::string action;
      initFromEnv(action, ClientTestImplementation::ENV_PARAMETER_RESOURCE_ACTION);

      if (!silent)
        {
          sout << "[RESOURCE_TYPE=" << resourceType << "]ACTION=[" << action << "]";
        }

      if (daq::am::BCMAction::getValueForActionCONFIGURE() == action)
        {
          resource = daq::am::BCMResource::getInstanceForActionCONFIGURE();
        }
      else if (daq::am::BCMAction::getValueForActionUPDATE() == action)
        {
          resource = daq::am::BCMResource::getInstanceForActionUPDATE();
        }
      else
        {
          ERS_INFO("Invalid bcm action value [" << action << "]");
        }

    }
  else
    {
      sout << "Resource type [" << resourceType << "] not implemented!" << std::endl;
    }

  return resource;
}

/*
 * Run the test a number of nIterations times
 */
int ClientTestImplementation::run() {

	int retcode = 0;

	if (timing && !silent) {
		if (nIterations > 1) {
			sout << testId << "###" << " Number of iterations:\t" << nIterations << std::endl;
		}

		if (delayIterations > 0) {
			sout << testId << "###" << " Delay between iterations (microseconds):\t" << delayIterations << std::endl;
		}
	}


	// check the resource type environment variable
	std::string resourceType;
	if (! initFromEnv(resourceType, ClientTestImplementation::ENV_PARAMETER_RESOURCE_TYPE)){
		return 1;
	}

	double sumExecTimes = 0;

	// repeat the test "nIter" times
	for(int iter = 1; iter <= nIterations; iter++){

		if (timing && !silent && nIterations > 1) {
			sout << testId << "#" << iter << "\t";
		}

		if (iter > 1){
			usleep(delayIterations);
		}

		struct timeval timeStart, timeFinish;

		// initialize the start time
		gettimeofday(&timeStart,NULL);

		const std::unique_ptr<const daq::am::Resource> resource = getResourceInstance(resourceType);
		if (resource.get() == NULL) {
			return 1;
		}

		// check the access subject environment variable
		std::string asUserName;
		initFromEnv(asUserName, ClientTestImplementation::ENV_PARAMETER_ACCESS_SUBJ_USERNAME);

		std::string asHostName;
		initFromEnv(asHostName, ClientTestImplementation::ENV_PARAMETER_ACCESS_SUBJ_HOSTNAME);

		// initialize the access subject object
		std::unique_ptr<daq::am::RequestorInfo> pAccessSubject;
		if ( asUserName.length() > 0 ){
			pAccessSubject.reset(new daq::am::RequestorInfo(asUserName, asHostName));
		}

		daq::am::ServerInterrogator si;

		bool allowed = false;
		try{

			if (NULL != pAccessSubject.get()){
				allowed = si.isAuthorizationGranted(*resource, *pAccessSubject);
			}else {
				allowed = si.isAuthorizationGranted(*resource);
			}

			if (allowed){
				sout << "[AUTHORIZATION=ALLOWED]";
			} else {
				sout << "[AUTHORIZATION=DENIED]";
			}

		} catch (daq::am::NetworkError& ex){
			ers::error(ex);
			retcode += 10;
		} catch (daq::am::CommunicationIssue& ex){
			ers::error(ex);
			retcode += 10;
		} catch (daq::am::XACMLIssue& ex){
			ers::error(ex);
			retcode += 100;
		} catch (daq::am::ServerProcessingIssue& ex){
			ers::error(ex);
			retcode += 1000;
		} catch (daq::am::Exception& ex){
			ers::error(ex);
			retcode += 10000;
		} catch (...) {
			ERS_INFO("Can not decode the response! ");
			throw;
		}

		if (timing) {
			sout << "\nstatus_msg=\t" << si.getStatusMessage();
		} else {
			sout << "\n" << si.getStatusMessage() << std::endl;
		}

		// get the finish time
		gettimeofday(&timeFinish,NULL);

		// compute the execution time in milliseconds
		double execTime = (timeFinish.tv_sec  - timeStart.tv_sec)  * 1000.0 +
						  (timeFinish.tv_usec - timeStart.tv_usec) / 1000.0;

		if (timing && !silent) {
			sout << "\tauth_req_duration(ms)=\t" << execTime << std::endl;
		}

		// skip the first iteration because is longer than the others
		sumExecTimes += (iter==1) ? 0 : execTime;
	}

	// print the average
	if (nIterations > 1 ) {
		this->averageExecutionTime = sumExecTimes/(nIterations-1);
		sout << testId << "### Average execution times(ms) for the last " << nIterations-1 << " iterations =\t"
			 << this->averageExecutionTime << std::endl;
	}

	return retcode;
}
