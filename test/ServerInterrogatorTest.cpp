#include <test/ClientTestImplementation.h>

#define AM_SERVER_INTERROGATOR_TEST_VERSION		"$Revision$"

#include <iostream>

using namespace std;

void printHelp(){
	cout << "Access Manager Server Interrogator test application for C++ API." << endl
		 << "Version: " << AM_SERVER_INTERROGATOR_TEST_VERSION << " built on "
		 << __DATE__ << " " << __TIME__ << endl;
}


int main() {

	int retcode = 0;
	ClientTestImplementation clientTest;
	
	retcode = clientTest.run();
	if (retcode == 1) {
		printHelp();
		cout << clientTest.printHelp();
	}

	return retcode;
}
