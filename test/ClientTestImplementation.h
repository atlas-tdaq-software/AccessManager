#ifndef CLIENTTESTIMPLEMENTATION_H_
#define CLIENTTESTIMPLEMENTATION_H_

#include <AccessManager/xacml/Resource.h>

#include <string>
#include <iostream>
#include <memory>

class ClientTestImplementation
{
private:

	std::string testId;

	/*
	 * test specific variables
	 */
	bool 			silent;
	int  			nIterations;
	useconds_t 		delayIterations;
	bool 			timing;
	float			averageExecutionTime;
	std::ostream&	sout;

	/*
	 *  get the value of an environment variable and put it in the variable var
	 * return true if the variable has been found, if not false
	 */
	bool initFromEnv(std::string &var, const std::string& envParam) const;

	/*
	 * Get a Resource instance based on the environment variables
	 */
	std::unique_ptr<const daq::am::Resource> getResourceInstance(const std::string& resourceType);

public:
	static const std::string ENV_PARAMETER_SILENT;

	// environment variable to specify the number of test iterations and the delay between iterations
	static const std::string ENV_PARAMETER_NUMBER_ITERATIONS;
	static const std::string ENV_PARAMETER_DELAY_ITERATIONS;

	//environment variables to specify the resource type and its attributes
	static const std::string ENV_PARAMETER_RESOURCE_TYPE;
	static const std::string ENV_PARAMETER_RESOURCE_ATTR1;
	static const std::string ENV_PARAMETER_RESOURCE_ATTR2;
	static const std::string ENV_PARAMETER_RESOURCE_ATTR3;
	static const std::string ENV_PARAMETER_RESOURCE_ATTR4;
	static const std::string ENV_PARAMETER_RESOURCE_ATTR5;
	
	static const std::string ENV_PARAMETER_RESOURCE_ACTION;

	//environment variables to explicitly provide the access subject username and hostname
	static const std::string ENV_PARAMETER_ACCESS_SUBJ_USERNAME;
	static const std::string ENV_PARAMETER_ACCESS_SUBJ_HOSTNAME;


	ClientTestImplementation(const std::string& testId = "", std::ostream& sout = std::cout);
	virtual ~ClientTestImplementation();

	/*
	 * Run the tests
	 * Return 0 if the function ran OK.
	 */
	int run();

	/*
	 * Print the help screen with the valid values for attributes
	 */
	const std::string printHelp() const;

	/*
	 * Get the average execution time
	 */
	inline float getAverageExecutionTime() const { return averageExecutionTime; }

};

#endif /*CLIENTTESTIMPLEMENTATION_H_*/
