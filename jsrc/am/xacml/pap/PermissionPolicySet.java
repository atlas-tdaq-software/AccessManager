package am.xacml.pap;

import java.net.URI;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;
import java.util.logging.Logger;

import am.util.AMException;
import am.xacml.context.Resource;

import com.sun.xacml.AbstractPolicy;
import com.sun.xacml.PolicyReference;
import com.sun.xacml.PolicySet;
import com.sun.xacml.combine.FirstApplicablePolicyAlg;

/**
 * Builds a Permission Policy Set specific to the Role Based Access Control model.
 * @author mleahu
 *
 */
public class PermissionPolicySet extends GenericRBACPolicy {
	
	/**
	 * The default combining algorithm
	 */
	private static final String DEFAULT_COMBINING_ALG = 
		FirstApplicablePolicyAlg.algId;
	
	/**
	 * URL prefix for the policy ID for a policy that gathers rules in a PPS
	 */
	private static final URI PPS_RULES_ID_URI_PREFIX = URI.create("ppsrules");
	
	/**
	 * URL prefix for the policy ID for a role specific policy that gathers references to other PPS 
	 */
	private static final URI PPS_ROLES_ID_URI_PREFIX = URI.create("ppsroles");
	
	/**
	 * Policy Set object
	 */
	private PolicySet policySet;
	
	/**
	 * List of policies contained or referenced by this policy set.
	 */
	private Map<URI, PolicyReference> policiesReferences;
		
    private static final Logger logger =
        Logger.getLogger(PermissionPolicySet.class.getName());
	
    
	/**
	 * The constructor takes as arguments the id of this policy and its description.
	 * @param policySetId the policy id; should be in an URI format
	 * @param policySetDescription description of the policy
	 */
	/**
	 * The constructor takes as arguments the id of this policy and its description.
	 * @param policySetId the policy id;
	 * @param policySetDescription description of the policy
	 * @param PPSforRulesSet true if this PPS will gather references to rules 
	 * 						 false if this PPS will gather references to other PPS. Usually this PPS is referenced directly by a RPS 
	 */
	public PermissionPolicySet(String policySetId, String policySetDescription, boolean PPSforRulesSet){
		
		super(policySetId, policySetDescription, DEFAULT_COMBINING_ALG);
		addPolicyIdPrefix((PPSforRulesSet)? PPS_RULES_ID_URI_PREFIX : PPS_ROLES_ID_URI_PREFIX);
		
		policySet = null;
		policiesReferences = new Hashtable<URI, PolicyReference>();

		logger.info("Permission Policy Set constructor:" + policySetDescription);
	}
		
	/**
	 * Initialize the target of this permission policy set.
	 * Only the resource category attribute of Resource object is considered
	 * at this level. The others are processed in the Permission Policy
	 * objects.
	 * @param resource resource object containing also the resource's action
	 */
	public void setTarget(Resource resource){
		policyResourcesTarget.clear();
		policyActionsTarget.clear();
		updatePolicyResourceAndActionTargets(resource);
	}
		
	/**
	 * Include in this policy set a policy reference.
	 * @param policyRef policy reference to be included
	 * @return true if the operation is a success, false otherwise
	 */
	public boolean addPolicyReference(PolicyReference policyRef){
		if (!policiesReferences.containsKey(policyRef.getReference())) {
			policiesReferences.put(policyRef.getReference(), policyRef);
			logger.fine("Add policy reference [" + policyRef.getReference() + "]");
			return true;
		} else {
			logger.fine("Add policy reference [" + policyRef.getReference() + "] ALREADY EXISTS!");
			return false;
		}
	}
	
	/**
	 * Include in this policy set a permission policy. The inclusion is actually
	 * a reference to the policy.
	 * @param policy policy to be included
	 * @return true if the operation is a success, false otherwise
	 */
	public boolean addPermissionPolicy(PermissionPolicy policy){
		return addPolicyReference(policy.getPolicyReference());
	}
	
	/**
	 * Add a reference to the Permission Policy Set of a junior role.
	 * This is the implementation of the role hierarchy mechanism in RBAC.
	 * @param juniorPPS the junior permission policy set 
	 * @return true if the operation is successful, false otherwise
	 */
	public boolean addJuniorPPS(PermissionPolicySet juniorPPS){
		return addPolicyReference(juniorPPS.getPolicySetReference());
	}
	
	
	/**
	 * Build the policy set object
	 * @return true if the operation was successful, false otherwise
	 */
	private PolicySet buildPolicySet() {
		ArrayList<PolicyReference> policiesReferencesList = 
			new ArrayList<PolicyReference>(policiesReferences.values().size());
		policiesReferencesList.addAll(policiesReferences.values());
		// create the policy set object
		try {
			policySet = new PolicySet(
					getPolicyId(),
					null,
					createPolicyCombinigAlgorithm(getCombiningAlg()),
					getPolicyDescription(),
					createPolicyTarget(),
					policiesReferencesList);
		} catch (AMException e) {
			logger.severe("Policy object could not be built:" + e.getMessage());
			e.printStackTrace();
		}
		
		return policySet;
	}
	
	/* (non-Javadoc)
	 * @see am.xacml.pap.GenericRBACPolicy#getUpdatedPolicy()
	 */
	public AbstractPolicy getUpdatedPolicy(){
		return buildPolicySet();
	}
	
	/* (non-Javadoc)
	 * @see am.xacml.pap.GenericRBACPolicy#getPolicy()
	 */
	public AbstractPolicy getPolicy(){
		return policySet;
	}
	
	/* (non-Javadoc)
	 * @see am.xacml.pap.GenericRBACPolicy#isPolicySet()
	 */
	public boolean isPolicySet(){
		return true;
	}

	/* (non-Javadoc)
	 * @see am.xacml.pap.GenericRBACPolicy#getResourceAttributeIDForPolicyTarget(am.xacml.context.Resource)
	 */
	@Override
	protected URI getResourceAttributeIDForPolicyTarget(Resource resource) {
		return resource.getAttributeIDForPPSTarget();
	}
}
