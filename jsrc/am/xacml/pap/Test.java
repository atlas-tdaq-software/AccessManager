package am.xacml.pap;

import java.util.logging.*;

import com.sun.xacml.*;

import am.rbac.*;
import am.xacml.context.Resource;
import am.xacml.context.impl.IDLResource;


public class Test {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		Handler fh = new FileHandler("am.xacml.pap.log");
		fh.setFormatter(new SimpleFormatter());
		Logger.getLogger("am.xacml.pap").addHandler(fh);
		Logger.getLogger("am.xacml.pap").setLevel(Level.ALL);
		
		
		PermissionPolicy pp = new PermissionPolicy("amServer", "Description: amServer");
		
		System.out.println(pp);
		
		Resource res = new IDLResource("IDL:am/amServer:1.0","_get_information");

		pp.allowResourceAction(res);
				
		
		Policy p = (Policy)pp.getUpdatedPolicy();
		p.encode(System.out, new Indenter());
		
		
		
		System.out.println("POLICY SET");
		
		PermissionPolicySet pps = new PermissionPolicySet(
				"amServer", 
				"Description: pps amServer",
				true); 
		
		pps.addPermissionPolicy(pp);
		pps.addJuniorPPS(pps);
		pps.setTarget(res);
		
		PolicySet policySet = (PolicySet)pps.getUpdatedPolicy();
		policySet.encode(System.out, new Indenter());
		
		System.out.println("ROLE POLICY SET");
		RolePolicySet rps = new RolePolicySet("observer", "Description: Role Policy Set for Observer");
		Role role = new RoleImpl("role:observer", false);
		rps.addTarget(role);
		rps.setPermissionPolicySet(pps);
		
		PolicySet rolePolicySet = (PolicySet)rps.getUpdatedPolicy();
		rolePolicySet.encode(System.out, new Indenter());

	}		
}
