package am.xacml.pap;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import am.rbac.Role;
import am.util.AMException;
import am.xacml.context.AttributeIDsValues;
import am.xacml.context.Resource;

import com.sun.xacml.AbstractPolicy;
import com.sun.xacml.PolicyReference;
import com.sun.xacml.PolicySet;
import com.sun.xacml.TargetMatch;
import com.sun.xacml.UnknownIdentifierException;
import com.sun.xacml.attr.AttributeDesignator;
import com.sun.xacml.attr.AttributeValue;
import com.sun.xacml.combine.FirstApplicablePolicyAlg;
import com.sun.xacml.cond.Function;
import com.sun.xacml.cond.FunctionFactory;
import com.sun.xacml.cond.FunctionTypeException;


/**
 * Builds a Role Policy Set to represent the policies associated with roles
 * as specified in the RBAC model.
 * The target of the RPS should not restrict the resource, action or environment.
 * Hence the subject of the target is the only field that can be set.
 * Also, the RPS can reference one and only one PPS.
 * @author mleahu
 *
 */
public class RolePolicySet extends GenericRBACPolicy {
	
	
	/**
	 * The default combining algorithm
	 */
	private static final String DEFAULT_COMBINING_ALG = 
		FirstApplicablePolicyAlg.algId;
	
	/**
	 * URL prefix for the policy ID 
	 */
	private static final URI RPS_ID_URI_PREFIX = URI.create("rps");
		
	/**
	 * Policy Set object
	 */
	private PolicySet rolePolicySet;
	
	/**
	 * The permission policy set used by this RPS
	 */
	protected PolicyReference PPSPolicyReference;
	
	/**
	 * The RPS referenced by this RPS
	 */
	protected List<PolicyReference> policyReferences;
	
	/**
	 * The list of roles comprised in the target. 
	 */
	protected List<Role> roles;
		
    private static final Logger logger =
        Logger.getLogger(RolePolicySet.class.getName());
	    
	/**
	 * The constructor takes as arguments the id of this policy, its description and
	 * the permission policy set.
	 * @param rolePolicySetId the policy id; should be in an URI format
	 * @param rolePolicySetDescription description of the policy
	 * @param permissionPolicySet the only permission policy set used by this RPS
	 */
	public RolePolicySet(
			String rolePolicySetId,
			String rolePolicySetDescription,
			PermissionPolicySet permissionPolicySet)throws AMException{
		
		super(rolePolicySetId, rolePolicySetDescription, DEFAULT_COMBINING_ALG);
		
		addPolicyIdPrefix(RPS_ID_URI_PREFIX);
		
		rolePolicySet = null;
		roles = new ArrayList<Role>();
		PPSPolicyReference = null;
		policyReferences = new ArrayList<PolicyReference>();
		
		setPermissionPolicySet(permissionPolicySet);
		logger.info("Role Policy Set constructor:" + rolePolicySetDescription);
	}
	
	/**
	 * The constructor takes as arguments the id of this policy and its description.
	 * @param rolePolicySetId the policy id; should be in an URI format
	 * @param rolePolicySetDescription description of the policy
	 */
	public RolePolicySet(String rolePolicySetId, String rolePolicySetDescription) throws AMException{		
		this(rolePolicySetId, rolePolicySetDescription, null);
	}
	
	/**
	 * Add a role to the target of this policy as a subject attribute.
	 * @param role the role the subject has to have
	 * @return true if the addition took place, false otherwise
	 */
	protected boolean addTarget(Role role){
		URI subjectValue = role.getId();
		
		// add the role only once		
		if ( policySubjectsTarget.containsKey(subjectValue.toString())){
			logger.fine("Add role to the target [" + role + "] ALREADY EXISTS!");
			return false;
		}			

		URI subjectDesignatorType = Role.DATA_TYPE_ROLE;
		URI subjectDesignatorId = Role.IDENTIFIER_SUBJECT_ROLE;
		FunctionFactory factory = FunctionFactory.getTargetInstance();
		Function subjectFunction;

		try {
			subjectFunction = factory.createFunction(Role.MATCHID_ROLE);
		} catch (UnknownIdentifierException e) {
			String errorMsg = "Unknown identifier exception for function: " + Role.MATCHID_ROLE +
				" Error:" + e.getMessage();
			logger.severe(errorMsg);
			return false;
			
		} catch (FunctionTypeException e) {
			String errorMsg = "Function type exception for function: " + Role.MATCHID_ROLE +
				" Error:" + e.getMessage();
			logger.severe(errorMsg);
			return false;
		}

		AttributeDesignator subjectDesignator = new AttributeDesignator(
				AttributeDesignator.SUBJECT_TARGET,
				subjectDesignatorType,
				subjectDesignatorId,
				true);
								
		AttributeValue value = 
			AttributeIDsValues.createAttributeValueInstance(
					Role.DATA_TYPE_ROLE, 
					subjectValue.toString());
		TargetMatch subjectTargetMatch = new TargetMatch(
				TargetMatch.SUBJECT,
				subjectFunction,
				subjectDesignator,
				value);
		// update the subject target match objects
		policySubjectsTarget.put(subjectValue.toString(), subjectTargetMatch);
					
		logger.fine("Add role to the target [" + role + "]");

		// update the policy set ID 
		addPolicyIdSuffix(subjectValue);

		return true;
	}
	
	/**
	 * Initialize the target of this role policy set. Only roles can be used as target.
	 * @param role the role the subject has to have
	 * @return true if the addition took place, false otherwise
	 */
	public boolean setTarget(Role role){
		roles.clear();
		return addTarget(role);
	}
	
	/**
	 * Set the permission policy set to be used by this RPS.
	 * @param permissionPolicySet the permission policy set
	 * @return
	 */
	public boolean setPermissionPolicySet(PermissionPolicySet permissionPolicySet){
		if (permissionPolicySet != null) {
			logger.fine("Set the permission policy set to [" + 
					permissionPolicySet + "]");
			// Only one permission policy set reference is allowed for a regular RPS
			PPSPolicyReference = permissionPolicySet.getPolicySetReference();
			return true;
		}
		else {
			return false;
		}
	}
		
	/**
	 * Build the policy set object
	 * @return true if the operation was successful, false otherwise
	 */
	private boolean buildPolicySet() {
		
		List<PolicyReference> references = new ArrayList<PolicyReference>();
		references.addAll(policyReferences);
		if (PPSPolicyReference != null) {
			references.add(PPSPolicyReference);
		}
		// create the policy set object
		try {
			rolePolicySet = new PolicySet(
					getPolicyId(),
					null,
					createPolicyCombinigAlgorithm(getCombiningAlg()),
					getPolicyDescription(),
					createPolicyTarget(),
					references);
		} catch (AMException e) {
			logger.severe("Policy object could not be built:" + e.getMessage());
			e.printStackTrace();
		}
		
		return true;
	}
	
	/* (non-Javadoc)
	 * @see am.xacml.pap.GenericRBACPolicy#getUpdatedPolicy()
	 */
	public AbstractPolicy getUpdatedPolicy(){
		buildPolicySet();
		return rolePolicySet;
	}
	
	/* (non-Javadoc)
	 * @see am.xacml.pap.GenericRBACPolicy#getPolicy()
	 */
	public AbstractPolicy getPolicy(){
		return rolePolicySet;
	}
	
	/* (non-Javadoc)
	 * @see am.xacml.pap.GenericRBACPolicy#isPolicySet()
	 */
	public boolean isPolicySet(){
		return true;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return 	"Role Policy Set: " + 
				"id[" + getPolicyId() + "] " + 
				"description[" + getPolicyDescription() + "]\n" + 
				"Target roles: [" + roles +"]\n" +
				"Permission Policy Set: [" + policyReferences + "]"; 
	}

	@Override
	protected URI getResourceAttributeIDForPolicyTarget(Resource resource) {
		// No resource should be present in the RPS target
		return null;
	}
}
