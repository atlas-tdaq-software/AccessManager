package am.xacml.pap;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.sun.xacml.AbstractPolicy;
import com.sun.xacml.Policy;
import com.sun.xacml.Target;
import com.sun.xacml.TargetMatch;
import com.sun.xacml.TargetMatchGroup;
import com.sun.xacml.TargetSection;
import com.sun.xacml.Rule;
import com.sun.xacml.attr.AttributeDesignator;
import com.sun.xacml.combine.DenyOverridesRuleAlg;
import com.sun.xacml.combine.RuleCombiningAlgorithm;
import com.sun.xacml.cond.Condition;
import com.sun.xacml.ctx.Result;

import am.util.AMCommon;
import am.util.AMException;
import am.xacml.context.Action;
import am.xacml.context.Environment;
import am.xacml.context.Resource;

/**
 * This class creates a Policy to be included in a Permission Policy Set.
 * This policies don't put restriction on the subject attribute of the target
 * The resource attribute will contain the resource-id types and subtypes, 
 * while the resource-type attribute should be checked at the level of PPS.
 * @author mleahu
 *
 */
public class PermissionPolicy extends GenericRBACPolicy {
	
	/**
	 * Match IDs 
	 */
	
	private static final String DEFAULT_COMBINING_ALG = 
		DenyOverridesRuleAlg.algId;
	
	/**
	 * URL prefix for the policy ID 
	 */
	protected static final URI PP_ID_URI_PREFIX = URI.create("pp");
	
	/**
	 * URL prefix for the policy ID 
	 */
	protected static final URI RULE_ID_URI_PREFIX = URI.create("rule");

	/**
	 * The list of rules 
	 */
	private List<Rule> rules;
		
	/**
	 * The policy object. 
	 */
	private Policy policy;
	
    private static final Logger logger =
        Logger.getLogger(PermissionPolicy.class.getName());
    
	    
	/**
	 * Add a rule to the rule list
	 */
	/**
	 * @param ruleId the rule's identification
	 * @param description the rule's description
	 * @param res the resource containing also the action information
	 * @param env environment settings
	 * @param effect the effect of the rule. can be allow, deny, indeterminate..
	 * @return true if the rule has been successfully create, false otherwise
	 * @throws AMException
	 */
	private boolean addRuleResourceActionEnvironment (
			String description,
			Resource res,
			Environment env,
			int effect)  throws AMException{
		
		// create the rule's TARGET		
		Target ruleTarget;
		
		TargetSection subjectsSection;
		TargetSection resourcesSection;
		TargetSection actionsSection;
		
		
		// SUBJECT SECTION
		// the subject of a rule in RBAC is null;
		// it will be defined at the upper level in RPS
		subjectsSection = new TargetSection(
				null,
				TargetMatch.SUBJECT,
				XACML_VERSION);
		
		// RESOURCE AND ACTION SECTION
		// create the Resource section
		List<TargetMatch> resource = new ArrayList<TargetMatch>();
		List<TargetMatch> action = new ArrayList<TargetMatch>();

		
		List<URI> resTypes = res.getAttributeIDForRuleTarget();
		if (resTypes.size() == 0){
			logger.fine("No resource type!");
		}
		
		// get each resource type
		for(URI identifier: resTypes){
			// the resource			
			TargetMatch resourceTargetMatch = 
				GenericRBACPolicy.createTargetMatch(
						TargetMatch.RESOURCE, 
						AttributeDesignator.RESOURCE_TARGET, 
						res, 
						identifier);
			//createResourceTargetMatch(res, identifier);			
			resource.add(resourceTargetMatch);
			logger.info("Resource added to the rule:" + resourceTargetMatch);
			
			// the action
			Action act = res.getAction(identifier);
			if (act != null){
				TargetMatch actionTargetMatch =	
					GenericRBACPolicy.createTargetMatch(
							TargetMatch.ACTION,
							AttributeDesignator.ACTION_TARGET,
							act,
							act.getActionId()); 
				action.add(actionTargetMatch);				
				logger.info("Action added to the rule:" + actionTargetMatch);
			} else {
				logger.warning("No action found for resource type identifier " + identifier);
			}
		}

		TargetMatchGroup resourcesGroup = new TargetMatchGroup(resource, TargetMatch.RESOURCE);
		List<TargetMatchGroup> resourcesGroups = null;
		if (resource.size() > 0) {
			resourcesGroups = new ArrayList<TargetMatchGroup>();
			resourcesGroups.add(resourcesGroup);		
		}
		resourcesSection = new TargetSection(
				resourcesGroups,
				TargetMatch.RESOURCE,
				XACML_VERSION);		
		
		TargetMatchGroup actionsGroup = new TargetMatchGroup(action, TargetMatch.ACTION);
		List<TargetMatchGroup> actionsGroups = null;
		if (action.size() > 0) {
			actionsGroups = new ArrayList<TargetMatchGroup>();
			actionsGroups.add(actionsGroup);
		}
		
		actionsSection = new TargetSection(
				actionsGroups,
				TargetMatch.ACTION,
				XACML_VERSION);		
		
		
		// create the rule's target object
		ruleTarget = new Target(subjectsSection, resourcesSection, actionsSection);
		
		// condition of this rule
		Condition condition = null;
		
		// build the rule id
		URI ruleID = AMCommon.concatenateURI( '?', 
				RULE_ID_URI_PREFIX,
				GenericRBACPolicy.createIDURIfromResource(res, res.getAttributeIDForRuleTarget(), false));
		logger.info("Adding rule id [" + ruleID + "] with details:" +
				" Effect=[" + effect + "]" +
				" Description=[" + description + "]" +
				" ruleTarget=[" + ruleTarget + "]" +
				" condition=[" + condition +"]");
		
		rules.add(new Rule(ruleID, effect, description, ruleTarget, condition));
				
		// update the policy resource target
		updatePolicyResourceAndActionTargets(res);
		
		return true;
	}
		
	public PermissionPolicy(String policyId, String policyDescription){
		super(policyId,policyDescription, DEFAULT_COMBINING_ALG);
		addPolicyIdPrefix(PP_ID_URI_PREFIX);
		rules = new ArrayList<Rule>();
		policy = null;
	}
		
	public boolean allowResourceActionEnvironment(
			Resource resourceAction,
			String ruleDescription,
			Environment env)  throws AMException{
		return addRuleResourceActionEnvironment(
				ruleDescription,
				resourceAction,
				env,
				Result.DECISION_PERMIT);
	}
	
	public boolean allowResourceAction(
			Resource resourceAction)  throws AMException{
		return allowResourceActionEnvironment(
				resourceAction, 
				"Rule for " + resourceAction.getDescription(),
				(Environment)null);
	}

	/**
	 * Add a rule for a resource with one of the effects specified in the Result.DECISIONS array. 
	 * @param resourceAction the resource object
	 * @param effect the effect id from the Result static values
	 * @return true if rule added successfully
	 * @throws AMException
	 */  
	public boolean addRuleResourceAction(
			Resource resourceAction, 
			int effect)  throws AMException{
		return addRuleResourceActionEnvironment(
				"Rule for " + resourceAction.getDescription(), 
				resourceAction, (Environment)null, effect);
	}
	
	
	/**
	 * Generate a policy using the rules' list.
	 * @return the Policy object
	 */
	private Policy buildPolicy(){
		
		// Combining algorithm
		RuleCombiningAlgorithm combiningAlg = 
			createRuleCombinigAlgorithm(getCombiningAlg());
		
		try {
			policy = new Policy(
					getPolicyId(),
					null,
					combiningAlg,
					getPolicyDescription(),
					createPolicyTarget(),
					rules);
		} catch (AMException e) {
			logger.severe("Policy object could not be built:" + e.getMessage());
			e.printStackTrace();
		}
		
		return policy;
	}


	/* (non-Javadoc)
	 * @see am.xacml.pap.GenericRBACPolicy#getUpdatedPolicy()
	 */
	public AbstractPolicy getUpdatedPolicy(){
		buildPolicy();
		return policy;
	}
		
	/* (non-Javadoc)
	 * @see am.xacml.pap.GenericRBACPolicy#getPolicy()
	 */
	public AbstractPolicy getPolicy(){
		return policy;
	}			
	
	/* (non-Javadoc)
	 * @see am.xacml.pap.GenericRBACPolicy#isPolicy()
	 */
	public boolean isPolicy(){
		return true;
	}

	/* (non-Javadoc)
	 * @see am.xacml.pap.GenericRBACPolicy#getResourceAttributeIDForPolicyTarget(am.xacml.context.Resource)
	 */
	@Override
	protected URI getResourceAttributeIDForPolicyTarget(Resource resource) {
		return resource.getAttributeIDForPPTarget();
	}
}
