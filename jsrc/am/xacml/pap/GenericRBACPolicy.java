package am.xacml.pap;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import am.util.AMCommon;
import am.util.AMException;
import am.xacml.context.Action;
import am.xacml.context.AttributeIDsValues;
import am.xacml.context.Resource;

import com.sun.xacml.AbstractPolicy;
import com.sun.xacml.PolicyMetaData;
import com.sun.xacml.PolicyReference;
import com.sun.xacml.Target;
import com.sun.xacml.TargetMatch;
import com.sun.xacml.TargetMatchGroup;
import com.sun.xacml.TargetSection;
import com.sun.xacml.VersionConstraints;
import com.sun.xacml.attr.AttributeDesignator;
import com.sun.xacml.attr.AttributeValue;
import com.sun.xacml.combine.CombiningAlgFactory;
import com.sun.xacml.combine.PolicyCombiningAlgorithm;
import com.sun.xacml.combine.RuleCombiningAlgorithm;
import com.sun.xacml.cond.Function;
import com.sun.xacml.cond.FunctionFactory;
import com.sun.xacml.finder.PolicyFinder;

/**
 * A generic policy interface implemented by the PermissionPolicy, PermissionPolicySet
 * and RolePolicySet
 * @author mleahu
 *
 */
public abstract class GenericRBACPolicy {
	
    private static Logger logger =
        Logger.getLogger(GenericRBACPolicy.class.getName());

	/**
	 * The policy's base id
	 */
	private URI policyBaseId;
	
	/**
	 * Dynamic list of policies prefixes generated from the policies types (rps, pps, pp ...)  
	 */
	private List<URI> policyIdPrefix;

	/**
	 * Dynamic list of policies suffixes generated from the policies target and rules  
	 */
	private List<URI> policyIdSuffix;
	
	/**
	 * 
	 */
	private String policyDescription;
	
	/**
	 * The URI of the combining algorithm for the current policy.
	 */
	private URI combiningAlgId;
	
	/**
	 * The subject targets for the policy. 
	 * The key is the value of the role
	 */
	protected Map<String, TargetMatch> policySubjectsTarget;

	/**
	 * The resource targets for the policy. 
	 * The key is the value of the resource attribute designed to be the target of this policy
	 */
	protected Map<String, TargetMatch> policyResourcesTarget;
	
	/**
	 * The action targets for the policy. 
	 * The key is the value of the action assigned to the resource attribute designed to be the target of this policy
	 */
	protected Map<String, TargetMatch> policyActionsTarget;
	
	/**
	 * The environment targets for the policy. 
	 * The key is the value of the environment
	 */
	protected Map<String, TargetMatch> policyEnvironmentsTarget;
	
    /**
     * The XACML version used by the RBAC policies is 2.0
     */
    protected static final int XACML_VERSION = PolicyMetaData.XACML_VERSION_2_0;
    
    /**
     * Create a target match with a given target match type and attribute designator type.
     * An attributeIDsValues object is used to get the data type, match function and value for a given
     * identifier. 
     * @param targetMatchType
     * @param attributeDesignatorType
     * @param attr
     * @param identifier
     * @return a new TargetMatch object
     */
    protected static TargetMatch createTargetMatch(
    		int targetMatchType,
			int attributeDesignatorType,
			AttributeIDsValues attr,
			URI identifier) {
		try {

			// get the factory that handles Target functions and get an
			// instance of the right function
			FunctionFactory factory = FunctionFactory.getTargetInstance();
			Function function = factory.createFunction(attr.getMatchFunction(identifier));

			AttributeDesignator designator = new AttributeDesignator(
					attributeDesignatorType,
					attr.getDataType(identifier),
					identifier,
					true);
			

			AttributeValue value = attr.createAttributeValueInstance(identifier);

			// create the TargetMatch
			return new TargetMatch(targetMatchType, function, designator, value);
		} catch (Exception e) {
			logger.severe("Could not create the target match for:" +
					" target match type [" + targetMatchType + "]" +
					" and attributeIDsValues:" + attr);
			return null;
		}
	}

    /**
     * Create a rule combining algorithm object
     * @param combiningAlgId
     * @return
     */
    protected static RuleCombiningAlgorithm createRuleCombinigAlgorithm(
			URI combiningAlgId) {
		try {
			// get the factory that handles Combining Algorithm
			CombiningAlgFactory caFactory = CombiningAlgFactory.getInstance();			
			// create the RuleCombiningAlgorithm
			return (RuleCombiningAlgorithm) (caFactory.createAlgorithm(combiningAlgId));
		} catch (Exception e) {
			logger.severe("Could not create a rule combinig algorithm for " + combiningAlgId);
			return null;
		}
	}
	
    /**
     * Creates a policy combining algorithm object
     * @param combiningAlgId
     * @return
     */
    protected static PolicyCombiningAlgorithm createPolicyCombinigAlgorithm(
			URI combiningAlgId) {
		try {
			// get the factory that handles Combining Algorithm
			CombiningAlgFactory caFactory = CombiningAlgFactory.getInstance();			
			// create the PolicyCombiningAlgorithm
			return (PolicyCombiningAlgorithm) (caFactory.createAlgorithm(combiningAlgId));
		} catch (Exception e) {
			logger.severe("Could not create a policy combinig algorithm for " + combiningAlgId);
			return null;
		}
	}

    /**
     * Create an URI to be used as part of an ID. A resource and list of identifiers
     * is used to generate the URI.
     * @param res the resource object
     * @param resIdentifiers list of identifiers
     * @param hierarchical if true, the URI will be in a format 
     * 						like "resourceValue/actionValue/resourceValue/actionValue",
     * 						else it will in an opaque format 
     * 						like "resourceValue=actionValue&resourceValue=actionValue"
     * @return the URI object
     */
    public static URI createIDURIfromResource(
    		Resource res,
    		List<URI> resIdentifiers,
    		boolean hierarchical){
    	String id = null;
    	if (res != null && resIdentifiers != null){
    		for(URI resId: resIdentifiers){
    			String resValue = res.getValue(resId);
    			resValue = (resValue == null)? "" :resValue;
    			// delete non word characters
    			resValue = resValue.replaceAll("\\W", "");
    			
    			Action act = res.getAction(resId);
    			String actValue = (act == null) ? "" : act.getValue(act.getActionId()) ;
    			if (hierarchical){
    				id = (id == null) ? "" : id + "//";
    			} else {
    				id = (id == null) ? "" : id + "&";
    			}
				id += "R" + resValue;
				id += (hierarchical) ? "/" : "=";
				id += "A" + actValue;
    		}
    	}
		return (id != null) ? URI.create(id) : null;
    }
    
    /**
     * Create an URI to be used as part of an ID. A resource and one identifier
     * are used to generate the URI.
     * @param res the resource object
     * @param resIdentifier the URI identifier
     * @param hierarchical if true, the URI will be in a format 
     * 						like "resourceValue/actionValue/resourceValue/actionValue",
     * 						else it will in an opaque format 
     * 						like "resourceValue=actionValue&resourceValue=actionValue"
     * @return the URI object
     */
    public static URI createIDURIfromResource(
    		Resource res,
    		URI resIdentifier,
    		boolean hierarchical){
    	List<URI> resIdentifiers = new ArrayList<URI>(1);
    	resIdentifiers.add(resIdentifier);
    	return GenericRBACPolicy.createIDURIfromResource(res, resIdentifiers, hierarchical);
    }

    /**
     * Create an URI to be used as part of an ID. A resource and more identifiers
     * are used to generate the URI. The identifiers are the resourceId and resourceTypes
     * @param res the resource object
     * @param hierarchical if true, the URI will be in a format 
     * 						like "resourceValue/actionValue/resourceValue/actionValue",
     * 						else it will in an opaque format 
     * 						like "resourceValue=actionValue&resourceValue=actionValue"
     * @return the URI object
     */
    public static URI createIDURIfromResource(
    		Resource res,
    		boolean hierarchical){
    	List<URI> resIdentifiers = new ArrayList<URI>();
    	resIdentifiers.add(res.getAttributeIDForPPTarget());
    	resIdentifiers.addAll(res.getAttributeIDForRuleTarget());
    	return GenericRBACPolicy.createIDURIfromResource(res, resIdentifiers, hierarchical);
    }    
    
    public GenericRBACPolicy(
    		String policyBaseId,
    		String policyDescription, 
    		String combiningAlgId){
		this.policyBaseId = URI.create(policyBaseId);
		this.policyIdPrefix = new ArrayList<URI>();
		this.policyIdSuffix = new ArrayList<URI>();
		this.policyDescription = policyDescription;
		this.combiningAlgId = URI.create(combiningAlgId);
		this.policySubjectsTarget = new HashMap<String, TargetMatch>();
		this.policyResourcesTarget = new HashMap<String, TargetMatch>();
		this.policyActionsTarget = new HashMap<String, TargetMatch>();
		this.policyEnvironmentsTarget = new HashMap<String, TargetMatch>();
    }
    
	/**
	 * Returns the Policy description
	 * @return Returns the policyDescription.
	 */
	public String getPolicyDescription() {
		return policyDescription;
	}

	/**
	 * Add another policy ID prefix
	 * @param prefix
	 */
	protected void addPolicyIdPrefix(URI prefix){
		if (policyIdPrefix.contains(prefix)){
			logger.finest("The policy ID prefix [" + prefix + "] already added!");
		} else {
			this.policyIdPrefix.add(prefix);
			logger.finest("Added the policy ID prefix [" + prefix + "]!");
		}
	}
	
	/**
	 * Add another policy ID suffix
	 * @param suffix
	 */
	protected void addPolicyIdSuffix(URI suffix){
		if (policyIdSuffix.contains(suffix)){
			logger.finest("The policy ID suffix [" + suffix + "] already added!");
		} else {
			this.policyIdSuffix.add(suffix);
			logger.finest("Added the policy ID suffix [" + suffix + "]!");
		}
	}

	/**
	 * Generate the full policy ID using the internal prefix, suffix and provided base ID.
	 * @return Returns the full policyId.
	 */
	public URI getPolicyId() {
		// build the policy id
		URI prefix = null;
		for(URI prf: policyIdPrefix){
			prefix = AMCommon.concatenateURI(':', prefix, prf);
		}
		URI suffix = null;
		for(URI suf: policyIdSuffix){
			suffix = AMCommon.concatenateURI('@', suffix, suf);
		}
		URI policyId = AMCommon.concatenateURI(
				':', prefix, AMCommon.concatenateURI(
						'?', policyBaseId, suffix));
		return policyId;
	}

	/**
	 * Verify if the resource object matches the target of this policy
	 * @return true /false
	 */
	public boolean resourceMatchPolicyTarget(Resource resource){
		URI resId = getResourceAttributeIDForPolicyTarget(resource);
		Action act = resource.getAction(resId);
		if (! ( policyResourcesTarget.containsKey(resource.getValue(resId))) ) {
			return false;
		}
		if ( act != null && ! policyActionsTarget.containsKey(act.getValue(act.getActionId()))) {
			return false;
		}
		return true;
	}

	/**
	 * Update the policy;s resource target section by extracting the value of the 
	 * resource attribute designed to be a target for this policy.
	 * Use also the value of the action assigned to the same resource attribute.
	 * @param resource the resource object
	 */
	protected void updatePolicyResourceAndActionTargets(Resource resource){
		
		URI resIdentifier = getResourceAttributeIDForPolicyTarget(resource);
		if (resIdentifier == null) {
			logger.finer("Nothing to add to the resource and action target from the resource:" + resource);
			return;
		}
		
		// add a policy suffix
		addPolicyIdSuffix(
				GenericRBACPolicy.createIDURIfromResource(resource, resIdentifier, true));
		
		String resValue = resource.getValue(resIdentifier);
		if (resValue != null && resValue.length() > 0){
			if (! policyResourcesTarget.containsKey(resValue)){
				logger.config("Update the policy resources target with resource [" + resource + "]");
				TargetMatch resourceTargetMatch = 
					GenericRBACPolicy.createTargetMatch(
							TargetMatch.RESOURCE, 
							AttributeDesignator.RESOURCE_TARGET, 
							resource, 
							resIdentifier);
				policyResourcesTarget.put(resValue, resourceTargetMatch);
			} else{
				logger.config("The policy resource target already exists for res [" + resource + "]");			
			}
		} else {
			logger.fine("No target resource added for resource identifier [" + resIdentifier + "]. " +
					"The reason: no value");
		}
		
		Action act = resource.getAction(resIdentifier);
		if (act != null ){
			URI actIdentifier = act.getActionId();
			String actValue = act.getValue(actIdentifier);
			if (! policyActionsTarget.containsKey(actValue)){
				if (actValue != null && actValue.length() > 0){
					TargetMatch actionTargetMatch = 
						GenericRBACPolicy.createTargetMatch(
								TargetMatch.ACTION, 
								AttributeDesignator.ACTION_TARGET, 
								act, 
								actIdentifier);
		
					policyActionsTarget.put(actValue, actionTargetMatch);
				} else {
					logger.fine("No target action added for resource identifier [" + resIdentifier + "]." +
							" The reason: no action value");
				}
			} else{
				logger.config("The policy action target already exists for action [" + act + "]");			
			}
		} else {
			logger.fine("No target action added for resource identifier [" + resIdentifier + "]." +
					" The reason: no action assigned");
		}
	}
	
	/**
	 * Return the resource attribute id to be used as target resource for the policy.
	 * This method has to be implemented in the extended classes.
	 * @param resource the Resource object
	 * @return the URI representing the attribute id
	 */
	protected abstract URI getResourceAttributeIDForPolicyTarget(Resource resource);

	
	/**
	 * Build a generic target section from a given set of TargetMatch objects
	 * and the target match type + attribute designator type
	 * @param targetMatches a collection of TargetMatch objects
	 * @param targetMatchType one of the static values of TargetMatch class
	 * @return the target section object
	 * @throws AMException
	 */
	protected TargetSection createTargetSection(Collection<TargetMatch> targetMatches, int targetMatchType
			) throws AMException {

		/*
		 * The <Subjects> element SHALL contain a disjunctive sequence of <Subject> elements.
		 * The <Subject> element SHALL contain a conjunctive sequence of <SubjectMatch> elements.
		 * 
		 * So, the TargetSection contains a disjunctive sequence of TargetMatchGroup 
		 * and the TargetMatchGroup contains a conjunctiv sequence of TargetMatch
		 */
		
		List<TargetMatchGroup> targetMatchGroups = null;

		// create a target match group for each target match object so that they are a disjunction of target match
		if (targetMatches.size() > 0) {
			
			targetMatchGroups = new ArrayList<TargetMatchGroup>();
			
			for(TargetMatch targetMatch: targetMatches){
				// build one target match group from all the target matches objects
				List<TargetMatch> targetMatchesList = new ArrayList<TargetMatch>();
				targetMatchesList.add(targetMatch);
				
				TargetMatchGroup targetMatchGroup = new TargetMatchGroup(
						targetMatchesList,
						targetMatchType);
				
				// build the list of target match groups 
				targetMatchGroups.add(targetMatchGroup);				
			}
		}
		
		return new TargetSection(
				targetMatchGroups,
				targetMatchType,
				XACML_VERSION);		
	}
	
	/**
	 * Create a policy target with all 4 sections (subjects, resources, actions and environments) 
	 * @return the target object
	 * @throws AMException
	 */
	protected Target createPolicyTarget() throws AMException{
		return new Target(
				createTargetSection(policySubjectsTarget.values(), TargetMatch.SUBJECT),
				createTargetSection(policyResourcesTarget.values(), TargetMatch.RESOURCE),
				createTargetSection(policyActionsTarget.values(), TargetMatch.ACTION),
				createTargetSection(policyEnvironmentsTarget.values(), TargetMatch.ENVIRONMENT)
				);
	}
	
	/**
	 * Get the policy base id provided in the constructor
	 * @return base id in URI format
	 */
	public URI getPolicyBaseId(){
		return policyBaseId;
	}
	
	/**
	 * @return Returns the combining algorithm.
	 */
	protected URI getCombiningAlg() {
		return combiningAlgId;
	}
	
	/**
	 * Rebuild and retrieve the policy object
	 * @return Policy object
	 */
	public abstract AbstractPolicy getUpdatedPolicy();
		
	/**
	 * Retrieve the policy object
	 * @return Policy object
	 */
	public abstract AbstractPolicy getPolicy();

	/**
	 * Tells that the abstract policy returned by getUpdatedPolicy and getPolicy is a Policy
	 * @return 
	 */
	public boolean isPolicy(){
		return false;
	}
	
	/**
	 * Tells that the abstract policy returned by getUpdatedPolicy and getPolicy is a PolicySet
	 * @return 
	 */
	public boolean isPolicySet(){
		return false;
	}
	
	/**
	 * Generates a policy reference object for the current policy.
	 * @return PolicyReference object
	 */
	public PolicyReference getPolicyReference(){
		return new PolicyReference(
				getPolicyId(),
				PolicyReference.POLICY_REFERENCE,
				new VersionConstraints(null, null, null),
				new PolicyFinder(),
				new PolicyMetaData());
	}
	
	/**
	 *  Creates and return a Policy Reference object for the policy set
	 * @return Policy Reference object
	 */
	public PolicyReference getPolicySetReference(){
		return new PolicyReference(
				getPolicyId(),
				PolicyReference.POLICYSET_REFERENCE,
				new VersionConstraints(null, null, null),
				new PolicyFinder(),
				new PolicyMetaData());		
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return 	"RBAC Policy: " + 
				"id[" + getPolicyId() + "] " + 
				"description[" + getPolicyDescription() + "]"; 
	}
}
