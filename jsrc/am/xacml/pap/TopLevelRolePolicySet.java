/**
 * 
 */
package am.xacml.pap;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import com.sun.xacml.AbstractPolicy;
import com.sun.xacml.TargetMatch;
import com.sun.xacml.UnknownIdentifierException;
import com.sun.xacml.attr.AttributeDesignator;
import com.sun.xacml.attr.AttributeValue;
import com.sun.xacml.cond.Function;
import com.sun.xacml.cond.FunctionFactory;
import com.sun.xacml.cond.FunctionTypeException;

import am.rbac.Role;
import am.util.AMException;
import am.xacml.context.AttributeIDsValues;

/**
 * This class gathers all the roles used for the RPSs and creates another RPS to reference them.
 * @author mleahu
 * @version 1.0
 */
public class TopLevelRolePolicySet extends RolePolicySet {

	/**
	 * The depth of the tree of intermediate roles
	 */
	private int intermediateRoleHierarchyDepth;
	
	/**
	 * The roles of this policy and the intermediate roles if any 
	 */
	private HashMap<String, TopLevelRolePolicySet> topRoles;
	
	/**
	 * The separator of the fields in the top role name.
	 */
	private static final String TOP_ROLE_FIELD_SEPARATOR = ":";
	
	/**
	 * The intermediate roles prefix.
	 */
	private static final String TOP_ROLE_INTERMEDIARY_PREFIX = "IntermediaryTopRole";
	
    private static final Logger logger =
        Logger.getLogger(TopLevelRolePolicySet.class.getName());
    
	/**
	 * Build a top level RPS object to be used to gather all RPSs
	 * @param rolePolicySetId
	 * @param rolePolicySetDescription
	 * @param intermediateRoleHierarchyDepth the depth of the role intermediary tree
	 * @throws AMException
	 */
	public TopLevelRolePolicySet(
			String rolePolicySetId,
			String rolePolicySetDescription,
			int intermediateRoleHierarchyDepth) throws AMException {
		super(rolePolicySetId, rolePolicySetDescription);
		this.intermediateRoleHierarchyDepth = intermediateRoleHierarchyDepth;
		if (intermediateRoleHierarchyDepth < 0 ) {
			throw new AMException("Hierarchy depth should be positive, not:" + intermediateRoleHierarchyDepth,
					AMException.WRONG_ARGUMENTS);
		}
		topRoles = new HashMap<String, TopLevelRolePolicySet>();
	}

	/**
	 * Build a top level RPS object to be used to gather all RPSs
	 * @param rolePolicySetId
	 * @param rolePolicySetDescription
	 * @throws AMException
	 */
	public TopLevelRolePolicySet(
			String rolePolicySetId,
			String rolePolicySetDescription) throws AMException {
		this(rolePolicySetId, rolePolicySetDescription, 0);
	}
	
	public void addRole(Role role, RolePolicySet rps){
		try {
			addRole("", role.getId().toString(), rps);
		} catch (AMException e) {
			logger.severe("Role [" + role + "] not added to the policy because:" + e.getMessage());
		}
	}
	
	/**
	 * Add a RPS to be referenced by this top level RPS
	 * @param RPS
	 */
	private void addRole(String rolePrefix, String roleId, RolePolicySet rps) throws AMException{
		if (intermediateRoleHierarchyDepth == 0) {
			if (rolePrefix != null && rolePrefix.length() > 0) {
				roleId = rolePrefix + TOP_ROLE_FIELD_SEPARATOR + roleId;
			}
			addTarget(roleId, false);
			policyReferences.add(rps.getPolicySetReference());
		} else {
			// split the role in fields using the separator character
			String[] fields = roleId.split(TOP_ROLE_FIELD_SEPARATOR, 2);
			
			String nextPrefix = fields[0];
			if (rolePrefix != null && rolePrefix.length() > 0) {
				nextPrefix = rolePrefix + TOP_ROLE_FIELD_SEPARATOR + nextPrefix;
			}
			// for each first field, create if necessary a top level policy with 
			// the intermediate role hierarchy decreased with 1
			if (fields.length == 1){
				// the separator has not appeared in the role id, 
				//so don't go down in the hierarchy for this role
				addTarget(nextPrefix, false);
				policyReferences.add(rps.getPolicySetReference());
			} else {
				addTarget(nextPrefix, true);
				
				String nextRole = fields[1];
				TopLevelRolePolicySet nextTRPS = topRoles.get(nextPrefix);
				if (nextTRPS == null){
					nextTRPS = new TopLevelRolePolicySet(
							TOP_ROLE_INTERMEDIARY_PREFIX + TOP_ROLE_FIELD_SEPARATOR + nextPrefix, 
							"Intermediate RPS for role prefix [" + nextPrefix + "]",
							intermediateRoleHierarchyDepth - 1);
					topRoles.put(nextPrefix, nextTRPS);
					policyReferences.add(nextTRPS.getPolicySetReference());
				}
				nextTRPS.addRole(nextPrefix, nextRole, rps);
			}
			
		}
	}
	
	/**
	 * Add a role to the target of this policy as a subject attribute.
	 * @param roleId the role to be added to the target
	 * @param regexp the role name in the subject target is a regular expression
	 */
	protected boolean addTarget(String roleId, boolean regexp){
		String subjectValue = roleId;
		URI subjectMatchId = Role.MATCHID_ROLE;
		URI subjectDataType = Role.DATA_TYPE_ROLE;
		
		if (regexp) {
			subjectValue += "*";
			subjectMatchId = Role.MATCHID_ROLE_REGEXP;
			subjectDataType = Role.DATA_TYPE_ROLE_REGEXP;
		}
		
		// add the role only once		
		if ( policySubjectsTarget.containsKey(subjectValue)){
			logger.fine("Add role to the target [" + subjectValue + "] ALREADY EXISTS!");
			return false;
		}			

		FunctionFactory factory = FunctionFactory.getTargetInstance();
		Function subjectFunction;

		try {
			subjectFunction = factory.createFunction(subjectMatchId);
		} catch (UnknownIdentifierException e) {
			String errorMsg = "Unknown identifier exception for function: " + subjectMatchId +
				" Error:" + e.getMessage();
			logger.severe(errorMsg);
			return false;
			
		} catch (FunctionTypeException e) {
			String errorMsg = "Function type exception for function: " + subjectMatchId +
				" Error:" + e.getMessage();
			logger.severe(errorMsg);
			return false;
		}

		URI attributeDataType = Role.DATA_TYPE_ROLE;
		URI attributeDesignatorId = Role.IDENTIFIER_SUBJECT_ROLE;
		AttributeDesignator subjectDesignator = new AttributeDesignator(
				AttributeDesignator.SUBJECT_TARGET,
				attributeDataType,
				attributeDesignatorId,
				true);
								
		AttributeValue value = 
			AttributeIDsValues.createAttributeValueInstance(
					subjectDataType,
					subjectValue.toString());
		
		TargetMatch subjectTargetMatch = new TargetMatch(
				TargetMatch.SUBJECT,
				subjectFunction,
				subjectDesignator,
				value);
		// update the subject target match objects
		policySubjectsTarget.put(subjectValue.toString(), subjectTargetMatch);
					
		logger.fine("Add role to the target [" + roleId + "]");

		return true;
	}
	
	/**
	 * Get all the top level policies including the intermediary ones.
	 * @return a list of AbstractPolicies
	 */
	public List<AbstractPolicy> getUpdatedPolicies(){
		ArrayList<AbstractPolicy> policies = new ArrayList<AbstractPolicy>();
		// add the current policy
		policies.add(getUpdatedPolicy());
		for(TopLevelRolePolicySet tlrps: topRoles.values()){
			policies.addAll(tlrps.getUpdatedPolicies());
		}
		return policies;
	}
	
}
