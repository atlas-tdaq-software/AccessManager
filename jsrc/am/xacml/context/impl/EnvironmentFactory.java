package am.xacml.context.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import am.util.AMException;
import am.xacml.context.Environment;

/**
 * The factory class for Environment objects. 
 * @author mleahu
 * @version 1.0
 */
public class EnvironmentFactory {
	
	private static final Logger logger =
        Logger.getLogger(EnvironmentFactory.class.getName());
 
	/**
	 * Build a Environment object with an instance of one of the Environment class extensions.
	 * The Environment type is identified by the value of the EnvironmentCategory attribute. Valid values
	 * of Environment category are those returned by the EnvironmentFactory.getEnvironmentCategories: 
	 * 	"SystemState" . 
	 * An exception is thrown if the EnvironmentCategory attribute is not provided in the attributesMap
	 * or has an invalid value.
	 * @param attributesMap the attribute map has to contain for each keyword a set of values.
	 * @return the new Environment object
	 * @throws AMException
	 */
	public static Environment getEnvironment(HashMap<String, List<String>> attributesMap) throws AMException{

		List<String> lEnvironmentCategory = attributesMap.get("EnvironmentCategory");
		if (lEnvironmentCategory == null || lEnvironmentCategory.size() == 0){
			logger.severe("No Environment category attributes found in the map:" + attributesMap);
			throw new AMException("No Environment category found in the attributes map!", AMException.WRONG_ARGUMENTS);
		}
		
		
		String environmentCategory = lEnvironmentCategory.get(0);
		if (lEnvironmentCategory.size() > 1){
			logger.warning("More than one Environment category attributes found! " +
					"Only the first one is used:" + environmentCategory);
		}
		
		
		if (SystemStateEnvironment.ENVIRONMENT_CATEGORY.equals(environmentCategory)){
			return SystemStateEnvironment.getInstance(attributesMap);
		} else {
			logger.severe("Environment category attributes [" + environmentCategory + "] doesn't have an implementation!");
			throw new AMException(
					"Environment category attributes [" + environmentCategory + "] doesn't have an implementation!",
					AMException.WRONG_ARGUMENTS);
		}
	}
	
	/**
	 * Get the valid environment categories.
	 * @return a list of strings
	 */
	public static List<String> getValidEnvironmentCategories(){
		List<String> environmentCategories = new ArrayList<String>();
		
		environmentCategories.add(SystemStateEnvironment.ENVIRONMENT_CATEGORY);
		
		return environmentCategories;
	}
}
