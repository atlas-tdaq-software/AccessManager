/**
 * 
 */
package am.xacml.context.impl;

import am.xacml.context.Action;

/**
 * The action implementation for the OS resource.
 * @author mleahu
 * @version 1.0
 */
public class OSAction extends Action {
	
	/**
	 * The constructor takes as argument any string
	 */
	public OSAction(String action) {
		super();
		// add the value for action id into the internal map
		addValue(getActionId(), action);
	}
	
}
