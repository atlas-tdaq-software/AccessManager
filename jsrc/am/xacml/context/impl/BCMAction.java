/**
 * 
 */

package am.xacml.context.impl;

import am.xacml.context.Action;

/**
 * Action object for the BCM resource type.
 * @author scannicc
 * @version 1.0
 */
public class BCMAction extends Action {

	/**
	 * Values for action: update and configure
	 */
	
	public static final String VALUE_ACTION_UPDATE 		= "update";
	public static final String VALUE_ACTION_CONFIGURE	= "configure";

	/**
	 * Constructor that takes as argument the action value.
	 * @param actionValue action value has to be one of the VALUE_ACTION_*
	 */
	public BCMAction(String actionValue){
		super();
		addValue(getActionId(), actionValue);
	}

	/* (non-Javadoc)
	 * @see am.xacml.context.Action#toString()
	 */
	public String toString(){
		return "BCM Action: " + super.toString();
	}
}
