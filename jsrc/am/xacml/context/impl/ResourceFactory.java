package am.xacml.context.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import am.util.AMException;
import am.xacml.context.Resource;

/**
 * The factory class for Resource objects. 
 * @author mleahu
 * @version 1.0
 */
public class ResourceFactory {
	
	private static final Logger logger =
        Logger.getLogger(ResourceFactory.class.getName());
 
	/**
	 * Build a resource object with an instance of one of the Resource class extensions.
	 * The resource type is identified by the value of the ResourceCategory attribute. Valid values
	 * of resource category are those returned by the ResourceFactory.getResourceCategories: 
	 * 	DataBase, RDB, os, pmg, ResourceManager, RunControl, TriggerCommander, igui, AMRM. 
	 * An exception is thrown if the ResourceCategory attribute is not provided in the attributesMap
	 * or has an invalid value.
	 * @param attributesMap the attribute map has to contain for each keyword a set of values.
	 * @return the new Resource object
	 * @throws AMException
	 */
	public static Resource getResource(HashMap<String, List<String>> attributesMap) throws AMException{

		List<String> lresourceCategory = attributesMap.get("ResourceCategory");
		if (lresourceCategory == null || lresourceCategory.size() == 0){
			logger.severe("No resource category attributes found in the map:" + attributesMap);
			throw new AMException("No resource category found in the attributes map!", AMException.WRONG_ARGUMENTS);
		}
		
		
		String resourceCategory = lresourceCategory.get(0);
		if (lresourceCategory.size() > 1){
			logger.warning("More than one resource category attributes found! " +
					"Only the first one is used:" + resourceCategory);
		}
		
		
		if (DBResource.VALUE_RESOURCE_CATEGORY.equals(resourceCategory)){
			return DBResource.getInstance(attributesMap);
		} else if (RDBResource.VALUE_RESOURCE_CATEGORY.equals(resourceCategory)){
			return RDBResource.getInstance(attributesMap);
    } else if (IGUIResource.VALUE_RESOURCE_CATEGORY.equals(resourceCategory)){
      return IGUIResource.getInstance(attributesMap);
		} else if (OSResource.VALUE_RESOURCE_CATEGORY.equals(resourceCategory)){
			return OSResource.getInstance(attributesMap);
		} else if (PMGResource.VALUE_RESOURCE_CATEGORY.equals(resourceCategory)){
			return PMGResource.getInstance(attributesMap);
		} else if (RMResource.VALUE_RESOURCE_CATEGORY.equals(resourceCategory)){
			return RMResource.getInstance(attributesMap);
		} else if (RunControlResource.VALUE_RESOURCE_CATEGORY.equals(resourceCategory)){
			return RunControlResource.getInstance(attributesMap);
		} else if (TriggerCommanderResource.VALUE_RESOURCE_CATEGORY.equals(resourceCategory)){
			return TriggerCommanderResource.getInstance(attributesMap);
		} else if (BCMResource.VALUE_RESOURCE_CATEGORY.equals(resourceCategory)){
			return BCMResource.getInstance(attributesMap);			
    } else if (AMRMResource.VALUE_RESOURCE_CATEGORY.equals(resourceCategory)){
      return AMRMResource.getInstance(attributesMap);          
		} else {
			logger.severe("Resource category attributes [" + resourceCategory + "] doesn't have an implementation!");
			throw new AMException(
					"Resource category attributes [" + resourceCategory + "] doesn't have an implementation!",
					AMException.WRONG_ARGUMENTS);
		}
	}
	
	/**
	 * Get the valid resource categories.
	 * @return a list of strings
	 */
	public static List<String> getValidResourceCategories(){
		List<String> resourceCategories = new ArrayList<String>();
		
		resourceCategories.add(DBResource.VALUE_RESOURCE_CATEGORY);
    resourceCategories.add(RDBResource.VALUE_RESOURCE_CATEGORY);
		resourceCategories.add(IGUIResource.VALUE_RESOURCE_CATEGORY);
		resourceCategories.add(OSResource.VALUE_RESOURCE_CATEGORY);
		resourceCategories.add(PMGResource.VALUE_RESOURCE_CATEGORY);
		resourceCategories.add(RMResource.VALUE_RESOURCE_CATEGORY);
		resourceCategories.add(RunControlResource.VALUE_RESOURCE_CATEGORY);
		resourceCategories.add(TriggerCommanderResource.VALUE_RESOURCE_CATEGORY);
		resourceCategories.add(BCMResource.VALUE_RESOURCE_CATEGORY);
    resourceCategories.add(AMRMResource.VALUE_RESOURCE_CATEGORY);

		return resourceCategories;
	}
}
