package am.xacml.context.impl;

import am.xacml.context.Action;

/**
 * Action object for PMG operations.
 * @author mleahu
 * @version 1.0
 */
public class PMGAction extends Action {

	/**
	 * Values for action: start and terminate
	 */
	
	public static final String VALUE_ACTION_START 		= "start";
	public static final String VALUE_ACTION_TERMINATE 	= "terminate";

	/**
	 * Constructor that takes as argument the action value.
	 * @param actionValue action value has to be one of the VALUE_ACTION_*
	 */
	public PMGAction(String actionValue){
		super();
		addValue(getActionId(), actionValue);
	}

	/* (non-Javadoc)
	 * @see am.xacml.context.Action#toString()
	 */
	public String toString(){
		return "PMG Action: " + super.toString();
	}
}
