/**
 * 
 */
package am.xacml.context.impl;

import am.xacml.context.Action;

/**
 * Action class for the IGUI resource type.
 * @author mleahu
 * @version 1.0
 */
public class IGUIAction extends Action {
	
	/**
	 * The value of action id identifier
	 */
	private static String VALUE_ACTION_ID = "view";
	
	
	/**
	 * Default constructor
	 */
	public IGUIAction() {
		super();
		// add the value for action id into the internal map
		addValue(getActionId(), VALUE_ACTION_ID);
	}
}
