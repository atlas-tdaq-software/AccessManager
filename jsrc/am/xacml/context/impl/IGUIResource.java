/**
 * 
 */
package am.xacml.context.impl;

import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import am.util.AMException;
import am.xacml.context.Action;
import am.xacml.context.Resource;

/**
 * The Resource implementation for the IGUI resource type.
 * The action attribute is assigned internally as an IGUIAction object.
 * @author mleahu
 * @version 1.0
 */
public class IGUIResource extends Resource {

	private static final Logger logger =
        Logger.getLogger(IGUIResource.class.getName());
	
	/**
	 * the resource category is "igui"
	 */
	public static final String VALUE_RESOURCE_CATEGORY = "igui";
	
	/**
	 * The only possible action.
	 */
	private static Action iguiActionVIEW = new IGUIAction();
		
	/**
	 * The view mode value for "display only"
	 */
	public static final int VIEW_MODE_DISPLAY = 0;
	
	/**
	 * The view mode value for "control"
	 */
	public static final int VIEW_MODE_CONTROL = 1;

	/**
	 * The view mode value for "expert"
	 */
	public static final int VIEW_MODE_EXPERT = 2;
	
	/**
	 * The string values for all the commands
	 */
	public static final String [] VIEW_MODES = { "display", "control", "expert" };	

	
	/**
	 * Build an IGUIResource instance from the attributes values provided as input.
	 * The attributes should be:
	 *	- ResourceId = the view mode
	 * @param attributesMap the attributes as key and their values as a list of strings
	 * @return the new IGUI resource object
	 * @throws AMException
	 */
	public static IGUIResource getInstance(HashMap<String, List<String>> attributesMap) throws AMException{ 

		List<String> lViewMode 	= attributesMap.get("ResourceId");
		
		String viewMode = null;
		if (lViewMode != null && lViewMode.size() > 0){
			viewMode = lViewMode.get(0);
			logger.fine("Only the first view mode is considered:" + viewMode);
		} else {
			logger.severe("No view mode found! A value must be provided!");
			throw new AMException("No view mode found! A value must be provided!", AMException.WRONG_ARGUMENTS);
		}
		
		int viewModeId = -1;
		String[] modes = IGUIResource.VIEW_MODES;
		for(int i=0; i < modes.length; i++){
			if (modes[i] == null){
				if (viewMode == null){
					viewModeId = i;
					break;
				}
			} else {
				if (modes[i].equals(viewMode)){
					viewModeId = i;
					break;
				}
			}
		}
		
		if (viewModeId < 0){
			logger.severe("The viewMode [" + viewMode + "] was not found in the VIEW_MODES list");
			throw new AMException("The viewMode [" + viewMode + "] was not found in the VIEW_MODES list", AMException.WRONG_ARGUMENTS);
		}

		return new IGUIResource(viewModeId);
	}
	
	/**
	 * Creates a IGUI resource object with the provided view mode attribute.
	 * @param viewModeId the view mode id has to be one of the IGUIResource.VIEW_MODE_ constants
	 * @throws AMException with XACML_WRONG_ATTRIBUTE_VALUE id if the viewModeId is out of range
	 */
	public IGUIResource(int viewModeId) throws AMException {
		super();
		
		if (viewModeId < 0 || viewModeId >= VIEW_MODES.length){
			throw new AMException("View Mode id (" + viewModeId + ") out of range [0," + VIEW_MODES.length + "]",
					AMException.XACML_WRONG_ATTRIBUTE_VALUE);
		}
		
		addValue(getResourceCategory(), VALUE_RESOURCE_CATEGORY);		
		addValue(getResourceId(), VIEW_MODES[viewModeId]);
		
		// set the action for the resource id
		actions.put(getResourceId(), iguiActionVIEW);
		
		// set the description field
		this.description = "IGUI Resource";
	}
	
	/* (non-Javadoc)
	 * @see am.xacml.context.Resource#toString()
	 */
	public String toString(){
		return "IGUI Resource:" + super.toString();
	}

}
