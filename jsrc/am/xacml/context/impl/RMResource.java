package am.xacml.context.impl;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import am.util.AMCommon;
import am.util.AMException;
import am.xacml.context.Resource;

/**
 * The Resource implementation for Resource Manager (RM) resource type.
 * The constructor takes as argument the partition name. If not important for the request, the parameter may not be provided.
 * The action attribute is assigned internally as an RMAction object.
 * @author mleahu
 * @version 1.0
 */
public class RMResource extends Resource {

	private static final Logger logger =
        Logger.getLogger(RMResource.class.getName());

	/**
	 * the resource category is "ResourceManager"
	 */
	public static final String VALUE_RESOURCE_CATEGORY = "ResourceManager";
	
	/**
	 * the resource id prefix is "rm"
	 */
	private static final String VALUE_RESOURCE_ID = "rm";
	
	/**
	 * the partition name resource identifier;
	 * It has ":partition" attached to the resource type identifier.
	 */
	private static final URI IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME = 
		AMCommon.concatenateURI(':', IDENTIFIER_RESOURCE_TYPE, URI.create("partition"));
	
	/**
	 * The 2 possible RM actions.
	 */
	private static RMAction rmActionANY = new RMAction(RMAction.VALUE_ACTION_ANY);
	
	
	/**
	 * Build a RMResource instance from the attributes values provided as input.
	 * The attributes should be:
	 * 	- ResourceTypePartition = the partition name
	 * 	- ActionId = the action should be "any"
	 * @param attributesMap the attributes as key and their values as a list of strings
	 * @return the new RM resource object
	 * @throws AMException
	 */
	public static RMResource getInstance(HashMap<String, List<String>> attributesMap) throws AMException{
		List<String> lpartition, lactions;
		
		lpartition 	= attributesMap.get("ResourceTypePartition");			
		lactions 	= attributesMap.get("ActionId");
				
		String partition = null;
		if (lpartition != null && lpartition.size() > 0){
			partition = lpartition.get(0);
			logger.fine("Only the first partition name value is considered:" + partition);
		} else {
			logger.fine("No partition value found!");
		}
		
		if (lactions == null || lactions.size() == 0) {
			logger.severe("No action id value found! A value must be provided!");
			throw new AMException("No action id value found! A value must be provided!",
					AMException.WRONG_ARGUMENTS);
		}
		String action = lactions.get(0);

		RMResource rmResource = new RMResource(partition);
		RMAction rmAction = new RMAction(action);
		
		rmResource.setAction(rmAction);
		
		return rmResource;
	}
	
	/**
	 * Constructor that takes as parameter the partition name
	 * @param partitionName
	 */
	public RMResource(String partitionName){
		super();
		
		addValue(getResourceCategory(), VALUE_RESOURCE_CATEGORY);
		addValue(getResourceId(), VALUE_RESOURCE_ID);
		
		if (partitionName != null){
			this.types.add(IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME);
			addValue(IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME, partitionName);
		}
		
		// set the description field
		this.description = "RM Resource";
		
	}
		
    /**
	 * Set the action for the resource id
	 */
	private void setAction(RMAction rmAction){
		actions.put(getResourceId(), rmAction);
	}
	
	
    /**
	 * Set the action "any" for the resource id
	 */
	public void setActionANY(){
		setAction(rmActionANY);
	}

	/* (non-Javadoc)
	 * @see am.xacml.context.Resource#toString()
	 */
	public String toString(){
		return "RM Resource:" + super.toString();
	}
}
