package am.xacml.context.impl;

import am.util.AMException;
import am.xacml.context.Action;

/**
 * Action object for Data Base (DB) operations.
 * @author mleahu
 * @version 1.0
 */
public class DBAction extends Action {

    /**
     * The action ids in the ACTIONS array 
     */
    public static final int ACTION_ID_ADMIN 		= 0;
    public static final int ACTION_ID_CREATE_FILE	= 1;
    public static final int ACTION_ID_UPDATE_FILE	= 2;
    public static final int ACTION_ID_DELETE_FILE	= 3;
    public static final int ACTION_ID_CREATE_SUBDIR	= 4;
    public static final int ACTION_ID_DELETE_SUBDIR	= 5;
    
	/**
	 * The actions array
	 * ACTIONS[ACTION_ID_]
	 */
	public static final String[] ACTIONS = { 
		"admin",
		"create_file",
		"update_file",
		"delete_file",
		"create_subdir",
		"delete_subdir"		
	};
	
	/**
	 * Constructor that takes as argument the action id in the ACTIONS array.
	 * @param actionId the action id in the ACTIONS array
	 * @throws AMException 
	 */
	public DBAction(int actionId) throws AMException{
		super();
		if (actionId < 0 || actionId >= ACTIONS.length ) {
			throw new AMException("DB action id out of range!", AMException.WRONG_ARGUMENTS);
		}
		addValue(getActionId(), ACTIONS[actionId]);
	}

	/* (non-Javadoc)
	 * @see am.xacml.context.Action#toString()
	 */
	public String toString(){
		return "DB Action: " + super.toString();
	}
}
