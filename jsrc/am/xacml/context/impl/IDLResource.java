/**
 * 
 */
package am.xacml.context.impl;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import am.util.AMCommon;
import am.util.AMException;
import am.xacml.context.Resource;
import am.xacml.context.Action;

/**
 * Resource implementation specific for IDL interface and methods
 * @author mleahu
 * @version 1.0
 */
public class IDLResource extends Resource {

	/**
	 *  the resource category is "idl"
	 */
	private static String VALUE_RESOURCE_CATEGORY = "idl";
	
	/**
	 * The idl method resource identifier. Has ":method" attached to the resource type identifier. 
	 */
	private static URI IDENTIFIER_RESOURCE_TYPE_IDLMETHOD = 
		AMCommon.concatenateURI(':', IDENTIFIER_RESOURCE_TYPE, URI.create("method"));

	/**
	 * The idl specific action for all methods and interfaces
	 */
	private Action resourceTypeIDLAction;
	

	/**
	 * Creates a resource object that has associated for a single interface one or more method
	 * @param IDLInterface interface name
	 * @param IDLMethods list of methods
	 * @throws AMException
	 */
	public IDLResource(String IDLInterface, Collection<String> IDLMethods) throws AMException{
		super();
		
		initialize();
		
		setIDLInterfaceValue(IDLInterface);
		setIDLMethodValue(IDLMethods);
		
		// set the description field
		this.description = "IDL Resource";
	}
	
	/**
	 * Creates a resource object that has associated for a single interface only one method
	 * @param IDLInterface interface name
	 * @param IDLMethods list of methods
	 * @throws AMException
	 */
	public IDLResource(String IDLInterface, String IDLMethod) throws AMException{
		super();
		
		initialize();
		
		setIDLInterfaceValue(IDLInterface);
		List<String> IDLMethods = new ArrayList<String>(1);
		IDLMethods.add(IDLMethod);
		setIDLMethodValue(IDLMethods);
		
		// set the description field
		this.description = "IDL Resource";
	}
	
	/**
	 * Populate the list and maps from super class with known values.
	 */
	private void initialize(){
		// TYPES INITIALIZATION
		// add the idl method resource type
		this.types.add(IDENTIFIER_RESOURCE_TYPE_IDLMETHOD);
		
		// VALUES INITIALIZATION		
		addValue(getResourceCategory(), VALUE_RESOURCE_CATEGORY);
		
		// ACTIONS INITIALIZATION		
		// initialize the action object which will be used for all resource type identifiers
		this.resourceTypeIDLAction = new IDLAction();
		
		// initialize the actions for the idl method resource identifier
		this.actions.put(IDENTIFIER_RESOURCE_TYPE_IDLMETHOD, resourceTypeIDLAction);		
	}
		
	/**
	 * Set the IDL interface name as resource id
	 * @param IDLInterface the IDl interface name
	 * @throws AMException if the parameter is null
	 */
	private void setIDLInterfaceValue(String IDLInterface) throws AMException{
		if (IDLInterface == null){
			throw new AMException("NULL IDL Interface!", AMException.WRONG_ARGUMENTS);
		}
		addValue(getResourceId(), IDLInterface);
	}
	
	/**
	 * Set the IDL method name as value resource type with identifier IDL Method
	 * @param IDLMethod IDL method name
	 * @throws AMException if the parameter is null
	 */
	private void setIDLMethodValue(Collection<String> IDLMethods) throws AMException{
		if (IDLMethods == null){
			throw new AMException("No IDL Methods list!", AMException.WRONG_ARGUMENTS);
		}
		addValues(IDENTIFIER_RESOURCE_TYPE_IDLMETHOD, IDLMethods);
	}
	
}
