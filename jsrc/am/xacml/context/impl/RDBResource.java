/**
 * 
 */
package am.xacml.context.impl;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import am.util.AMCommon;
import am.util.AMException;
import am.xacml.context.Action;
import am.xacml.context.Resource;

/**
 * The Resource implementation for the RDB resource type.
 * The action attribute is assigned internally as an RDBAction object.
 * @author isolov
 * @version 1.0
 */
public class RDBResource extends Resource {

	private static final Logger logger =
        Logger.getLogger(RDBResource.class.getName());
	
	/**
	 * the resource category is "RDB"
	 */
	public static final String VALUE_RESOURCE_CATEGORY = "RDB";

	/**
	 * the resource id prefix is "rdb"
	 */
	private static final String VALUE_RESOURCE_ID = "rdb";
	
	/**
	 * the RDB's command name resource identifier;
	 * This is command to be executed by the RDB server
	 * It has ":command" attached to the resource type identifier.
	 */
	private static final URI IDENTIFIER_RESOURCE_TYPE_COMMAND_NAME = 
		AMCommon.concatenateURI(':', IDENTIFIER_RESOURCE_TYPE, URI.create("command"));
	
	/**
	 * the RDB's partition name resource identifier;
	 * This is the partition affected by the RDB command execution
	 * It has ":partition" attached to the resource type identifier.
	 */
	private static final URI IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME = 
		AMCommon.concatenateURI(':', IDENTIFIER_RESOURCE_TYPE, URI.create("partition"));

	/**
	 * The only possible action.
	 */
	private static Action rcActionEXEC = new RDBAction();
	
	/**
	 * The value for any command
	 */
	public static final int COMMAND_ANY = 0;
	
	/**
	 * The command "open" database
	 */
	public static final int	COMMAND_OPEN = 1;

	/**
	 * The command "close" database
	 */
	public static final int	COMMAND_CLOSE = 2;

  /**
   * The command "update" database file status
   */
  public static final int COMMAND_UPDATE = 3;

  /**
   * The command "reload" database
   */
  public static final int COMMAND_RELOAD = 4;

  /**
   * The command "open session" to edit database
   */
  public static final int COMMAND_OPEN_SESSION = 5;

  /**
   * The command "list sessions"
   */
  public static final int COMMAND_LIST_SESSIONS = 6;	
    
	/**
	 * The string values for all the commands
	 */
	public static final String [] COMMANDS = { null, "OPEN", "CLOSE", "UPDATE", "RELOAD", "OPEN_SESSION", "LIST_SESSIONS" };	
	
	/**
	 * Build an RDBResource instance from the attributes values provided as input.
	 * The attributes should be:
	 * 	- ResourceTypeCommand = the RDB command
	 * @param attributesMap the attributes as key and their values as a list of strings
	 * @return the new RDB resource object
	 * @throws AMException
	 */
	public static RDBResource getInstance(HashMap<String, List<String>> attributesMap) throws AMException{
		List<String> lCommand;
		List<String> lPartition;
		lCommand 	= attributesMap.get("ResourceTypeCommand");			
		lPartition 	= attributesMap.get("ResourceTypePartition");			
		
		String command = null;
		if (lCommand != null && lCommand.size() > 0){
			command = lCommand.get(0);
			logger.fine("Only the first command name is considered:" + command);
		} else {
			logger.fine("No command name found!");
		}

		String partition = null;
		if (lPartition != null && lPartition.size() > 0){
			partition = lPartition.get(0);
			logger.fine("Only the first partition name is considered:" + partition);
		} else {
			logger.finest("No partition name found!");
		}

		int commandId = -1;
		String[] cmds = RDBResource.COMMANDS;
		for(int i=0; i < cmds.length; i++){
			if (cmds[i] == null){
				if (command == null){
					commandId = i;
					break;
				}
			} else {
				if (cmds[i].equals(command)){
					commandId = i;
					break;
				}
			}
		}
		
		if (commandId < 0){
			logger.severe("The command [" + command + "] was not found in the COMMANDS list");
			throw new AMException("The command [" + command + "] was not found in the COMMANDS list",
					AMException.WRONG_ARGUMENTS);
		}
		
		return new RDBResource(commandId, partition);
	}
	
	/**
	 * Creates a RDB resource object with the provided command name and partition name attributes.
	 * @param commandId the command id has to be one of the RDBResource.COMMAND_ constants
	 * @param partitioName the partition name
	 * @throws AMException with XACML_WRONG_ATTRIBUTE_VALUE id if the commandId is out of range
	 */
	public RDBResource(int commandId, String partitionName) throws AMException {
		super();
		
		if (commandId < 0 || commandId >= COMMANDS.length){
			throw new AMException("Command id (" + commandId + ") out of range [0," + COMMANDS.length + "]",
					AMException.XACML_WRONG_ATTRIBUTE_VALUE);
		}
		
		addValue(getResourceCategory(), VALUE_RESOURCE_CATEGORY);
		addValue(getResourceId(), VALUE_RESOURCE_ID);
		
		
		if (COMMANDS[commandId] != null){
			this.types.add(IDENTIFIER_RESOURCE_TYPE_COMMAND_NAME);
			addValue(IDENTIFIER_RESOURCE_TYPE_COMMAND_NAME, COMMANDS[commandId]);
		}

		if (partitionName != null){
			this.types.add(IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME);
			addValue(IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME, partitionName);
		}

		// set the action for the resource id
		actions.put(getResourceId(), rcActionEXEC);

		// set the description field
		this.description = "RDB Resource";
	}
	
	/**
	 * Creates a RDB resource using only the command name.
	 * @param commandId the command id has to be one of the RDBResource.COMMAND_ constants
	 * @throws AMException with XACML_WRONG_ATTRIBUTE_VALUE id if the commandId is out of range
	 */
	public RDBResource(int commandId) throws AMException{
		this(commandId, null);
	}
	
	/* (non-Javadoc)
	 * @see am.xacml.context.Resource#toString()
	 */
	public String toString(){
		return "RDB Resource:" + super.toString();
	}	
}
