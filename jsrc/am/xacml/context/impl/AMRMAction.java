package am.xacml.context.impl;

import am.util.AMException;
import am.xacml.context.Action;

/**
 * Action object for AMRM (Access Manager Roles Manager) operations.
 * @author isolov
 * @version 1.0
 */
public class AMRMAction extends Action {

    /**
     * The action ids in the ACTIONS array 
     */
    public static final int ACTION_ID_ADMIN 		= 0;
    public static final int ACTION_ID_ASSIGN_ROLE	= 1;
    public static final int ACTION_ID_ENABLE_ROLE	= 2;
    
	/**
	 * The actions array
	 * ACTIONS[ACTION_ID_]
	 */
	public static final String[] ACTIONS = { 
		"admin",
		"assign",
		"enable"		
	};
	
	/**
	 * Constructor that takes as argument the action id in the ACTIONS array.
	 * @param actionId the action id in the ACTIONS array
	 * @throws AMException 
	 */
	public AMRMAction(int actionId) throws AMException{
		super();
		if (actionId < 0 || actionId >= ACTIONS.length ) {
			throw new AMException("AMRM action id out of range!", AMException.WRONG_ARGUMENTS);
		}
		addValue(getActionId(), ACTIONS[actionId]);
	}

	/* (non-Javadoc)
	 * @see am.xacml.context.Action#toString()
	 */
	public String toString(){
		return "AMRM Action: " + super.toString();
	}
}
