/**
 * 
 */
package am.xacml.context.impl;

import am.xacml.context.Action;

/**
 * Action class for the TriggerCommander resource type.
 * @author isolov
 * @version 1.0
 */
public class TriggerCommanderAction extends Action {
	
	/**
	 * The value of action id identifier
	 */
	private static String VALUE_ACTION_ID = "exec_cmd";
	
	
	/**
	 * Default constructor
	 */
	public TriggerCommanderAction() {
		super();
		// add the value for action id into the internal map
		addValue(getActionId(), VALUE_ACTION_ID);
	}
}
