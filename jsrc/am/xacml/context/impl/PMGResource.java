package am.xacml.context.impl;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import am.util.AMCommon;
import am.util.AMException;
import am.xacml.context.Action;
import am.xacml.context.Resource;

/**
 * The Resource implementation for PMG resource type.
 * The constructor takes as argument the process binary path. If not important for the request,
 * none can be provided 
 * The action attribute is assigned internally as an PMGAction object.
 * @author mleahu
 * @version 1.0
 */
public class PMGResource extends Resource {

	private static final Logger logger =
        Logger.getLogger(PMGResource.class.getName());

	/**
	 * the resource category is "pmg"
	 */
	public static final String VALUE_RESOURCE_CATEGORY = "pmg";
	
	/**
	 * the pmg hostname resource identifier;
	 * this is the hostname where the action is allowed on the process
	 * It has ":hostname" attached to the resource type identifier.
	 */
	private static final URI IDENTIFIER_RESOURCE_TYPE_HOSTNAME = 
		AMCommon.concatenateURI(':', IDENTIFIER_RESOURCE_TYPE, URI.create("hostname"));
	
	/**
	 * the pmg arguments resource identifier;
	 * these are the arguments passed to the process at start
	 * It has ":arguments" attached to the resource type identifier.
	 */
	private static final URI IDENTIFIER_RESOURCE_TYPE_ARGUMENTS = 
		AMCommon.concatenateURI(':', IDENTIFIER_RESOURCE_TYPE, URI.create("arguments"));

	/**
	 * the pmg resource attribute "owned-by-requester";
	 * these must be true or false: true if the requester owns the resource, false otherwise
	 * It has ":owned-by-requester" attached to the resource type identifier.
	 */
	private static final URI IDENTIFIER_RESOURCE_TYPE_OWNED_BY_REQUESTER = 
		AMCommon.concatenateURI(':', IDENTIFIER_RESOURCE_TYPE, URI.create("owned-by-requester"));

        /**
         * the pmg partition name resource identifier;
         * This is the partition affected by the pmg command execution
         * It has ":partition" attached to the resource type identifier.
         */
        private static final URI IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME =
                AMCommon.concatenateURI(':', IDENTIFIER_RESOURCE_TYPE, URI.create("partition"));


	/**
	 * The 2 possible PMG actions.
	 */
	private static Action pmgActionSTART = new PMGAction(PMGAction.VALUE_ACTION_START);
	private static Action pmgActionTERMINATE = new PMGAction(PMGAction.VALUE_ACTION_TERMINATE);
	
	
	/**
	 * Build an PMGResource instance from the attributes values provided as input.
	 * The attributes should be:
	 * 	- ResourceId = the process binary path
	 * 	- ResourceTypeHostname = the process hostname
	 * 	- ResourceTypeArguments = the process arguments
	 * 	- ResourceTypePartition = partition Name
	 * 	- ActionId = the action should be "start" or "terminate"
	 * @param attributesMap the attributes as key and their values as a list of strings
	 * @return the new PMG resource object
	 * @throws AMException
	 */
	public static PMGResource getInstance(HashMap<String, List<String>> attributesMap) throws AMException{
		List<String> lprocessBinaryPath, lprocessHostname, lprocessArguments, lprocessOwnedByReq, lpartition, lactions;
		lprocessBinaryPath 	= attributesMap.get("ResourceId");			
		lprocessHostname 	= attributesMap.get("ResourceTypeHostname");
		lprocessArguments	= attributesMap.get("ResourceTypeArguments");
		lprocessOwnedByReq	= attributesMap.get("ResourceTypeOwnedByRequester");
		lpartition      	= attributesMap.get("ResourceTypePartition");
		lactions 		= attributesMap.get("ActionId");
				
		String processBinaryPath = null;
		if (lprocessBinaryPath != null && lprocessBinaryPath.size() > 0){
			processBinaryPath = lprocessBinaryPath.get(0);
			logger.fine("Only the first process binary path value is considered:" + processBinaryPath);
		} else {
			logger.fine("No process binary path value found!");
		}

		String processHostname = null;
		String processArguments = null;
		Boolean processOwnedByReq = null;
				
		if (lprocessHostname != null)
			processHostname = lprocessHostname.get(0);
		if (lprocessArguments != null)
			processArguments = lprocessArguments.get(0);
		
		if (lprocessOwnedByReq != null)
			processOwnedByReq = new Boolean(lprocessOwnedByReq.get(0));

		String partition = null;            
		if (lpartition != null && lpartition.size() > 0){
                        partition = lpartition.get(0);
                        logger.fine("Only the first partition name is considered:" + partition);
                } else {
                        logger.finest("No partition name found!");
                }
				
		if (lactions == null || lactions.size() == 0) {
			logger.severe("No action id value found! A value must be provided!");
			throw new AMException("No action id value found! A value must be provided!",
					AMException.WRONG_ARGUMENTS);
		}
		String action = lactions.get(0);

		PMGResource pmgres = new PMGResource(
									processBinaryPath, 
									processHostname, 
									processArguments,
									processOwnedByReq,
									partition);
		if (PMGAction.VALUE_ACTION_START.equals(action))
			pmgres.setStartAction();
		if (PMGAction.VALUE_ACTION_TERMINATE.equals(action))
			pmgres.setTerminateAction();
		
		return pmgres;
	}
	
	/**
	 * Constructor that takes as parameter the process binary path,
	 * the hostname where the process is running,
	 * and the process arguments
	 * @param processBinaryPath
	 * @param processHostName
	 * @param processArguments
	 * @param ownedByRequester true or false
	 * @param partitioName the partition name
	 */
	public PMGResource(
			String processBinaryPath, 
			String processHostName, 
			String processArguments, 
			Boolean ownedByRequester,
			String partitionName){
		super();
		
		addValue(getResourceCategory(), VALUE_RESOURCE_CATEGORY);
		addValue(getResourceId(), processBinaryPath);
		
		if (processHostName != null){
			this.types.add(IDENTIFIER_RESOURCE_TYPE_HOSTNAME);
			addValue(IDENTIFIER_RESOURCE_TYPE_HOSTNAME, processHostName);
		}
		
		if (processArguments != null){
			this.types.add(IDENTIFIER_RESOURCE_TYPE_ARGUMENTS);
			addValue(IDENTIFIER_RESOURCE_TYPE_ARGUMENTS, processArguments);
		}
		
		if (ownedByRequester != null){
			this.types.add(IDENTIFIER_RESOURCE_TYPE_OWNED_BY_REQUESTER);
			addValue(IDENTIFIER_RESOURCE_TYPE_OWNED_BY_REQUESTER, ownedByRequester.toString());
		}
		
		if (partitionName != null){
                        this.types.add(IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME);
                        addValue(IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME, partitionName);
                }

		
		// set the description field
		this.description = "PMG Resource";
		
	}
	
	/**
	 * Constructor that takes as parameter the process binary path
	 * @param processBinaryPath
	 */
	public PMGResource(String processBinaryPath){
		this(processBinaryPath, null, null, null, null);
	}
	
    /**
	 * Set the action "start" for the resource id
	 */
	public void setStartAction(){
		actions.put(getResourceId(), pmgActionSTART);
	}
	
	/**
	 * Set the action "terminate" for the resource id
	 */
	public void setTerminateAction(){
		actions.put(getResourceId(), pmgActionTERMINATE);
	}

	/* (non-Javadoc)
	 * @see am.xacml.context.Resource#toString()
	 */
	public String toString(){
		return "PMG Resource:" + super.toString();
	}
}
