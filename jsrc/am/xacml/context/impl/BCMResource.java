/**
 * 
 */
package am.xacml.context.impl;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import am.util.AMCommon;
import am.util.AMException;
import am.xacml.context.Resource;

/**
 * The Resource implementation for BCM resource type.
 * The constructor takes as argument the partition name. If not important for the request, the parameter may not be provided.
 * The action attribute is assigned internally as an RMAction object.
 * @author scannicc
 * @version 1.0
 */
public class BCMResource extends Resource {

	private static final Logger logger =
        Logger.getLogger(BCMResource.class.getName());

	/**
	 * the resource category is "BCM"
	 */
	public static final String VALUE_RESOURCE_CATEGORY = "BCM";
	
	/**
	 * the resource id prefix is "bcm"
	 */
	private static final String VALUE_RESOURCE_ID = "bcm";
	
	/**
	 * the partition name resource identifier;
	 * It has ":partition" attached to the resource type identifier.
	 */
//D.	private static final URI IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME = 
//D.		AMCommon.concatenateURI(':', IDENTIFIER_RESOURCE_TYPE, URI.create("partition"));
	
	/**
	 * The 2 possible BCM actions.
	 */
	private static BCMAction bcmActionUPDATE = new BCMAction(BCMAction.VALUE_ACTION_UPDATE);
	private static BCMAction bcmActionCONFIGURE = new BCMAction(BCMAction.VALUE_ACTION_CONFIGURE);
	
	
	/**
	 * Build a BCMResource instance from the attributes values provided as input.
	 * The attributes should be:
	 * 	- ResourceTypePartition = the partition name
	 * 	- ActionId = the action should be "free", "lock" or regular expression
	 * @param attributesMap the attributes as key and their values as a list of strings
	 * @return the new BCM resource object
	 * @throws AMException
	 */
	 
	 
	public static BCMResource getInstance(HashMap<String, List<String>> attributesMap) throws AMException{
		List<String> lactions;
		
//D.		lpartition 	= attributesMap.get("ResourceTypePartition");			
		lactions 	= attributesMap.get("ActionId");
		
/*		String partition = null;
		if (lpartition != null && lpartition.size() > 0){
			partition = lpartition.get(0);
			logger.fine("Only the first partition name value is considered:" + partition);
		} else {
			logger.fine("No partition value found!");
		}
*/ // D.
              
		
		if (lactions == null || lactions.size() == 0) {
			logger.severe("No action id value found! A value must be provided!");
			throw new AMException("No action id value found! A value must be provided!",
					AMException.WRONG_ARGUMENTS);
		}
		String action = lactions.get(0);

		BCMResource bcmResource = new BCMResource(action);
		BCMAction bcmAction = new BCMAction(action);
		
		bcmResource.setAction(bcmAction);
		
		return bcmResource;
	}
 	
	/**
	 * Constructor that takes as parameter the partition name
	 * @param partitionName
	 */
	 
	public BCMResource(String action){
		super();
		
		addValue(getResourceCategory(), VALUE_RESOURCE_CATEGORY);
		addValue(getResourceId(), VALUE_RESOURCE_ID);
		
/* D.		if (partitionName != null){
			this.types.add(IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME);
			addValue(IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME, partitionName);
		}
*/ //D.
		
		// set the description field
		this.description = "BCM Resource";
		
	} 

    /**
	 * Set the action for the resource id
	 */
	private void setAction(BCMAction bcmAction){
		actions.put(getResourceId(), bcmAction);
	}
	
    /**
	 * Set the action "update" for the resource id
	 */
	public void setActionUPDATE(){
		setAction(bcmActionUPDATE);
	}
	
    /**
	 * Set the action "configure" for the resource id
	 */
	public void setActionCONFIGURE(){
		setAction(bcmActionCONFIGURE);
	}

	/* (non-Javadoc)
	 * @see am.xacml.context.Resource#toString()
	 */
	public String toString(){
		return "BCM Resource:" + super.toString();
	}
}
