/**
 * 
 */
package am.xacml.context.impl;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import am.util.AMCommon;
import am.util.AMException;
import am.xacml.context.Action;
import am.xacml.context.Resource;

/**
 * The Resource implementation for the RunControl resource type.
 * The action attribute is assigned internally as an RunControlAction object.
 * @author mleahu
 * @version 1.0
 */
public class RunControlResource extends Resource {

	private static final Logger logger =
        Logger.getLogger(RunControlResource.class.getName());
	
	/**
	 * the resource category is "RunControl"
	 */
	public static final String VALUE_RESOURCE_CATEGORY = "RunControl";

	/**
	 * the resource id prefix is "rc"
	 */
	private static final String VALUE_RESOURCE_ID = "rc";
	
	/**
	 * the run control's command name resource identifier;
	 * This is command to be executed by the run controller
	 * It has ":command" attached to the resource type identifier.
	 */
	private static final URI IDENTIFIER_RESOURCE_TYPE_COMMAND_NAME = 
		AMCommon.concatenateURI(':', IDENTIFIER_RESOURCE_TYPE, URI.create("command"));
	
	/**
	 * the run control's partition name resource identifier;
	 * This is the partition affected by the run control command execution
	 * It has ":partition" attached to the resource type identifier.
	 */
	private static final URI IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME = 
		AMCommon.concatenateURI(':', IDENTIFIER_RESOURCE_TYPE, URI.create("partition"));

	/**
	 * The only possible action.
	 */
	private static Action rcActionEXEC = new RunControlAction();
	
	
	/**
	 * The value for any command
	 */
	public static final int COMMAND_ANY = 0;
	
	/**
	 * The command "publish"
	 */
	public static final int	COMMAND_PUBLISH = 1;

	/**
	 * The command "publish stats"
	 */
	public static final int	COMMAND_PUBLISH_STATS = 2;

    /**
     * The command "publish statistics"
     */
    public static final int COMMAND_PUBLISH_STATISTICS = 3;

    /**
     * The command "user"
     */
    public static final int COMMAND_USER = 4;

    /**
     * The command "user_broadcast"
     */
    public static final int COMMAND_USER_BROADCAST = 5;
	
	/**
	 * The string values for all the commands
	 */
	public static final String [] COMMANDS = { null, "PUBLISH", "PUBLISHSTATS", "PUBLISHSTATISTICS", "USER", "USERBROADCAST" };	
	
	/**
	 * Build an RunControlResource instance from the attributes values provided as input.
	 * The attributes should be:
	 * 	- ResourceTypeCommand = the run control command
	 * @param attributesMap the attributes as key and their values as a list of strings
	 * @return the new RunControl resource object
	 * @throws AMException
	 */
	public static RunControlResource getInstance(HashMap<String, List<String>> attributesMap) throws AMException{
		List<String> lCommand;
		List<String> lPartition;
		lCommand 	= attributesMap.get("ResourceTypeCommand");			
		lPartition 	= attributesMap.get("ResourceTypePartition");			
		
		String command = null;
		if (lCommand != null && lCommand.size() > 0){
			command = lCommand.get(0);
			logger.fine("Only the first command name is considered:" + command);
		} else {
			logger.fine("No command name found!");
		}

		String partition = null;
		if (lPartition != null && lPartition.size() > 0){
			partition = lPartition.get(0);
			logger.fine("Only the first partition name is considered:" + partition);
		} else {
			logger.finest("No partition name found!");
		}

		int commandId = -1;
		String[] cmds = RunControlResource.COMMANDS;
		for(int i=0; i < cmds.length; i++){
			if (cmds[i] == null){
				if (command == null){
					commandId = i;
					break;
				}
			} else {
				if (cmds[i].equals(command)){
					commandId = i;
					break;
				}
			}
		}
		
		if (commandId < 0){
			logger.severe("The command [" + command + "] was not found in the COMMANDS list");
			throw new AMException("The command [" + command + "] was not found in the COMMANDS list",
					AMException.WRONG_ARGUMENTS);
		}
		
		return new RunControlResource(commandId, partition);
	}
	
	/**
	 * Creates a Run Control resource object with the provided command name and partition name attributes.
	 * @param commandId the command id has to be one of the RunControlResource.COMMAND_ constants
	 * @param partitioName the partition name
	 * @throws AMException with XACML_WRONG_ATTRIBUTE_VALUE id if the commandId is out of range
	 */
	public RunControlResource(int commandId, String partitionName) throws AMException {
		super();
		
		if (commandId < 0 || commandId >= COMMANDS.length){
			throw new AMException("Command id (" + commandId + ") out of range [0," + COMMANDS.length + "]",
					AMException.XACML_WRONG_ATTRIBUTE_VALUE);
		}
		
		addValue(getResourceCategory(), VALUE_RESOURCE_CATEGORY);
		addValue(getResourceId(), VALUE_RESOURCE_ID);
		
		
		if (COMMANDS[commandId] != null){
			this.types.add(IDENTIFIER_RESOURCE_TYPE_COMMAND_NAME);
			addValue(IDENTIFIER_RESOURCE_TYPE_COMMAND_NAME, COMMANDS[commandId]);
		}

		if (partitionName != null){
			this.types.add(IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME);
			addValue(IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME, partitionName);
		}

		// set the action for the resource id
		actions.put(getResourceId(), rcActionEXEC);

		// set the description field
		this.description = "Run Control Resource";
	}
	
	/**
	 * Creates a Run Control resource using only the command name.
	 * @param commandId the command id has to be one of the RunControlResource.COMMAND_ constants
	 * @throws AMException with XACML_WRONG_ATTRIBUTE_VALUE id if the commandId is out of range
	 */
	public RunControlResource(int commandId) throws AMException{
		this(commandId, null);
	}
	
	/* (non-Javadoc)
	 * @see am.xacml.context.Resource#toString()
	 */
	public String toString(){
		return "RC Resource:" + super.toString();
	}	
}
