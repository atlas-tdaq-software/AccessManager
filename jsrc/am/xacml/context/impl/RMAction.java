package am.xacml.context.impl;

import am.xacml.context.Action;

/**
 * Action object for Resource Manager (RM) operations.
 * @author mleahu
 * @version 1.0
 */
public class RMAction extends Action {

	/**
	 * Values for action: any
	 */
	
	public static final String VALUE_ACTION_ANY 		= "any";

	/**
	 * Constructor that takes as argument the action value.
	 * @param actionValue action value has to be one of the VALUE_ACTION_*
	 */
	public RMAction(String actionValue){
		super();
		addValue(getActionId(), actionValue);
	}

	/* (non-Javadoc)
	 * @see am.xacml.context.Action#toString()
	 */
	public String toString(){
		return "RM Action: " + super.toString();
	}
}
