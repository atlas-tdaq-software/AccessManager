package am.xacml.context.impl;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import am.util.AMCommon;
import am.util.AMException;
import am.xacml.context.Resource;

/**
 * The Resource implementation for RolesAdmin (Access Manager Roles Manager) resource type.
 * The constructor takes as argument the name to the role to be accessed.
 * The action attribute is set with a predefined AMRMAction object.
 * @author isolov
 * @version 1.0
 */
public class AMRMResource extends Resource {

	private static final Logger logger =
        Logger.getLogger(AMRMResource.class.getName());

	/**
	 * The resource category is "DataBase"
	 */
	public static final String VALUE_RESOURCE_CATEGORY = "RolesAdmin";
		
    /**
     * The id of resource id attribute in the RESOURCE_IDS array 
     */
    private static final int RESOURCE_IDS_ADMIN     = 0;
    private static final int RESOURCE_IDS_ROLE      = 1;
    
	/**
	 * The resource ids array
	 * RESOURCE_IDS[RESOURCE_IDS_]
	 */
	private static final String[] RESOURCE_IDS = { 
		"admin",
		"role"
	};
	
	/**
	 * the resource type identifier for the role name;
	 * It has ":name" attached to the resource type identifier.
	 */
	private static final URI IDENTIFIER_RESOURCE_TYPE_NAME = 
		AMCommon.concatenateURI(':', IDENTIFIER_RESOURCE_TYPE, URI.create("name"));
		
	
	/**
	 * Creates an AMRMResourceobject with an action attached
	 * @param resourceId   the id of the resource id attribute in the RESOURCE_IDS attributes
	 * @param name         the name of the role; is (null) for admin resource id
	 * @param actionId the action id from the AMRMAction.ACTION_ID_ constants
	 * @throws AMException if the id arguments are out of range
	 */
	private AMRMResource(int resourceId, String name, int actionId) throws AMException{
		super();
		
		if (resourceId < 0 || resourceId >= RESOURCE_IDS.length ) {
			throw new AMException("AMRM resource id out of range!", AMException.WRONG_ARGUMENTS);
		}
		
		addValue(getResourceCategory(), VALUE_RESOURCE_CATEGORY);
		addValue(getResourceId(), RESOURCE_IDS[resourceId]);
		
		if (name != null){
			this.types.add(IDENTIFIER_RESOURCE_TYPE_NAME);
			addValue(IDENTIFIER_RESOURCE_TYPE_NAME, name);
			actions.put(IDENTIFIER_RESOURCE_TYPE_NAME, new AMRMAction(actionId));
		} else {
			actions.put(getResourceId(), new AMRMAction(actionId));
		}
		
		// set the description field
		this.description = "Access Manager Roles Manager Resource";
		
	}
	
	/**
	 * Build an AMRMResource instance from the attributes values provided as input.
	 * The attributes should be:
	 * 	- ResourceId = the resource id which should be "admin" or "role"
	 * 	- ResourceTypeName = the name of the role
	 *  - ActionId = the action id as one of the constants AMRMAction.ACTION_ID_
	 * @param attributesMap the attributes as key and their values as a list of strings
	 * @return the new AMRM resource object
	 * @throws AMException
	 */
	public static AMRMResource getInstance(HashMap<String, List<String>> attributesMap) throws AMException{
		List<String> lresourceIds, lnames, lactions;
		
		lresourceIds = attributesMap.get("ResourceId");			
		lnames = attributesMap.get("ResourceTypeName");			
		lactions = attributesMap.get("ActionId");
		
		if (lresourceIds == null || lresourceIds.size() == 0) {
			logger.severe("No resource id value found! A value must be provided!");
			throw new AMException("No resource id value found! A value must be provided!",
					AMException.WRONG_ARGUMENTS);
		}
		int resourceIdId = -1;
		for(int i=0; i<AMRMResource.RESOURCE_IDS.length; i++){
			if (AMRMResource.RESOURCE_IDS[i].equals(lresourceIds.get(0))) {
				resourceIdId = i;
				break;
			}
		}
		if (resourceIdId < 0) {
			logger.severe("Invalid resource id value!");
			throw new AMException("Invalid resource id value!",	AMException.WRONG_ARGUMENTS);
		}
		
		
		String name = null;
		if (lnames != null && lnames.size() > 0){
			name = lnames.get(0);
			logger.fine("Only the first name value is considered:" + name);
		} else {
			logger.fine("No name value found!");
		}
		
		
		if (lactions == null || lactions.size() == 0) {
			logger.severe("No action id value found! A value must be provided!");
			throw new AMException("No action id value found! A value must be provided!",
					AMException.WRONG_ARGUMENTS);
		}
		int actionId = -1;
		for(int i=0; i<AMRMAction.ACTIONS.length; i++){
			if (AMRMAction.ACTIONS[i].equals(lactions.get(0))) {
				actionId = i;
				break;
			}
		}
		if (actionId < 0) {
			logger.severe("Invalid action id value!");
			throw new AMException("Invalid action id value!", AMException.WRONG_ARGUMENTS);
		}

		return new AMRMResource(resourceIdId, name, actionId);
	}
	
	/**
	 * Create an AMRMResource instance for ADMIN operations.
	 * @return new AMRMResource object
	 */
	public static AMRMResource getInstanceForAdminOperations() {
		try {
			return new AMRMResource(AMRMResource.RESOURCE_IDS_ADMIN, null, AMRMAction.ACTION_ID_ADMIN);
		} catch (AMException e) {
			logger.warning("Could not create AMRM resource instance for admin operations! Error: " + e.getMessage());
			return null;
		}
	}
	
	/**
	 * Create an AMRMResource instance for directory operations.
	 * @param name the name of the role
	 * @param actionId the action id as one of the constants in the AMRMAction.ACTION_ID_*_ROLE
	 * @return a new AMRMResource object
	 */
	public static AMRMResource getInstanceForRoleOperations(String name, int actionId) {
		try {
			return new AMRMResource(AMRMResource.RESOURCE_IDS_ROLE, name, actionId);
		} catch (AMException e) {
			logger.warning("Could not create AMRM resource instance for role operations! Error: " + e.getMessage());
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see am.xacml.context.Resource#toString()
	 */
	public String toString(){
		return "AMRM Resource:" + super.toString();
	}
}
