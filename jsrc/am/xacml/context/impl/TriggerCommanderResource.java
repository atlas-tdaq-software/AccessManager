/**
 * 
 */
package am.xacml.context.impl;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import am.util.AMCommon;
import am.util.AMException;
import am.xacml.context.Action;
import am.xacml.context.Resource;

/**
 * The Resource implementation for the TriggerCommander resource type.
 * The action attribute is assigned internally as an TriggerCommanderAction object.
 * @author isolov
 * @version 1.0
 */
public class TriggerCommanderResource extends Resource {

	private static final Logger logger =
        Logger.getLogger(TriggerCommanderResource.class.getName());
	
	/**
	 * the resource category is "TriggerCommander"
	 */
	public static final String VALUE_RESOURCE_CATEGORY = "TriggerCommander";

	/**
	 * the resource id prefix is "tc"
	 */
	private static final String VALUE_RESOURCE_ID = "tc";
	
	/**
	 * the trigger commander's command name resource identifier;
	 * This is command to be executed by the CHIP
	 * It has ":command" attached to the resource type identifier.
	 */
	private static final URI IDENTIFIER_RESOURCE_TYPE_COMMAND_NAME = 
		AMCommon.concatenateURI(':', IDENTIFIER_RESOURCE_TYPE, URI.create("command"));
	
	/**
	 * the run control's partition name resource identifier;
	 * This is the partition affected by the run control command execution
	 * It has ":partition" attached to the resource type identifier.
	 */
	private static final URI IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME = 
		AMCommon.concatenateURI(':', IDENTIFIER_RESOURCE_TYPE, URI.create("partition"));

	/**
	 * The only possible action.
	 */
	private static Action tcActionEXEC = new TriggerCommanderAction();
	
	
	/**
	 * The value for any command
	 */
	public static final int COMMAND_ANY = 0;
	
	/**
	 * The command "hold"
	 */
	public static final int	COMMAND_HOLD = 1;

	/**
	 * The command "resume"
	 */
	public static final int	COMMAND_RESUME = 2;

    /**
     * The command "set prescales"
     */
    public static final int COMMAND_SETPRESCALES = 3;

    /**
     * The command "set LEVEL-1 prescales"
     */
    public static final int COMMAND_SETL1PRESCALES = 4;

    /**
     * The command "set HLT prescales"
     */
    public static final int COMMAND_SETHLTPRESCALES = 5;

    /**
     * The command "set bunch group"
     */
    public static final int COMMAND_SETBUNCHGROUP = 6;

    /**
     * The command "set prescales and bunch group"
     */
    public static final int COMMAND_SETPRESCALESANDBUNCHGROUP = 7;

    /**
     * The command "set lumi block len"
     */
    public static final int COMMAND_SETLUMIBLOCKLEN = 8;

    /**
     * The command "set min lumi block len"
     */
    public static final int COMMAND_SETMINLUMIBLOCKLEN = 9;

    /**
     * The command "increase lumi block"
     */
    public static final int COMMAND_INCREASELUMIBLOCK = 10;

    /**
     * The command "set conditions update"
     */
    public static final int COMMAND_SETCONDITIONSUPDATE = 11;

	/**
	 * The string values for all the commands
	 */
	public static final String [] COMMANDS = { null, "HOLD", "RESUME", "SETPRESCALES", "SETL1PRESCALES", "SETHLTPRESCALES", "SETBUNCHGROUP", "SETPRESCALESANDBUNCHGROUP", "SETLUMIBLOCKLEN", "SETMINLUMIBLOCKLEN", "INCREASELUMIBLOCK", "SETCONDITIONSUPDATE" };	

	/**
	 * Build an TriggerCommanderResource instance from the attributes values provided as input.
	 * The attributes should be:
	 * 	- ResourceTypeCommand = the trigger commander command
	 * @param attributesMap the attributes as key and their values as a list of strings
	 * @return the new TriggerCommander resource object
	 * @throws AMException
	 */
	public static TriggerCommanderResource getInstance(HashMap<String, List<String>> attributesMap) throws AMException{
		List<String> lCommand;
		List<String> lPartition;
		lCommand 	= attributesMap.get("ResourceTypeCommand");			
		lPartition 	= attributesMap.get("ResourceTypePartition");			
		
		String command = null;
		if (lCommand != null && lCommand.size() > 0){
			command = lCommand.get(0);
			logger.fine("Only the first command name is considered:" + command);
		} else {
			logger.fine("No command name found!");
		}

		String partition = null;
		if (lPartition != null && lPartition.size() > 0){
			partition = lPartition.get(0);
			logger.fine("Only the first partition name is considered:" + partition);
		} else {
			logger.finest("No partition name found!");
		}

		int commandId = -1;
		String[] cmds = TriggerCommanderResource.COMMANDS;
		for(int i=0; i < cmds.length; i++){
			if (cmds[i] == null){
				if (command == null){
					commandId = i;
					break;
				}
			} else {
				if (cmds[i].equals(command)){
					commandId = i;
					break;
				}
			}
		}
		
		if (commandId < 0){
			logger.severe("The command [" + command + "] was not found in the COMMANDS list");
			throw new AMException("The command [" + command + "] was not found in the COMMANDS list",
					AMException.WRONG_ARGUMENTS);
		}
		
		return new TriggerCommanderResource(commandId, partition);
	}
	
	/**
	 * Creates a Trigger Commander resource object with the provided command name and partition name attributes.
	 * @param commandId the command id has to be one of the TriggerCommanderResource.COMMAND_ constants
	 * @param partitioName the partition name
	 * @throws AMException with XACML_WRONG_ATTRIBUTE_VALUE id if the commandId is out of range
	 */
	public TriggerCommanderResource(int commandId, String partitionName) throws AMException {
		super();
		
		if (commandId < 0 || commandId >= COMMANDS.length){
			throw new AMException("Command id (" + commandId + ") out of range [0," + COMMANDS.length + "]",
					AMException.XACML_WRONG_ATTRIBUTE_VALUE);
		}
		
		addValue(getResourceCategory(), VALUE_RESOURCE_CATEGORY);
		addValue(getResourceId(), VALUE_RESOURCE_ID);
		
		
		if (COMMANDS[commandId] != null){
			this.types.add(IDENTIFIER_RESOURCE_TYPE_COMMAND_NAME);
			addValue(IDENTIFIER_RESOURCE_TYPE_COMMAND_NAME, COMMANDS[commandId]);
		}

		if (partitionName != null){
			this.types.add(IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME);
			addValue(IDENTIFIER_RESOURCE_TYPE_PARTITION_NAME, partitionName);
		}

		// set the action for the resource id
		actions.put(getResourceId(), tcActionEXEC);

		// set the description field
		this.description = "Trigger Commander Resource";
	}
	
	/**
	 * Creates a Run Control resource using only the command name.
	 * @param commandId the command id has to be one of the TriggerCommanderResource.COMMAND_ constants
	 * @throws AMException with XACML_WRONG_ATTRIBUTE_VALUE id if the commandId is out of range
	 */
	public TriggerCommanderResource(int commandId) throws AMException{
		this(commandId, null);
	}
	
	/* (non-Javadoc)
	 * @see am.xacml.context.Resource#toString()
	 */
	public String toString(){
		return "TC Resource:" + super.toString();
	}	
}
