package am.xacml.context.impl;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import am.util.AMCommon;
import am.util.AMException;
import am.xacml.context.Resource;

/**
 * The Resource implementation for DataBase (DB) resource type.
 * The constructor takes as argument the path to the directory or file to be accessed.
 * The action attribute is set with a predefined DBAction object.
 * @author mleahu
 * @version 1.0
 */
public class DBResource extends Resource {

	private static final Logger logger =
        Logger.getLogger(DBResource.class.getName());

	/**
	 * the resource category is "DataBase"
	 */
	public static final String VALUE_RESOURCE_CATEGORY = "DataBase";
		
    /**
     * The id of resource id attribute in the RESOURCE_IDS array 
     */
    private static final int RESOURCE_IDS_ADMIN 		= 0;
    private static final int RESOURCE_IDS_DIRECTORY	= 1;
    private static final int RESOURCE_IDS_FILE		= 2;
    
	/**
	 * The resource ids array
	 * RESOURCE_IDS[RESOURCE_IDS_]
	 */
	private static final String[] RESOURCE_IDS = { 
		"admin",
		"directory",
		"file"		
	};
	
	/**
	 * the resource type identifier for the file or directory path;
	 * It has ":path" attached to the resource type identifier.
	 */
	private static final URI IDENTIFIER_RESOURCE_TYPE_PATH = 
		AMCommon.concatenateURI(':', IDENTIFIER_RESOURCE_TYPE, URI.create("path"));
		
	
	/**
	 * Creates a DB resource object with an action attached
	 * @param resourceId the id of the resource id attribute in the RESOURCE_IDS attributes
	 * @param path the path to the file or directory. Must be null for admin resource id
	 * @param actionId the action id from the DBAction.ACTION_ID_ constants
	 * @throws AMException if the id arguments are out of range
	 */
	private DBResource(int resourceId, String path, int actionId) throws AMException{
		super();
		
		if (resourceId < 0 || resourceId >= RESOURCE_IDS.length ) {
			throw new AMException("DB resource id out of range!", AMException.WRONG_ARGUMENTS);
		}
		
		addValue(getResourceCategory(), VALUE_RESOURCE_CATEGORY);
		addValue(getResourceId(), RESOURCE_IDS[resourceId]);
		
		if (path != null){
			this.types.add(IDENTIFIER_RESOURCE_TYPE_PATH);
			addValue(IDENTIFIER_RESOURCE_TYPE_PATH, path);
			actions.put(IDENTIFIER_RESOURCE_TYPE_PATH, new DBAction(actionId));
		} else {
			actions.put(getResourceId(), new DBAction(actionId));
		}
		
		// set the description field
		this.description = "DB Resource";
		
	}
	
	/**
	 * Build a DBResource instance from the attributes values provided as input.
	 * The attributes should be:
	 * 	- ResourceId = the resource id which should be "admin", "directory" or "file"
	 * 	- ResourceTypePath = the path to the file or directory
	 *  - ActionId = the action id as one of the constants DBAction.ACTION_ID_
	 * @param attributesMap the attributes as key and their values as a list of strings
	 * @return the new DB resource object
	 * @throws AMException
	 */
	public static DBResource getInstance(HashMap<String, List<String>> attributesMap) throws AMException{
		List<String> lresourceIds, lpaths, lactions;
		
		lresourceIds = attributesMap.get("ResourceId");			
		lpaths = attributesMap.get("ResourceTypePath");			
		lactions = attributesMap.get("ActionId");
		
		if (lresourceIds == null || lresourceIds.size() == 0) {
			logger.severe("No resource id value found! A value must be provided!");
			throw new AMException("No resource id value found! A value must be provided!",
					AMException.WRONG_ARGUMENTS);
		}
		int resourceIdId = -1;
		for(int i=0; i<DBResource.RESOURCE_IDS.length; i++){
			if (DBResource.RESOURCE_IDS[i].equals(lresourceIds.get(0))) {
				resourceIdId = i;
				break;
			}
		}
		if (resourceIdId < 0) {
			logger.severe("Invalid resource id value!");
			throw new AMException("Invalid resource id value!",	AMException.WRONG_ARGUMENTS);
		}
		
		
		String path = null;
		if (lpaths != null && lpaths.size() > 0){
			path = lpaths.get(0);
			logger.fine("Only the first path value is considered:" + path);
		} else {
			logger.fine("No path value found!");
		}
		
		
		if (lactions == null || lactions.size() == 0) {
			logger.severe("No action id value found! A value must be provided!");
			throw new AMException("No action id value found! A value must be provided!",
					AMException.WRONG_ARGUMENTS);
		}
		int actionId = -1;
		for(int i=0; i<DBAction.ACTIONS.length; i++){
			if (DBAction.ACTIONS[i].equals(lactions.get(0))) {
				actionId = i;
				break;
			}
		}
		if (actionId < 0) {
			logger.severe("Invalid action id value!");
			throw new AMException("Invalid action id value!",	AMException.WRONG_ARGUMENTS);
		}

		return new DBResource(resourceIdId, path, actionId);
	}
	
	/**
	 * Create a DBResource instance for ADMIN operations.
	 * @return new DBResource object
	 */
	public static DBResource getInstanceForAdminOperations() {
		DBResource dbResource = null;
		try {
			dbResource = new DBResource(DBResource.RESOURCE_IDS_ADMIN, null, DBAction.ACTION_ID_ADMIN);
		} catch (AMException e) {
			logger.warning("Could not create DB resource instance for admin operations! Error: " + e.getMessage());
		}
		return dbResource;
	}
	
	/**
	 * Create a DBResource instance for directory operations.
	 * @param path the path to the directory
	 * @param actionId the action id as one of the constants in the DBAction.ACTION_ID_
	 * @return a new DBResource object
	 */
	public static DBResource getInstanceForDirectoryOperations(String path, int actionId) {
		DBResource dbResource = null;
		try {
			dbResource = new DBResource(DBResource.RESOURCE_IDS_DIRECTORY, path, actionId);
		} catch (AMException e) {
			logger.warning("Could not create DB resource instance for directory operations! Error: " + e.getMessage());
		}
		return dbResource;
	}
	
	/**
	 * Create a DBResource instance for file operations.
	 * @param path the path to the file
	 * @param actionId the action id as one of the constants in the DBAction.ACTION_ID_
	 * @return a new DBResource object
	 */
	public static DBResource getInstanceForFileOperations(String path, int actionId) {
		DBResource dbResource = null;
		try {
			dbResource = new DBResource(DBResource.RESOURCE_IDS_FILE, path, actionId);
		} catch (AMException e) {
			logger.warning("Could not create DB resource instance for file operations! Error: " + e.getMessage());
		}
		return dbResource;
	}
	
	/* (non-Javadoc)
	 * @see am.xacml.context.Resource#toString()
	 */
	public String toString(){
		return "DB Resource:" + super.toString();
	}
}
