/**
 * 
 */
package am.xacml.context.impl;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import am.util.AMCommon;
import am.util.AMException;
import am.xacml.context.Resource;

/**
 * The Resource implementation for the Operating System resource type.
 * The constructor takes as argument the resource id and type strings.
 * @author mleahu
 * @version 1.0
 */

public class OSResource extends Resource {

	private static final Logger logger =
        Logger.getLogger(OSResource.class.getName());
	
	/**
	 * the resource category is "os"
	 */
	public static final String VALUE_RESOURCE_CATEGORY = "os";
	
	/**
	 * The application name that is affected by the permission.
	 */
	private static final URI IDENTIFIER_RESOURCE_TYPE_APPLICATION = 
		AMCommon.concatenateURI(':', IDENTIFIER_RESOURCE_TYPE, URI.create("application"));

	/**
	 * The arguments for the application.
	 */
	private static final URI IDENTIFIER_RESOURCE_TYPE_ARGUMENTS = 
		AMCommon.concatenateURI(':', IDENTIFIER_RESOURCE_TYPE, URI.create("arguments"));
		
	/**
	 * Build an OSResource instance from the attributes values provided as input.
	 * The attributes should be:
	 * 	- ResourceId = the resource location
	 * 	- ResourceType = the application
	 * 	- ResourceTypeArguments = the application arguments
	 * 	- ActionId = the action
	 * @param attributesMap the attributes as key and their values as a list of strings
	 * @return the new OS resource object
	 * @throws AMException
	 */
	public static OSResource getInstance(HashMap<String, List<String>> attributesMap) throws AMException{
		
		List<String> lresourceLocation, lapplication, larguments, lactions;
		lresourceLocation 	= attributesMap.get("ResourceId");	
		lapplication 		= attributesMap.get("ResourceType");
		larguments			= attributesMap.get("ResourceTypeArguments");
		lactions 			= attributesMap.get("ActionId");
				
		String resourceLocation = null;
		if (lresourceLocation != null && lresourceLocation.size() > 0){
			resourceLocation = lresourceLocation.get(0);
			logger.fine("Only the first resource location value is considered:" + resourceLocation);
		} else {
			logger.severe("No resource location value found! At least one value must be provided!");
			throw new AMException("No resource location value found! At least one value must be provided!",
					AMException.WRONG_ARGUMENTS);
		}

		String application = null;
		if (lapplication != null && lapplication.size() > 0){
			application = lapplication.get(0);
			logger.fine("Only the first application value is considered:" + application);
		} else {
			logger.severe("No application value found! At least one value must be provided!");
			throw new AMException("No application value found! At least one value must be provided!",
					AMException.WRONG_ARGUMENTS);
		}
		
		String arguments = null;
		if (larguments != null && larguments.size() > 0){
			arguments = larguments.get(0);
			logger.fine("Only the first arguments value is considered:" + arguments);
		} else {
			logger.fine("No arguments value found!");
		}
		
		String action = null;
		if (lactions != null && lactions.size() > 0){
			action = lactions.get(0);
			logger.fine("Only the first action value is considered:" + action);
		} else {
			logger.severe("No action id value found! At least one value must be provided!");
			throw new AMException("No action id value found! At least one value must be provided!",
					AMException.WRONG_ARGUMENTS);
		}
		
		OSResource osResourceObj= new OSResource(resourceLocation, application, arguments);
		osResourceObj.setAction(action);
		return osResourceObj;
	}
	
	/**
	 * The constructor of an OS resource.
	 * @param resourceLocation the resource location (e.g., the gateways, the control room desktop)
	 * @param application the application at that location (the login script,  the shell)
	 * @param arguments the arguments for the application. This is optional.
	 */
	public OSResource(String resourceLocation, String application, String arguments) {
		super();
		
		addValue(getResourceCategory(), VALUE_RESOURCE_CATEGORY);
		addValue(getResourceId(), resourceLocation);
		
		this.types.add(IDENTIFIER_RESOURCE_TYPE_APPLICATION);
		addValue(IDENTIFIER_RESOURCE_TYPE_APPLICATION, application);		
		
		if (arguments != null && arguments.length() > 0 ) {
			this.types.add(IDENTIFIER_RESOURCE_TYPE_ARGUMENTS);
			addValue(IDENTIFIER_RESOURCE_TYPE_ARGUMENTS, arguments);		
		}
	}
	
	/**
	 * The constructor of an OS resource.
	 * @param resourceLocation the resource location (e.g., the gateways, the control room desktop)
	 * @param application the application at that location (the login script,  the shell)
	 */
	public OSResource(String resourceLocation, String application) {
		this(resourceLocation, application, null);
	}

	
	/**
	 * Set the action for the application resource type.
	 * @param action the action object
	 */
	private void setAction(OSAction action) {
		actions.put(IDENTIFIER_RESOURCE_TYPE_APPLICATION, action);	
	}
	
	/**
	 * Set the action for the OS resource.
	 * @param action the action string.
	 */
	public void setAction(String action) {
		setAction(new OSAction(action));
	}
	
}
