package am.xacml.context.impl;

import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import am.util.AMException;
import am.xacml.context.Environment;

/**
 * An ATLAS specific environment class to specify the system status like "running", 
 * "offline",  "maintenance".
 * @author mleahu
 * @version 1.0
 */
public class SystemStateEnvironment extends Environment{

	private static final Logger logger =
        Logger.getLogger(SystemStateEnvironment.class.getName());
	
	/**
	 * The category of environment information.
	 */
	public static final String ENVIRONMENT_CATEGORY = "SystemState";
	
	/**
	 * ATLAS specific identifier
	 */
	public static final URI IDENTIFIER_ENVIRONMENT_SYSTEM_STATE = URI.create(
		"urn:oasis:names:tc:xacml:1.0:environment:system-state");
	
	public static final URI DATA_TYPE_SYSTEM_STATE = URI.create(
	"http://www.w3.org/2001/XMLSchema#string");

	/**
	 * The default match function.
	 */
	private static final URI DEFAULT_MATCH_FUNCTION = URI.create(		
		"urn:oasis:names:tc:xacml:1.0:function:string-equal");	

	/**
	 * Build an SystemState environment instance from the attributes values provided as input.
	 * The attributes should be:
	 * 	- EnvironmentSystemState = the state of the system
	 * @param attributesMap the attributes as key and their values as a list of strings
	 * @return the new SystemState environment object
	 * @throws AMException
	 */
	public static SystemStateEnvironment getInstance(HashMap<String, List<String>> attributesMap) throws AMException{
		SystemStateEnvironment ssEnv = null;
		List<String> lenvironmentStates;
		lenvironmentStates = attributesMap.get("EnvironmentSystemState");			
				
		if (lenvironmentStates == null || lenvironmentStates.size() == 0){
			logger.fine("No system states values found!");
		} else {
			ssEnv = new SystemStateEnvironment();
			ssEnv.addValues(lenvironmentStates);
		}
		
		return ssEnv;
	}
	
	
	/**
	 * Creates a system state environment object
	 * @param value a string to specify a system state
	 */
	public SystemStateEnvironment(){
		super();
	}

	/**
	 * Get the system state attribute identifier
	 * @return the identifier as URI
	 */
	public URI id() {
		return IDENTIFIER_ENVIRONMENT_SYSTEM_STATE;
	}
	
	/**
	 * Add one value for the system status
	 * @param value
	 */
	public void addValue(String value){
		addValue(id(), value);
	}
	
	/**
	 * Add more values for the system status
	 * @param valuesCollection
	 */
	public void addValues(Collection<String> valuesCollection){
		addValues(id(), valuesCollection);
	}
	/**
	 * Get the system state value
	 * @return the system state value
	 */
	public List<String> values() {
		return getValues(id());
	}
	
	/* (non-Javadoc)
	 * @see am.xacml.context.AttributeIDsValues#getDataType(java.net.URI)
	 */
	@Override
	public URI getDataType(URI identifier) {
		return DATA_TYPE_SYSTEM_STATE;
	}

	/* (non-Javadoc)
	 * @see am.xacml.context.AttributeIDsValues#getMatchFunction(java.net.URI)
	 */
	@Override
	public URI getMatchFunction(URI identifier) {
		return DEFAULT_MATCH_FUNCTION;
	}
	
	
}
