/**
 * 
 */
package am.xacml.context.impl;

import am.xacml.context.Action;

/**
 * Action object for IDL operations.
 * @author mleahu
 * @version 1.0
 */
public class IDLAction extends Action {

	
	/**
	 * The value of action id identifier
	 */
	private static String VALUE_ACTION_ID = "call";
	
	
	/**
	 * Default constructor
	 */
	public IDLAction() {
		super();
		// add the value for action id into the internal map
		addValue(getActionId(), VALUE_ACTION_ID);
	}
	
}
