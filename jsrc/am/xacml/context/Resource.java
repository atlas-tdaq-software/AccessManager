/**
 * 
 */
package am.xacml.context;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * The abstract implementation of Resource interface
 * @author mleahu
 * @version 1.0
 */
public abstract class Resource extends AttributeIDsValues{

    private static final Logger logger =
        Logger.getLogger(Resource.class.getName());

    /**
	 * Resouces identifiers 
	 */
	
	/**
	 * Resource category used in the PPS target definition.
	 */
	protected static final URI IDENTIFIER_RESOURCE_CATEGORY = URI.create(
		"urn:oasis:names:tc:xacml:1.0:resource:resource-category");	
	
	/**
	 * Resource id used in the PP target definition.
	 */
	protected static final URI IDENTIFIER_RESOURCE_ID = URI.create(
		"urn:oasis:names:tc:xacml:1.0:resource:resource-id");
	
	/**
	 * Resource type used in the rule target definition.
	 */
	protected static final URI IDENTIFIER_RESOURCE_TYPE = URI.create(
		"urn:oasis:names:tc:xacml:1.0:resource:resource-type");	

	/**
	 * Resource location: for future use;
	 */
	protected static final URI IDENTIFIER_RESOURCE_LOCATION = URI.create(
		"urn:oasis:names:tc:xacml:1.0:resource:resource-location");
	
	/**
	 * The possible resource types
	 */
	protected List<URI> types;	
	
	/**
	 * The action for each resource identifier.
	 */
	protected Map<URI, Action> actions;
	
	
	/**
	 * The resource description
	 */
	protected String description;
	
	/**
	 * Default constructor 
	 */
	public Resource(){
		super();
		this.types = new ArrayList<URI>();
		this.actions = new Hashtable<URI, Action>();
		this.description = "";
	}
	
	/**
	 * Get the resource category used in the PPS target definition.
	 * @return resource category identifier
	 */
	protected URI getResourceCategory() {
		return IDENTIFIER_RESOURCE_CATEGORY;
	}


	/**
	 * Get the resource id used in the PP target definition.
	 * @return resource id identifier
	 */
	protected URI getResourceId() {
		return IDENTIFIER_RESOURCE_ID;
	}

	/**
	 * Get the resource types
	 * @return list of identifiers for resource types
	 */
	protected List<URI> getResourceTypes() {
		return types;
	}

	/**
	 * Get the resource attribute id to be used in the Permission Policy Set target
	 * @return resource attribute identifier
	 */
	public URI getAttributeIDForPPSTarget() {
		return getResourceCategory();
	}
	
	/**
	 * Get the resource attribute id to be used in the Permission Policy target
	 * @return resource attribute identifier
	 */
	public URI getAttributeIDForPPTarget() {
		return getResourceId();
	}
	
	/**
	 * Get the resource attribute id to be used in the rule target
	 * @return resource attribute identifier
	 */
	public List<URI> getAttributeIDForRuleTarget() {
		return getResourceTypes();
	}
	
	/**
	 * Get the resource dscription
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Get the action object associated with the resource identifier
	 * @param identifier resource identifier
	 * @return action object
	 */
	public Action getAction(URI identifier){
		if (actions.containsKey(identifier)){
			return actions.get(identifier);
		} else {
			logger.fine("Action for resource identifier [" + identifier + "] not found");
			return null;
		}		
	}	

	/**
	 * Get all the action objects
	 * @return action object
	 */
	public Collection<Action> getActions(){
		return actions.values();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return "Resource: " + super.toString() +
			"ACTIONS=[" 	+ actions 		+ "]";
	}
	
}
