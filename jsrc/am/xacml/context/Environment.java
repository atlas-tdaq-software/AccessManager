package am.xacml.context;

import java.net.URI;

/**
 * Interface to define the XACML identifiers for the environment.
 * @author mleahu
 *
 */
public abstract class Environment extends AttributeIDsValues{

	 /**
	 * Environment identifiers 
	 */
	public static final URI IDENTIFIER_ENVIRONMENT_DATE = URI.create(
		"urn:oasis:names:tc:xacml:1.0:environment:date");
	
	public static final URI IDENTIFIER_ENVIRONMENT_TIME = URI.create(
		"urn:oasis:names:tc:xacml:1.0:environment:time");	

	/**
	 * The data type specified in URI format
	 */
	public static final URI DATA_TYPE_DATE = URI.create(
		"http://www.w3.org/2001/XMLSchema#date");

	public static final URI DATA_TYPE_TIME = URI.create(
	"http://www.w3.org/2001/XMLSchema#date");

}
