package am.xacml.context;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.sun.xacml.Obligation;
import com.sun.xacml.ctx.ResponseCtx;
import com.sun.xacml.ctx.Result;
import com.sun.xacml.ctx.Status;

/**
 * Takes a sunxacml Response object and get out from it the various pieces like the 
 * result of the evaluation, the reason etc
 * @author mleahu
 * @version 1.0
 */
public class ResponseProcessor {

	public static final String STATUS_MSG_SERVER_BUSY = "SERVER_BUSY";
	public static final String STATUS_MSG_CLIENT_REDIRECTED = "SERVER_BUSY";
	
	/**
	 * Get the decisions from the response.
	 * @param response
	 * @return
	 */
	public static String getDecision(ResponseCtx response){
		if (response == null){
			return "Null response object!";
		}
		String message = "";
		for(Result result: (Set<Result>)response.getResults()){
			message += Result.DECISIONS[result.getDecision()] + " ";
		}		
		return message;
	}
	
	/**
	 * Get the obligations from the response.
	 * @param response
	 * @return
	 */
	public static String getObligations(ResponseCtx response){
		if (response == null){
			return "Null response object!";
		}
		
		String message = "";
		boolean hasObligations = false;
		for(Result result: (Set<Result>)response.getResults()){
			Set<Obligation> obligations = result.getObligations();
			if (obligations == null || obligations.size() > 0 ) {
				hasObligations = true;
				for(Obligation obligation: obligations){
					message += obligation.toString() + " ";					
				}
			}
				
		}
		
		if (!hasObligations){
			message = "";
		}
		
		return message;
	}

	/**
	 * Get the resource from the response.
	 * @param response
	 * @return
	 */
	public static String getResource(ResponseCtx response){
		if (response == null){
			return "Null response object!";
		}
		String message = "";
		boolean hasResource = false;
		for(Result result: (Set<Result>)response.getResults()){
			String resource = result.getResource();
			if (resource != null){
				hasResource = true;
				message += resource + " ";
			}
		}
		if (!hasResource){
			message = "";
		}
		
		return message;
	}
	
	/**
	 * Get the status from the response.
	 * @param response
	 * @return
	 */
	public static String getStatus(ResponseCtx response){
		if (response == null){
			return "Null response object!";
		}
		String message = "";
		for(Result result: (Set<Result>)response.getResults()){
			Status status =  result.getStatus();
			message += status.getCode();
			if (status.getMessage() != null) {
				message += "(" + status.getMessage() + ")";
			}
			message += " ";
		}
		return message;
	}

	/**
	 * Get the status details from the response.
	 * @param response
	 * @return
	 */
	public static String getStatusDetails(ResponseCtx response){
		if (response == null){
			return "Null response object!";
		}
		String message = "";
		for(Result result: (Set<Result>)response.getResults()){
			Status status =  result.getStatus();
			
			if (status.getMessage() != null) {
				message += "(" + status.getMessage() + ")";
			}
			message += " ";
		}
		return message;
	}
	
	/**
	 * Check if the response is positive or not. If it's positive, then the access 
	 * request has been approved. If not, the reason may be found out using the
	 * getStatus method.
	 * @param response
	 * @return
	 */
	public static boolean isResponsePositive(ResponseCtx response){
		boolean permit = false;
		if (response == null){
			return permit;
		}
		
		permit = true;
		for(Result result: (Set<Result>)response.getResults()){
			permit = permit && (result.getDecision() == Result.DECISION_PERMIT);
		}		
		
		return permit;
	}
	
	/**
	 * Check if the response from the server indicates the server as being BUSY
	 * @param response
	 * @return
	 */
	public static boolean isServerBusy(ResponseCtx response){
		boolean busy = false;
		if (response == null){
			return busy;
		}
		
		for(Result result: (Set<Result>)response.getResults()){
			if (result.getDecision() == Result.DECISION_INDETERMINATE &&
				result.getStatus().getCode().contains(Status.STATUS_OK) &&
				result.getStatus().getMessage().indexOf(STATUS_MSG_SERVER_BUSY) >= 0){
				busy = true;
				break;
			}
		}
		
		return busy;
	}
	
	/**
	 * Check if the response from the server redirects the client
	 * @param response
	 * @return
	 */
	public static boolean isClientRedirected(ResponseCtx response){
		boolean redirected = false;
		if (response == null){
			return redirected;
		}
		
		for(Result result: (Set<Result>)response.getResults()){
			if (result.getDecision() == Result.DECISION_INDETERMINATE &&
				result.getStatus().getCode().contains(Status.STATUS_OK) &&
				result.getStatus().getMessage().indexOf(STATUS_MSG_CLIENT_REDIRECTED) >= 0){
				redirected = true;
				break;
			}
		}
		
		return redirected;
	}

	/**
	 * Check if the response is positive or not. If it's positive, then the access 
	 * request has been approved. If not, the reason may be found out using the
	 * getStatus method.
	 * @param response
	 * @return
	 */
	public static List<String> getSecondaryAMServers(ResponseCtx response){
		List<String> secondaryAMServers = new ArrayList<String>();
		for(Result result: (Set<Result>)response.getResults()){
			String statusMessage = result.getStatus().getMessage();
			// get server1 and server2 from something in a format like "text[server1,server2]text2"
			for(String s: statusMessage.split(".*\\[|,|\\].*")){
				if (s.length() > 0) {
					secondaryAMServers.add(s);
				}
			}
		}		
		
		return secondaryAMServers;
	}
	
	/**
	 * Get all the known components of a response
	 * @param response
	 * @return
	 */
	public static String toString(ResponseCtx response){
		if (response == null){
			return "Invalid response object!";
		}
		String message = "[RESPONSE]";
		
		String decision = getDecision(response);
		String obligations = getObligations(response);
		String resource = getResource(response);
		String status = getStatus(response);
		
		if (decision.length() > 0)
			message += "\n" + "DECISION:" + decision;
		if (obligations.length() > 0)
			message += "\n" + "OBLIGATIONS:" + obligations;
		if (resource.length() > 0)
			message += "\n" + "RESOURCE:" + resource;
		if (status.length() > 0)
			message += "\n" + "STATUS:" + status;
		
		return message;
	}
}
