package am.xacml.context;

import java.util.HashSet;
import java.util.Set;

import com.sun.xacml.ctx.Attribute;
import com.sun.xacml.ctx.RequestCtx;
import com.sun.xacml.ctx.Subject;

/**
 * Takes a sunxacml Request object and get out from it the various pieces like the subject,
 * resource to access, the operation and so on.
 * @author mleahu
 *
 */
public abstract class RequestProcessor {
	
	private static int COMPONENT_SUBJECT 		= 0;
	private static int COMPONENT_RESOURCE 		= 1;
	private static int COMPONENT_ACTION 		= 2;
	private static int COMPONENT_ENVIRONMENT 	= 3;
	
	private static String[] COMPONENTS_NAMES = {
		"SUBJECT",
		"RESOURCE",
		"ACTION",
		"ENVIRONMENT"
	};
	
	/**
	 * Create a String with the attributes id and values of a give request component as
	 * they are listed in the COMPONENTS_NAMES
	 * @param componentId
	 * @param request
	 * @return
	 */
	private static String getRequestComponent(int componentId, RequestCtx request){
		String component = "";
		
		if (componentId < 0 || componentId >= COMPONENTS_NAMES.length){
			return component;
		}
		
		component = COMPONENTS_NAMES[componentId] + ":"; 
			
		if (request == null){
			return component;
		}
		
		Set<Attribute> attrs = null;
		
		
		if (componentId == COMPONENT_SUBJECT ){			
			for(Subject sbj: (Set<Subject>)request.getSubjects()){
				if (attrs == null){
					attrs = new HashSet<Attribute>();
				}
				attrs.addAll(sbj.getAttributes());
			}
		} else if (componentId == COMPONENT_RESOURCE ){			
				attrs = request.getResource();
		} else if (componentId == COMPONENT_ACTION ){			
			attrs = request.getAction();
		} else if (componentId == COMPONENT_ENVIRONMENT ){			
			attrs = request.getEnvironmentAttributes();
		} else {
			return component;
		}
		
		for(Attribute attr: attrs){
			component += "[" + attr.getId() + "=" + attr.getValue().encode() + "] ";
		}
			
		return component;
	}

	
	/**
	 * Get the subject attributes from the request
	 * @param request
	 * @return a string containing all the attributes names and values
	 */
	public static String getSubjects(RequestCtx request){
		return getRequestComponent(COMPONENT_SUBJECT, request);
	}
	
	/**
	 * Get the resource attributes from the request
	 * @param request
	 * @return a string containing all the attributes names and values
	 */
	public static String getResource(RequestCtx request){
		return getRequestComponent(COMPONENT_RESOURCE, request);
	}

	/**
	 * Get the action attributes from the request
	 * @param request
	 * @return a string containing all the attributes names and values
	 */
	public static String getAction(RequestCtx request){
		return getRequestComponent(COMPONENT_ACTION, request);
	}

	/**
	 * Get the environment attributes from the request
	 * @param request
	 * @return a string containing all the attributes names and values
	 */
	public static String getEnvironment(RequestCtx request){
		return getRequestComponent(COMPONENT_ENVIRONMENT, request);
	}
	
	/**
	 * Get briefly the information from the XACML request
	 * @param request the XACML request
	 * @return the string with the information
	 */
	public static String toBriefString(RequestCtx request){
		String message = "";
		Set<Attribute> attrs = null;		
		
		message = "[";
		for(Subject sbj: (Set<Subject>)request.getSubjects()){
			message += sbj.getCategory();
			for(Attribute attr:(Set<Attribute>)sbj.getAttributes()){
				message += "(" + attr.getId() + "=" + attr.getValue().encode() + ")";
			}
		}
		message += "] requests access to [";
		for(Attribute attr:(Set<Attribute>)request.getResource()){
			message += "(" + attr.getId() + "=" + attr.getValue().encode() + ")";
		}
		
		message += "] for [";
		for(Attribute attr:(Set<Attribute>)request.getAction()){
			message += "(" + attr.getId() + "=" + attr.getValue().encode() + ")";
		}
		
//		message += "] in the environment[";
//		for(Attribute attr:(Set<Attribute>)request.getEnvironmentAttributes()){
//			message += "(" + AMCommon.cutURI(attr.getId()) + "=" + attr.getValue().encode() + ")";
//		}
		message +="]. ";
		
		return message;
	}
	
	/**
	 * Return all the known components from a request
	 * @param request
	 * @return 
	 */
	public static String toString(RequestCtx request){
		String message = "[REQUEST]";
		if (request == null){
			return "Invalid request object!";
		}
		message += "\n" + getSubjects(request);
		message += "\n" + getResource(request);
		message += "\n" + getAction(request);
		message += "\n" + getEnvironment(request);
		
		return message;
	}
	
}
