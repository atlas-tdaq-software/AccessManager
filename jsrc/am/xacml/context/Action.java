/**
 * 
 */
package am.xacml.context;

import java.net.URI;

/**
 * The abstract base class for action which extends the generic AttributeIDsValues class. 
 * @author mleahu
 * @version 1.0
 */
public abstract class Action extends AttributeIDsValues {
	
    /**
	 * Actions identifiers 
	 */
	private static final URI IDENTIFIER_ACTION_ID = URI.create(
		"urn:oasis:names:tc:xacml:1.0:action:action-id");
	
	/**
	 * The default constructor
	 */
	public Action(){
		super();
	}
	
	/**
	 * Get the action identifier
	 * @return the action URI identifier
	 */
	public URI getActionId() {
		return IDENTIFIER_ACTION_ID;
	}
		
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return "Action:" + super.toString();
	}
}
