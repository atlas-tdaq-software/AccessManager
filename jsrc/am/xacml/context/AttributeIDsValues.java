package am.xacml.context;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.sun.xacml.attr.AttributeFactory;
import com.sun.xacml.attr.AttributeValue;
import com.sun.xacml.ctx.Attribute;

import am.util.AMException;

/**
 * Generic class that contains generic identifiers and a list of value for each identifier.
 * The value of an identifier has a data type and a match function.
 * @author mleahu
 * @version 1.0
 */
public class AttributeIDsValues {

    private static final Logger logger =
        Logger.getLogger(AttributeIDsValues.class.getName());

    
    /**
     * The data type ids in the DATA_TYPES array 
     */
    protected static final int DATA_TYPE_ID_STRING = 0;
    protected static final int DATA_TYPE_ID_ANYURI = 1;
    protected static final int DATA_TYPE_ID_DEFAULT = DATA_TYPE_ID_STRING;
    
	/**
	 * The data types array
	 * DATA_TYPES[DATA_TYPE_ID]
	 */
	protected static final URI[] DATA_TYPES = { 
		URI.create("http://www.w3.org/2001/XMLSchema#string"),
		URI.create("http://www.w3.org/2001/XMLSchema#anyURI")
	};

    /**
     * The match function ids in the MATCH_FUNCTIONS array 
     */
    protected static final int MATCH_FUNCTION_ID_EQUAL = 0;
    protected static final int MATCH_FUNCTION_ID_REGEXP = 1;
    protected static final int MATCH_FUNCTION_ID_DEFAULT = MATCH_FUNCTION_ID_EQUAL;

	/**
	 * The match functions 
	 * MATCH_FUNCTIONS[MATCH_FUNCTION_ID][DATA_TYPE_ID] 
	 */
	protected static final URI[][] MATCH_FUNCTIONS = {
		// "equal" match functions
		{ URI.create("urn:oasis:names:tc:xacml:1.0:function:string-equal"),
			URI.create("urn:oasis:names:tc:xacml:1.0:function:anyURI-equal")},
			
		// regexp match functions
		{ URI.create("urn:oasis:names:tc:xacml:1.0:function:string-regexp-match"),
			URI.create("urn:oasis:names:tc:xacml:2.0:function:anyURI-regexp-match")},
	};
    
	/**
	 * The prefix to identify the match function from the attribute value 
	 * MATCH_FUNCTION_PREFIXES[MATCH_FUNCTION_ID]
	 */
	protected static final String[] MATCH_FUNCTION_PREFIXES = { 
		"",
		"{regexp}"
	};
	
	/**
	 * The values of each URI identifier
	 */
	private Map<URI, List<String>> values;
	
	/**
	 * The data type of each URI identifier 
	 */
	private Map<URI, URI> dataType;
	
	/**
	 * The match function of each URI identifier 
	 */
	private Map<URI, URI> matchFunction;
	
	/**
	 * Default constructor
	 */
	public AttributeIDsValues(){
		values = new HashMap<URI, List<String>>();
		dataType = new HashMap<URI, URI>();
		matchFunction = new HashMap<URI, URI>();
	}
		
	/**
	 * Add a value for an URI identifier and specify the data type of the values.
	 * The match function is detected automatically from the value prefix 
	 * (e.g., is it starts with '[regexp]', then the match function is regexp)
	 * @param identifier the URI identifier
	 * @param value the value as a String
	 * @param dataTypeId the data type id (one of the DATA_TYPE_ID_ predefined values)
	 */
	protected void addValue(URI identifier, String value, int dataTypeId){		
		int matchFunctionId = MATCH_FUNCTION_ID_DEFAULT;
		if (value != null) {
			String prefix;
			for(int i=0; i<MATCH_FUNCTION_PREFIXES.length; i++){
				prefix = MATCH_FUNCTION_PREFIXES[i]; 
				if (prefix.length() > 0 && value.startsWith(prefix)){
					// delete the prefix from the value
					value = value.substring(prefix.length());
					matchFunctionId = i;
					break;
				}
			}
			logger.finest("Match function detection for identifier [" + identifier + "] value[" + value + "] returned id[" + matchFunctionId + "]");
		} else {
			logger.finest("Match function detection skipped for identifier [" + identifier + "] because its value is null.");
		}
		
		// add the value
		List<String> lvalues = null;
		if (containsIdentifier(identifier)){
			lvalues = values.get(identifier);
		}else{
			lvalues = new ArrayList<String>();
			values.put(identifier, lvalues);
		}
		lvalues.add(value);
		
		setDataTypeAndMatchFunction(identifier, dataTypeId, matchFunctionId);
	}
	
	/**
	 * Add a value for an URI identifier with the data type "string".
	 * The match function is detected automatically from the value prefix 
	 * (e.g., is it starts with '[regexp]', then the match function is regexp)
	 * @param identifier the URI identifier
	 * @param value the value as a String
	 */
	protected void addValue(URI identifier, String value){
		addValue(identifier, value, DATA_TYPE_ID_STRING);
	}
	/**
	 * Add a collection of values to an URI identifier and specify the data type of the values
	 * and the match function to use (regular expression or exact match)
	 * @param identifier the URI identifier
	 * @param value the value as a String
	 * @param dataTypeId the data type id (one of the DATA_TYPE_ID_ predefined values)
	 * @param matchFunctionId the match function id (one of the MATCH_FUNCTION_ID_ predefined values)
	 */
	protected void addValues(URI identifier, Collection<String> valuesCollection, int dataTypeId, int matchFunctionId){
		List<String> lvalues = null;
		if (containsIdentifier(identifier)){
			lvalues = values.get(identifier);
		}else{
			lvalues = new ArrayList<String>();
			values.put(identifier, lvalues);
		}
		lvalues.addAll(valuesCollection);
		
		setDataTypeAndMatchFunction(identifier, dataTypeId, matchFunctionId);
	}
	
	/**
	 * Add a collection of values to an URI identifier with the default data type "String" 
	 * and the match function "equal"
	 * @param identifier the URI identifier
	 * @param values the value as a collection of Strings
	 */
	protected void addValues(URI identifier, Collection<String> valuesCollection){
		addValues(identifier, valuesCollection, DATA_TYPE_ID_STRING, MATCH_FUNCTION_ID_EQUAL);
	}
	
	/**
	 *  Helper method to set the data type and match function for an URI identifier
	 * @param identifier the URI identifier
	 * @param dataTypeId the data type id (one of the DATA_TYPE_ID_ predefined values)
	 * @param matchFunctionId the match function id (one of the MATCH_FUNCTION_ID_ predefined values)
	 */
	private void setDataTypeAndMatchFunction(URI identifier, int dataTypeId, int matchFunctionId){
		// set the data type
		int lDataTypeId = DATA_TYPE_ID_DEFAULT;
		if (dataTypeId >=0 && dataTypeId < DATA_TYPES.length) {
			lDataTypeId = dataTypeId;
		}
		dataType.put(identifier, DATA_TYPES[lDataTypeId]);
		
		// set the match function
		int lMatchFunctionId = MATCH_FUNCTION_ID_DEFAULT;		
		if (matchFunctionId >=0 && matchFunctionId < MATCH_FUNCTIONS.length) {
			lMatchFunctionId = matchFunctionId;
		}
		matchFunction.put(identifier, MATCH_FUNCTIONS[lMatchFunctionId][lDataTypeId]);		
	}
	
	/**
	 * Checks if the identifier is defined.
	 * @param identifier
	 * @return true if the identifier is defined internally in the map
	 */
	public boolean containsIdentifier(URI identifier){
		return values.containsKey(identifier);
	}
	
	/**
	 * Get the value of an attribute based on the attribute URI identifier.
	 * If there are more values available, only the first one is returned.
	 * @param identifier the attribute URI identifier
	 * @return the string value or null if no value found for the identifier;
	 */
	public String getValue(URI identifier){		
		if (containsIdentifier(identifier)){
			List<String> lvalues = values.get(identifier);
			if (lvalues == null || lvalues.size() == 0){
				logger.warning("No values for identifier [" + identifier + "]");
				return null;
			}
			return lvalues.get(0);
		} else {
			logger.warning("No values for identifier [" + identifier + "]");
			return null;
		}
	}

	/**
	 * Get the values of an attribute based on the attribute URI identifier.
	 * @param identifier the attribute URI identifier
	 * @return the strings list  or null if no value found for the identifier;
	 */
	public List<String> getValues(URI identifier){
		if (containsIdentifier(identifier)){
			return values.get(identifier);
		} else {
			logger.warning("No values for identifier [" + identifier + "]");
			return null;
		}
	}
		
	/**
	 * Get the data type for the given URI identifier
	 * @param identifier URI identifier
	 * @return the data type in URI format or null if no data type found for the identifier;
	 */
	public URI getDataType(URI identifier) {
		return dataType.get(identifier);
	}
	
	/**
	 * Get the match function for the given URI identifier
	 * @param identifier URI identifier
	 * @return the match function in URI format or null if no match function found for the identifier;
	 */
	public URI getMatchFunction(URI identifier) {
		return matchFunction.get(identifier);
	}

	/**
	 * Create an AttributeValue object based on the data type identifier
	 * and using the attribute factory.
	 * @param dataType the URI defining the type of the attribute
	 * @param value the value of attribute
	 * @return the AttributeValue object
	 */
	public static AttributeValue createAttributeValueInstance(URI dataType, String value) {
		try {
			AttributeFactory factory = AttributeFactory.getInstance();
			return factory.createValue(dataType, value);
		} catch (Exception e) {
			logger.severe("Could not create the attribute value object:" +
						" type [" + dataType + "] value [" + value + "]" +
						" Error: " + e.getMessage());
			return null;
		}
	}

	/**
	 * Create an AttributeValue object for the identifier's data type and value
	 * @param identifier
	 * @return the AttributeValue object
	 */
	public AttributeValue createAttributeValueInstance(URI identifier) {
		try {
			AttributeFactory factory = AttributeFactory.getInstance();
			return factory.createValue(getDataType(identifier), getValue(identifier));
		} catch (Exception e) {
			logger.severe("Could not create the attribute value for the identifier " + identifier +
						" Error: " + e.getMessage());
			return null;
		}
	}

	/**
	 * Builds an Attribute instance for the given identifier.
	 * The data type and value are automatically identified. 
	 * @param identifier the URI identifier
	 * @return new Attribute object
	 * @throws AMException in case the object can not be build
	 */
	public Attribute createAttributeInstance(URI identifier) throws AMException {
		AttributeValue value = createAttributeValueInstance(identifier);
		if (value == null){
			throw new AMException("Can not create attribute instance for identifier [" + identifier + "] ");
		}
		Attribute attribute = new Attribute(identifier, null, null, value);
		return attribute;
	}

	/**
	 * Builds a collection of attributes for all the identifiers in the values map
	 * @return collection of attribute objects
	 * @throws AMException
	 */
	public Collection<Attribute> createAttributeInstances() throws AMException {
		List<Attribute> attributes = new ArrayList<Attribute>();
		for(URI identifier: values.keySet()){
			AttributeValue value = createAttributeValueInstance(identifier);
			if (value == null){
				throw new AMException("Can not create attribute instance for identifier [" + identifier + "] ");
			}
			attributes.add(new Attribute(identifier, null, null, value));
		}
		return attributes;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return 	"Values[" 			+ values.toString() 		+ "] " +
				"DataTypes[" 		+ dataType.toString() 		+ "] " +
				"MatchFunction[" 	+ matchFunction.toString() 	+ "]";
	}
	
}
