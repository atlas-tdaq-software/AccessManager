package am.xacml.context;

import java.net.URI;

/**
 * ABC to define the XACML identifiers for the subject and the 
 * methods to get information about the subject.
 * @author mleahu
 *
 */
public abstract class Subjects extends AttributeIDsValues {
	

    /**
	 * Subjects identifiers 
	 */
	public static final URI IDENTIFIER_SUBJECT_ID = URI.create( 
		"urn:oasis:names:tc:xacml:1.0:subject:subject-id");	
	
	public static final URI IDENTIFIER_SUBJECT_DNSNAME = URI.create(
		"urn:oasis:names:tc:xacml:1.0:subject:authn-locality:dns-name");
	
	public static final URI IDENTIFIER_SUBJECT_IPADDR = URI.create( 
		"urn:oasis:names:tc:xacml:1.0:subject:authn-locality:ip-address");

	
	/**
	 * Subjects categories
	 */
	public static final URI CATEGORY_ACCESS_SUBJECT	= URI.create(
		"urn:oasis:names:tc:xacml:1.0:subject-category:access-subject");
	public static final URI CATEGORY_RECIPIENT_SUBJECT	= URI.create(
		"urn:oasis:names:tc:xacml:1.0:subject-category:recipient-subject");
	public static final URI CATEGORY_INTERMEDIARY_SUBJECT	= URI.create(
		"urn:oasis:names:tc:xacml:1.0:subject-category:intermediary-subject");
	public static final URI CATEGORY_CODEBASE	= URI.create(
		"urn:oasis:names:tc:xacml:1.0:subject-category:codebase");
	public static final URI CATEGORY_REQUESTING_MACHINE	= URI.create(
		"urn:oasis:names:tc:xacml:1.0:subject-category:requesting-machine");	
	public static final URI DEFAULT_CATEGORY = CATEGORY_ACCESS_SUBJECT;

	
	/**
	 * The default data type specified as "string"
	 */
	public static final URI DEFAULT_DATA_TYPE = DATA_TYPES[DATA_TYPE_ID_STRING];
	
	/**
	 * Default constructor
	 */
	public Subjects(){
		super();
	}
	
	/**
	 * Read the user name identifier.
	 * @return user name URI identifier
	 */
	public URI getUser() {
		return IDENTIFIER_SUBJECT_ID;
	}
	
	/**
	 * Read the host name identifier.
	 * @return host name URI identifier
	 */
	public URI getHost() {
		return IDENTIFIER_SUBJECT_DNSNAME;
	}
	/**
	 * Read the IP address identifier.
	 * @return IP address URI identifier
	 */
	public URI getIp() {
		return IDENTIFIER_SUBJECT_IPADDR;
	}
	
	/* (non-Javadoc)
	 * @see am.xacml.context.AttributeIDsValues#toString()
	 */
	public String toString(){
		return "Subjects:" + super.toString();
	}
}
