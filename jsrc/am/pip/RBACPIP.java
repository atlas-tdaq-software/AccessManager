/**
 * 
 */
package am.pip;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import am.pip.db.DBGeneric;
import am.rbac.RBACUser;
import am.rbac.RBACUserImpl;
import am.rbac.Role;
import am.rbac.RoleImpl;
import am.rbac.RolesSet;
import am.rbac.RolesSetImpl;
import am.util.AMException;
import am.util.cache.Cachetable;

/**
 * Database operations specific to the Role Based Access Control 
 * @author mleahu
 * @version 0.2
 */
public class RBACPIP {

	private static final Logger logger =
        Logger.getLogger(RBACPIP.class.getName());
		
	/**
	 * Generic DB used to get RBAC specific info.
	 */
	private DBGeneric db;
	
	/**
	 * Roles set object 
	 */
	private RolesSetImpl rolesSet;
	
	/**
	 * The RBAC users cache enabled flag
	 */
	private boolean rbacUsersCacheEnabled;

	/**
	 * The RBAC Users cache expiration time
	 */
	private static long RU_CACHE_TIMEOUT = 60000; // 60 sec

	/**
	 * Cache table for the RBACUser objects that don't contain protected information
	 */
	private Cachetable<String> rbacUsersCache;

	/**
	 * The constructor needs to know a DB generic implementation to work with. 
	 * @param db
	 */
	public RBACPIP(DBGeneric db) throws AMException{
		
		this.db = db;
		if (logger.isLoggable(Level.CONFIG)){
			logger.config("Initialize DB with: " + db);			
		}
		
		this.rolesSet = null;
		this.rbacUsersCache = null;
		
		// enable the cache
		this.rbacUsersCacheEnabled = true;
		
		// initialize the cache
		if(this.rbacUsersCacheEnabled){
			this.rbacUsersCache = new Cachetable<String>(RU_CACHE_TIMEOUT, "RBAC users");
			logger.fine("RBAC Users cache for users with public information is enabled! Timeout (ms):" + RU_CACHE_TIMEOUT);
		}

	}
	
	/**
	 * Flush the internal cache structures. 
	 * @return number of entries cleared from cache
	 */
	public int flushCache(){
		int entries = 0;
		if (rbacUsersCacheEnabled){
			entries = rbacUsersCache.size();
			rbacUsersCache.clear();
			logger.fine("Flush the RBAC Users cache");
		} else {
			logger.finest("RBAC Users cache not enabled: flush not possible.");
		}
		return entries;
	}
	
	private void refreshRolesSet() throws AMException{
		
		//TODO: cache mechanism here for roles set information
		if (logger.isLoggable(Level.FINE))
			logger.fine("Refresh the roles set ");
		
		// retrieve the roles assigned
		Map<String, List<String>> allRoles = db.getRoles();		
		rolesSet = new RolesSetImpl();
		
		//add the roles assignable
		for(String roleName: allRoles.get(DBGeneric.ROLES_ASSIGNABLE)){
			rolesSet.addRole(new RoleImpl(roleName, true));
		}
		// add the roles not assignable
		for(String roleNotAssignable: allRoles.get(DBGeneric.ROLES_NOT_ASSIGNABLE)){
			rolesSet.addRole(new RoleImpl(roleNotAssignable, false));
		}
	
		if (logger.isLoggable(Level.FINE))
			logger.fine("Built role set with roles [" + rolesSet.getRoles() + "]");			
		
		
		if (logger.isLoggable(Level.FINE))
			logger.fine("Check the senior");
		
		// for each role in roles set, check the seniors
		for(Role role: rolesSet.getRoles()){
			String roleName = role.getId().toString();
			
			List<String> seniors = db.getRoleSeniors(roleName);
			for (String roleSenior: seniors){
				try {
					rolesSet.setSeniorJuniorRelation(roleSenior,roleName);
				} catch (AMException e) {
					logger.severe( "Could not set the Senior-Junior relation! Error: " + e.getMessage());
					continue;
				}
			}
		}				
	}

	/**
	 * Get the roles set object
	 * @return roles set object
	 */
	public RolesSet getRolesSet() throws AMException {
		refreshRolesSet();
		return rolesSet;
	}

	/**
	 * Get the user information from DB and store it in the RBACUser object.
	 * A cache mechanism is implemented for the user objects without protected information.
	 * @param username the user name
	 * @return RBACUser object for the username
	 * @throws AMException if the user doesn't exist
	 */
	private RBACUser refreshUser(String username) throws AMException{
		
		// check the cache for the users with public information
		if (rbacUsersCacheEnabled) {
    		RBACUser cachedUser = (RBACUser)rbacUsersCache.get(username);
    		
    		if (cachedUser != null){
    			if (cachedUser.isOSValid()){
	        		if (logger.isLoggable(Level.FINEST)){
	        			logger.finest("Cache hit: " +
	        					"RBACUser object found for the username " + username);
	        		}
	    			return cachedUser;
    			} else {
    	    		if (logger.isLoggable(Level.FINEST)){
    	    			logger.finest("Cache hit: " +
    	    					"No OS valid user [" + username + "]");
    	    		}
    				throw new AMException("No OS valid user [" + username + "]!(cache info)", AMException.USER_NOT_FOUND);
    			}
    		} else{
        		if (logger.isLoggable(Level.FINEST)){
        			logger.finest("The RBACUser object for the username " + 
        					username + " not found in the cache");
        		}    			
    		}			
		}
		
		if (logger.isLoggable(Level.FINE))
			logger.fine("Refresh user information for [" + username + "]");

		RBACUserImpl user = new RBACUserImpl();
		
		if (! db.isSystemUser(username)){
			// update the cache with an invalid RBAC user object for the non existent user
			if (rbacUsersCacheEnabled) {
				rbacUsersCache.put(username, user);
	    		if (logger.isLoggable(Level.FINEST)){
	    			logger.finest("Cache update: " +
	    					"Put EMPTY RBACUser object " +
	    					"for the username [" + username + "] which was not found in the DB.");
	    		}
			}
			throw new AMException("No OS valid user [" + username + "]!", AMException.USER_NOT_FOUND);
		}
			
		// get the public attributes
		Map<String, List<String>> attrs = db.getUserAttributes(username, RBACUserImpl.USER_ATTRIBUTES);
		if (logger.isLoggable(Level.FINEST)){
			logger.finest("User public attributes: " + attrs);
		}
				
		// add the attributes to the user's object
		user.addAttributesValues(attrs);
		
		// get the roles for the user
		Map<String, List<String>> allRoles = db.getAllUserRoles(username);
		user.addRoles(allRoles.get(DBGeneric.ROLES_ASSIGNED), false);
		user.addRoles(allRoles.get(DBGeneric.ROLES_ENABLED), true);
		
		// update the cache
		if (rbacUsersCacheEnabled) {
			rbacUsersCache.put(username, user);
    		if (logger.isLoggable(Level.FINEST)){
    			logger.finest("Cache update: " +
    					"Put RBACUser object [" + user + "] " +
    					"for the username [" + username + "]");
    		}			
		}

		return user;
	}
		
	/**
	 * Builds a RBACUserImpl object from the DB extracting only the attributes
	 * that are public accessible
	 * @param username the username
	 * @return RBACUser object 
	 * @throws AMException if the user doesn't exist
	 */
	public RBACUser getUser(String username) throws AMException {
		return refreshUser(username);
	}
	
}
