package am.pip.db;

import java.util.*;

import am.util.AMException;

/**
 * A database generic interface. The methods only read information from the database.
 * @author mleahu
 * @version 2.0
 */
public interface DBGeneric {

	/**
	 * Map key for the roles assigned
	 */
	public static final String ROLES_ASSIGNED = "ASSIGNED";
	
	/**
	 * Map key for the roles enabled
	 */
	public static final String ROLES_ENABLED = "ENABLED";

	/**
	 * Map key for the roles assignable
	 */
	public static final String ROLES_ASSIGNABLE = "ASSIGNABLE";

	/**
	 * Map key for the roles not assignable
	 */
	public static final String ROLES_NOT_ASSIGNABLE = "NOT_ASSIGNABLE";

	/**
	 * Checks whether a user is registered in the system or not
	 * @param username the user login id 
	 * @return true if the user is found in the system, false otherwise
	 * @throws AMException
	 */
	public boolean isSystemUser(String username) throws AMException;
	
	/**
	 * Read attributes of an user entry in DB. The attributes are read only if they are public accessible.
	 * @param username the username
	 * @param attributesNames the list of attributes to be retrieved
	 * @return map of attributes names and the values of each attribute
	 * @throws AMException
	 */
	public Map<String, List<String>> getUserAttributes(String username, String[] attributesNames) throws AMException;

	/**
	 * Read attributes of an user entry in DB. The attributes are read if the current user logged to the database has accessed to them.
	 * @param username the username
	 * @param attributesNames the list of attributes to be retrieved
	 * @return map of attributes names and the values of each attribute
	 * @throws AMException
	 */
	public Map<String, List<String>> getUserProtectedAttributes(String username, String[] attributesNames) throws AMException;
	
	/**
	 * Retrieve the roles assigned to an user
	 * @param username the user name
	 * @param rolesEnabled true if the roles have to be in addition enabled
	 * @return a list of role names
	 * @throws AMException
	 */
	public List<String> getUserRoles(String username, boolean rolesEnabled) throws AMException;
	
	/**
	 * Retrieve all the roles assigned or enabled to the user.
	 * @param username the user name
	 * @return a map with two keys: ROLES_ENABLED and ROLES_ASSIGNED. For each key a list with enabled/assigned roles is returned.
	 * @throws AMException
	 */
	public Map<String, List<String>> getAllUserRoles(String username) throws AMException;

	/**
	 * Get the roles available in the system.
	 * @return a map with three keys: 
	 * 		ROLES_ASSIGNABLE and ROLES_NOT_ASSIGNABLE. 
	 * For each key a list with assignable/not assignable roles is returned.
	 * @throws AMException
	 */
	public Map<String, List<String>> getRoles() throws AMException ;
	
	/**
	 * Get the junior roles for the given role
	 * @param role the name of the role
	 * @return a list of role names representing the junior roles
	 * @throws AMException
	 */
	public List<String> getRoleJuniors(String role) throws AMException ;

	/**
	 * Get the senior roles for the given role
	 * @param role the name of the role
	 * @return a list of role names representing the senior roles
	 * @throws AMException
	 */
	public List<String> getRoleSeniors(String role) throws AMException ;
		

}