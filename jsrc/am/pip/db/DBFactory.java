/**
 * 
 */
package am.pip.db;

import java.util.logging.Logger;

import am.util.AMException;

/**
 * The PIP DataBase factory class. 
 * @author mleahu
 * @version 1.0
 */
public class DBFactory {
	private static final Logger logger =
	        Logger.getLogger(DBFactory.class.getName());
	 
	
	/**
	 * The LDAP database type
	 */
	public static final String DB_TYPE_LDAP = "LDAP";
	
	
	/**
	 * Creates a DB object to be used by the PIP.
	 * @param dbType can be one of the static DB_TYPE values
	 * @param parameters parameters are passed to specific db constructors. For LDAP type, 
	 * 					 the parameters should be: server name (mandatory), basedn (mandatory), port number.
	 * @return the new DB object
	 * @throws AMException
	 */
	public static DBGeneric getDB(String dbType, String[] parameters) throws AMException{
		
		if (dbType == null){
			logger.warning("No db type!");
			return null;
		}
		
		if (dbType.equals(DBFactory.DB_TYPE_LDAP)){
			String host = "";
			String basedn = "";
			int port = -1;
			
			if (parameters.length < 2) {
				logger.severe("At least 2 parameters must be provided to build a LDAP DB object!" +
						"Received:" + parameters);
				return null;
			} else if (parameters.length == 2){
				//first parameter is the LDAP host name
				host = parameters[0];
				//second parameter is the basedn
				basedn = parameters[1];
				return new DBLDAP(host, basedn);
			}
			
			//first parameter is the LDAP host name
			host = parameters[0];
			//second parameter is the basedn
			basedn = parameters[1];
			//third parameter is the port number
			try{
				port = Integer.valueOf(parameters[2]);
			} catch (NumberFormatException e){
				port = -1;
				logger.severe("Invalid port number for LDAP server " + parameters[2]);
			}
			return new DBLDAP(host, basedn, port);
		} else{
			logger.severe("Could not create a db object. Type unknown: " + dbType);
			return null;
		}
	}
	
}
