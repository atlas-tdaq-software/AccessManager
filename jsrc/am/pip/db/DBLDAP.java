package am.pip.db;


import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import am.util.AMException;

import com.novell.ldap.LDAPAttribute;
import com.novell.ldap.LDAPConnection;
import com.novell.ldap.LDAPConstraints;
import com.novell.ldap.LDAPEntry;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPSearchResults;
import com.novell.ldap.LDAPUrl;

/**
 * Interrogation of a LDAP database.
 * The roles are defined in the LDAP as NIS netgroups. The role is specified as a netgroup with the following
 * syntax: 
 *  - for roles assigned: cn=RA-rolename,ou=netgroup,BASEDN
 *  - for roles enabled:  cn=RE-rolename,ou=netgroup,BASEDN
 *  The users with a role assigned are members of RA-rolename netgroups. THe users with a role assigned
 *  and enabled are members of both RA-rolename and RE-rolename netgroups.
 * @author mleahu
 * @version 2.0
 */
public class DBLDAP implements DBGeneric{

	private static final Logger logger =
        Logger.getLogger(DBLDAP.class.getName());
	
	/**
	 * RDN of users records
	 */
	private static String RDN_USERS = "ou=People";
	
	private static String ATTR_USER_UID="uid";
	
	/**
	 * RDN of roles definitions tree
	 */
	private static String RDN_ROLES_DEFINITIONS = "ou=netgroup";

	/**
	 * Attribute name of a role that contains the role name
	 */
	private static String ATTR_ROLE = "cn";
		
	/**
	 * The attribute to specify if a role can be assigned to the users or not
	 */
	private static String ATTR_ROLE_ASSIGNABLE = "amRoleAssignable";
	
	/**
	 * The value that says the role can be assigned to users
	 */
	private static String VALUE_ROLE_ASSIGNABLE = "true";

	/**
	 * The value that says the role can be assigned to users
	 */
	private static String VALUE_ROLE_NOT_ASSIGNABLE = "false";
	
	/**
	 * Attribute name of a user member of a role
	 */
	private static String ATTR_ROLE_USER_MEMBER = "nisNetgroupTriple";

	/**
	 * Attribute name of a role member of a role
	 */
	private static String ATTR_ROLE_ROLE_MEMBER = "memberNisNetgroup";
	
	/**
	 * Object class name of a role definition
	 */
	private static String OBJCLASS_ROLE_DEFINITION = "amRole";

	/**
	 * The prefix of the RDN that designates a role assigned
	 */
	private static String RDN_PREFIX_ROLE_ASSIGNED = "RA";

	/**
	 * The prefix of the RDN that designates a role enabled
	 */
	private static String RDN_PREFIX_ROLE_ENABLED = "RE";
	
	/**
	 * The role field separator character in the netgroup name
	 */
	private static String ROLE_FIELD_SEPARATOR = "-";
		
	/**
	 * The default socket timeout in miliseconds
	 */
	private static int DEFAULT_SOCKET_TIMEOUT = 3000;
	
	/**
	 * The default time limit for ldap operations (in miliseconds)
	 */
	private static int DEFAULT_TIME_LIMIT = 3000;

	/**
	 * The basic LDAP server URL. it contains at least the host, basedn and port.
	 * The format is ldap[s]://<hostname>:<port>/<base_dn>?<attributes>?<scope>?<filter>?<extension>
	 */
	private LDAPUrl serverURL;
	
	/**
	 * Login DN for bind operation.
	 */
	private String loginDN;
	
	/**
	 * Last login was anonymous
	 */
	private boolean lastLoginAnonymous;
	
	/**
	 * Login password for bind operation.
	 */
	private byte[] loginPW;
	
	/**
	 * The main LDAP connection.
	 */
	private LDAPConnection ldapConnection;
			
	/**
	 * The LDAP constraints for main LDAP connection
	 */
	private LDAPConstraints ldapConstraints;
	
	/**
	 * Create a new connection to an LDAP server
	 * @param host the server name
	 * @param basedn the server basedn
	 * @param port the port number
	 */
	protected DBLDAP(String host, String basedn, int port) {
		
		serverURL = new LDAPUrl(host, port, basedn);
		
		logger.config("LDAP server URL [" + serverURL + "]");
		
		loginDN = ""; //default for anonymously login
		lastLoginAnonymous = false;
		loginPW = null;
		
		ldapConnection = new LDAPConnection(DEFAULT_SOCKET_TIMEOUT);
		ldapConstraints = ldapConnection.getConstraints();
				
		setTimeLimit(DEFAULT_TIME_LIMIT);
		
		logger.fine(this.toString());
		
		// allow the server object to be created although the ldap server may not be accessible
		// at the moment, but maybe later will be
		//refreshLDAPConnection(); 
	}
	
	/**
	 * Create a new connection to an LDAP server
	 * @param host the server name
	 * @param basedn the server basedn
	 */
	protected DBLDAP(String host,String basedn) throws AMException {
		this(host, basedn, LDAPConnection.DEFAULT_PORT);
	}
	
	/**
	 * Prints to the logger the error messages.
	 * @param operatioName the description of operation that has failed
	 * @param e LDAP exception if any 
	 * @param amErrorCode AM exception code as listed in AMException class 
	 */
	private void reportLDAPError(String operationName, LDAPException e, int amErrorCode) throws AMException {
		String message = "Failed operation: " + operationName;
		if ( e != null){
			message += " [LDAP ERROR][" + e.getResultCode() + "]: " + e.getLocalizedMessage();
			String ldapError = e.getLDAPErrorMessage();
			if ( ldapError != null )
				message += "(" + ldapError + ")";
		}
		
		logger.severe(Thread.currentThread().getName() + message);
		throw new AMException(message, amErrorCode);
		//e.printStackTrace();
	}
	
	/**
	 * Check the LDAP connection and reopen if not connected.
	 * @return true if the connection has been established
	 */
	private synchronized boolean refreshLDAPConnection() throws AMException {
		if (!ldapConnection.isConnected()){
			logger.fine(Thread.currentThread().getName() + "Refreshing LDAP connection...");
			try {
				ldapConnection.connect(serverURL.getHost(), serverURL.getPort());
				logger.fine(Thread.currentThread().getName() + "Connected to LDAP server " + serverURL);
			} catch (LDAPException e) {
				reportLDAPError("Connection to LDAP server: " + serverURL, e, AMException.LDAP_SERVER_CONNECTION_FAILED);
				return false;
			}
		} else {
			if (logger.isLoggable(Level.FINE)){
				logger.fine(Thread.currentThread().getName() + "No need to refresh the LDAP connection");				
			}
		}
		return true;
	}
	
	/**
	 * Check if the authentication has been done and, if not, do it (anonymously if the parameter is true). 
	 * @param anonymously
	 * @return true if the authentication is done
	 * @throws AMException
	 */
	private synchronized boolean refreshLDAPAuthentication(boolean anonymously) throws AMException {
		
		String localLoginDN = loginDN;
		byte[] localLoginPW = loginPW;

		if (logger.isLoggable(Level.FINEST)){
			logger.finest(Thread.currentThread().getName() + "Refreshing LDAP authentication...");
		}
		
		if (anonymously && lastLoginAnonymous ){
			if (!ldapConnection.isBound()){
				if (logger.isLoggable(Level.FINE)){
					logger.fine(Thread.currentThread().getName() + "Anonymous access: no need to bind to LDAP server!");
				}
				return true;
			}
		}
		
		if (anonymously){
			localLoginDN = "";
			localLoginPW = null;
		}

		// before that, check if the connection is ok
		refreshLDAPConnection();
		
		try {
			ldapConnection.bind(LDAPConnection.LDAP_V3, localLoginDN, localLoginPW);
			if (logger.isLoggable(Level.FINEST)){
				logger.finest(Thread.currentThread().getName() + 
						"Successfully bound to LDAP server " + serverURL + " with DN[" + localLoginDN + "]" +
						((localLoginPW==null)?" whithout PW":" with PW"));				
			}
			lastLoginAnonymous = anonymously;
		} catch (LDAPException e) {
			reportLDAPError("Bind to the LDAP server", e, AMException.LDAP_SERVER_BIND_FAILED);
			return false;
		}

		return true;
	}
		
	/**
	 * 	Set the socket timeout for network connection to the ldap server
	 * @param socketTimeout the socket timeout in miliseconds
	 * @return true if the setting has been successfully applied
	 */
	public synchronized void setSocketTimeout(int socketTimeout){
		ldapConnection.setSocketTimeOut(socketTimeout);
		ldapConstraints = ldapConnection.getConstraints();
		logger.finest("Set the socket timeout to " + socketTimeout);
	}
	
	/**
	 * 	Set the time limit for LDAP operation
	 * @param timeLimit time limit in miliseconds
	 * @return true if the setting has been successfully applied
	 */
	public synchronized void setTimeLimit(int timeLimit){
		ldapConstraints.setTimeLimit(timeLimit);
		ldapConnection.setConstraints(ldapConstraints);
		logger.finest("Set the time limit to " + timeLimit);
	}

	/**
	 * Set the login DN to use when authentication is needed.
	 * A null or empty string means an anonymously login.
	 * @param loginDN The loginDN to set.
	 */
	public void setLoginDN(String loginDN) {
		this.loginDN = loginDN;
		if (this.loginDN == null)
			this.loginDN = "";
		else
			this.loginDN = this.loginDN + ",ou=People," + this.serverURL.getDN();
		logger.fine("Login DN set to [" + this.loginDN + "]");		
	}

	/**
	 * Set the password for use with login when authentication is needed 
	 * @param loginPW The loginPW to set.
	 */
	public void setLoginPW(byte[] loginPW) {
		this.loginPW = loginPW;
		logger.fine("Login PW set!");		
	}
	
	/**
	 * Set the password for use with login when authentication is needed 
	 * @param loginPW The loginPW to set.
	 */
	public void setLoginPW(String loginPW) {
		try {
			this.loginPW = loginPW.getBytes("UTF8");
			logger.fine("Login PW set!");
		} catch (UnsupportedEncodingException e) {
			logger.warning("Could not set the login PW. Error:" + e.getMessage());
		}
	}

	@Override
	public boolean isSystemUser(String username) throws AMException{
		
		refreshLDAPConnection();
		
		String searchBase = RDN_USERS + "," + serverURL.getDN();
		int searchScope = LDAPConnection.SCOPE_SUB;
		String searchFilter = "(" + ATTR_USER_UID + "=" + username + ")";
		String[] attrNames = { ATTR_USER_UID };
				
		if (logger.isLoggable(Level.FINE)){
			logger.fine(Thread.currentThread().getName() +
					"Check if the username [" + username + "] exists.");
		}
		
		try {
			if (logger.isLoggable(Level.FINEST)){
				logger.finest(Thread.currentThread().getName() +
						"LDAP search:" +
						"BASE[" + searchBase + "], " +
						"Scope[" + searchScope + "], " +
						"Filter[" + searchFilter + "]");
			}
			LDAPSearchResults sr = ldapConnection.search(
					searchBase,
					searchScope,
					searchFilter,
					attrNames,
					false);
			
			if (logger.isLoggable(Level.FINE)){
				logger.fine(Thread.currentThread().getName() +
						"Username [" + username + "] found: " + sr.hasMore());
			}
			
			return sr.hasMore();
					
		} catch (LDAPException e) {
			reportLDAPError("Check the user [" + username + "] exists", e, AMException.LDAP_SEARCH_FAILED);
			return false;
		}		
	}

	/**
	 * Helper method to retrieve user attributes
	 * @param anonymous
	 * @param username
	 * @param attributesNames
	 * @return a map with the user attributes
	 * @throws AMException
	 */
	private Map<String, List<String>> getUserAttributes(
			boolean anonymous,
			String username,
			String[] attributesNames) throws AMException {

		refreshLDAPAuthentication(anonymous);

		Hashtable<String, List<String>> attrs = new Hashtable<String, List<String>>();
		
		String searchBase = ATTR_USER_UID + "=" + username + "," + 
							RDN_USERS + "," + 
							serverURL.getDN();
		int searchScope = LDAPConnection.SCOPE_BASE;
		String searchFilter = "";
		String[] attrNames = attributesNames;
		
		if (logger.isLoggable(Level.FINE)){
			logger.fine(Thread.currentThread().getName() +
					"Get the attributes for user [" + username + "]");
		}
		try {
			if (logger.isLoggable(Level.FINEST)){
				logger.finest(Thread.currentThread().getName() +
						"LDAP search:" +
						"BASE[" + searchBase + "], " +
						"Scope[" + searchScope + "], " +
						"Filter[" + searchFilter + "]");
			}
			LDAPSearchResults sr = ldapConnection.search(
					searchBase,
					searchScope,
					searchFilter,
					attrNames,
					false);
			
			while (sr.hasMore()){
				LDAPEntry nextEntry = sr.next();
				
				// check only the attributes of the DN we've requested
				if (nextEntry.getDN().compareToIgnoreCase(searchBase) == 0){
					for (LDAPAttribute attribute:(Set<LDAPAttribute>)nextEntry.getAttributeSet()){
						String attributeName = attribute.getName();

						String[] allValues = attribute.getStringValueArray();
						List<String> newValues = new ArrayList<String>();					
						if( allValues != null) {
							for(String value:allValues){
								newValues.add(value);
							}
						}
						// add the key and value to the map
						attrs.put(attributeName, newValues);
						if (logger.isLoggable(Level.FINE)){
							logger.fine(Thread.currentThread().getName() +
									"Attribute [" + attributeName + "]" +
									" with values [" + newValues + "]");
						}
					}					
					break;
				} else	{
					if (logger.isLoggable(Level.FINEST)){
						String msg = Thread.currentThread().getName() +
							"Ignore search result with DN [" + nextEntry.getDN() + "]";
						logger.finest(msg);
					}
				}
			}					
		} catch (LDAPException e) {
			reportLDAPError("Get attributes for user [" + username + "]", e, AMException.LDAP_SEARCH_FAILED);
		}
		
		return attrs;
	}

	@Override
	public Map<String, List<String>> getUserAttributes(String username, String[] attributesNames) throws AMException {
		return getUserAttributes(true, username, attributesNames);
	}
	
	@Override
	public Map<String, List<String>> getUserProtectedAttributes(String username, String[] attributesNames) throws AMException {
		return getUserAttributes(false, username, attributesNames);
	}
	
	private static final AMException makeLDAQSearchException(LDAPException e) {
		final String matchedDN = e.getMatchedDN();
		final String errorText = "Failed to retrieve LDAP search result "
				+ (matchedDN == null ? "" : "for matched DN " + e.getMatchedDN() + " ") + "because: " + e.getMessage();
		logger.severe(errorText);
		return new AMException(errorText, AMException.LDAP_SEARCH_FAILED);
	}


	/**
	 * Get the roles from an LDAP search result object
	 * @param ldapSearchResults the ldap search result object
	 * @param roleAttributeName use this attribute name to identify the role name
	 * @return the list of roles as strings
	 */
	private static List<String> getRolesFromLDAPSearchResults(
			LDAPSearchResults ldapSearchResults,
			String roleAttributeName) throws AMException {
		List<String> roles = new ArrayList<String>();
		
		if (ldapSearchResults == null){
			return roles;
		}
		
		while (ldapSearchResults.hasMore()){
			LDAPEntry nextEntry;
			try {
				nextEntry = ldapSearchResults.next();
			} catch (LDAPException e) {
				throw makeLDAQSearchException(e);
			}
			LDAPAttribute roleNameLDAPAttr = nextEntry.getAttribute(roleAttributeName);
			if (roleNameLDAPAttr == null){
				if (logger.isLoggable(Level.FINEST)){
					logger.finest(Thread.currentThread().getName() +
							"The role attribute name:" + roleAttributeName + " not found in the LDAP search result!");
				}
			} else {
				// get the role name from the result
				String[] roleNames = nextEntry.getAttribute(roleAttributeName).getStringValueArray();
				for (String roleName: roleNames){
					//cut the role prefix
					int pos = roleName.indexOf(ROLE_FIELD_SEPARATOR);
					roleName =roleName.substring(pos + ROLE_FIELD_SEPARATOR.length());
					// the role may exist already in the list because the prefix could be either RN, RA, RE
					if (!roles.contains(roleName)){
						roles.add(roleName);
					}
				}
			}
		}		
		if (logger.isLoggable(Level.FINE)){
			logger.fine(Thread.currentThread().getName() +
					"Roles retrieved from LDAP search result: [" + roles + "]");
		}
		return roles;		
	}
		
	/**
	 * Retrieves the roles from LDAP in a list of strings.
	 * @param ldapConnection the LDAP connection to be used for search
     *  @param base           The base distinguished name to search from.
     *<br><br>
     *  @param scope          The scope of the entries to search. The following
     *                        are the valid options:
     *<ul>
     *   <li>SCOPE_BASE - searches only the base DN
     *
     *   <li>SCOPE_ONE - searches only entries under the base DN
     *
     *   <li>SCOPE_SUB - searches the base DN and all entries
     *                           within its subtree
     *</ul><br><br>
     *  @param filter         Search filter specifying the search criteria.
     *<br><br>
     *  @param attrs          Names of attributes to retrieve.
     *<br><br>
     *  @param typesOnly      If true, returns the names but not the values of
     *                        the attributes found. If false, returns the
     *                        names and values for attributes found.
	 * @return the list of strings representing the roles found in LDAP
	 */
	private static LDAPSearchResults searchLDAP(
			LDAPConnection ldapConnection,
			String base,
			int scope,
			String filter,
			String[] attrs,
			boolean typesOnly) throws AMException{
		
		LDAPSearchResults ldapSearchResults = null;
		if (logger.isLoggable(Level.FINEST)){
			String message = Thread.currentThread().getName();
			message += "LDAP search:";
			message += "BASE[" + base + "], ";
			message += "Scope[" + scope + "], ";
			message += "Filter[" + filter + "], ";
			message += "Attributes[";
			for(String attr:attrs) message += attr + ",";
			message += "]";
			logger.finest(message);
		}
		try {
			ldapSearchResults = ldapConnection.search(base, scope, filter, attrs, typesOnly);
		} catch (LDAPException e) {
			throw makeLDAQSearchException(e);
		}
		return ldapSearchResults;
	}
	
	@Override
	public Map<String, List<String>> getRoles() throws AMException {
		
		refreshLDAPAuthentication(true);
		
		String searchBase = RDN_ROLES_DEFINITIONS + "," + serverURL.getDN();
		int searchScope = LDAPConnection.SCOPE_SUB;
		String searchFilter = 
				"(&" + 
					"(objectClass=" + OBJCLASS_ROLE_DEFINITION + ")" + 
					"(&" +
						"(" + ATTR_ROLE + "=" + RDN_PREFIX_ROLE_ASSIGNED + ROLE_FIELD_SEPARATOR + "*)" +
						"(" + ATTR_ROLE_ASSIGNABLE + "=" + VALUE_ROLE_ASSIGNABLE + ")" +
					")" +
				")";		
		
		String[] attrNames = { ATTR_ROLE };
		
		if (logger.isLoggable(Level.FINE)){
			logger.fine(Thread.currentThread().getName() + "Get all the roles");
		}

		Map<String, List<String>> allRoles = new HashMap<String, List<String>>();
		//get the assignable roles
		allRoles.put(ROLES_ASSIGNABLE,
					getRolesFromLDAPSearchResults(
						searchLDAP(
								ldapConnection, 
								searchBase, 
								searchScope, 
								searchFilter, 
								attrNames, 
								false),
						attrNames[0])
					);
		
		// get the not assignable roles
		searchFilter = 
			"(&" + 
				"(objectClass=" + OBJCLASS_ROLE_DEFINITION + ")" + 
				"(&" +
					"(" + ATTR_ROLE + "=" + RDN_PREFIX_ROLE_ASSIGNED + ROLE_FIELD_SEPARATOR + "*)" +
					"(" + ATTR_ROLE_ASSIGNABLE + "=" + VALUE_ROLE_NOT_ASSIGNABLE + ")" +
				")" +
			")";		
		allRoles.put(ROLES_NOT_ASSIGNABLE,
				getRolesFromLDAPSearchResults(
					searchLDAP(
							ldapConnection, 
							searchBase, 
							searchScope, 
							searchFilter, 
							attrNames, 
							false),
					attrNames[0])
				);
				
		if (logger.isLoggable(Level.FINE)){
			logger.fine(Thread.currentThread().getName() +
					"All roles retrieved from LDAP search result: [" + allRoles + "]");
		}
		return allRoles;
	}

	@Override
	public List<String> getRoleJuniors(String role) throws AMException {
		
		refreshLDAPAuthentication(true);
		
		// Junior roles are the roles who's permissions are included by the senior role
		// However, the set of users of a junior includes the set of users of senior role
		// so the netgroups in LDAP are organized like this: junior netgroup includes the senior netgroup.
		String searchBase = RDN_ROLES_DEFINITIONS + "," + serverURL.getDN();
		int searchScope = LDAPConnection.SCOPE_SUB;
		String searchFilter = "(objectClass=" + OBJCLASS_ROLE_DEFINITION + ")";

		//add the senior role name as a filter
		searchFilter = "(" +
					"&" + 
					searchFilter + 
					"(" + ATTR_ROLE_ROLE_MEMBER + "=*-" + role +")" +
				")";
		
		
		String[] attrNames = { ATTR_ROLE };
		
		if (logger.isLoggable(Level.FINE)){
			logger.fine(Thread.currentThread().getName() + "Get the JUNIOR roles for role " + role);
		}
		
		return getRolesFromLDAPSearchResults(
				searchLDAP(
						ldapConnection, 
						searchBase, 
						searchScope, 
						searchFilter, 
						attrNames, 
						false),
				attrNames[0]);
	}

	@Override
	public List<String> getRoleSeniors(String role) throws AMException {
		refreshLDAPAuthentication(true);
		
		// Junior roles are the roles who's permissions are included by the senior role
		// However, the set of users of a junior includes the set of users of senior role
		// so the netgroups in LDAP are organized like this: junior netgroup includes the senior netgroup.
		String searchBase = RDN_ROLES_DEFINITIONS + "," + serverURL.getDN();
		int searchScope = LDAPConnection.SCOPE_SUB;
		String searchFilter = "(objectClass=" + OBJCLASS_ROLE_DEFINITION + ")";

		String rolePrefixAssignable = RDN_PREFIX_ROLE_ASSIGNED + ROLE_FIELD_SEPARATOR;
		searchFilter = 
			"(&" + 
				searchFilter +
				"(" + ATTR_ROLE + "=" + rolePrefixAssignable + role + ")" +
			")";		
		
		String[] attrNames = { ATTR_ROLE_ROLE_MEMBER };
		
		if (logger.isLoggable(Level.FINE)){
			logger.fine(Thread.currentThread().getName() + "Get the SENIOR roles for role " + role);
		}
		
		return getRolesFromLDAPSearchResults(
				searchLDAP(
						ldapConnection, 
						searchBase, 
						searchScope, 
						searchFilter, 
						attrNames, 
						false),
				attrNames[0]);
	}
	
	@Override
	public List<String> getUserRoles(String username, boolean rolesEnabled) throws AMException{
		
		refreshLDAPAuthentication(true);
		
		String searchBase = RDN_ROLES_DEFINITIONS + "," + serverURL.getDN();
		int searchScope = LDAPConnection.SCOPE_SUB;
		String searchFilter = "(objectClass=" + OBJCLASS_ROLE_DEFINITION + ")";
		String rolePrefix = 
				((rolesEnabled)?RDN_PREFIX_ROLE_ENABLED:RDN_PREFIX_ROLE_ASSIGNED) 
				+ ROLE_FIELD_SEPARATOR;
		searchFilter = "(&" + searchFilter + 
				"(" + ATTR_ROLE + "=" + rolePrefix +"*))";
		//add the user name as a filter
		searchFilter = "(&" + searchFilter + 
				"(" + ATTR_ROLE_USER_MEMBER + "=\\(," + username +",\\)))";
		
		
		String[] attrNames = { ATTR_ROLE };
		
		if (logger.isLoggable(Level.FINE)){
			logger.fine(Thread.currentThread().getName() + "Get the roles " +
					((rolesEnabled)?"enabled":"assigned") + " to user [" + username + "]");
		}
		
		return getRolesFromLDAPSearchResults(
				searchLDAP(
						ldapConnection, 
						searchBase, 
						searchScope, 
						searchFilter, 
						attrNames, 
						false),
				attrNames[0]);
	}

	@Override
	public Map<String, List<String>> getAllUserRoles(String username) throws AMException{
		
		refreshLDAPAuthentication(true);
		
		String searchBase = RDN_ROLES_DEFINITIONS + "," + serverURL.getDN();
		int searchScope = LDAPConnection.SCOPE_SUB;
		String searchFilter = "(objectClass=" + OBJCLASS_ROLE_DEFINITION + ")";
		searchFilter = 
				"(&" + 
					searchFilter + 
					"(|" +
					"(" + ATTR_ROLE + "=" + RDN_PREFIX_ROLE_ASSIGNED + ROLE_FIELD_SEPARATOR + "*)" +
					"(" + ATTR_ROLE + "=" + RDN_PREFIX_ROLE_ENABLED + ROLE_FIELD_SEPARATOR + "*)" +
					")" +
				")";
		//add the user name as a filter
		searchFilter = "(&" + searchFilter + 
				"(" + ATTR_ROLE_USER_MEMBER + "=\\(," + username +",\\)))";
		
		
		String[] attrNames = { ATTR_ROLE };
		
		if (logger.isLoggable(Level.FINE)){
			logger.fine(Thread.currentThread().getName() + "Get all the roles for user [" + username + "]");
		}
		// prepare the map to be returned
		Map<String, List<String>> allRoles = new HashMap<String, List<String>>();
		allRoles.put(ROLES_ASSIGNED, new ArrayList<String>());
		allRoles.put(ROLES_ENABLED, new ArrayList<String>());
		
		LDAPSearchResults ldapSearchResults = searchLDAP(
												ldapConnection, 
												searchBase, 
												searchScope, 
												searchFilter, 
												attrNames, 
												false);
		if (ldapSearchResults == null){
			return allRoles;
		}
		String roleAttributeName = attrNames[0];
		while (ldapSearchResults.hasMore()){
			LDAPEntry nextEntry;
			try {
				nextEntry = ldapSearchResults.next();
			} catch (LDAPException e) {
				throw makeLDAQSearchException(e);
			}
			LDAPAttribute roleNameLDAPAttr = nextEntry.getAttribute(roleAttributeName);
			if (roleNameLDAPAttr == null){
				if (logger.isLoggable(Level.FINEST)){
					logger.finest(Thread.currentThread().getName() +
							"The role attribute name:" + roleAttributeName + " not found in the LDAP search result!");
				}
				continue;
			}
			
			String[] roleNames = roleNameLDAPAttr.getStringValueArray();
			String roleName = roleNames[0];
			if (roleNames.length > 1){
				if (logger.isLoggable(Level.FINEST)){
					logger.finest(Thread.currentThread().getName() +
							"Only one value expected for [" + roleAttributeName + "] Found: " +
							roleNames.length + ". Only the first one is used!");
				}
			}
			
			int pos = roleName.indexOf(ROLE_FIELD_SEPARATOR);
			String prefix = roleName.substring(0, pos);
			roleName =roleName.substring(pos + ROLE_FIELD_SEPARATOR.length());
			if (RDN_PREFIX_ROLE_ASSIGNED.equals(prefix)){
				allRoles.get(ROLES_ASSIGNED).add(roleName);
			} else if (RDN_PREFIX_ROLE_ENABLED.equals(prefix)){
				allRoles.get(ROLES_ENABLED).add(roleName);
			} else {
				if (logger.isLoggable(Level.WARNING)){
					logger.finest(Thread.currentThread().getName() +
							"Skip role [" + roleName + "] because doesn't start with prefixes [" + 
							RDN_PREFIX_ROLE_ASSIGNED + " or " + RDN_PREFIX_ROLE_ENABLED + "]");
				}
			}
		}
		
		if (logger.isLoggable(Level.FINE)){
			logger.fine(Thread.currentThread().getName() +
					"All roles retrieved from LDAP search result: [" + allRoles + "]");
		}
		return allRoles;
	}
	
	@Override
	public String toString(){
		String info = "";
		if (ldapConnection == null){
			info = " LDAP connection object is null";
		} else{
			info = "LDAP connection:" + ldapConnection.toString();
		}
		return info;
	}

}
