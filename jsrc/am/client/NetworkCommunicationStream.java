/**
 * 
 */
package am.client;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.channels.UnresolvedAddressException;
import java.util.logging.Level;
import java.util.logging.Logger;

import am.util.AMException;

/**
 * The communication stream implementation for the network sockets.
 * @author mleahu
 * @version 1.0
 */
public class NetworkCommunicationStream implements CommunicationStream {

    private static Logger logger =
        Logger.getLogger(NetworkCommunicationStream.class.getName());
    
    /**
     * The socket channel
     */
    private SocketChannel socketChannel;
    
    /**
     * The socket timeout for read operations from input stream.
     */
    private static final int SOCKET_SO_TIMEOUT = 1000; //ms

    
    /**
     * The output stream 
     */
    private ByteArrayOutputStream outputStream = null;
    
    /**
     * The byte buffer to be used to read and write in the channel.
     */
    private ByteBuffer buffer = ByteBuffer.allocate(200);
	/**
	 * buffer used for draining the byte buffer into the outputstream
	 */
	private byte [] transferBuffer = new byte [64];
    
	/**
	 * Create a network communication stream object connected to a server host and port.
	 * @param serverHost server name
	 * @param serverPort server port
	 * @throws AMException
	 */
	public NetworkCommunicationStream(String serverAddress, int serverPort) throws AMException{
		// intialize the network connection
		
		if (serverAddress == null || serverAddress.length() ==0){
			logger.warning("Empty server address!");
			throw new AMException("Empty server address!", AMException.AM_CLNT_CONFIG_ERROR);
		}
		if (serverPort <=0){
			logger.warning("Invalid port number: " + serverPort);
			throw new AMException(
					"Invalid server port number: [" + serverPort + "]", 
					AMException.AM_CLNT_CONFIG_ERROR);
		}
		
		try{
			 // Create a blocking socket channel
	        socketChannel = SocketChannel.open();
	        socketChannel.configureBlocking(true);
	    
	        // Send a connection request to the server; this method is blocking
	        socketChannel.connect(new InetSocketAddress(serverAddress, serverPort));
	        
	        // disable nagling algorithm
			socketChannel.socket().setTcpNoDelay(true);
			// set the timeout for read operations
			socketChannel.socket().setSoTimeout(SOCKET_SO_TIMEOUT);
			
			logger.config("Connected to " + serverAddress + ":" + serverPort);
		} catch (UnresolvedAddressException e1){
			throw new AMException(
					"Failed to connect to [" + serverAddress + ":" + serverPort + "] " +
					"Reason: unresolved address!",
					AMException.AM_CLNT_COMMUNICATION_ERROR);
		} catch (Exception e){
			throw new AMException(
					"Failed to connect to [" + serverAddress + ":" + serverPort + "] " +
					"Reason:" + e.getLocalizedMessage(),
					AMException.AM_CLNT_COMMUNICATION_ERROR);
		}
		
		// initialize the output stream
		outputStream = new ByteArrayOutputStream(buffer.capacity());
	}
	
	/* (non-Javadoc)
	 * @see am.client.CommunicationStream#canReadMessage()
	 */
	public boolean canReadMessage() {
		return socketChannel.socket().isInputShutdown();
	}

	/* (non-Javadoc)
	 * @see am.client.CommunicationStream#canSendMessage()
	 */
	public boolean canSendMessage() {
		return socketChannel.socket().isOutputShutdown();
	}

	/* (non-Javadoc)
	 * @see am.client.CommunicationStream#getOutputStream()
	 */
	public OutputStream getOutputStream() {
		return outputStream;
	}

	/* (non-Javadoc)
	 * @see am.client.CommunicationStream#readMessageInStream()
	 */
	public InputStream readMessageInStream() throws AMException {
		ByteArrayOutputStream tempOutputStream = new ByteArrayOutputStream(buffer.capacity());

		int nbytes = 0;
		boolean EOF = false;
		
		while (!EOF) {
			buffer.clear();
			
			try {
				nbytes = socketChannel.read(buffer);
			} catch (IOException e) {
				throw new AMException("Read failed on the socket channel:" + e.getMessage(), AMException.AM_CLNT_COMMUNICATION_ERROR);
			}
			
			if (logger.isLoggable(Level.FINEST)){
				logger.finest("Bytes received:" + nbytes);
			}
			if (nbytes > 0) {
				// reuse the nbytes variable because we don't need it anymore
				// transfer the bytes from the buffer in the output stream
				buffer.flip();
				while (buffer.hasRemaining( )) {
					nbytes = Math.min (buffer.remaining( ), transferBuffer.length);
					buffer.get(transferBuffer, 0, nbytes);
					tempOutputStream.write(transferBuffer, 0, nbytes);
				}
			} else if (nbytes == 0) {
				// do nothing
			} else if (nbytes == -1) {
				EOF = true;
				if (logger.isLoggable(Level.FINE)){
					logger.fine("EOF" + nbytes);
				}
			}
		}
		
		// create the input stream needed for the input handler
		ByteArrayInputStream inputStream =  new ByteArrayInputStream(tempOutputStream.toByteArray());
		if (logger.isLoggable(Level.FINE)){
			logger.fine("Return input stream with size:" + inputStream.available());
		}
		return inputStream;
	}
	
	/* (non-Javadoc)
	 * @see am.client.CommunicationStream#sendMessageFromOutputStream()
	 */
	public void sendMessageFromOutputStream() throws AMException {
		byte[] outputBuffer = null;
		int outputBufferRemaining = 0;

		if (logger.isLoggable(Level.FINE)){
			logger.fine("Send message from output stream of size:" + outputStream.size());
		}

		try {
			if (outputStream.size() > 0){
				outputBuffer = outputStream.toByteArray();
				outputBufferRemaining = outputBuffer.length;
			
				while (outputBufferRemaining > 0){
					if (logger.isLoggable(Level.FINER)){
						logger.finer(this + "Data size to be sent:" + outputBufferRemaining);
					}
					
					buffer.clear();
					buffer.put(
							outputBuffer,
							outputBuffer.length - outputBufferRemaining, 
							Math.min(buffer.capacity(), outputBufferRemaining));
					buffer.flip();
					outputBufferRemaining -= socketChannel.write(buffer);
					if (logger.isLoggable(Level.FINER)){
						logger.finer(this + "Remaining data to be sent:" + outputBufferRemaining);
					}
				}
			}
			
			// close the output stream
			socketChannel.socket().shutdownOutput();
			
		} catch (IOException e) {
			throw new AMException("Failure in sending the message:" + e.getMessage(), AMException.AM_CLNT_COMMUNICATION_ERROR);
		}
		
	}

	/* (non-Javadoc)
	 * @see am.client.CommunicationStream#close()
	 */
	public void close() {
		try {
			socketChannel.close();
		} catch (IOException e) {
			logger.severe("Failure in closing the socket channel:" + e.getMessage());
		}
	}

}
