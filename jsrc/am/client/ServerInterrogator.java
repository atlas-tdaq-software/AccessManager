package am.client;

import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

import am.util.AMException;
import am.xacml.context.Resource;
import am.xacml.context.ResponseProcessor;

import com.sun.xacml.ParsingException;
import com.sun.xacml.ctx.ResponseCtx;

/**
 * Class to work directly with the AM server. The client should use this class to send requests
 * and receive answers. 
 * @author mleahu
 * @version 1.0
 */
public class ServerInterrogator {
	
	private static final Logger logger =
        Logger.getLogger(ServerInterrogator.class.getName());
	
	/**
	 * AM server information 
	 */
	private String[] serverHostNames;
	private int serverPort = -1;
	
	private LinkedList<String> secondaryAMServers;
	private boolean useSecondaryAMServers;
	
	/**
	 * the current subject which is the access subject most of the time.
	 * when another accessSubject is provided, the current subject is intermediary subject
	 */
	private RequestorInfo currentSubject;
	
	/**
	 * Disable the am authorization;
	 */
	private boolean authEnabled = false;
	
	/**
	 * store the error messages if a request could not be successfully honored
	 */
	private String statusMessage;
	
	/**
	 * The network communication with the server
	 */
	private CommunicationStream commStream;
		
	/**
	 * Initialize the server interrogator object.
	 */
	public ServerInterrogator(){
		commStream = null;
		useSecondaryAMServers = false;
		secondaryAMServers = new LinkedList<String>();
		authEnabled = false;
		statusMessage = null;
		currentSubject = new RequestorInfo();
		ClientConfig config = new ClientConfig();
		configure(config);
	}

	/**
	 * Set the client configuration using the client configuration object 
	 * @param clientConfig
	 */
	public void configure(ClientConfig config){
		if (config == null){
			logger.severe("The client configuration object is null!");
			return;
		}
		
		authEnabled = config.isAMAuthorizationEnabled();
		logger.config("AM authorization enabled =  " + authEnabled);
		
		if (!authEnabled){
			logger.warning("AM authorization is DISABLED!");
			return;
		}
		
		serverHostNames = config.getAMServerHosts();
		logger.config("Server host names: [" + serverHostNames + "]");
		
		serverPort = config.getAMServerPort();
		
		logger.config("Server port number: [" + serverPort + "]");
		
		useSecondaryAMServers = config.useSecondaryAMServers();
		logger.config("Use secondary AM servers:" + useSecondaryAMServers);
	}
	
	/**
	 * Initialize the communication using a network communication stream.
	 * @throws AMException
	 */
	private void initServerCommunication(String serverHostName) throws AMException{
		if (commStream != null){
			logger.fine("The communication with the server is already initialized ...");
			if (!commStream.canReadMessage() || ! commStream.canSendMessage()){
				logger.fine("The communication is not active! Reinitializing ...");
				closeServerCommunication();
			}
		}
		
		commStream = new NetworkCommunicationStream(serverHostName, serverPort);
		logger.fine("Initiating the communication with the server [" + serverHostName + "]...");
	}
	
	/**
	 * Close the communication with the server
	 */
	private void closeServerCommunication(){
		if (commStream != null){
			commStream.close();
			commStream = null;
			logger.fine("Communication with the server is closed!");
		}
	}
	
	/**
	 * Ask the server if the resource is allowed to be accessed for an action.
	 * @param resource
	 * @return true if the authorization is granted or false otherwise
	 * @throws AMException
	 */
	public boolean isAuthorizationGranted(Resource resource) throws AMException{
		return isAuthorizationGranted(resource, null);
	}
	
	/**
	 * Ask the server if the resource is allowed to be accessed for an action.
	 * The authorization request is sent on behalf of another subject "accessSubject"
	 * @param resource
	 * @param accessSubject
	 * @return true if the authorization is granted or false otherwise
	 * @throws AMException
	 */
	public boolean isAuthorizationGranted(Resource resource, RequestorInfo accessSubject) throws AMException{
		
		boolean decision = false;
		
		if (!authEnabled){
			decision = true;
			statusMessage = "AM Authorization is disabled! Default decision is " + decision;
			return decision;
		}
		
		if (resource == null){
			statusMessage ="Resource object is null!";
			throw new AMException(statusMessage, AMException.AM_CLNT_REQUEST_ERROR);
		}
		
		if (serverHostNames.length <= 0) {
			statusMessage ="No AM server specified!";
			throw new AMException(statusMessage, AMException.AM_CLNT_CONFIG_ERROR);
		}

		//prepare the request
		RequestComposer request;
		if (accessSubject == null){
			logger.fine("No explicit access subject provided. The current subject is considered the access subject");
			request = new RequestComposer(currentSubject);
		} else {
			logger.fine("Explicit access subject provided. The current subject is considered the intermediary subject");
			request = new RequestComposer(accessSubject, currentSubject);
		}
		request.addResource(resource);		
		
		
		String amServerHistory = "AMServerHistory=";
		int serverIdx = 0;
		String currentAMServer = serverHostNames[serverIdx++];
		secondaryAMServers.clear();
		
		while (currentAMServer.length() > 0) {
			amServerHistory += currentAMServer + ",";
			
			try{
				initServerCommunication(currentAMServer);
			} catch (AMException amex) {
				logger.severe(amex.getMessage());
				commStream = null;
				if (useSecondaryAMServers && ! secondaryAMServers.isEmpty()){
					currentAMServer = secondaryAMServers.removeFirst();
					amServerHistory += "(SB)";
					logger.warning("Try next AM server from server_busy list [" + currentAMServer + "]");
					continue;
				} else {
					currentAMServer = "";
					if (serverIdx < serverHostNames.length) {
						currentAMServer = serverHostNames[serverIdx++];
					}
					if (currentAMServer.length() == 0) {
						statusMessage = "[" + amServerHistory + "]" + 
							"The initialization of the communication with the server has failed!";
						throw amex;
					}
					logger.warning("Try next AM server from client configuration [" + currentAMServer + "]");
					continue;
				}
			}
			
			//sending the request
			logger.fine("Sending the request...");		
			if (logger.isLoggable(Level.FINEST)){
				logger.finest(request.toString());
			}
			
			request.encode(commStream.getOutputStream());
			
			try {
				commStream.sendMessageFromOutputStream();
			} catch (AMException e) {
				throw new AMException("Problem with network communication after the request has been sent! Error:" + e.getMessage(),
						AMException.AM_CLNT_COMMUNICATION_ERROR);
			}
			logger.fine("Request sent!");
			
			//receiving the request
			logger.fine("Receiving the answer ...");
			ResponseCtx response = null;
			try {
				response = ResponseCtx.getInstance(commStream.readMessageInStream());
			} catch (AMException e) {
				throw new AMException("Problem with network communication after the response has been received! Error:" + e.getMessage(),
						AMException.AM_CLNT_COMMUNICATION_ERROR);
			} catch (ParsingException e) {
				throw new AMException("Answer not received correctly! Error: " + e.getMessage(),
						AMException.AM_CLNT_COMMUNICATION_ERROR);
			}
			
			logger.fine("Answer received!");
			if (logger.isLoggable(Level.FINEST)){
				logger.finest("Full answer:" + ResponseProcessor.toString(response));
			}
			
			closeServerCommunication();
			currentAMServer = "";
			
			decision =  ResponseProcessor.isResponsePositive(response);
			if (decision == false && 
				useSecondaryAMServers && ResponseProcessor.isServerBusy(response)) {
				secondaryAMServers.clear();
				for(String secSrv: ResponseProcessor.getSecondaryAMServers(response)) {
					secondaryAMServers.addLast(secSrv);
				}
				if (!secondaryAMServers.isEmpty()) {
					currentAMServer = secondaryAMServers.removeFirst();
					amServerHistory += "(SB)";
					logger.warning("Try next AM server from server_busy list [" + currentAMServer + "]");
					continue;
				}
			}
			
			statusMessage = "[" + amServerHistory + "]" + ResponseProcessor.getStatus(response);
		}
		
		return decision;
	}

	/**
	 * return the status message that may contain more details about the failure or success of an authorization grant/deny
	 * @return the statusMessage
	 */
	public String getStatusMessage() {
		return statusMessage;
	}
}
