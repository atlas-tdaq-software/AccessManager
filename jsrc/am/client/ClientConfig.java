package am.client;

import java.util.logging.Logger;

import am.util.AMCommon;
import am.util.AMException;
import am.util.AMLogger;


/**
 * Provides the client configuration settings retrieved from different sources like 
 * the system environment variables, configuration files, configurations in data bases.
 * @author mleahu
 * @version 1.0
 */
public class ClientConfig {

	private static final Logger logger =
        Logger.getLogger(ClientConfig.class.getName());

	private static final String AM_CLIENT_CONFIGURATION_FILE = "/sw/tdaq/AccessManager/cfg/client.cfg";
	
	private static final String AM_CLIENT_CONFIGURATION_FILE_ENV = "TDAQ_AM_CONFIGURATION_FILE";

	/**
	 * AM server host name
	 */
	private static String PARAMETER_AM_SERVER_HOST = "TDAQ_AM_SERVER_HOST";

	/**
	 * AM server host name separator
	 */
	private static String AM_SERVER_HOST_SEPARATOR = ",";
	
	/**
	 * AM server port
	 */
	private static String PARAMETER_AM_SERVER_PORT = "TDAQ_AM_SERVER_PORT";

	
	/**
	 * AM authorization enabling flag
	 */
	private static String PARAMETER_AM_AUTHORIZATION = "TDAQ_AM_AUTHORIZATION";

	/**
	 * AM authorization value for "on"
	 */
	private static String VALUE_AM_AUTHORIZATION_ENABLED = "on";
	
	/**
	 * The use of the secondary AM servers if information sent by the server 
	 */
	private static String PARAMETER_AM_CLIENT_USE_SECONDARY_SERVERS = "TDAQ_AM_CLIENT_USE_SECONDARY_SERVERS";

	/**
	 * The default value for the parameter to use the secondary servers
	 */
	private static boolean VALUE_AM_CLIENT_USE_SECONDARY_SERVERS_DEFAULT = false;
	
	/**
	 * The AM logs directory
	 */
	public static final String PARAMETER_AM_LOGS_DIR = "TDAQ_AM_LOGS_DIR";

	/**
	 * The client log level
	 */
	private static String PARAMETER_AM_CLIENT_LOG_LEVEL = "TDAQ_AM_CLIENT_LOG_LEVEL";

	private static String[] packagesToLog = {
		"am.client",
		"am.config",
		"am.util",
		"am.util.cache",
		"am.communication",
		"am.xacml.context",
		"com.sun.xacml",
	};
	
	/**
	 * The AM specific logger
	 */
	private AMLogger amLogger = null;

	
	private am.config.Configuration amConfig;
	
	public ClientConfig(){
		amConfig = new am.config.Configuration();
		
		try {
			amConfig.addConfigurationSource(new am.config.ConfigurationFromFile(AM_CLIENT_CONFIGURATION_FILE));

			String s = System.getenv(AM_CLIENT_CONFIGURATION_FILE_ENV);
		  if (s != null)
        amConfig.addConfigurationSource(new am.config.ConfigurationFromFile(s));

			amConfig.addConfigurationSource(new am.config.ConfigurationFromEnvironment());
		} catch (AMException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		logger.config(this.toString());	

		enableLogging();

	}
		
	/**
	 * Reads the AM server host names;
	 * @return an array with the Access Manager server host names
	 */
	public String[] getAMServerHosts(){
		return amConfig.get(PARAMETER_AM_SERVER_HOST, "SERVER_NOT_SPECIFIED").split(AM_SERVER_HOST_SEPARATOR);
	}
		
	/**
	 * Reads the AM server port;
	 * @return Access Manager server port 
	 */
	public int getAMServerPort(){
		return amConfig.getInt(PARAMETER_AM_SERVER_PORT, -1);
	}
	
	/**
	 * Checks if the AM authorization is enabled
	 * @return true if the AM authorization is enabled, false if not
	 */
	public boolean isAMAuthorizationEnabled(){
		String value = amConfig.get(PARAMETER_AM_AUTHORIZATION, "off");
		return (value != null && VALUE_AM_AUTHORIZATION_ENABLED.equals(value.toLowerCase()));
	}
	
	/**
	 * Check if the client should use the secondary servers sent by the AM server.
	 * @return true if the secondary servers should be used
	 */
	public boolean useSecondaryAMServers(){
		return amConfig.getBoolean(PARAMETER_AM_CLIENT_USE_SECONDARY_SERVERS, VALUE_AM_CLIENT_USE_SECONDARY_SERVERS_DEFAULT);
	}
	
	/**
	 * Reads the AM client log level;
	 * @return AM client log level as string
	 */
	public String getLogLevel(){
		return isAMAuthorizationEnabled() ? 
				amConfig.get(PARAMETER_AM_CLIENT_LOG_LEVEL, "NORMAL") : 
				AMLogger.getAMLogLevelName(AMLogger.LOG_LEVEL_ID_NONE);
	}
	
	/**
	 * Start the logging for the AM server
	 */
	private void enableLogging(){
		
		// get the AM log level id
		String sLogLevel = getLogLevel().toUpperCase();
		
		int amLogLevelId;
		try {
			//check if it's a number
			amLogLevelId = Integer.valueOf(sLogLevel);
			amLogLevelId = AMLogger.validateAMLogLevelId(amLogLevelId);
		} catch (NumberFormatException e){
			// it's not a number, consider it a string and get the id
			amLogLevelId = AMLogger.getAMLogLevelId(sLogLevel);
		}

		// if the log level is greater than NONE, then create the directories and enable the logging on all
		// the packages
		if (amLogLevelId > AMLogger.LOG_LEVEL_ID_NONE){
			try {
				if (amLogger == null) {
					String amLogsDir = amConfig.get(PARAMETER_AM_LOGS_DIR, ".");
					amLogsDir += ((amLogsDir.length() > 0) ? AMCommon.getFileSeparator() : ""); 
					amLogsDir += "amClient" + AMCommon.getFileSeparator();
				//	amLogger = new AMLogger(amLogsDir);
					amLogger = new AMLogger();  // to avoid the creation of the amLogsDir
				}
			} catch (AMException e) {
				System.err.println("Could not create AM logger! Error: " + e.getMessage());
				return;
			}
			
			// first of all enable the logging for am.config package in case there is a problem 
			// while the configuration parameters are read		
			//amLogger.enableLoggingToFile(AMConfig.class.getPackage().getName());

			// enable the loggers for all the packages we are interested in
			for(String packageName: packagesToLog){
		//		amLogger.enableLoggingToFile(packageName, amLogLevelId);    // log in files
				amLogger.enableLoggingToConsole(packageName, amLogLevelId); // log in Console
                               
			}
			amLogger.setAMFormatterToAllLoggers();
		} else {
			System.out.println("AM client logging is DISABLED!");
		}
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		String message = "Client configuration:";
		message += "AMServerHost: [" + getAMServerHosts() + "]\n";
		message += "AMServerPort: [" + getAMServerPort() + "]\n";
		message += "AMAuthEnabled: [" + isAMAuthorizationEnabled() + "]\n";
		message += "UseSecondaryServers: [" + useSecondaryAMServers() + "]\n";
		return message;
	}
}
