/**
 * 
 */
package am.client;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.OutputStream;
import java.net.URI;

import am.util.AMException;
import am.xacml.context.Action;
import am.xacml.context.Environment;
import am.xacml.context.RequestProcessor;
import am.xacml.context.Resource;
import am.xacml.context.Subjects;

import com.sun.xacml.Indenter;
import com.sun.xacml.attr.AttributeFactory;
import com.sun.xacml.attr.AttributeValue;
import com.sun.xacml.attr.DateAttribute;
import com.sun.xacml.attr.TimeAttribute;
import com.sun.xacml.ctx.Attribute;
import com.sun.xacml.ctx.RequestCtx;
import com.sun.xacml.ctx.Subject;


/**
 *Contains the information needed for an authorization request.
 *
 *@author mleahu
 *@since 1.0
 */

public class RequestComposer {

	/**
	 * Request Context object used to encode the request. 
	 */
	private RequestCtx reqCtx = null; 
	
	/**
	 * Subjects issuing the request; 
	 */
	private Set<Subject> subjects;
	
	/**
	 * Resources to request authorization for
	 */
	private Set<Attribute> resources;

	/**
	 * Actions to be allowed on the resources 
	 */
	private Set<Attribute> actions;

	/**
	 * Environment attributes for the access request;
	 */
	private Set<Attribute> environment;

	// the logger we'll use for all messages
	private static final Logger logger =
		Logger.getLogger(RequestorInfo.class.getName());

	/**
	 * Create an AttributeValue object based on the data type identifier
	 * and using the attribute factory.
	 * @param dataType the URI defining the type of the attribute
	 * @param value the value of attribute
	 * @return the AttributeValue object
	 */
	protected static AttributeValue createAttributeValue(URI dataType, String value) {
		try {
			AttributeFactory factory = AttributeFactory.getInstance();
			return factory.createValue(dataType, value);
		} catch (Exception e) {
			logger.warning("Failed to create the attribute " + 
					"[DataType=" + dataType + "] " +
					"[Value=" + value + "] " + 
					"Error:" + e.getMessage());
			return null;
		}
	}
	
	/**
	 * Fill the subjects set with the information from the requestor information
	 * structure.
	 */
	private void addSubjects(URI category, Subjects subject) {

		Set<Attribute> attributes = new HashSet<Attribute>();
		
		logger.fine("Add the subjects object: " + subject);
		
		try {
			attributes.add(subject.createAttributeInstance(subject.getUser()));
			attributes.add(subject.createAttributeInstance(subject.getHost()));
			attributes.add(subject.createAttributeInstance(subject.getIp()));
		} catch (AMException e) {
			logger.severe("Can not add subject because:" + e.getMessage());
			return;
		}
		subjects.add(new Subject(category, attributes));
	}
		
	/**
	 * Fill the actions set
	 */
	private void initEnvironment() {
		environment.clear();
		logger.fine("Reinitialize the environment");
		
		// set the environment parameters date and time
		URI envDate = Environment.IDENTIFIER_ENVIRONMENT_DATE;
		Attribute dateAttribute = new Attribute(envDate, null, null, new DateAttribute()); 
		environment.add(dateAttribute);		
		if (logger.isLoggable(Level.FINEST)){
			logger.finest("Date attribute [" + dateAttribute.encode());			
		}
		
		URI envTime = Environment.IDENTIFIER_ENVIRONMENT_TIME;
		Attribute timeAttribute = new Attribute(envTime, null, null, new TimeAttribute());
		environment.add(timeAttribute);
		if (logger.isLoggable(Level.FINEST)){
			logger.finest("Time attribute [" + timeAttribute.encode());			
		}
	}
	
	/**
	 * Create the request context object
	 */
	private void initRequestCtx(){
		
		initEnvironment();
		
		// create the request context object
		reqCtx = new RequestCtx(subjects, resources, actions, environment);
		logger.fine("Request object prepared");
		if (logger.isLoggable(Level.FINEST)){
			logger.finest(RequestProcessor.toString(reqCtx));			
		}		
	}
	
	/**
	 * Prepare the access request on behalf of 'accessSubject' by an intermediary subject.
	 * @param accessSubject a RequestorInfo object holding the information about the subject which
	 * 						will access the resource
	 * @param intermediarySubject a RequestorInfo object holding the information about the subject which
	 * 						is requesting access on behalf of the access subject.
	 */
	public RequestComposer(Subjects accessSubject, Subjects intermediarySubject) {
		
		this.subjects 		= new HashSet<Subject>();
		this.resources 		= new HashSet<Attribute>();
		this.actions 		= new HashSet<Attribute>();
		this.environment 	= new HashSet<Attribute>();
		
		addSubjects(Subjects.CATEGORY_ACCESS_SUBJECT, accessSubject);
		if (intermediarySubject != null){
			addSubjects(Subjects.CATEGORY_INTERMEDIARY_SUBJECT, intermediarySubject);
		}
		
		if (logger.isLoggable(Level.FINEST)){
			logger.finest("Request composer initialized with the requestor information: "
					+ " AccessSubject=" + accessSubject 
					+ " IntermediarySubject=" + intermediarySubject);
		}
	}

	/**
	 * Prepare the access request for 'accessSubject'.
	 * @param accessSubject a RequestorInfo object holding the information about the subject which
	 * 						will access the resource
	 */
	public RequestComposer(Subjects accessSubject) {
		this(accessSubject, null);
	}
	
	/**
	 * Add the resource with action for the access request
	 */
	public boolean addResource(Resource res) {

		logger.fine("Add a resource and action object");
		
		try {
			resources.addAll(res.createAttributeInstances());
			for(Action action:res.getActions()){
				actions.addAll(action.createAttributeInstances());
			}
		} catch (AMException e) {
			logger.severe("Could not add resource or action:" + e.getMessage());
			return false;
		}
		
		return true;
	}

	/**
	 * Set the resources and actions for the access request.
	 * This empty the existing set of resources and actions.
	 */
	public boolean setResource(Resource res) {
		resources.clear();
		logger.finest("Resources cleared!");
		actions.clear();
		logger.finest("Actions cleared!");
		return addResource(res);
	}
	
	/**
	 *Generates the XML format for the authorization request
	 */

	public void encode(OutputStream output) {
		initRequestCtx();
		reqCtx.encode(output);
		logger.fine("Request encoded into the output stream...");
	}
	
	/**
	 *Generates the XML format for the authorization request with indentation
	 */
	public void encode(OutputStream output, Indenter indenter){
		initRequestCtx();
		reqCtx.encode(output, indenter);
		logger.fine("Request encoded into the output stream with user defined indenter ...");
	}

	/**
	 * Put a set into a string to be used by toString method
	 * @param set
	 * @return
	 */
	private String setToString(Set set){
		String sout = "";
		if (set != null){
			for(Iterator it = set.iterator(); it.hasNext();){
				Object o = it.next();
				String s = "";
				// for Subject, get each attribute and encode it
				if (o.getClass().getName().equals(Subject.class.getName())){
					for(Iterator sit = ((Subject)o).getAttributes().iterator(); sit.hasNext();){
						s += ((Attribute)sit.next()).encode() + " ";
					}
				} else if (o.getClass().getName().equals(Attribute.class.getName())){
					s += ((Attribute)o).encode() + " ";
				}
				sout += "[" + s + "]";
			}
		}
		return sout;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "[[REQUEST]]:\n" +
		"[SUBJECT]" + setToString(subjects) + "\n" +
		"[RESOURCE]" + setToString(resources) + "\n" +
		"[ACTION]" + setToString(actions) + "\n" +
		"[ENVIRONMENT]" + setToString(environment) + "\n";	
	}
}
