package am.client;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Logger;

import am.xacml.context.Subjects;

/**
 * 
 * Stores information about the requester.
 * 
 * @author mleahu
 * @since 1.0
 */
public class RequestorInfo extends Subjects {
	
	/* OS type: linux, windows, solaris */
	public static final String OS_TYPE_LINUX = "linux";
	public static final String OS_TYPE_WINDOWS = "windows";
	public static final String OS_TYPE_SOLARIS = "solaris";
	public static final String OS_TYPE_MAC = "mac";
	private String osType;
	
	/* OS name (i.e. Windows XP ...) */
	private String osName;
	
    // the logger we'll use for all messages
    private static final Logger logger =
        Logger.getLogger(RequestorInfo.class.getName());

    /**
     * Default constructor; detects automatically the username, hostname and ip address
     */
    public RequestorInfo(){
    	//System.out.println(System.getProperties());    	
    	initialize(System.getProperty("user.name"), null);
    }
    
    
    /**
     * Constructor that uses the provided username and hostname.
     * @param username
     * @param hostname
     */
    public RequestorInfo(String username, String hostname){
    	initialize(username, hostname);
    }
    
    /**
     * Put the values in the map and initialize the OS types and names
     * @param username
     * @param hostname
     */
    private void initialize(String username, String hostname){
       	
    	String hn = hostname;
    	String ip = null;
    	
    	if (hn == null){
        	try{
        		hn = InetAddress.getLocalHost().getHostName();
        	} catch (UnknownHostException e) {
        		logger.warning("Could not detect host name for this machine!");
        	}
    	}

    	try{
    		ip = InetAddress.getLocalHost().getHostAddress();
    	} catch (UnknownHostException e) {
    		logger.warning("Could not detect the IP address for this machine (host/ip)");
    	}
    	
    	addValue(getUser(), username);
    	addValue(getHost(), hn);
    	addValue(getIp(),  ip);

    	this.osName = System.getProperty("os.name");
    	if (osName.toLowerCase().indexOf("linux") >= 0)
    		osType = OS_TYPE_LINUX;
    	else if (osName.toLowerCase().indexOf("windows") >= 0)
    		osType = OS_TYPE_WINDOWS;
    	else if (osName.toLowerCase().indexOf("solaris") >= 0)
    		osType = OS_TYPE_SOLARIS;
    	else if (osName.toLowerCase().indexOf("mac") >= 0)
    		osType = OS_TYPE_MAC;
    	else osType = "unknown";    
    	
    	logger.fine(this.toString());
    }
    
	/**
	 * @return Returns the osName.
	 */
	public String getOsName() {
		return osName;
	}

	/**
	 * @return Returns the osType.
	 */
	public String getOsType() {
		return osType;
	}

	/* (non-Javadoc)
	 * @see am.xacml.context.Subjects#toString()
	 */
	public String toString(){
		return  "RequestorInfo: " +
				"[osType=" + osType + "] " +
				"[osName=" + osName + "] " +
				super.toString();
	}
}