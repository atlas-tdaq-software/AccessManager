package am.client;

import java.io.InputStream;
import java.io.OutputStream;

import am.util.AMException;


/**
 * Interface for the communication streams.
 * @author mleahu
 * @version 1.0
 */
public interface CommunicationStream {

	/**
	 * Return true if the message can be sent.
	 * @return true if the message can be sent
	 */
	public boolean canReadMessage();
	
	/**
	 * Get the message in an input stream
	 * @return input stream object from where the message can be read
	 * @throws AMException
	 */
	public InputStream readMessageInStream() throws AMException;

	/**
	 * Return true if the message can be sent.
	 * @return true if the message can be sent
	 */
	public boolean canSendMessage();

	/**
	 * Returnt the output stream where the meesage to be sent can be stored.
	 * @return the output stream object
	 */
	public OutputStream getOutputStream();
	
	/**
	 * Send the message that has been already written in the output stream
	 * @throws AMException
	 */
	public void sendMessageFromOutputStream() throws AMException;
	
	/**
	 * Close the communication streams.
	 */
	public void close();
}
