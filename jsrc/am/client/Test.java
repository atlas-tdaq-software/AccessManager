package am.client;

import am.util.AMException;
//import am.xacml.context.impl.IDLResource;
import am.xacml.context.impl.PMGResource;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args){
		
		RequestorInfo accessSubject = new RequestorInfo();
		
		ServerInterrogator srv = new ServerInterrogator();
		
		try {
			PMGResource respmg = new PMGResource("ANY:PROCESS");
			respmg.setStartAction();
			
			System.out.println("REQPMG RESULT:" + srv.isAuthorizationGranted(respmg) + 
					" Status:" + srv.getStatusMessage());
//			System.out.println("REQ1 RESULT:" + srv.isAuthorizationGranted(new IDLResource("IDL:pmg/AGENT:1.0","set_mrs_usage")) + 
//					" Status:" + srv.getStatusMessage());
//			System.out.println("REQ2 RESULT:" + srv.isAuthorizationGranted(new IDLResource("IDL:am/amServer:1.0","_get_information")) + 
//					" Status:" + srv.getStatusMessage());
//			System.out.println("REQ3 RESULT:" + srv.isAuthorizationGranted(new IDLResource("IDL:am/amServer:1.0","_get_information-WRONG-VALUE")) + 
//					" Status:" + srv.getStatusMessage());
			
			
		} catch (AMException e) {
			// TODO Auto-generated catch block
			System.err.println(e.getMessage());
		}
	}
}
