package am.client;

import am.util.AMException;
import am.xacml.context.impl.AMRMAction;
import am.xacml.context.impl.AMRMResource;

public class TestAMRM {

	/**
	 * @param args
	 */
	public static void main(String[] args){

		RequestorInfo access_subject = null;

		ServerInterrogator srv = new ServerInterrogator();

		String role_name = null;
        String action = null;
		String user_name = null;
		int action_id = -1;

        for (int i = 0; i < args.length; i++)
          {
            if (args[i].equals("-r") || args[i].equals("--role-name"))
              {
                role_name = args[++i];
              }
            else if (args[i].equals("-a") || args[i].equals("--action"))
              {
                action = args[++i];
              }
            else if (args[i].equals("-u") || args[i].equals("--user-name"))
              {
                user_name = args[++i];
              }
            else if (args[i].equals("-h") || args[i].equals("--help"))
              {
                System.out.println("Usage: java ... TestAMRM -r role -a action [-u user]\n");
                System.out.println();
                System.out.println("Options/Arguments:");
                System.out.println("  -r | --role-name [name]   Required name of the role");
                System.out.println("  -a | --action [name]      Required action (\"assign\" or \"enable\")");
                System.out.println("  -u | --user-name [name]   Name of user (\"current\" user by default)");
                System.out.println();
                System.out.println("Description:");
                System.out.println("  Test Access Manager Roles Management client implementation.");
                System.exit(0);
              }
            else
              {
                System.err.println("ERROR [TestAMRM.main()]: unexpected parameter \'" + args[i] + '\'');
                System.exit(1);
              }
          }
        
        if(role_name == null)
          {
            System.err.println("ERROR [TestAMRM.main()]: role-name is required parameter.");
            System.exit(1);
          }

        if(action == null)
          {
            System.err.println("ERROR [TestAMRM.main()]: action is required parameter.");
            System.exit(1);
          }
        else
          {
            if(action.equalsIgnoreCase("assign"))
              {
                action_id = AMRMAction.ACTION_ID_ASSIGN_ROLE;
              }
            else if(action.equalsIgnoreCase("enable"))
              {
                action_id = AMRMAction.ACTION_ID_ENABLE_ROLE;
              }
            else
              {
                System.err.println("ERROR [TestAMRM.main()]: unexpected action \'" + action + '\'');
                System.exit(1);
              }
          }
        
        // change current user if needed
        if(user_name != null)
          {
            access_subject = new RequestorInfo(user_name, null);
          }

        try
          {
            System.out.print("REQUEST AMRM RESULT: ");
            // check if the user is admin, so (s)he is allowed any actions
            if(srv.isAuthorizationGranted(AMRMResource.getInstanceForAdminOperations(), access_subject))
              {
                System.out.println("granted (admin privileges)");
              }
            // else check permissions for role operation
            else
              {
                System.out.println(srv.isAuthorizationGranted(AMRMResource.getInstanceForRoleOperations(role_name, action_id), access_subject) ? "granted" : "denied");
              }
		  }
        catch (AMException e)
          {
		    // TODO Auto-generated catch block
			System.err.println(e.getMessage());
		  }
	}
}
