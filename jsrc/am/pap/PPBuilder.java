package am.pap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import am.pap.input.PoliciesInput;
import am.pap.storage.PolicyStorage;
import am.util.AMException;
import am.xacml.context.Resource;
import am.xacml.context.impl.ResourceFactory;
import am.xacml.pap.GenericRBACPolicy;
import am.xacml.pap.PermissionPolicy;
import am.xacml.pap.PermissionPolicySet;

import com.sun.xacml.AbstractPolicy;
import com.sun.xacml.Indenter;
import com.sun.xacml.PolicyReference;
import com.sun.xacml.ctx.Result;

/**
 * Builds the policies for the basic rules for each resource category.
 * @author mleahu
 * @version 1.0
 */
public class PPBuilder{

	
    private static final Logger logger =
        Logger.getLogger(PPBuilder.class.getName());
	
    /**
     * The default rule decision is PERMIT
     */
    private static final int DEFAULT_RULE_DECISION = Result.DECISION_PERMIT;
    
    // the PolicyReference objects for each rule id
    private HashMap<String, Set<PolicyReference>> policyReferences;

    // the policy input for attributes used to compose the rules or the roles hierarchy 
    private PoliciesInput policiesInput;    
    
    // the set of all policies that will be encoded in the end
    private Set<GenericRBACPolicy> policiesToEncode;
    
    /**
     * Default constructor
     * @throws AMException 
     */
    public PPBuilder(PoliciesInput policiesInput) throws AMException{
		this.policiesInput = policiesInput;
		if (this.policiesInput == null){
			throw new AMException("Null policy input object!", AMException.WRONG_ARGUMENTS);
		}
		
		policiesInput.getAttributes();
		this.policyReferences = new HashMap<String, Set<PolicyReference>>();
    	this.policiesToEncode = new HashSet<GenericRBACPolicy>();
    }
        
	/**
	 * Build the rules from the policies input.
	 */
	public void buildRuleSets() {
		//get the rules and build the policies for them.
		Set<String> rules = policiesInput.getValues("Rule");
		for(String rule: rules){
			try {
				Set<PolicyReference> newPRefSet = buildPolicyForRuleSet(rule);
				if (policyReferences.containsKey(rule)) {
					logger.warning("policy already defined for rule:" + rule);
				}
				policyReferences.put(rule, newPRefSet);				
				logger.finer("Added policy references for rule [" + rule + "]: " + newPRefSet);
			} catch (AMException e) {
				logger.severe("Failed to build policy for role [" + rule + "]. Error:" + e.getMessage());
			}
		}
	}


	/**
	 * Build the policies for the given rule. Each resource id
	 * @param rule the rule name
	 * @return the set of policy references to the PPSs
	 * @throws AMException
	 */
	private Set<PolicyReference> buildPolicyForRuleSet(String ruleSet) throws AMException{		
		
		logger.info("Create policies for rule [" + ruleSet + "]");
		
		List<PermissionPolicy> ppList = new ArrayList<PermissionPolicy>();
		List<PermissionPolicySet> ppsList = new ArrayList<PermissionPolicySet>();
		
		// generate the permission policies
		List<HashMap<String, List<String>>> attrForRule =
			policiesInput.getAttributesForKeyValue("Rule", ruleSet);
		
		for(HashMap<String, List<String>> attributesMap: attrForRule) {
			Resource resource = null;
			try {
				resource = ResourceFactory.getResource(attributesMap);
			} catch (AMException aex) {
				logger.severe("Resource not built from the attributes map:" + attributesMap);
				continue;
			}
			
			// BUILD THE PP FOR THIS RESOURCE OR ADD THE RULE FOR THIS RESOURCE TO AN EXISTING PP
			// SUITABLE FOR THIS RESOURCE ( the pp target matches the resource)
			PermissionPolicy pp = null;			
			for(PermissionPolicy ppIt: ppList){
				// if the resource is already in the target, the use this pp
				if (ppIt.resourceMatchPolicyTarget(resource)){
					pp = ppIt;
					break;
				}
			}
			// a PP suitable for this resource not found, then create a new one
			if ( pp == null ){
				pp = new PermissionPolicy(
						ruleSet, 
						"Permission policy for rule " + ruleSet);
				ppList.add(pp);
			}
			
			//check if there is a decision specified in the rule
			int decisionIdx = 0;
			List<String> decisionValues = attributesMap.get("Decision");
			String decisionValue = null;
			if (decisionValues == null || decisionValues.size() == 0){
				// allow by default
				logger.fine("Set default rule decision to " + Result.DECISIONS[DEFAULT_RULE_DECISION]);
				decisionIdx = DEFAULT_RULE_DECISION;
			} else {
				decisionValue = decisionValues.get(0);
				if (decisionValues.size() > 1){
					logger.warning("Only one [Decision] attribute allowed for a rule. Use only the first one found:" + decisionValue);
				}
				for(; decisionIdx < Result.DECISIONS.length; decisionIdx++){
					if (Result.DECISIONS[decisionIdx].equals(decisionValue)){
						break;
					}
				}
			}
			
			if (decisionIdx < Result.DECISIONS.length) {
				pp.addRuleResourceAction(resource, decisionIdx);
			} else {
				logger.severe("Invalid decision value [" + decisionValue + "] for rule " + ruleSet);
				continue;
			}
			
			// BUILD THE PPS FOR THIS RESOURCE CATEGORY IF NOT ALREADY EXISTS ONE
			PermissionPolicySet pps = null;
			for(PermissionPolicySet ppsIt: ppsList){
				// if the resource is already in the target, use this PPS 
				if (ppsIt.resourceMatchPolicyTarget(resource)){
					pps = ppsIt;
					break;
				}
			}
			// a PPs suitable for this resource not found, then create a new one
			if ( pps == null ){
				pps = new PermissionPolicySet(
						ruleSet,
						"Permission Policy Set for rule [" + ruleSet + "]",
						true);
				pps.setTarget(resource);
				ppsList.add(pps);
			}
			
			//add the PP to this PPS
			pps.addPermissionPolicy(pp);
			
		}
		
		// add ALL policies to be encoded to the list of policies to encode
		policiesToEncode.addAll(ppList);
		policiesToEncode.addAll(ppsList);
		
		// build the policy reference set
		HashSet<PolicyReference> policyReferences = new HashSet<PolicyReference>();
		for(PermissionPolicySet ppsIt: ppsList){
			policyReferences.add(ppsIt.getPolicySetReference());
		}
		return policyReferences;
	}


	/**
	 * Get a map of rule names and their policy reference object for the policy
	 * holding the pps.
	 * @return map of rules names as keys and a set of policy reference objects
	 */
	public HashMap<String, Set<PolicyReference>> getPolicyReferencesForRules() {
		return policyReferences;
	}

	/**
	 * Write the policies to the Policy Storage
	 * @param ps policy storage object
	 */
	public void encode(PolicyStorage ps){
		for(GenericRBACPolicy rbacPolicy: policiesToEncode){
			AbstractPolicy p;
			try {
				logger.info("Writing policy ...");
				p = rbacPolicy.getUpdatedPolicy();
				p.encode(ps.getOutputStream(p.getId(), 0), new Indenter());	
				logger.info("Policy [" + p.getId() + "] successfully written");
			} catch (AMException e) {
				logger.severe("Failed!!!");
				e.printStackTrace();
			}
		}
	}

}
