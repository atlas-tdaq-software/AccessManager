package am.pap.storage;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;

import org.w3c.dom.Document;

import am.util.AMException;

/**
 * Abstract class to define the methods of a storage solutions for the policies.
 * @author mleahu
 * @version 1.0
 */
public abstract class PolicyStorage {
	
	/**
	 * Get the input stream to read and parse the policies from.
     * @param policyIdReference an identifier specifying some policy
     * @param policyType type of reference (policy or policySet) as identified by
     *             the fields in <code>PolicyReference</code>
	 * @return the input stream to read the policy from
	 * @throws AMException
	 */
	public InputStream getInputStream(URI policyIdReference, int policyType) throws AMException{
		throw new AMException("Method not implemented!", AMException.METHOD_NOT_IMPLEMENTED);
	}
	
	
	/**
	 * Get the output stream where the policy can be written.
     * @param policyIdReference an identifier specifying some policy
     * @param policyType type of reference (policy or policySet) as identified by
     *             the fields in <code>PolicyReference</code>
	 * @return the output stream where the policy can be written
	 * @throws AMException
	 */
	public OutputStream getOutputStream(URI policyIdReference, int policyType) throws AMException{
		throw new AMException("Method not implemented!", AMException.METHOD_NOT_IMPLEMENTED);
	}
	
	/**
	 * Checks if the class is able to parse the input stream and return an XML document
	 * using the <code>getXMLDocument</code> method.
	 * @return true if the getXMLDocument is implemented, else false.
	 */
	public boolean isXMLDocumentSupported(){
		return false;
	}
	
	/**
	 * Get the XML document containing a policy.
     * @param policyIdReference an identifier specifying some policy
     * @param policyType type of reference (policy or policySet) as identified by
     *             the fields in <code>PolicyReference</code>
	 * @return the XML document
	 * @throws AMException
	 */
	public Document getXMLDocument(URI policyIdReference, int policyType) throws AMException{
		throw new AMException("Method not implemented!", AMException.METHOD_NOT_IMPLEMENTED);
	}
	
	/**
	 * Put the XML document containing a policy into the ploicy storage.
     * @param policyIdReference an identifier specifying some policy
     * @param policyType type of reference (policy or policySet) as identified by
     *             the fields in <code>PolicyReference</code>
	 * @throws AMException
	 */
	public void putXMLDocument(URI policyIdReference, int policyType, Document document) throws AMException{
		throw new AMException("Method not implemented!", AMException.METHOD_NOT_IMPLEMENTED);
	}
}
