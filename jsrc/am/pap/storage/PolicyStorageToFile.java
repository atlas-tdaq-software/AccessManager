package am.pap.storage;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import am.util.AMCommon;
import am.util.AMException;

public class PolicyStorageToFile
	extends PolicyStorage
    implements ErrorHandler {

    // the logger we'll use for all messages
    private static final Logger logger =
        Logger.getLogger(PolicyStorageToFile.class.getName());
    
    
    private static String POLICY_FILE_EXTENSION = ".xml";
    
    /**
     * The property which is used to specify the schema
     * file to validate against (if any)
     */
    public static final String POLICY_SCHEMA_PROPERTY =
        "com.sun.xacml.PolicySchema";


    public static final String JAXP_SCHEMA_LANGUAGE =
        "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
    
    public static final String W3C_XML_SCHEMA =
        "http://www.w3.org/2001/XMLSchema";

    public static final String JAXP_SCHEMA_SOURCE =
        "http://java.sun.com/xml/jaxp/properties/schemaSource";
    
    
    /**
     * The directory containing the policies
     */
    public String policiesDirectory;
        
    /**
     * The constructor with policies directory parameter. All the policies will use this path to
     * store the files.
     * @param policiesDirectory
     */
    public PolicyStorageToFile(String policiesDirectory){
    	this.policiesDirectory = policiesDirectory;
    	if (policiesDirectory == null){
    		this.policiesDirectory = "";
    	} else {
    		logger.config("Policies directory is [" + policiesDirectory + "]");
    	}    		
    }
	        
    
	/**
	 * Obtain the storage path from the policy ID;
	 * @param policyId policy id
	 * @return storage path
	 */
	public static String convertPolicyIdToStoragePath(URI policyId) {
		char sep = AMCommon.getFileSeparator();
		String storagePath = policyId.toString();
		//replace some characters with file separator or '_'
		storagePath = storagePath.replace(':',sep);
		storagePath = storagePath.replace('/',sep);
		storagePath = storagePath.replace('&',sep);
		storagePath = storagePath.replace('?',sep);
		storagePath = storagePath.replace('=','_');
		return storagePath + POLICY_FILE_EXTENSION;
	}

	/**
	 * Get the list of policies id from a storage path that is relative to a base policies directory.
	 * It searches recursive through subdirectories.
	 * @param policiesBaseDirectory the root directory for all the policies
	 * @param storageRelativePath the relative path of the policies to be identified
	 * @param recursive if true then search recursive in the subdirectories
	 * @return a list of policies ids
	 */
	public static List<URI> getPolicyIdsFromStoragePath(
			String policiesBaseDirectory, 
			String storageRelativePath,
			boolean recursive) {
		List<URI> policyIds = new ArrayList<URI>();
		
		if (policiesBaseDirectory == null || storageRelativePath == null){
			return policyIds;
		}

		FilenameFilter searchFilter = new FilenameFilter(){
			public boolean accept(File dir, String name){
				return (name.endsWith(POLICY_FILE_EXTENSION));
			}				
		};  
		
		char sep = AMCommon.getFileSeparator();
		// get the files from this directory that have .xml extension
		String fullPath = policiesBaseDirectory + AMCommon.getFileSeparator() + storageRelativePath;
		File currentDir = new File(fullPath);
		File[] entries = currentDir.listFiles();
		if (entries != null) {
			for(File entry:entries){
				// if this is a directory, the go inside and search for more files
				if (recursive && entry.isDirectory()){
					policyIds.addAll(
							getPolicyIdsFromStoragePath(
									policiesBaseDirectory, 
									storageRelativePath + sep + entry.getName(),
									recursive));
				} else {				
					String fileName = entry.getName();
					if (searchFilter.accept(currentDir, fileName)){
						String filename2 = storageRelativePath + sep + fileName.substring(0, 
								fileName.lastIndexOf(POLICY_FILE_EXTENSION));
						// construct the URI
						policyIds.add(URI.create(filename2.replace(sep, ':')));
					} 
				}
			}
		}
		return policyIds;
	}

	/* (non-Javadoc)
	 * @see am.pap.PolicyStorage#getInputStream(java.net.URI, int)
	 */
	@Override
	public InputStream getInputStream(URI policyIdReference, int policyType) throws AMException {
		String policyFileName = convertPolicyIdToStoragePath(policyIdReference);
		policyFileName = policiesDirectory + AMCommon.getFileSeparator() + policyFileName; 
		InputStream fis = null;
		
		try {
			fis = new FileInputStream(policyFileName);
			logger.fine("Opened stream to read policy from file [" + policyFileName + "]");
		} catch (FileNotFoundException e) {
			logger.warning("Could not open file [" + policyFileName + "] for read operation. Error:" + e.getMessage());
			throw new AMException("Could not open file [" + policyFileName + "] for read operation. Error:" + e.getMessage(),
					AMException.AM_PAP_POLICY_READ_ERROR);
		}
		
		return fis;
	}

	/* (non-Javadoc)
	 * @see am.pap.PolicyStorage#getOutputStream(java.net.URI, int)
	 */
	@Override
	public OutputStream getOutputStream(URI policyIdReference, int policyType) throws AMException {
		String policyFileName = convertPolicyIdToStoragePath(policyIdReference);
		policyFileName = policiesDirectory + AMCommon.getFileSeparator() + policyFileName; 
		AMCommon.createDirectories(policyFileName);
		
		OutputStream fos = null;
		
		try {
			if ((new File(policyFileName)).exists()){
				logger.warning("The file [" + policyFileName + "] already exists and will be overwritten!");
			}
			fos = new FileOutputStream(policyFileName);
			logger.fine("Opened stream to write policy to file [" + policyFileName + "]");
		} catch (FileNotFoundException e) {
			logger.warning("Could not open file [" + policyFileName + "] for write operation. Error:" + e.getMessage());
			throw new AMException("Could not open file [" + policyFileName + "] for write operation. Error:" + e.getMessage(),
					AMException.AM_PAP_POLICY_WRITE_ERROR);
		}
		
		return fos;
	}

	/* (non-Javadoc)
	 * @see am.pap.PolicyStorage#getXMLDocument(java.net.URI, int)
	 */
	@Override
	public Document getXMLDocument(URI policyIdReference, int policyType) throws AMException {
		
		// no schema file
		String schemaFile = null;
		
		// error handler
		ErrorHandler errHandler = this;
		
		// get the input stream
		InputStream policyInputStream = getInputStream(policyIdReference, policyType);
		
		// xml document to be returned
		Document xmlDoc = null;
		
        // create the factory
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        
        // ignore the XML comments
        factory.setIgnoringComments(true);
        // as of 1.2, we always are namespace aware
        factory.setNamespaceAware(true);

        DocumentBuilder db = null;
        
        try {
            // set the factory to work the way the system requires
            if (schemaFile == null) {
                // we're not doing any validation
                factory.setValidating(false);
                db = factory.newDocumentBuilder();
            } else {
                // we're using a validating parser
                factory.setValidating(true);

                factory.setAttribute(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);
                factory.setAttribute(JAXP_SCHEMA_SOURCE, schemaFile);
                
                db = factory.newDocumentBuilder();
                db.setErrorHandler(errHandler);
            }

            // try to load the policy file
           xmlDoc = db.parse(policyInputStream);
           
        } catch (Exception e){
        	logger.severe("Error parsing the XML policy file for policy id [" + policyIdReference + "] Error:" + e.getMessage());
        	throw new AMException("Error parsing the XML policy file for policy id [" + policyIdReference + "] " +
        			"Error:" + e.getMessage(), AMException.AM_PAP_POLICY_READ_ERROR);
        }
		
		return xmlDoc;
	}

	/* (non-Javadoc)
	 * @see am.pap.PolicyStorage#isXMLDocumentSupported()
	 */
	@Override
	public boolean isXMLDocumentSupported() {		
		return true;
	}

	/* (non-Javadoc)
	 * @see am.pap.PolicyStorage#putXMLDocument(java.net.URI, int, org.w3c.dom.Document)
	 */
	@Override
	public void putXMLDocument(URI policyIdReference, int policyType, Document document) throws AMException {
		// get the output stream
		OutputStream policyOutputStream = getOutputStream(policyIdReference, policyType);
		
		try {
			// Prepare the DOM document for writing
			Source source = new DOMSource(document);
			Result result = new StreamResult(policyOutputStream);
			
            // Write the DOM document to the file
            Transformer xformer = TransformerFactory.newInstance().newTransformer();
            xformer.transform(source, result);
        } catch (Exception e) {
        	logger.severe("Error writing the XML policy file for policy id [" + policyIdReference + "] Error:" + e.getMessage());
        	throw new AMException("Error writin the XML policy file for policy id [" + policyIdReference + "] " +
        			"Error:" + e.getMessage(), AMException.AM_PAP_POLICY_WRITE_ERROR);
        }		
	}
	

    /**
     * Standard handler routine for the XML parsing.
     *
     * @param exception information on what caused the problem
     */
    public void warning(SAXParseException exception) throws SAXException {
        if (logger.isLoggable(Level.WARNING))
            logger.warning("Warning on line " + exception.getLineNumber() +
                           ": " + exception.getMessage());
    }

    /**
     * Standard handler routine for the XML parsing.
     *
     * @param exception information on what caused the problem
     *
     * @throws SAXException always to halt parsing on errors
     */
    public void error(SAXParseException exception) throws SAXException {
        if (logger.isLoggable(Level.WARNING))
            logger.warning("Error on line " + exception.getLineNumber() +
                           ": " + exception.getMessage() + " ... " +
                           "Policy will not be available");

        throw new SAXException("error parsing policy");
    }

    /**
     * Standard handler routine for the XML parsing.
     *
     * @param exception information on what caused the problem
     *
     * @throws SAXException always to halt parsing on errors
     */
    public void fatalError(SAXParseException exception) throws SAXException {
        if (logger.isLoggable(Level.WARNING))
            logger.warning("Fatal error on line " + exception.getLineNumber() +
                           ": " + exception.getMessage() + " ... " +
                           "Policy will not be available");

        throw new SAXException("fatal error parsing policy");
    }		
}
