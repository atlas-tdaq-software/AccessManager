package am.pap.storage;

import java.util.logging.Logger;

public class PolicyStorageFactory {
	
	 private static final Logger logger =
	        Logger.getLogger(PolicyStorageFactory.class.getName());
	 
	 /**
	 *  Default policy storage implementation
	 */
	private static String DEFAULT_POLICY_STORAGE = "FILE";
	
	/**
	 * Create a PolicyStorage object based on the storage id 
	 * @param storageId the storage type identifier
	 * @param policiesStoragePath the policies storage path
	 * @return policy storage object for the given storageId
	 */
	public static PolicyStorage getPolicyStorage(String storageId, String policiesStoragePath){
		
		if (storageId == null){
			logger.warning("No storage id!");
			return null;
		}
		
		if (storageId.equals("FILE")){
			return new PolicyStorageToFile(policiesStoragePath);
		} else{
			logger.severe("Could not create an policy storage object. Id unknown: " + storageId);
			return null;
		}
	}

	
	/**
	 * Return the default policy storage implementation
	 * @param policiesStoragePath the policies storage path
	 * @return the default policy storage object
	 */
	public static PolicyStorage getDefaultPolicyStorage(String policiesStoragePath){
		return getPolicyStorage(DEFAULT_POLICY_STORAGE, policiesStoragePath);
	}
	
}
