package am.pap;

import java.util.logging.Logger;

import am.util.AMException;
import am.util.AMLogger;
import am.util.AMCommon;

/**
 * Provide AM PAP configuration settings retrieved from different sources like 
 * the system environment variables, configuration files, configurations in data bases.
 * @author mleahu
 *
 */
public class PAPConfig {

	/**
	 * AM LDAP host name.
	 */
	public static final String PARAMETER_AM_LDAP_HOST="TDAQ_AM_LDAP_HOST";

	/**
	 * AM LDAP base dn
	 */
	public static final String PARAMETER_AM_LDAP_BASEDN="TDAQ_AM_LDAP_BASEDN";

	
	/**
	 * The AM logs directory
	 */
	public static final String PARAMETER_AM_LOGS_DIR="TDAQ_AM_LOGS_DIR";

	/**
	 * The AM PAP log level parameter name
	 */
	public static final String PARAMETER_AM_PAP_LOG_LEVEL="TDAQ_AM_PAP_LOG_LEVEL";

	/**
	 * The path where the policies are stored
	 */
	public static final String PARAMETER_AM_POLICIES_PATH="TDAQ_AM_POLICIES_PATH";

	/**
	 * The default value for LDAP host name
	 */
	public static final String DEFAULT_VALUE_AM_LDAP_HOST = "atlas-ldap.cern.ch";
	
	/**
	 * The default value for LDAP base dn
	 */
	public static final String DEFAULT_VALUE_AM_LDAP_BASEDN = "ou=atlas,o=cern,c=ch";

	private static String[] packagesToLog = {
		// am.common
		"am.config",
		"am.util",
		"am.rbac",
		"am.rbac",
		"am.pap",
		"am.pdp",
		"am.pip",
		"am.server",
		// am.xacml
		"am.xacml.context",
		"am.xacml.pap",
		// sunxacml
		"com.sun.xacml",
	};
	
	/**
	 * The AM specific logger
	 */
	private AMLogger amLogger = null;
	
	private am.config.Configuration amConfig;

	private static final Logger logger =
	        Logger.getLogger(PAPConfig.class.getName());

	
	public PAPConfig(){

		amConfig = new am.config.Configuration();
		
		try {
			amConfig.addConfigurationSource(new am.config.ConfigurationFromEnvironment());
		} catch (AMException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		logger.finest(this.toString());
		
		enableLogging();
	}
	
	
	/**
	 * Reads the LDAP server name;
	 * @return the LDAP server name
	 */
	public String getLDAPHost(){
		return amConfig.get(PARAMETER_AM_LDAP_HOST, DEFAULT_VALUE_AM_LDAP_HOST);
	}
	
	/**
	 * Reads the AM LDAP base DN;
	 * @return the LDAP base DN
	 */
	public String getLDAPBaseDN(){
		return amConfig.get(PARAMETER_AM_LDAP_BASEDN, DEFAULT_VALUE_AM_LDAP_BASEDN);
	}

	/**
	 * Get the AM PAP log level;
	 * @return the log level in string format
	 */
	public String getLogLevel(){
		return amConfig.get(PARAMETER_AM_PAP_LOG_LEVEL, "NORMAL");
	}
	
	/**
	 * Get the AM policies storage path;
	 * @return the path where the policies are stored
	 */
	public String getPoliciesPath(){
		return amConfig.get(PARAMETER_AM_POLICIES_PATH, "PARAMETER_NOT_DEFINED");
	}
	
	/**
	 * Start the logging for the AM PAP
	 */
	private void enableLogging(){
		
		try {
			if (amLogger == null) {
				String amLogsDir = amConfig.get(PARAMETER_AM_LOGS_DIR, ".");
				amLogsDir += ((amLogsDir.length() > 0) ? AMCommon.getFileSeparator() : ""); 
				amLogsDir += "amPAP" + AMCommon.getFileSeparator();
				amLogger = new AMLogger(amLogsDir);
			}
		} catch (AMException e) {
			System.err.println("Could not create AM logger! Error: " + e.getMessage());
			return;
		}
		
		// first of all enable the logging for am.config package in case there is a problem 
		// while the configuration parameters are read		
		//amLogger.enableLoggingToFile(AMConfig.class.getPackage().getName());
		
		// get the AM log level id
		String sLogLevel = getLogLevel().toUpperCase();
		
		int amLogLevelId;
		try {
			//check if it's a number
			amLogLevelId = Integer.valueOf(sLogLevel);
			amLogLevelId = AMLogger.validateAMLogLevelId(amLogLevelId);
		} catch (NumberFormatException e){
			// it's not a number, consider it a string and get the id
			amLogLevelId = AMLogger.getAMLogLevelId(sLogLevel);
		}
		
		// enable the loggers for all the packages we are interested in
		for(String packageName: packagesToLog){
			amLogger.enableLoggingToFile(packageName, amLogLevelId);
		}
		amLogger.setAMFormatterToAllLoggers();
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		String message = "Access Manager Policy Administration Point configurations:\n";
		message += "LDAPHost: [" + getLDAPHost() + "]\n";
		message += "LDAPBaseDN: [" + getLDAPBaseDN() + "]\n";
		message += "LogLevel: [" + getLogLevel() + "]\n";
		message += "PoliciesPath: [" + getPoliciesPath() + "]\n";
		return message;
	}
}
