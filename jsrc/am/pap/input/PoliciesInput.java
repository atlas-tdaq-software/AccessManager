package am.pap.input;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Interface to describe the methods to read input for policies builders.
 * @author mleahu
 *
 */
public interface PoliciesInput {

    /**
	 * Read each line from the input and put it in a list.
	 * Each line contains pairs of "[key=value]" which are translated into a hash map.
	 * @return a list of maps that contain keys and values read from a line
	 */
	public List<HashMap<String, List<String>>> getAttributes();

	/**
	 * Return only the maps containing the attribute "key=value"
	 * @param key
	 * @param value
	 * @return list of hash maps, one hash map per input text line
	 */
	public List<HashMap<String, List<String>>> getAttributesForKeyValue(String key, String value);

	/**
	 * Return a set of values for a given key in a list of hash maps. 
	 * @param key
	 * @return set of values
	 */
	public Set<String> getValues(String key);
	
}
