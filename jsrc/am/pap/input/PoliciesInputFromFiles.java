package am.pap.input;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

/**
 * Class with methods to process (read) the IDL interfaces and methods stored in a ASCII file 
 * @author mleahu
 * @version 1.0
 */

public class PoliciesInputFromFiles extends AbstractPoliciesInput {

    private static final Logger logger =
        Logger.getLogger(PoliciesInputFromFiles.class.getName());

    private String inputFileName;
    
    /**
     * Constructor that takes as argument the filename where from the rules will be read.
     * @param inputFileName
     */
    public PoliciesInputFromFiles(String inputFileName) {
		super();
		this.inputFileName = inputFileName;
	}
    
	/* (non-Javadoc)
	 * @see am.pap.PoliciesInput#getAttributes()
	 */
	public List<HashMap<String, List<String>>> getAttributes(){
		
		BufferedReader input = null;
		
		logger.info("Read rules from file:" + inputFileName);
		
		try{
			input = new BufferedReader(new FileReader(inputFileName));
			
			String line = null; //not declared within while loop
			while (( line = input.readLine()) != null){
				//if the line starts with #, the line is skipped and considered a comment
				if (line.startsWith("#")) {
					logger.fine("Comment ignored [" + line + "]");
					continue;
				}
				maps.add(processLine(line));				
			}
			
		} catch (FileNotFoundException ex) {
		      ex.printStackTrace();
	    }
	    catch (IOException ex){
	      ex.printStackTrace();
	    }
	    finally {
	      try {
	        if (input!= null) {
	          //flush and close both "input" and its underlying FileReader
	          input.close();
	        }
	      }
	      catch (IOException ex) {
	        ex.printStackTrace();
	      }
	    }
		
		return maps;
	}

	/**
	 * reads a line and returns a HashMap with all the keywords and their values
	 * found in the given string
	 * @param line
	 * @return
	 */
	private HashMap<String, List<String>> processLine(String line){
		
		HashMap<String, List<String>> map = new HashMap<String, List<String>>();
		
		String [] elements;
		elements = line.split("\\s*\\[|\\]\\s*\\[|\\]\\s*");
		for(String element:elements){
			// an element should have the format [attributeName=attributeValue]
			if (element.length() > 0){
				String[] fields = element.split("=");
				if (fields.length == 2){
					String key = fields[0];
					String value = fields[1];
					List<String> values = null;
					if (map.containsKey(key)){
						values = map.get(key);
						logger.finer("Key [" + key + "] present.");
					} else{
						values = new ArrayList<String>();
						map.put(key, values);
						logger.finer("Added key [" + key + "]");
					}
					values.add(value);
					logger.finer("Added value [" + value + "] for key [" + key + "]");
				} else{
					logger.finer("Ignored element [" + element + "]");
				}
			}
		}
		
		return map;
	}
}
