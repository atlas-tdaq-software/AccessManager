package am.pap.input;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Abstract class implementing some methods of PoliciesInput interface.
 * @author mleahu
 *
 */
public abstract class AbstractPoliciesInput implements PoliciesInput {

	
	protected List<HashMap<String, List<String>>> maps;
	
	/**
	 * Default constructor that initialize the map;
	 */
	public AbstractPoliciesInput() {
		maps = new ArrayList<HashMap<String, List<String>>>();
	}	
	
	/* (non-Javadoc)
	 * @see am.pap.PoliciesInput#getValues(java.lang.String)
	 */
	public Set<String> getValues(String key){
		Set<String> values = new HashSet<String>();
		for(HashMap<String, List<String>> line: maps){
			if (line.containsKey(key)){
				values.addAll(line.get(key));
			}
		}
		return values;
	}

	/* (non-Javadoc)
	 * @see am.pap.PoliciesInput#getAttributesForKeyValue(java.util.List, java.lang.String, java.lang.String)
	 */
	public List<HashMap<String, List<String>>> getAttributesForKeyValue(String key, String value) {
		List<HashMap<String, List<String>>> selection = new ArrayList<HashMap<String, List<String>>>();
		for(HashMap<String, List<String>> map: maps){
			if (map.containsKey(key)){
				List<String> values = map.get(key);
				if (values.contains(value)){
					selection.add(map);
				}
			}
		}
		return selection;
	}
	
	
}
