package am.pap;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import com.sun.xacml.AbstractPolicy;
import com.sun.xacml.Indenter;
import com.sun.xacml.PolicyReference;

import am.pap.input.PoliciesInput;
import am.pap.storage.PolicyStorage;
import am.pip.RBACPIP;
import am.rbac.Role;
import am.rbac.RoleImpl;
import am.rbac.RolesSet;
import am.util.AMException;
import am.xacml.pap.GenericRBACPolicy;
import am.xacml.pap.PermissionPolicySet;
import am.xacml.pap.RolePolicySet;
import am.xacml.pap.TopLevelRolePolicySet;

/**
 * Build the a Permission Policy Set for each role gathering all PolicyReferences objects
 * defined so far and create also the Role Policy Set object for each role.
 * @author mleahu
 * @version 1.0
 */
public class RPS_PPS_Builder{

    private static final Logger logger =
        Logger.getLogger(RPS_PPS_Builder.class.getName());
	
    // the policy input for attributes used to compose the rules or the roles hierarchy 
    private PoliciesInput policiesInput;    
    
    // the set of all policies that will be encoded in the end
    private Set<GenericRBACPolicy> policiesToEncode;
    
    // the top level policy
    private TopLevelRolePolicySet topLevelRPS;
    
    // A map of role policies to be used later to build the RBAC hierarchy
    // The key is the role name
    private HashMap<String, PermissionPolicySet> ppsMap;
    
    /**
     * The PolicyReferences for the rules;
     * The key is the rule name
     */
    private HashMap<String, Set<PolicyReference>> rulesPolicyReferences;
    
    /**
     * The RBAC Policy Information Point object
     */
    private RBACPIP pip;
    
	/**
	 * Create Role Policy Sets and Permission Policy Sets using the specification 
	 * from the policy input object and the rules already built
	 * @param policiesInput the policy input object
	 * @param rulesPolicyReferences the map of rules and their policy references
	 * @param pip the POlicy Information Point object
	 * @throws AMException
	 */
	public RPS_PPS_Builder(
			PoliciesInput policiesInput,
			HashMap<String, Set<PolicyReference>> rulesPolicyReferences,
			RBACPIP pip
			) throws AMException {
		this.policiesInput = policiesInput;
		if (this.policiesInput == null){
			throw new AMException("Null policy input object!", AMException.WRONG_ARGUMENTS);
		}		
		this.policiesInput.getAttributes();
    	this.policiesToEncode = new HashSet<GenericRBACPolicy>();
		this.ppsMap = new HashMap<String, PermissionPolicySet>();
		this.rulesPolicyReferences = rulesPolicyReferences;
		this.pip = pip;
		this.topLevelRPS = new TopLevelRolePolicySet(
				"ATLAS", 
				"The root policy for all the roles",
				2);
	}

	/**
	 * Build a PPS for a role
	 * @param roleId the role id
	 * @param attrForRole the attributes for that role
	 * @return a Permission Policy Set object
	 * @throws AMException
	 */
	private PermissionPolicySet buildPoliciesForRole(
			String role, 
			List<HashMap<String, List<String>>> attrForRole
			) throws AMException {
		
		logger.info("Create policies for [" + role + "] ");

		PermissionPolicySet pps = new PermissionPolicySet(
				role,
				"The main Permission Policy Set for role [" + role + "]",
				false);
		
		if (attrForRole == null || attrForRole.size() == 0) {
			logger.fine("No attributes for PPS:" + pps);
			return pps;
		}
		
		// get the rules included by this role
		for(HashMap<String, List<String>> attrMap: attrForRole) {
			List<String> rules = attrMap.get("IncludeRule");
			if (rules == null || rules.size() == 0) {
				logger.fine("No rules specified for role:" + role);
				continue;
			}
			
			for(String rule: rules){
				logger.info("Check the policy references for role minor [" + rule + "]");
				Set<PolicyReference> pRefs = rulesPolicyReferences.get(rule);
				if (pRefs == null){
					logger.warning("No policy reference found for role [" + rule + "]");
				}else {
					for(PolicyReference pRef: pRefs){
						pps.addPolicyReference(pRef);
					}
				}
			}
		}		
		
		// no target so far for PPS
		
		return pps;
	}

	/**
	 * Build PPS for any role (either minor or normal) from the policies input. The minor roles
	 * should not be included in the top level RPS
	 * @param roleKeyword the role keyword to be searched in the policy input
	 * @param includeInTopLevelRPS boolean value to specify if the roles should be referenced 
	 * 								directly in the top level RPS
	 */
	private void buildPPSForAnyRole(
			String roleKeyword,
			boolean includeInTopLevelRPS) {
		
		Set<String> roles = policiesInput.getValues(roleKeyword);
		for(String roleId: roles){			
			Role role = new RoleImpl(roleId, includeInTopLevelRPS);
			PermissionPolicySet pps;
			try {
				pps = buildPoliciesForRole(
									roleId,
									policiesInput.getAttributesForKeyValue(roleKeyword, roleId));
			} catch (AMException e) {
				logger.severe("Failed to build PPS for role [" + role + "] because:" + e.getMessage());
				continue;
			}
			if (ppsMap.containsKey(role)) {
				logger.warning("PPS already built for role:" + role);
			}
			ppsMap.put(roleId, pps);
			
			// add it to the encode queue
			policiesToEncode.add(pps);
			
			if (includeInTopLevelRPS) {
				//build a RPS 
				RolePolicySet rps;
				try {
					rps = new RolePolicySet(
							"RoleAssignable",
							"Role Policy Set for role " + roleId,
							pps);
					rps.setTarget(role);
					// add it to the encode queue
					policiesToEncode.add(rps);
				} catch (AMException e) {
					logger.severe("Failed to build RPS for role [" + role + "] because:" + e.getMessage());
					continue;
				}
				//add it to the top level RPS
				topLevelRPS.addRole(role, rps);
			}
		}
	}

	/**
	 * Build the PPS for all the roles
	 */
	public void buildPPSForAllRoles(){
		buildPPSForAnyRole("RoleMinor", false);
		buildPPSForAnyRole("Role", true);
	}
	
	/**
	 * Build the PPS for all the roles using the roles information from PIP
	 */
	public void buildPPSForAllRolesFromPIP() throws AMException {

		RolesSet rolesSet = pip.getRolesSet();
		for(Role role: rolesSet.getRoles()){
			//role keyword to be set to Role after the roles hierarchy is retrieved only from PIP
			String roleKeyword = "Role";
			String roleId = role.getId().toString();
			PermissionPolicySet pps;
			try {
				pps = buildPoliciesForRole(
									roleId,
									policiesInput.getAttributesForKeyValue(roleKeyword, roleId));
			} catch (AMException e) {
				logger.severe("Failed to build PPS for role [" + role + "] because:" + e.getMessage());
				continue;
			}
			if (ppsMap.containsKey(role)) {
				logger.warning("PPS already built for role:" + role);
			}
			ppsMap.put(roleId, pps);
			
			// add it to the encode queue
			policiesToEncode.add(pps);
			
			// if the role is assignable to user, the build a Role POlicy Set
			if (role.isAssignableToUser()) {
				//build a RPS 
				RolePolicySet rps;
				try {
					rps = new RolePolicySet(
							"RoleAssignable",
							"Role Policy Set for role " + roleId,
							pps);
					rps.setTarget(role);
					// add it to the encode queue
					policiesToEncode.add(rps);
				} catch (AMException e) {
					logger.severe("Failed to build RPS for role [" + role + "] because:" + e.getMessage());
					continue;
				}
				//add it to the top level RPS
				topLevelRPS.addRole(role, rps);
			}
		}
	}
		
	/**
	 * Build the hierarchy links between roles.
	 */
	public void buildHierarchicalRBAC() {
		//get the roles and build the policies for them.
		Set<String> seniors = policiesInput.getValues("RoleSenior");
		
		for(String senior: seniors){
			
			logger.info("Set the SENIOR-JUNIOR relations for senior role [" + senior + "] ");
			if (! ppsMap.containsKey(senior)){
				logger.warning("There is no PPS for senior role [" + senior + "]");
				continue;
			}
			
			List<HashMap<String, List<String>>> attrForSenior =
				policiesInput.getAttributesForKeyValue("RoleSenior", senior);
			
			List<String> juniors;
			for(HashMap<String, List<String>> attrMap: attrForSenior) {	
				juniors = attrMap.get("RoleJunior");
				if (juniors == null || juniors.size() == 0){
					logger.warning("RoleJunior not found for the current senior role in the given map!");
					continue;
				}
				
				for(String junior : juniors){
					if (! ppsMap.containsKey(junior)){
						logger.warning("There is no PPS for junior role [" + junior + "]");
						continue;
					}
					PermissionPolicySet seniorPPS = ppsMap.get(senior);
					PermissionPolicySet juniorPPS = ppsMap.get(junior);
					seniorPPS.addJuniorPPS(juniorPPS);
				}
			}
		}		
	}
	
	/**
	 * Build the hierarchy links between roles.
	 */
	public void buildHierarchicalRBACFromPIP()throws AMException {

		RolesSet rolesSet = pip.getRolesSet();
		
		for(Role role:rolesSet.getRoles()){
			String roleName = role.getId().toString();
			PermissionPolicySet rolePPS = ppsMap.get(roleName);
			if (rolePPS == null){
				logger.warning("No PPS for role: " + role);
				continue;
			}
			
			logger.info("Set the SENIORs in PPS for role [" + roleName + "] ");
			for(Role seniorRole: role.getSeniors()){
				String seniorName = seniorRole.getId().toString();
				PermissionPolicySet seniorPPS = ppsMap.get(seniorName);
				if (seniorPPS == null){
					logger.warning("There is no PPS for senior role [" + seniorName + "]");
					continue;
				}
				seniorPPS.addJuniorPPS(rolePPS);
			}
		}
	}
	
	/**
	 * Write the policies to the Policy Storage
	 * @param ps policy storage object
	 */
	public void encode(PolicyStorage ps){
		// encode all the top level policies
		for(AbstractPolicy p: topLevelRPS.getUpdatedPolicies()){
			try {
				p.encode(ps.getOutputStream(p.getId(), 0), new Indenter());	
				logger.info("Top level policy [" + p.getId() + "] successfully written");
			} catch (AMException e) {
				logger.severe("Top level policy [" + p.getId() + "] failed to be written!");
				e.printStackTrace();
			}
		}
		
		for(GenericRBACPolicy rbacPolicy: policiesToEncode){
			AbstractPolicy p;
			try {
				p = rbacPolicy.getUpdatedPolicy();
				p.encode(ps.getOutputStream(p.getId(), 0), new Indenter());	
				logger.info("Policy [" + p.getId() + "] successfully written");
			} catch (AMException e) {
				logger.severe("Policy [" + rbacPolicy.getPolicyId() + "] failed to be written!");
				e.printStackTrace();
			}
		}
	}
}
