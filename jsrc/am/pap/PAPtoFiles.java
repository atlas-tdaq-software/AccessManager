package am.pap;

import java.util.logging.Logger;

import am.pap.input.PoliciesInput;
import am.pap.input.PoliciesInputFromFiles;
import am.pap.storage.PolicyStorage;
import am.pap.storage.PolicyStorageFactory;
import am.pip.RBACPIP;
import am.pip.db.DBFactory;
import am.util.AMCommon;
import am.util.AMException;

/**
 * Translates the policies in XML files format.
 * Reads the rules from text files.
 * @author mleahu
 *
 */
public class PAPtoFiles {

    private static final Logger logger =
        Logger.getLogger(PAPtoFiles.class.getName());
    
	/**
	 * Reads the rules from text files and builds the XACML policies.
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args){

		PAPConfig papCfg = new PAPConfig();
		String policiesStoragePath = papCfg.getPoliciesPath();
		policiesStoragePath += ((policiesStoragePath.length() > 0) ? AMCommon.getFileSeparator() : "");
		System.out.println("The policy storage path is [" + policiesStoragePath + "]");
		
		PoliciesInput policyInput = new PoliciesInputFromFiles(policiesStoragePath + "AMrules.txt"); 
		
		// get the rules and create the PP for them
		PPBuilder rulesBuilder = null;
		try {
			rulesBuilder = new PPBuilder(policyInput);
			
		} catch (AMException e) {
			String msg = "Could not create the rules builder object: " +e.getMessage();
			logger.severe(msg);
			System.err.println(msg);
			System.exit(1);
		}		
		rulesBuilder.buildRuleSets();
		
		// get the LDAP parameters
		String[] ldapParameters = new String[2];
		ldapParameters[0] = papCfg.getLDAPHost();
		ldapParameters[1] = papCfg.getLDAPBaseDN();
		
		
		// get the roles, their relation with rules and the roles hierarchy
		RPS_PPS_Builder rps_pps_Builder = null;
		try {
			// initialize the RBAC specific DB object
			RBACPIP pip = new RBACPIP(
					DBFactory.getDB(DBFactory.DB_TYPE_LDAP, ldapParameters));
			rps_pps_Builder = new RPS_PPS_Builder(
					policyInput,
					rulesBuilder.getPolicyReferencesForRules(),
					pip);
			
		} catch (AMException e) {
			String msg ="Could not create the RPS PPS builder object: " + e.getMessage();
			logger.severe(msg);
			System.err.println(msg);
			System.exit(1);
		}
		
//		rps_pps_Builder.buildPPSForAllRoles();
//		rps_pps_Builder.buildHierarchicalRBAC();
		
		try {
			rps_pps_Builder.buildPPSForAllRolesFromPIP();
			rps_pps_Builder.buildHierarchicalRBACFromPIP();
		} catch (AMException e) {
			String msg = "RPS and PPS built failed due to:" + e.getMessage();
			logger.severe(msg);
			System.err.println(msg);
			System.exit(2);
		}
		
		// ENCODE ALL POLICIES
		// create the Policy Storage object
		PolicyStorage ps = PolicyStorageFactory.getPolicyStorage("FILE", policiesStoragePath);
		rulesBuilder.encode(ps);
		rps_pps_Builder.encode(ps);
		
	}
}
