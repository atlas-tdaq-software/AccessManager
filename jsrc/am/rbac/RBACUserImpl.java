package am.rbac;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class describes an user from OS point of view, i.e. uid, gid
 * and, in addition, has RBAC information like the rolesAssigned.
 * @author mleahu
 * @version 2.0
 */
public class RBACUserImpl implements RBACUser {

	private static final Logger logger =
        Logger.getLogger(RBACUserImpl.class.getName());
	
	/**
	 * User attributes that are accessible by everyone
	 */
	public static String[] USER_ATTRIBUTES = {
		"cn",
		"gecos",
		"gidNumber",
		"homeDirectory",
		"loginShell",
		"uid",
		"uidNumber",
		"mail"
	};
	
	public static final int ATTR_ID_CN				= 0;
	public static final int ATTR_ID_GECOS			= 1;
	public static final int ATTR_ID_GIDNUMBER		= 2;
	public static final int ATTR_ID_HOMEDIR			= 3;
	public static final int ATTR_ID_LOGINSHELL		= 4;
	public static final int ATTR_ID_UID				= 5;
	public static final int ATTR_ID_UIDNUMBER		= 6;
	public static final int ATTR_ID_MAIL			= 7;	
	
	/**
	 * A map of attributes names and their values.
	 */
	private Map<String, List<String>> attributes;
	
	/**
	 * A map of rolesAssigned names and their properties with their values.
	 */
	private List<String> rolesAssigned;
	
	private List<String> rolesEnabled;
	
	public RBACUserImpl(){
		attributes = new Hashtable<String, List<String>>();
		rolesAssigned = new ArrayList<String>();
		rolesEnabled = new ArrayList<String>();
	}
	
	/**
	 * Add a value for an attribute
	 * @param attrName
	 * @param attrValue
	 */
	public void addAttributeValue(String attrName, List<String> attrValue){
		attributes.put(attrName, attrValue);
	}

	/**
	 * Add more attributes and their values 
	 * @param attrs
	 */
	public void addAttributesValues(Map<String, List<String>> attrs){
		attributes.putAll(attrs);
	}
	
	/**
	 * Add a role to this user.
	 * @param roleName the role name
	 * @param roleEnabled true if the role is enabled
	 */
	public void addRole(String roleName, boolean roleEnabled){
		if (logger.isLoggable(Level.FINE)){
			logger.fine(this + " # add role " + ((roleEnabled)? "ENABLED":"DISABLED") + 
					" [" + roleName + "]");
		}
		if (roleEnabled) {
			rolesEnabled.add(roleName);
		} else {
			rolesAssigned.add(roleName);
		}
	}
	
	/**
	 * Add a list of rolesAssigned to this user.
	 * @param roleNames the role names
	 * @param roleEnabled true if the rolesAssigned are enabled
	 */
	public void addRoles(List<String> roleNames, boolean roleEnabled){
		if (logger.isLoggable(Level.FINE)){
			logger.fine(this + " # add rolesAssigned " + ((roleEnabled)? "ENABLED":"DISABLED") + 
					" [" + roleNames + "]");
		}
		if (roleEnabled) {
			if (rolesEnabled.size() == 0){
				rolesEnabled = roleNames;
			} else {
				rolesEnabled.addAll(roleNames);
			}
		} else {
			if (rolesAssigned.size() == 0){
				rolesAssigned = roleNames;
			} else {
				rolesAssigned.addAll(roleNames);
			}
		}
	}
	
	
	private String getAttrValue(String attrName){
		List<String> values = attributes.get(attrName);
		if (values == null){
			return null;
		}else{
			return values.get(0);
		}		
	}
	
	// Public methods
	
	@Override
	public int getUID(){
		String s_uid = getAttrValue(USER_ATTRIBUTES[ATTR_ID_UIDNUMBER]);
		if (s_uid == null){
			logger.severe("Could not retrieve uid number for user [" + getUserName() + "]!");
			return -1;
		}
		return Integer.parseInt(s_uid);
	}
	
	@Override
	public String getLoginId(){
		return getAttrValue(USER_ATTRIBUTES[ATTR_ID_UID]);
	}
	
	@Override
	public String getUserName(){
		return getAttrValue(USER_ATTRIBUTES[ATTR_ID_CN]);
	}

	@Override
	public List<String> getRolesAssigned(){
		return rolesAssigned;
	}
	
	@Override
	public List<String> getRolesEnabled(){		
		return rolesEnabled;
	}
	
	@Override
	public boolean hasRoleAssigned(String role) {
		return rolesAssigned.contains(role);
	}

	@Override
	public boolean hasRoleAssigned(Role role) {
		return hasRoleAssigned(role.getId().toString());
	}
	
	
	@Override
	public boolean hasRoleEnabled(String role) {
		getRolesEnabled();
		return rolesEnabled.contains(role);
	}

	@Override
	public boolean hasRoleEnabled(Role role) {
		return hasRoleEnabled(role.getId().toString());
	}	
	
	@Override
	public boolean isOSValid() {
		return (getLoginId() != null && getLoginId().length() > 0);
	}

	@Override
	public boolean isRBACValid() {
		return (rolesAssigned.size() > 0);
	}
	
	@Override
	public String toString(){
		return "user [" + getLoginId() + "]"; 
	}
}
