package am.rbac;

import java.util.List;

/**
 * The RBAC user interface with methods to retrieve RBAC specific information
 * @author mleahu
 * @version 2.0
 */
public interface RBACUser {

	/**
	 * Checks if the internal values are correct for a valid OS user (uid, loginid)
	 * @return true if the RBAC user object is valid for an OS user, false otherwise
	 */
	public abstract boolean isOSValid();

	/**
	 * Checks if the internal values are correct for a valid RBAC user (has to contain at least one role assigned)
	 * @return true if the RBAC user object is valid, false otherwise
	 */
	public abstract boolean isRBACValid();

	/**
	 * Get the user id number (the unix uid)
	 * @return the user id number (the unix uid)
	 */
	public abstract int getUID();

	/**
	 * Get the login id 
	 * @return the login id
	 */
	public abstract String getLoginId();

	/**
	 * Get the user name
	 * @return the user name
	 */
	public abstract String getUserName();

	/**
	 * Get the list of all the roles assigned to this user.
	 * @return the list of role names assigned
	 */
	public abstract List<String> getRolesAssigned();

	/**
	 * Get the list of all the roles assigned and enabled for this user.
	 * @return the list of role names assigned and enabled
	 */
	public abstract List<String> getRolesEnabled();

	/**
	 * Check whether a role is assigned to the user
	 * @param role role name as a string
	 * @return true if the user has the role assigned, false otherwise 
	 */
	public abstract boolean hasRoleAssigned(String role);

	/**
	 * Check whether a role is assigned to the user
	 * @param role role object
	 * @return true if the user has the role assigned, false otherwise 
	 */
	public abstract boolean hasRoleAssigned(Role role);

	/**
	 * Check whether a role is assigned and enabled to the user
	 * @param role role name as a string
	 * @return true if the user has the role assigned and enabled, false otherwise 
	 */
	public abstract boolean hasRoleEnabled(String role);

	/**
	 * Check whether a role is assigned and enabled to the user
	 * @param role role object
	 * @return true if the user has the role assigned and enabled, false otherwise 
	 */
	public abstract boolean hasRoleEnabled(Role role);

}