/**
 * This package gathers the RBAC specific classes. At one point, this could go 
 * one level up to something like am.rbac
 */
package am.rbac;

import java.net.URI;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Abstract class to define the methods to manipulate the RBAC roles entities.
 * @author mleahu
 *
 */
public class RoleImpl implements Role {
	
	private static final Logger logger =
        Logger.getLogger(RoleImpl.class.getName());

	
	// Attributes names for junior,senior, ssd and dsd relations
	
	// Other attributes that could be added to the roles
	public static final String[] ROLE_ATTRS = {
		"amRoleAssignable",
	};
	
	/**
	 * The attribute index in the ROLE_ATTR array;
	 */
	public static final int ATTR_ID_ASSIGNABLE 		= 0;
	
	/**
	 * The attribute values
	 */
	public static final String VALUE_ROLE_ASSIGNABLE 	= "true";

	/**
	 * The attribute values
	 */
	public static final String VALUE_ROLE_NOT_ASSIGNABLE 	= "false";

	/**
	 * Role id 
	 */
	private URI id;
	
	/**
	 * The junior roles of this role.
	 */
	private Map<URI, Role> juniors;
	
	/**
	 * The senior roles of this role.
	 */
	private Map<URI, Role> seniors;

	/**
	 * The roles in direct SSD relation with this role.
	 */
	private Map<URI, Role> SSD;
	
	/**
	 * The roles in direct DSD relation with this role.
	 */
	private Map<URI, Role> DSD;
	
	
	/**
	 * Other attributes one may add to a role;
	 */
	private Map<String, String> attributes;
	
	/**
	 * Constructor that initialize the role id
	 * @param id id in URI format
	 */
	public RoleImpl(URI id, boolean assignable){
		this.id = id;
		this.juniors = new Hashtable<URI, Role>();
		this.seniors = new Hashtable<URI, Role>();
		this.SSD = new Hashtable<URI, Role>();
		this.DSD = new Hashtable<URI, Role>();
		this.attributes = new Hashtable<String, String>();
		String assignableValue = (assignable) ? VALUE_ROLE_ASSIGNABLE : VALUE_ROLE_NOT_ASSIGNABLE;
		addAttribute(ROLE_ATTRS[ATTR_ID_ASSIGNABLE], assignableValue);
	}
	
	/**
	 * Constructor that initialize the id of the current role
	 * @param sid id in string format
	 * @param assignable true if this role is assignable
	 */
	public RoleImpl(String sid, boolean assignable){
		this(URI.create(sid), assignable);
	}

	/**
	 * Add a role which is junior for the current role
	 * @param roleJunior
	 * @return true if the relation has been successfully set, false otherwise
	 */
	public boolean addJunior(Role roleJunior){
		if (juniors.containsKey(roleJunior.getId())){
			logger.fine(this + " already has " + roleJunior + " as junior!");
			return false;			
		}
		juniors.put(roleJunior.getId(), roleJunior);
		if (logger.isLoggable(Level.FINE)){
			logger.fine(this + " # add junior " + roleJunior);
		}
		return true;
	}
	
	/**
	 * Remove a junior of this role
	 * @param roleJunior
	 * @return true if the relation has been successfully deleted, false otherwise
	 */
	public boolean removeJunior(Role roleJunior){
		if (! juniors.containsKey(roleJunior.getId())){
			logger.fine(this + " doesn't have " + roleJunior + " as junior!");
			return false;
		}
		juniors.remove(roleJunior.getId());
		if (logger.isLoggable(Level.FINE)){
			logger.fine(this + " # remove junior " + roleJunior);
		}
		return true;
	}

	/**
	 * Add a role which is senior for the current role
	 * @param roleSenior
	 * @return true if the relation has been successfully set, false otherwise
	 */
	public boolean addSenior(Role roleSenior){
		if (seniors.containsKey(roleSenior.getId())){
			logger.fine(this + " already has " + roleSenior + " as senior!");
			return false;			
		}
		seniors.put(roleSenior.getId(), roleSenior);
		if (logger.isLoggable(Level.FINE)){
			logger.fine(this + " # add senior " + roleSenior);
		}
		return true;
	}
	
	/**
	 * Remove a senior of this role
	 * @param roleSenior
	 * @return true if the relation has been successfully deleted, false otherwise
	 */
	public boolean removeSenior(Role roleSenior){
		if (! seniors.containsKey(roleSenior.getId())){
			logger.fine(this + " doesn't have " + roleSenior + " as senior!");
			return false;
		}
		seniors.remove(roleSenior.getId());
		if (logger.isLoggable(Level.FINE)){
			logger.fine(this + " # remove senior " + roleSenior);
		}
		return true;
	}

	/**
	 * Enable the SSD relation with a role
	 * @param roleSSD
	 * @return true if the relation has been successfully set, false otherwise
	 */
	public boolean setSSD(Role roleSSD){
		if (SSD.containsKey(roleSSD.getId())){
			logger.fine(this + " SSD relation with " + roleSSD + " already set!");
			return false;			
		}
		SSD.put(roleSSD.getId(), roleSSD);
		if (logger.isLoggable(Level.FINE)){
			logger.fine(this + " # set SSD with " + roleSSD);
		}
		return true;
	}
	
	/**
	 * Delete the SSD relation with a role
	 * @param roleSSD
	 * @return true if the relation has been successfully deleted, false otherwise
	 */
	public boolean deleteSSD(Role roleSSD){
		if (! SSD.containsKey(roleSSD.getId())){
			logger.fine(this + " SSD relation with " + roleSSD + " NOT set!");
			return false;
		}
		SSD.remove(roleSSD.getId());
		if (logger.isLoggable(Level.FINE)){
			logger.fine(this + " # deleted SSD with " + roleSSD);
		}
		return true;
	}
	
	/**
	 * Enable the DSD relation with a role
	 * @param roleDSD
	 * @return true if the relation has been successfully set, false otherwise
	 */
	public boolean setDSD(Role roleDSD){
		if (DSD.containsKey(roleDSD.getId())){
			logger.fine(this + " DSD relation with " + roleDSD + " already set!");
			return false;			
		}
		DSD.put(roleDSD.getId(), roleDSD);
		if (logger.isLoggable(Level.FINE)){
			logger.fine(this + " # set DSD with " + roleDSD);
		}
		return true;
	}
	
	/**
	 * Delete the DSD relation with a role
	 * @param roleDSD
	 * @return true if the relation has been successfully deleted, false otherwise
	 */
	public boolean deleteDSD(Role roleDSD){
		if (! DSD.containsKey(roleDSD.getId())){
			logger.fine(this + " DSD relation with " + roleDSD + " NOT set!");
			return false;
		}
		DSD.remove(roleDSD.getId());
		if (logger.isLoggable(Level.FINE)){
			logger.fine(this + " # deleted DSD with " + roleDSD);
		}
		return true;
	}
	
	/**
	 * Add an attribute (name and value) for this role
	 * @param attrName unique name for the attribute
	 * @param attrValue the attribute value
	 * @return true if the attribute has been added
	 */
	public boolean addAttribute(String attrName, String attrValue){
		if (attributes.containsKey(attrName)){
			logger.warning("Attribute [" + attrName + "] not set to value [" + attrValue +
					"] It already exists with value [" + attributes.get(attrName) + "] ");
			return false;
		}
		
		attributes.put(attrName, attrValue);
		if (logger.isLoggable(Level.FINE)){
			logger.fine(this + " # added attribute [" + attrName + "=" + attrValue + "]");
		}
		return true;
	}
	
	/**
	 * Remove an attribute of this role
	 * @param attrName unique name for the attribute
	 * @return true if the attribute has been removed
	 */
	public boolean removeAttribute(String attrName){
		if (!attributes.containsKey(attrName)){
			logger.warning(this + " # Could no remove attribute [" + attrName + "] because it doesn't exist!");
			return false;
		}
		
		attributes.remove(attrName);
		if (logger.isLoggable(Level.FINE)){
			logger.fine(this + " # removed attribute [" + attrName + "]");
		}
		return true;
	}
	
	/**
	 * Get all the attributes names
	 * @return a list with the attributes names
	 */
	public List<String> getAttributesNames(){
		return new ArrayList<String>(attributes.keySet());
	}

	/**
	 * Read the value of an attribute
	 * @param attrName the name of attribute
	 * @return the value of the attribute
	 */
	public String getAttributeValue(String attrName){
		return attributes.get(attrName);
	}
	
	/* (non-Javadoc)
	 * @see am.rbac.Role#getId()
	 */
	public URI getId(){
		return id;
	}
		
	/* (non-Javadoc)
	 * @see am.rbac.Role#getJuniors()
	 */
	public List<Role> getJuniors(){
		return  new ArrayList<Role>(juniors.values());
	}
	
	/* (non-Javadoc)
	 * @see am.rbac.Role#getSeniors()
	 */
	public List<Role> getSeniors(){
		return  new ArrayList<Role>(seniors.values());
	}
	
	/* (non-Javadoc)
	 * @see am.rbac.Role#isJuniorOf(am.rbac.Role)
	 */
	public boolean isJuniorOf(Role roleSenior){
		return seniors.containsKey(roleSenior.getId());
	}
	
	/* (non-Javadoc)
	 * @see am.rbac.Role#hasSenior(am.rbac.Role)
	 */
	public boolean hasSenior(Role roleSenior) {
		return isJuniorOf(roleSenior);
	}

	/* (non-Javadoc)
	 * @see am.rbac.Role#isSeniorOf(am.rbac.Role)
	 */
	public boolean isSeniorOf(Role roleJunior){
		return juniors.containsKey(roleJunior.getId());
	}
	
	/* (non-Javadoc)
	 * @see am.rbac.Role#hasJunior(am.rbac.Role)
	 */
	public boolean hasJunior(Role roleJunior) {
		return isSeniorOf(roleJunior);
	}

	/* (non-Javadoc)
	 * @see am.rbac.Role#getSSD()
	 */
	public List<Role> getSSD() {
		return  new ArrayList<Role>(SSD.values());
	}

	/* (non-Javadoc)
	 * @see am.rbac.Role#getDSD()
	 */
	public List<Role> getDSD() {
		return  new ArrayList<Role>(DSD.values());
	}

	/* (non-Javadoc)
	 * @see am.rbac.Role#isSSDWith(am.rbac.Role)
	 */
	public boolean isSSDWith(Role roleSSD) {
		return SSD.containsKey(roleSSD.getId());
	}

	/* (non-Javadoc)
	 * @see am.rbac.Role#isDSDWith(am.rbac.Role)
	 */
	public boolean isDSDWith(Role roleDSD) {
		return DSD.containsKey(roleDSD.getId());
	}
	
	@Override
	public boolean isAssignableToUser() {
		return VALUE_ROLE_ASSIGNABLE.equals(getAttributeValue(ROLE_ATTRS[ATTR_ID_ASSIGNABLE]));
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o){
		return (this.getId() == ((Role)o).getId());
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return 	"role id [" + getId() + "]"; 
	}
	
}
