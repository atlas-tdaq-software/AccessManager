package am.rbac;

import am.util.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RolesSetImpl implements RolesSet {

	
	private static final Logger logger =
        Logger.getLogger(RolesSetImpl.class.getName());
	
	private Map<URI, RoleImpl> roles;
	
	/**
	 * Creates a roles set object
	 * @param rolesSetName
	 * @param rolesSetDescription
	 */
	public RolesSetImpl(){
		roles = new Hashtable<URI, RoleImpl>();
	}
	
	/**
	 * Add a role to the set.
	 * @param role
	 * @return true if the role has been added, false otherwise (maybe is already added, insufficient info..)
	 */
	public boolean addRole(RoleImpl role){
		
		if (containsRole(role.getId())){
			logger.finest(role + " already added to " + this);
			return false;
		}
		
		roles.put(role.getId(), role);		
		logger.fine(role + " added to " + this);
		
		return true;
	}

	/**
	 * Add a list of role to the set.
	 * @param roles the list of roles
	 */
	public void addRoles(Collection<RoleImpl> roles){
		if (roles != null){
			for(RoleImpl role: roles)
				addRole(role);
		}
		else
			logger.finest("Roles list is null!");
	}

	/**
	 * Set a senior-junior relation between 2 roles.
	 * @param roleSenior the senior role
	 * @param roleJunior the junior role
	 */
	public void setSeniorJuniorRelation(URI roleSenior, URI roleJunior) throws AMException{

		if (!containsRole(roleSenior)){
			throw new AMException("Role [" + roleSenior + "] doesn't exists in " + this, AMException.RBAC_SENIOR_ROLE_NOT_FOUND);
		}
		if (!containsRole(roleJunior)){
			throw new AMException("Role [" + roleJunior + "] doesn't exists in " + this, AMException.RBAC_JUNIOR_ROLE_NOT_FOUND);
		}
		
		RoleImpl senior = roles.get(roleSenior);
		RoleImpl junior = roles.get(roleJunior);
		
		senior.addJunior(junior);
		junior.addSenior(senior);
		
		if (logger.isLoggable(Level.FINE)){
			logger.fine("Relation senior-junior between " + senior + " and " + junior + " set!");
		}
	}

	/* (non-Javadoc)
	 * @see am.rbac.RolesSetImpl#setSeniorJuniorRelation(java.net.URI,java.net.URI)
	 */
	public void setSeniorJuniorRelation(String roleSenior, String roleJunior) throws AMException{
		setSeniorJuniorRelation(URI.create(roleSenior), URI.create(roleJunior));
	}

	/**
	 * Set a Separation of Duty relation between 2 roles 
	 * @param sdType can be "SSD" or "DSD"
	 * @param role1 first role
	 * @param role2 second role
	 * @throws AMException a RBAC_ROLE_NOT_FOUND exception may be thrown
	 */
	private void setSDRelation(String sdType, URI role1URI, URI role2URI) throws AMException{
		if (!containsRole(role1URI)){
			throw new AMException("Role [" + role1URI + "] doesn't exists in " + this, AMException.RBAC_ROLE_NOT_FOUND);
		}
		if (!containsRole(role2URI)){
			throw new AMException("Role [" + role2URI + "] doesn't exists in " + this, AMException.RBAC_ROLE_NOT_FOUND);
		}
		
		RoleImpl role1 = roles.get(role1URI);
		RoleImpl role2 = roles.get(role2URI);
		
		if ( "SSD".equals(sdType) ){
			role1.setSSD(role2);
			role2.setSSD(role1);
			logger.fine("SSD between " + role1 + " and " + role2 + " set!");		
		}
		if ( "DSD".equals(sdType) ){
			role1.setDSD(role2);
			role2.setDSD(role1);
			logger.fine("DSD between " + role1 + " and " + role2 + " set!");		
		}
	}
	
	/**
	 * Set a Static Separation of Duty relation between 2 roles 
	 * @param role1 first role
	 * @param role2 second role
	 * @throws AMException a RBAC_ROLE_NOT_FOUND exception may be thrown
	 */
	public void setSSDRelation(URI role1, URI role2) throws AMException{
		setSDRelation("SSD", role1, role2);
	}
	
	/**
	 * Set a Static Separation of Duty relation between 2 roles 
	 * @param role1 first role
	 * @param role2 second role
	 * @throws AMException a RBAC_ROLE_NOT_FOUND or AM_URI_CONVERSION_FAILED exception may be thrown
	 */
	public void setSSDRelation(String role1, String role2) throws AMException{
		setSSDRelation(URI.create(role1), URI.create(role2));
	}

	/**
	 * Set a Dynamic Separation of Duty relation between 2 roles 
	 * @param role1 first role
	 * @param role2 second role
	 * @throws AMException a RBAC_ROLE_NOT_FOUND exception may be thrown
	 */
	public void setDSDRelation(URI role1, URI role2) throws AMException{
		setSDRelation("DSD", role1, role2);
	}

	/**
	 * Set a Dynamic Separation of Duty relation between 2 roles 
	 * @param role1 first role
	 * @param role2 second role
	 * @throws AMException a RBAC_ROLE_NOT_FOUND or AM_URI_CONVERSION_FAILED exception may be thrown
	 */
	public void setDSDRelation(String role1, String role2) throws AMException{
		setDSDRelation(URI.create(role1), URI.create(role2));
	}

	@Override
	public boolean containsRole(String id) throws AMException {
		return containsRole(URI.create(id));
	}
	
	@Override
	public boolean containsRole(URI id) {
		return roles.containsKey(id);
	}

	@Override
	public Role getRole(String id){
		return getRole(URI.create(id));
	}
	
	@Override
	public Role getRole(URI id) {
		return roles.get(id);
	}

	@Override
	public List<Role> getRoles() {
		return new ArrayList<Role>(roles.values());
	}

	@Override
	public String toString(){
		return "Roles set";
	}
}
