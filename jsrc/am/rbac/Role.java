package am.rbac;

import java.net.URI;
import java.util.List;

public interface Role {

	// XACML specific parameters

	public static final URI IDENTIFIER_SUBJECT_ROLE = URI.create(
			"urn:oasis:names:tc:xacml:2.0:subject:role");

	/**
	 * The data type specified in URI format
	 */
	public static final URI DATA_TYPE_ROLE = URI.create(
			"http://www.w3.org/2001/XMLSchema#anyURI");

	/**
	 * The data type specified in URI format
	 */
	public static final URI DATA_TYPE_ROLE_REGEXP = URI.create(
			"http://www.w3.org/2001/XMLSchema#string");

	/**
	 * Match id for role type 
	 */
	public static final URI MATCHID_ROLE = URI.create(
		"urn:oasis:names:tc:xacml:1.0:function:anyURI-equal");	

	/**
	 * Match id for a regular expression match of role
	 */
	public static final URI MATCHID_ROLE_REGEXP = URI.create(
		"urn:oasis:names:tc:xacml:2.0:function:anyURI-regexp-match");
	
	/**
	 * Return the role's id in URI format.
	 * @return URI formatted role id
	 */
	public abstract URI getId();

	/**
	 * Retrieve the list of junior roles. It will return only the first level of juniors,
	 * i.e. not the juniors of juniors.
	 * @return list of Roles
	 */
	public abstract List<Role> getJuniors();

	/**
	 * Retrieve the list of senior roles. It will return only the first level of seniors,
	 * i.e. not the seniors of seniors.
	 * @return list of Roles
	 */
	public abstract List<Role> getSeniors();

	/**
	 * Responds to the question "is the current role a junior of the 'roleSenior'?" or
	 * "is the 'roleSenior' a senior of the current role ?"
	 * @param roleSenior the role that could be a senior for the current role
	 * @return true if the response to any of the questions is YES, false for NO
	 */
	public abstract boolean isJuniorOf(Role roleSenior);

	/* (non-Javadoc)
	 * @see am.rbac.Role#isJuniorOf(am.rbac.Role)
	 */
	public abstract boolean hasSenior(Role roleSenior);

	/**
	 * Responds to the question "is the current role a senior of the 'roleJunior'?" or
	 * "is the 'roleJunior' a junior of the current role ?"
	 * @param roleJunior the role that could be a junior for the current role
	 * @return true if the response to any of the questions is YES, false for NO
	 */
	public abstract boolean isSeniorOf(Role roleJunior);

	/* (non-Javadoc)
	 * @see am.rbac.Role#isSeniorOf(am.rbac.Role)
	 */
	public abstract boolean hasJunior(Role roleJunior);

	/**
	 * Retrieve the list of roles in SSD (Static Separation of Duties) relation with the current role.
	 * It will return only the first level of SSD roles, i.e. not the SSD of juniors.
	 * @return list of Roles
	 */
	public abstract List<Role> getSSD();

	/**
	 * Retrieve the list of roles in DSD (Dynamic Separation of Duties) relation with the current role.
	 * It will return only the first level of DSD roles, i.e. not the DSD of juniors.
	 * @return list of Roles
	 */
	public abstract List<Role> getDSD();

	/**
	 * Responds to the question "is the current role in a direct (i.e. no indirect through the juniors) SSD relation with 'roleSSD'?"
	 * @param roleSSD the role that could in a SSD relation with the curren role
	 * @return true if the response to any of the questions is YES, false for NO
	 */
	public abstract boolean isSSDWith(Role roleSSD);

	/**
	 * Responds to the question "is the current role in a direct (i.e. no indirect through the juniors) SSD relation with 'roleSSD'?"
	 * @param roleDSD the role that could in a DSD relation with the curren role
	 * @return true if the response to any of the questions is YES, false for NO
	 */
	public abstract boolean isDSDWith(Role roleDSD);

	/**
	 * Check if the role can be assigned to an user or is just a minor role in the hierarchy.
	 * @return true if the role is assignable to an user.
	 */
	public abstract boolean isAssignableToUser();
}