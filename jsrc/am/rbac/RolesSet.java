package am.rbac;

import java.net.URI;
import java.util.List;

import am.util.AMException;

public interface RolesSet {

	/**
	 * Check the existence of a role in the current set.
	 * @param id role id as a string
	 * @return true if the role exists in the current roles set
	 */
	public boolean containsRole(String id) throws AMException;

	/* (non-Javadoc)
	 * @see am.rbac.RolesSet#containsRole(java.lang.String)
	 */
	public boolean containsRole(URI id);

	/**
	 * Get the role object for a given role id.
	 * @param id
	 * @return role object
	 */
	public Role getRole(String id) throws AMException;

	/* (non-Javadoc)
	 * @see am.rbac.RolesSet#getRole(java.lang.String)
	 */
	public Role getRole(URI id);

	/**
	 * Get a list of all the roles in the set.
	 * @return list with roles
	 */
	public List<Role> getRoles();
	
}