/**
 * 
 */
package am.util.cache;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A cache implementation extending the hashtable and using the Ping class to call the clean
 * method from time to time.
 * @author mleahu
 * @version 1.0
 */
public class Cachetable<K> extends Hashtable<K, Object> implements Observer {

	private static final Logger logger =
        Logger.getLogger(Cachetable.class.getName());

	/**
	 * serial version uid 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identifier string to be used by the toString method.
	 */
	private String identifier = "";
	
	/**
	 * The cache life time in miliseconds
	 */
	private long expireTime;

	private Thread cleaner;
	
	/**
	 * Create an hastable object with cache capability.
	 * @param expireTime the cache expiration time in milliseconds 
	 * @param identifier the object string identifier
	 */
	public Cachetable(long expireTime, String identifier){
		super();
	
		this.expireTime = expireTime;
		
		this.identifier = identifier;
		
		// we want to be notified from time to time by the ping daemon
		Ping ping = new Ping(( long )( expireTime * 0.75 ));
		ping.addObserver(this);
		
		//set up the cleaner daemon
		cleaner = new Thread(ping, "Cleaner daemon for [" + this + "]");
		cleaner.setDaemon(true);
		cleaner.start();
		
		if (logger.isLoggable(Level.FINE)){
			logger.fine("Cachetable [" + this + "] initialized " +
					"with expire time " + expireTime + " ms " +
					"by the thread " + Thread.currentThread().getName());
		}
	}
	
	/* (non-Javadoc)
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	public void update(Observable arg0, Object arg1) {
		
		if (logger.isLoggable(Level.FINE)){
				logger.fine(this + " Cleaning the cache table...");
		}
		Enumeration e = super.keys();
        while( e.hasMoreElements() ) {
            Object key = e.nextElement();
            TimeWrapper tw = (TimeWrapper) super.get( key );
            if( ( tw != null ) && ( tw.getAge() > expireTime ) ) {
				if (logger.isLoggable(Level.FINE)){
					logger.fine(this + " Removed object from cache: [" + tw + "]");
				}				
                remove( key );
            }
        }
        // don't call explicitly the garbage collector
//	    System.gc();
	}



	/* (non-Javadoc)
	 * @see java.util.Hashtable#get(java.lang.Object)
	 */
	@Override
	public synchronized Object get(Object arg0) {
		//check if the super.get returned something != null
		Object object = super.get(arg0);		
		return (object!=null)? ((TimeWrapper)object).getObject() : null;
	}



	/* (non-Javadoc)
	 * @see java.util.Hashtable#put(K, V)
	 */
	@Override
	public synchronized Object put(K arg0, Object arg1) {
		return super.put(arg0, new TimeWrapper<Object>(arg1));
	}



	/* (non-Javadoc)
	 * @see java.util.Hashtable#putAll(java.util.Map)
	 */
	@Override
	public synchronized void putAll(Map<? extends K, ? extends Object> arg0) {
		for(K key: arg0.keySet()){
			Object value = arg0.get(key);
			super.put(key, new TimeWrapper<Object>(value));
		}
	}



	/* (non-Javadoc)
	 * @see java.util.Hashtable#values()
	 */
	@Override
	public Collection<Object> values() {
		Collection<Object> collection = new ArrayList<Object>();
		for(Object o:super.values()){
			collection.add(((TimeWrapper)o).getObject());
		}
		return collection;
	}	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return this.identifier;
	}
}
