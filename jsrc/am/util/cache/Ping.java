package am.util.cache;

import java.util.Observable;
import java.util.logging.Logger;

/**
 * This class is meant to run as a thread and ping the observers from time to time. 
 * @author mleahu
 * @version 1.0
 */
public class Ping extends Observable implements Runnable{

	private static final Logger logger =
        Logger.getLogger(Ping.class.getName());

	/**
	 * How much time to sleep
	 */
	private long sleepTime;
	
	/**
	 * Ping constructor
	 * @param sleepTime sleep time (in miliseconds)
	 */
	public Ping(long sleepTime){
		this.sleepTime = sleepTime;
		logger.config("Ping object initialized with sleep time [" + sleepTime + "]");
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		// run infinitely between sleeping and pinging
		while(true){
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				logger.warning("Ping interrupted! Reason: [" + e.getMessage() + "] Ignored!");
				continue;
			}
			
			// notify the observers
			logger.fine("Notify the observers");
			setChanged();
			notifyObservers();
		}		
	}
}
