/**
 * 
 */
package am.util.cache;

/**
 * Interface to be implemented by the classes that use internally a cache mechanism.
 * The purpose of this class is to provide a method to invalidate the cache.
 * @author mleahu
 *
 */
public interface CacheUser {
	
	/**
	 * Invalidate the internal cache.
	 */
	public void invalidateCache();

}
