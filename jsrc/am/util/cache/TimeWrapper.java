package am.util.cache;

/**
 * Attach a time stamp to a generic object. The class defines a generic type.
 * @author mleahu
 * @version 1.0
 */
public class TimeWrapper<O> {

	private long age;
	private O object;
	
	public TimeWrapper(O object){
		this.object = object;
		updateAge();
	}
	
	/**
	 * Update the object's age
	 */
	private void updateAge(){
		this.age = System.currentTimeMillis(); 
	}
	
	/**
	 * Get the object age in miliseconds
	 * @return object age in miliseconds
	 */
	public long getAge(){
		return (System.currentTimeMillis() - age);
	}
	
	/**
	 * Object getter
	 * @return the object
	 */
	public O getObject(){
		updateAge();
		return object;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return object.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object arg0) {
		if (object == null)
			return (arg0 == null);
		else
			return object.equals(arg0);
	}
	
}
