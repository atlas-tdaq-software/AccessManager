package am.util;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

/**
 * Access Manager specific exception class.
 * @author mleahu
 *
 */
public class AMException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Success code.
	 */
	public final static int SUCCESS = 0;

	/**
	 * General error.
	 */
	public final static int OPERATION_ERROR 		= 1;
	public final static int METHOD_NOT_IMPLEMENTED 	= 2;
	public final static int WRONG_ARGUMENTS 		= 3;
	
	// LDAP SPECIFIC ERRORS
	public final static int LDAP_SERVER_CONNECTION_FAILED 	= 100;
	public final static int LDAP_SERVER_BIND_FAILED 		= 101;
	public final static int LDAP_SEARCH_FAILED 				= 102;
	
	// RBAC SPECIFIC ERRORS
	public final static int RBAC_ROLE_NOT_FOUND = 200;
	public final static int RBAC_ROLES_SETS_NAMES_UNAVAILABLE = 201;
	public final static int RBAC_SENIOR_ROLE_NOT_FOUND = 202;
	public final static int RBAC_JUNIOR_ROLE_NOT_FOUND = 202;

	// USER SPECIFIC ERRORS
	public final static int USER_NOT_FOUND = 300;
	public final static int USER_PASSWORD_INACCESSIBLE = 301;
	
	// SERVER SPECIFIC ERRORS
	public final static int AM_SRV_INIT_ERROR           = 401;
	public final static int AM_SRV_COMMUNICATION_ERROR  = 402;
	public final static int AM_SRV_COMMUNICATION_CLOSED = 403;
	public final static int AM_SRV_CLIENT_REQUEST_ERROR = 404;
	public final static int AM_SRV_REQUEST_QUEUE_FULL   = 405;
	
	// CLIENT SPECIFIC ERRORS
	public final static int AM_CLNT_INIT_ERROR  		= 501;
	public final static int AM_CLNT_CONFIG_ERROR  		= 502;
	public final static int AM_CLNT_COMMUNICATION_ERROR = 503;
	public final static int AM_CLNT_REQUEST_ERROR 		= 504;
	
	// XACML ERRORS
	public final static int XACML_GENERIC_ERROR			= 601;
	public final static int XACML_ATTRIBUTE_ERROR  		= 602;
	public final static int XACML_WRONG_ATTRIBUTE_VALUE	= 603;
	

	// OTHER ERRORS
	public final static int AM_URI_CONVERSION_FAILED = 1001;
	public final static int AM_PDP_ERROR = 1200;
	
	public final static int AM_PAP_ERROR = 1300;
	public final static int AM_PAP_POLICY_READ_ERROR = 1301;
	public final static int AM_PAP_POLICY_WRITE_ERROR = 1301;
	
	public final static int AM_PIP_ERROR = 1400;
	
	
	private int errorCode;
	 
	public AMException(){
		super();
		errorCode = OPERATION_ERROR;
	}
	
	public AMException(String message){
		this(message, OPERATION_ERROR);
	}

	/**
	 * Creates an AMException object with a message and the AM specific error code.
	 * @param message a message telling what is the error
	 * @param errorCode an integer code from the static list of codes defined in this class
	 */
	public AMException(String message, int errorCode){
		super(message);
		this.errorCode = errorCode;
	}
	
	/**
	 * Read the error code
	 * @return integer value of the error code
	 */
	public int getErrorCode(){
		return errorCode;
	}
	
	/**
	 * Prepare and XACML response and write it to the output stream. The status code integer value is
	 * the error code of this AMException.
	 * @param out the outputstream
	 * @param decision the decision
	 * @param statusCodeValue the status code value
	 * @param statusMessage the message
	 * @return true if the response was successfully written to the output stream, false otherwise
	 */
	public boolean writeXACMLResponse(
			OutputStream out, 
			String decision, 
			String statusCodeValue, 
			String statusMessage){
		String xacmlResponse = 
			"<Response>" +
			"  <Result>" +
			"    <Decision>" + decision + "</Decision>" +
			"    <Status>" +
			"      <StatusCode Value=\"" + statusCodeValue + "\">" + getErrorCode() + "</StatusCode>" +
			"      <StatusMessage>" + statusMessage + "</StatusMessage>" +
			"    </Status>" +
			"  </Result>" +
			"</Response>";
		Writer wout = new BufferedWriter(new OutputStreamWriter(out));
		try {
			wout.write(xacmlResponse);
			wout.flush();
		} catch (IOException e1) {
			return false;
		}
		return true;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Throwable#toString()
	 */
	public String toString(){
		String message = "";
		
		message +="[AMException][" + errorCode + "] " +
					super.getMessage();
		
		return message;
	}
}
