package am.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.ConsoleHandler;

/**
 * This class can work with more log streams that can be chosen based on the log level.
 * The java Logger class has following log levels:
 *  SEVERE (highest value)
 *  WARNING
 *  INFO
 *  CONFIG
 *  FINE
 *  FINER
 *  FINEST (lowest value)
 *  
 *  The following usage rules should be followed: 
 *  SEVERE (highest value)  -> for critical errors that require the program to exit immediately
 *  WARNING					-> the functionality may suffer, but the program may continue to run
 *  INFO					-> information only
 *  CONFIG					-> information about the program configuration
 *  FINE					-> debug information 
 *  FINER					-> extra information
 *  FINEST (lowest value)	-> extra extra information
 *   
 * @author mleahu
 * @version 1.0
 */
public class AMLogger {

	
	/**
	 * Log levels ids 
	 */
	
	/**
	 * java.util.logging.Level.OFF 
	 */
	public static int	 LOG_LEVEL_ID_NONE	= 0;
	
	/**
	 * java.util.logging.Level.WARNING 
	 */
	public static int	 LOG_LEVEL_ID_MINIMUM	= 1;
	
	/**
	 * java.util.logging.Level.INFO 
	 */
	public static int	 LOG_LEVEL_ID_NORMAL	= 2;
	
	/**
	 * java.util.logging.Level.CONFIG 
	 */
	public static int	 LOG_LEVEL_ID_VERBOSE	= 3;
	
	/**
	 * java.util.logging.Level.FINE 
	 */
	public static int	 LOG_LEVEL_ID_DEBUG	= 4;
	
	/**
	 * java.util.logging.Level.ALL 
	 */
	public static int	 LOG_LEVEL_ID_ALL		= 5;
	
	
	public static int	 LOG_LEVEL_ID_DEFAULT	= LOG_LEVEL_ID_MINIMUM;	
	
	/**
	 * The recognized keywords for log levels
	 */
	public static String[] LOG_LEVEL_KEYWORDS = {
		"NONE",			// NO LOGS
		"MINIMUM",		// LOG ERRORS AND WARNINGS
		"NORMAL",		// + INFORMATION LOG
		"VERBOSE",		// + CONFIGURATION LOGS
		"DEBUG",		// + DEBUG LOG
		"ALL",			// ALL LOGS 
	};	
	
	
	/**
	 * Log levels id range 
	 */
	public static int	 LOG_LEVEL_ID_MIN		= 0;
	public static int	 LOG_LEVEL_ID_MAX		= LOG_LEVEL_KEYWORDS.length;
	
	
	/**
	 * The directory where all logs will be written;
	 */
	private String logsDir;
	
	/**
	 * The default log file prefix
	 */
	private static String LOGFILE_PREFIX = "";

	/**
	 * The extension of the log files.
	 */
	private static String LOGFILE_EXTENSION = ".log";
	
	/**
	 * The maximum log file size set to 1MB
	 */
	private static int LOGFILE_SIZE = 1024 * 1024;
	
	/**
	 * The maximum number of log files stored after rotation
	 */
	private static int LOGFILE_COUNT = 2;

	
	/**
	 * The class name of the file handler;
	 */
	private static String LOGFILE_HANDLER_CLASS_NAME = "java.util.logging.FileHandler";

	/**
	 * A map of packages names and their loggers
	 */
	private Map<String, Logger> packagesLoggers;
	
	
	/**
	 * Return the log level id for a given log level name; any invalid or empty name 
	 * will be translated into the lowest log level id
	 * @param amLogLevelName a string containing the am log level name as listed in the LOG_LEVEL_KEYWORDS array 
	 * @return the am log level id number
	 */
	public static int getAMLogLevelId(String amLogLevelName){
		if (amLogLevelName != null){
			for(int i = 0; i < LOG_LEVEL_KEYWORDS.length; i++){
				if (LOG_LEVEL_KEYWORDS[i].equals(amLogLevelName)){
					return i;
				}
			}
		}
		System.err.println("Unknown AM log level name [" + amLogLevelName + "]! Set to minimum!");
		return LOG_LEVEL_ID_MIN; 
	}

	/**
	 * Make sure the id value is in the range of [LOG_LEVEL_ID_MIN, LOG_LEVEL_ID_MAX]
	 * @param id the id to check
	 * @return the corrected id value
	 */
	public static int validateAMLogLevelId(int id){
		if (id < LOG_LEVEL_ID_MIN){
			System.err.println("Invalid AM log level id [" + id + "]! " +
					"It has to be in [" + LOG_LEVEL_ID_MIN + "," + LOG_LEVEL_ID_MAX + "]! " +
					"Set to minimum!");
			return LOG_LEVEL_ID_MIN;
		} else if (id > LOG_LEVEL_ID_MAX) {
			System.err.println("Invalid AM log level id [" + id + "]! " +
					"It has to be in [" + LOG_LEVEL_ID_MIN + "," + LOG_LEVEL_ID_MAX + "]! " +
					"Set to maximum!");
			return LOG_LEVEL_ID_MAX;
		} else {
			return id;
		}
	}
	
	/**
	 * Map the AM log levels to java.util.logging.Level
	 * @param amLogLevelId
	 * @return
	 */
	private static Level getLogLevel(int amLogLevelId){
		int amId = validateAMLogLevelId(amLogLevelId);
		
		if (amId == LOG_LEVEL_ID_NONE){
			return Level.OFF;
			
		} else if (amId == LOG_LEVEL_ID_MINIMUM) {
			return Level.WARNING;
			
		} else if (amId == LOG_LEVEL_ID_NORMAL) {
			return Level.INFO;
			
		} else if (amId == LOG_LEVEL_ID_VERBOSE) {
			return Level.CONFIG;
			
		} else if (amId == LOG_LEVEL_ID_DEBUG) {
			return Level.FINE;
			
		} else //if (amId == LOG_LEVEL_ID_ALL) 
		{			
			return Level.ALL;
		}
	}
	
	/**
	 * Map the java.util.logging.Level to AM log levels 
	 * @param amLogLevelId
	 * @return
	 */
	private static int getAMLogLevelId(Level logLevel){
		int amLogLevelId;
		
		for (amLogLevelId = LOG_LEVEL_ID_MIN; amLogLevelId < LOG_LEVEL_ID_MAX; amLogLevelId++){
			Level itLogLevel = getLogLevel(amLogLevelId);
			if (logLevel.intValue() >= itLogLevel.intValue()){
				return amLogLevelId;
			}
		}
		
		System.err.println("Could not match log level " + logLevel.getName() + " to AM log level! Set to minimum!");
		return LOG_LEVEL_ID_MIN;
	}
	
	/**
	 * Get the am log level name knowing its id. The id can be out of range because it will be normalized.
	 * @param amLogLevelId the log level id
	 * @return the name of the am log level that corresponds to the given id
	 */
	public static String getAMLogLevelName(int amLogLevelId){
		return LOG_LEVEL_KEYWORDS[validateAMLogLevelId(amLogLevelId)]; 
	}
	
	/**
	 * My own log formatter
	 */
	private static java.util.logging.Formatter logFormatter = new java.util.logging.Formatter() {
		public String format(LogRecord record){
			StringBuffer out = new StringBuffer();
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd HH:mm:ss");

			// add date
			out.append("[");
			out.append(dateFormat.format(new Date(record.getMillis())));
			out.append("]");
			
			// add class name and method
			out.append("[");
			out.append(record.getSourceClassName() + ":" + record.getSourceMethodName());
			out.append("]");
			
			// add the log level
			out.append(" ");
			out.append(record.getLevel().getName());
			out.append(": ");

			// add the message
			out.append(record.getMessage());
			
			// add new line
			out.append("\n");
			
			return out.toString();
		}
	};
	
	/**
	 * Build a log handler based on its class name
	 * @param packageName package name
	 * @param logLevel  log level
	 * @param logHandlerClass the log handler class name
	 * @return new Handler object
	 */
	private Handler buildLogHandler(
			String packageName,
			Level logLevel,
			String logHandlerClass){
		Handler h = null;
		
		if (LOGFILE_HANDLER_CLASS_NAME.equals(logHandlerClass)){
			
			String logFileName = constructLogFileName(packageName, logLevel);
			try{
				h = new FileHandler(
						logFileName,
						LOGFILE_SIZE,
						LOGFILE_COUNT);
			} catch (SecurityException e) {
				System.err.println("Could not create log file! Security exception: " + e.getMessage());
				return h;
			} catch (IOException e) {
				System.err.println("Could not create log file! IO exception: " + e.getMessage());
				return h;
			}
			
			h.setLevel(logLevel);
			
			System.out.println("LOGGER: " + 
					"Enabled for package [" + packageName + "] " + 
					((logLevel != null) ? "for level [" + logLevel.getName() + "] " : "") +
					"in file [" + logFileName + "]");
		}
		return h;
	}
	
	/**
	 * Constructor that will put all the log files in the current directory
	 */
	public AMLogger() throws AMException{
		this(null);
	}
	
	/**
	 * Constructor that takes as argument the directory where the logs will be stored.
	 * @param logsDir directory name; could be a relative path to the current directory or absolute path
	 */
	public AMLogger(String logsDir) throws AMException{
		
		// initialize the logs directory
		this.logsDir = AMCommon.makeFileNameSystemCompatible(logsDir);
		if (this.logsDir != null){
			// create the logs directory if needed
			if (!AMCommon.createDirectories(logsDir)){
				throw new AMException("Could not create directories [" + logsDir + "] for the log files!");
			}
			System.out.println("AM logger initialized to write the log files in [" + this.logsDir + "]");
		}
		
		packagesLoggers = new Hashtable<String, Logger>();
	}
	
	/**
	 * Set the logger formatter to the given logger and its parents 
	 * @param packageLogger
	 */
	private void setLoggerFormatterToPackageAndParent(Logger packageLogger, Formatter formatter){
		
		if (packageLogger == null){
			return;
		}
		
		//System.out.println("LOGGER: Set AM logger formatter for package: [" + packageLogger.getName() + "]");

		// set the formatter for the given package
		for(Handler h: packageLogger.getHandlers()){
			h.setFormatter(formatter);
		}
		
		// set the formatter for the parrent 
		setLoggerFormatterToPackageAndParent(packageLogger.getParent(), formatter);
	}
	
	/**
	 * Set the custom Am logger formatter to all handlers in the known packages
	 */
	public void setAMFormatterToAllLoggers(){		
		for(String packageName: packagesLoggers.keySet()){
			setLoggerFormatterToPackageAndParent(packagesLoggers.get(packageName), logFormatter);
		}
	}

	/**
	 * Enable the logging to log files for the given package name
	 * @param packageName package name
	 * @param logHandlerClass the class name of the log handler to be disabled
	 * @param logLevel log level. if null, the log level information is ignored
	 *  (if new logger created, then the parent log level is considered)
	 *  (if the log level is OFF, then no new handler is created!)
	 * @return true if the logging has been enabled, false otherwise
	 */
	private boolean enableLogging(String packageName, String logHandlerClass, Level logLevel){
		
		if (packageName == null)
			return false;
		
		Logger logger = packagesLoggers.get(packageName);
		if (logger == null){
			logger = Logger.getLogger(packageName);
			if (logger == null){
				// the package doesn't have any logger defined
				System.err.println("Could not retrieve logger for the package [" + packageName + "]");
				return false;
			}
			
			// get ALL the log messages from the package;
			// however, they could be filtered after that by the handlers
			logger.setLevel(Level.ALL);
			
			// don't write to the parent log file
			logger.setUseParentHandlers(false);
			
			//add the logger
			packagesLoggers.put(packageName, logger);
		}
		
		Handler handler = null;

		// check if there is already a file handler
		Handler[] handlers = logger.getHandlers();

		for(Handler h: handlers){
			// check if the log handler class is the one required
			if (logHandlerClass != null && ! logHandlerClass.equals(h.getClass().getName())){
				continue;
			}
			
			// check also if it is the same level
			if (logLevel != null && ! h.getLevel().equals(logLevel)) {
				// the log level is not the same, so skip this handler
				continue;
			}
			
			// a file handler for the given package and log level has been found
			//System.out.println("LOGGER: Found handler for package [" + packageName + "]: level=" + logLevel.getName());
			handler = h;
			break;
		}
		
		if (handler == null && logLevel.equals(Level.OFF)){
			// handler not found, but we don't need because the requested level is OFF 
			return true;
		}
		
			// build a new  handler
		if (handler == null){
			Handler newHandler = buildLogHandler(
						packageName,
						logLevel,
						logHandlerClass);
			
			if (newHandler == null){
				return false;
			}
			
			// set the formatter
			newHandler.setFormatter(logFormatter);
			
			// add the handler to the logger
			logger.addHandler(newHandler);
			
			handler = newHandler;
		}
		
		// set the log level to the handler
		handler.setLevel(logLevel);
		// handler.setLevel(Level.ALL);    // to log everything
		
		return true;
	}

	/**
	 * @param logLevel log level. if null, the log level information is ignored
	 *  (if new logger created, then the parent log level is considered)
	 */
	/**
	 * Enable the logging to log files for the given package name
	 * @param packageName package name
	 * @param amLogLevelId the AM log level id (one of the LOG_LEVEL_ID static values)
	 * @return true if the logging has been enabled, false otherwise
	 */
	public boolean enableLoggingToFile(String packageName, int amLogLevelId){
		return enableLogging(packageName, LOGFILE_HANDLER_CLASS_NAME, getLogLevel(amLogLevelId));
	}
	
        /** 
         * Enable the logging to Console OutStream for the given package name
         * @param packageName package name
         * @param amLogLevelId the AM log level id (one of the LOG_LEVEL_ID static values)
         * @return true if the logging has been enabled, false otherwise
       */

        public boolean enableLoggingToConsole(String packageName, int amLogLevelId){
                return enableLoggingConsole(packageName, getLogLevel(amLogLevelId));
        }

        /**
         * Enable the logging to Console OutStream for the given package name
         * @param packageName package name
         * @param logLevel log level. if null, the log level information is ignored
         *  (if new logger created, then the parent log level is considered)
         *  (if the log level is OFF, then no new handler is created!)
         * @return true if the logging has been enabled, false otherwise
         */

        private boolean enableLoggingConsole(String packageName, Level logLevel){

                Logger logger = Logger.getLogger(packageName);
                if (logger == null){
                     // the package doesn't have any logger defined
                     System.err.println("Could not retrieve logger for the package [" + packageName + "]");
                     return false;
                }

                // get ALL the log messages from the package;
                // however, they could be filtered after that by the handlers
                logger.setLevel(Level.ALL);

               // don't write to the parent log file
                logger.setUseParentHandlers(false);

               // set the formatter and add a ConsoleHandler
                System.out.println("AMLogger: Adding a ConsoleHandler for package [" + logger.getName() + "] for logging on standard output stream");
                ConsoleHandler consoleHandler = new ConsoleHandler();
//                consoleHandler.setLevel(Level.ALL);        // to log everything
                consoleHandler.setLevel(logLevel);
                consoleHandler.setFormatter(logFormatter);
                logger.addHandler(consoleHandler);

                return true;
        }


	/**
	 * Enable the logging to log files for the given package name to the default log level
	 * @param packageName package name
	 * @return true if the logging has been enabled, false otherwise
	 */
	public boolean enableLoggingToFile(String packageName){
		return enableLoggingToFile(packageName, LOG_LEVEL_ID_DEFAULT);
	}
	
	/**
	 * Disable the logging to a given logg handler type for the given package name
	 * @param packageName package name
	 * @param logHandlerClass the class name of the log handler to be disabled
	 * @param logLevel log level. if null all the loggers are considered no matter the log level defined for them
	 * @return true if the logging has been enabled, false otherwise
	 */
	private boolean disableLogging(String packageName, String logHandlerClass, Level logLevel){
		
		if (packageName == null)
			return false;
		
		Logger logger = packagesLoggers.get(packageName);
		if (logger == null){
			System.err.println("Could not find the logger for the package [" + packageName + "]");
			return false;
		}
			
		// check if there is already a file handler
		Handler[] handlers = logger.getHandlers();
		for(Handler h: handlers){
			// check if the log handler class is the one required
			if (logHandlerClass != null && ! logHandlerClass.equals(h.getClass().getName())){
				continue;
			}
			
			// check also if it is the same level
			if (logLevel != null && ! h.getLevel().equals(logLevel)) {
				// the log level is not the same, so skip this handler
				continue;
			}
			
			// remove the handler
			logger.removeHandler(h);
			h.close();
				
		}
		
		System.out.println("LOGGER: " + 
				"Disabled for package [" + packageName + "] " + 
				((logLevel != null) ? "for level [" + logLevel.getName() + "]" : ""));
		
		return true;
	}

	/**
	 * Disable the logging to log files for the given package name
	 * @param packageName package name
	 * @param logLevel log level. if null all the loggers are considered no matter the log level defined for them
	 * @return true if the logging has been enabled, false otherwise
	 */
	public boolean disableLoggingToFile(String packageName, int amLogLevelId){
		return disableLogging(packageName, LOGFILE_HANDLER_CLASS_NAME, getLogLevel(amLogLevelId));
	}
	
	/**
	 * Disable the logging to log files for the given package name
	 * @param packageName package name
	 * @return true if the logging has been enabled, false otherwise
	 */
	public boolean disableLoggingToFile(String packageName){
		return disableLogging(packageName, LOGFILE_HANDLER_CLASS_NAME, null);
	}

	/**
	 * Disable the logging for all the packages registered with enableLoggingToFile
	 * @return true if all the packages loggers have been successfully deleted
	 */
	public boolean disableLoggingToFile(){
		boolean result = true;
		
		for(String packageName: packagesLoggers.keySet()){
			result &= disableLoggingToFile(packageName);
		}
		
		return result;
		
	}
	
	/**
	 * Remove all the logging facilities for a package
	 * @param packageName
	 * @return
	 */
	public boolean disableAllLogging(String packageName){
		
		// add the package temporarily to the package list if not already there
		if (!packagesLoggers.containsKey(packageName)){
			packagesLoggers.put(packageName, Logger.getLogger(packageName));
		}
		
		// disable the logging
		boolean result = disableLogging(packageName, null, null);
		
		// delete the package from the loggers list
		if (result){
			packagesLoggers.remove(packageName);
		}
		return result;
	}
	
	/**
	 * Build a log file name using a prefix, the package name and the log level
	 * @param prefix a prefix
	 * @param packageName the package name
	 * @param logLevel the loglevel
	 * @return the filename
	 */
	private String constructLogFileName(String prefix, String packageName, Level logLevel){
		String filename = "";
		
		if (logsDir != null){
			filename = logsDir + AMCommon.getFileSeparator() + filename;
		}

		// add the prefix
		if (prefix != null){
			filename += prefix;
		}

		// add the package name
		if (packageName != null){
			filename += packageName; 
		}
		
		// add the loglevel
		if (logLevel != null){
			filename += "-" + getAMLogLevelName(getAMLogLevelId(logLevel));
		}
		
		// add the extension
		filename += LOGFILE_EXTENSION;
		
		return filename;
	}
	
	/**
	 * Build a log file name using the package name and the log level
	 * @param packageName the package name
	 * @param logLevel the loglevel
	 * @return the filename
	 */
	private String constructLogFileName(String packageName, Level logLevel){
		return constructLogFileName(LOGFILE_PREFIX, packageName, logLevel);
	
	}
}
