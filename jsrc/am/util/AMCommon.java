package am.util;

import java.io.File;
import java.net.URI;

public abstract class AMCommon {
	
    /**
     * Create another URI object from more URI values which will separted by ':'.
     * The method accepts a variable number of parameters
     * @param separator the separator character  
     * @param URIvalues an array of URIs
     * @return the concatenation of URIs or null if no or only null URIs have been provided
     */
    public static URI concatenateURI(char separator, URI... URIvalues){
    	String sURI = null;
    	for (int i = 0; i < URIvalues.length; i++){
    		if (URIvalues[i] != null && URIvalues[i].toString().length() > 0){
    			sURI = (sURI == null) ? "" : sURI + String.valueOf(separator); 
    			sURI += URIvalues[i];
    		}
    	}
		return (sURI != null) ? URI.create(sURI) : null;
    }	

    /**
	 * Create the directories for the given path. The path has to have at least one file separator '/'
	 * otherqise nothing will happen.
	 * @param path storage path
	 * @return true if the directories have been created
	 */
	public static boolean createDirectories(String path) {
		
		if (path.lastIndexOf(getFileSeparator()) < 0){
			return false;
		}
		
		String dir = path.substring(0, path.lastIndexOf(getFileSeparator()));
		File fdir = new File(dir);
		if (! fdir.isDirectory()) {
			return fdir.mkdirs();
		}
    	return true;
    }
	
	/**
	 * Get the path separator character for the current operating system
	 * @return a character
	 */
	public static char getPathSeparator(){
		String s = System.getProperty("path.separator");
		if (s == null || s.length()<=0 ) {
			System.err.println("Could not retrieve path separator property!");
			return 0;
		} else {
			return s.charAt(0);
		}
	}
	
	/**
	 * Get the file separator character for the current operating system
	 * @return a character that is '/' in Unix and '\' in Windows
	 */
	public static char getFileSeparator(){
		String s = System.getProperty("file.separator");
		if (s == null || s.length()<=0 ) {
			System.err.println("Could not retrieve file separator property!");
			return 0;
		} else {
			return s.charAt(0);
		}
	}

	/**
	 * Make sure the file name has the separator character compatible with the current
	 * operating system.
	 * @param filename the filename to check
	 * @return the filename with the correct separator characters
	 */
	public static String makeFileNameSystemCompatible(String filename){
		char c = getFileSeparator();
		if (filename != null && filename.length() > 0 && c != 0 ){
			// replace Unix specific file separator
			filename.replace('/', c);
			// replace Windows specific file separator
			filename.replace('\\', c);
		}
		return filename;
	}
}
