package am.server.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import am.util.AMException;

/**
 * Class to parse argument lists.
 * @author mleahu
 *
 */
public class CommandLineArguments {

	private static final Logger logger =
        Logger.getLogger(CommandLineArguments.class.getName());
	
	private final static String MANDATORY_START = "<";
	private final static String MANDATORY_END = ">";
	private final static String NON_MANDATORY_START = "[";
	private final static String NON_MANDATORY_END = "]";
	
	// HashMaps to map options to their values, descriptions and flags

	//Mapping of the options to their values, initially the default value
	private HashMap<String, Object> opt2value;
	private HashMap<String, Object> opt2defaultValue;

	//the values can be String, Integer or Boolean
	private HashMap<String, Object> opt2type;

	//Mapping of the options to their description
	private HashMap<String, String> opt2description;
	
	//Mapping of the options to their flags (String objects with the flag value without '-'); 
	//if a variable takes a value of a positional argument with no flag,
	// then the Object associated with it is an Integer 
	private HashMap<String, Object> opt2flag;
	// the reverse mapping
	private HashMap<Object, String> flag2opt;
	
	//The number of positional parameters defined so far
	private ArrayList<String> positionalArguments;

	// specifies if an option is mandatory to be initialized
	private HashMap<String, Boolean> opt2mandatory;
	
	/**
	 * Creates an object CommanLineInterface
	 */
	public CommandLineArguments(){
		opt2value 		= new HashMap<String, Object>();
		opt2defaultValue= new HashMap<String, Object>();
		opt2type 		= new HashMap<String, Object>();
		opt2description	= new HashMap<String, String>();
		opt2flag		= new HashMap<String, Object>();
		flag2opt		= new HashMap<Object, String>();
		positionalArguments = new ArrayList<String>();
		opt2mandatory	= new HashMap<String, Boolean>();
		logger.setLevel(Level.SEVERE);
	}
	
	
	
	/**
	 * Adds an option to be initialized from the arguments list
	 * @param option the option name
	 * @param type the value type
	 * @param description the description
	 * @param flag the command line flag to use
	 * @param defaultValue the default value
	 * @param mandatory true if the option is mandatory, false if not
	 * @throws AMException when the arguments are wrong
	 */
	private void addOption(
			String option,
			Object type,
			String description,
			Object flag,
			Object defaultValue,
			boolean mandatory) throws AMException {
		
		// the option must be non empty
		if (option == null || option.length() == 0){
			throw new AMException("Option name must be non empty!", AMException.WRONG_ARGUMENTS);
		}
		
		if (flag.getClass().getSimpleName().equals("String")){
			flag = "-" + flag;
		}
		
		//check if the option has been already registered or if the flag has been registered
		if (opt2value.containsKey(option)){
			throw new AMException("Option name already defined!", AMException.WRONG_ARGUMENTS);
		}
			
		if (flag2opt.containsKey(flag)){
			throw new AMException("Flag [" + flag + "] already used!", AMException.WRONG_ARGUMENTS);
		}
		
		// initialize the opt2* hash maps
		opt2value.put(option, defaultValue);
		opt2defaultValue.put(option, defaultValue);
		opt2type.put(option, type);
		opt2description.put(option, description);
		opt2flag.put(option, flag);
		flag2opt.put(flag, option);
		opt2mandatory.put(option, mandatory);
		logger.info("Option [" + option + "] added to the arguments list with "+
				"default value[" + defaultValue + "] " +
				"type[" + type.getClass().getSimpleName() + "] " +
				"description[" + description + "] " +
				"flag[" + flag.toString() + "] " +
				"mandatory[" + mandatory + "]");
	}
	
	
	/**
	 * Adds a String option to be initialized from the arguments list
	 * @param opt the option name
	 * @param description the description
	 * @param flag the command line flag to use; if null, then is a positional parameter
	 * @param defaultValue the default value
	 * @param mandatory true if the option is mandatory, false if not
	 * @throws AMException when the arguments are wrong
	 */
	public void addStringOption(
			String opt, 
			String description, 
			String flag,
			String defaultValue,
			boolean mandatory) throws AMException {
		
		Object flagToStore = flag;
		if (flagToStore == null){
			positionalArguments.add(opt);
			flagToStore = new Integer(positionalArguments.size());
		}
		addOption(opt, new String(), description, flagToStore, defaultValue, mandatory);
	}
	
	/**
	 * Adds an Integer option to be initialized from the arguments list
	 * @param opt the option name
	 * @param description the description
	 * @param flag the command line flag to use; if null, then is a positional parameter
	 * @param defaultValue the default value
	 * @param mandatory true if the option is mandatory, false if not
	 * @throws AMException when the arguments are wrong
	 */
	public void addIntegerOption(
			String opt, 
			String description, 
			String flag,
			Integer defaultValue,
			boolean mandatory) throws AMException {
		
		Object flagToStore = flag;
		if (flagToStore == null){
			positionalArguments.add(opt);
			flagToStore = new Integer(positionalArguments.size());
		}
		addOption(opt, new Integer(0), description, flagToStore, defaultValue, mandatory);
	}
	
	/**
	 * Adds a Boolean option to be initialized from the arguments list
	 * @param opt the option name
	 * @param description the description
	 * @param flag the command line flag to use; if null, then is a positional parameter
	 * @param defaultValue the default value
	 * @param mandatory true if the option is mandatory, false if not
	 * @throws AMException when the arguments are wrong
	 */
	public void addBooleanOption(
			String opt, 
			String description, 
			String flag,
			boolean defaultValue,
			boolean mandatory) throws AMException {
		
		if (flag == null){
			throw new AMException("Boolean options must have a flag", AMException.WRONG_ARGUMENTS);
		}
		addOption(opt, new Boolean(false), description, flag, new Boolean(defaultValue), mandatory);
	}
	
	/**
	 * Return the String value of an option
	 * @param option the option name
	 * @return the String value
	 * @throws AMException if the variable has not been registered, or has been but with other type
	 */
	public String getStringValue(String option) throws AMException {
		// the option must be non empty
		if (option == null || option.length() == 0){
			throw new AMException("Option name must be non empty!", AMException.WRONG_ARGUMENTS);
		}
		// the option must be previously added
		if ( ! opt2type.containsKey(option)) {
			throw new AMException("Option [" + option + "] not found!", AMException.WRONG_ARGUMENTS);
		}		
		// the option must be a String type
		if ( ! opt2type.get(option).getClass().getSimpleName().equals("String")) {
			throw new AMException("The type of option [" + option + "] is not " +
					"[" + "String" + "] but [" + opt2type.get(option).getClass().getSimpleName() + "]!", AMException.WRONG_ARGUMENTS);
		}
		
		Object value = opt2value.get(option);
		
		if (value == null){
			return null;
		}
		if ( ! value.getClass().getSimpleName().equals("String")) {
			throw new AMException("Internal error: the option value is not [" + "String" + "], " +
					"but [" + value.getClass().getName() + "]!", AMException.WRONG_ARGUMENTS);
		}
		
		return (String)value;
	}
	
	/**
	 * Return the Integer value of an option
	 * @param option the option name
	 * @return the Integer value
	 * @throws AMException if the variable has not been registered, or has been but with other type
	 */
	public Integer getIntegerValue(String option) throws AMException {
		// the option must be non empty
		if (option == null || option.length() == 0){
			throw new AMException("Option name must be non empty!", AMException.WRONG_ARGUMENTS);
		}
		// the option must be previously added
		if ( ! opt2type.containsKey(option)) {
			throw new AMException("Option [" + option + "] not found!", AMException.WRONG_ARGUMENTS);
		}		
		// the option must be an Integer type
		if ( ! opt2type.get(option).getClass().getSimpleName().equals("Integer")) {
			throw new AMException("The type of option [" + option + "] is not " +
					"[" + "Integer" + "] but [" + opt2type.get(option).getClass().getSimpleName() + "]!", AMException.WRONG_ARGUMENTS);
		}
		
		Object value = opt2value.get(option);
		if (value == null){
			return null;
		}
		if ( ! value.getClass().getSimpleName().equals("Integer")) {
			throw new AMException("Internal error: the option value is not [" + "Integer" + "], " +
					"but [" + value.getClass().getName() + "]!", AMException.WRONG_ARGUMENTS);
		}
		
		return (Integer)value;
	}
	
	/**
	 * Return the Boolean value of an option
	 * @param option the option name
	 * @return the boolean value
	 * @throws AMException if the variable has not been registered, or has been but with other type
	 */
	public boolean getBooleanValue(String option) throws AMException {
		// the option must be non empty
		if (option == null || option.length() == 0){
			throw new AMException("Option name must be non empty!", AMException.WRONG_ARGUMENTS);
		}
		// the option must be previously added
		if ( ! opt2type.containsKey(option)) {
			throw new AMException("Option [" + option + "] not found!", AMException.WRONG_ARGUMENTS);
		}		
		// the option must be an Integer type
		if ( ! opt2type.get(option).getClass().getSimpleName().equals("Boolean")) {
			throw new AMException("The type of option [" + option + "] is not " +
					"[" + "Boolean" + "] but [" + opt2type.get(option).getClass().getSimpleName() + "]!", AMException.WRONG_ARGUMENTS);
		}
		
		Object value = opt2value.get(option);
		if (value == null){
			return false;
		}
		if ( ! value.getClass().getSimpleName().equals("Boolean")) {
			throw new AMException("Internal error: the option value is not [" + "Boolean" + "], " +
					"but [" + value.getClass().getName() + "]!", AMException.WRONG_ARGUMENTS);
		}
		
		return ((Boolean)value).booleanValue();
	}
	
	/**
	 * Initialize the values according to the command line arguments
	 * @param args the arguments array
	 * @throws AMException if there is a problem with the arguments or the options have no value
	 */
	public void processArguments(String[] args) throws AMException{
		int argi = 0;
		
		logger.info("Parse the flags");
		// parse the flags with their arguments
		for(argi = 0;argi < args.length; argi++){
			if ( ! args[argi].startsWith("-")){
				// this is not a flag option, but a positional parameter,so exit
				break;
			}
			
			// skip if the flag is not recognized
			if (! flag2opt.containsKey(args[argi])) {
				System.err.println("Ignore unknown command line flag [" + args[argi] +"]");
				continue;
			}
			
			String opt = flag2opt.get(args[argi]);
			String opttype = opt2type.get(opt).getClass().getSimpleName();
			if (opttype.equals("Boolean")){
				// switch the boolean value and go on
				opt2value.put(opt, 
						new Boolean( !(Boolean)opt2value.get(opt) ));
				logger.info("Switch boolean option [" + opt + "] to [" + opt2value.get(opt) + "]");
			} else {
				// the flag has an argument
				if ( ++argi >= args.length) {
					System.out.println("Arguments list exhausted.Could not initialize the option [" + opt + "]");
				} else {
					if (opttype.equals("Integer")){
						// try to convert the option argument to integer
						try {
							Integer newvalue = new Integer(args[argi]);
							opt2value.put(opt, newvalue);
							logger.info("Integer option [" + opt + "] set to [" + opt2value.get(opt) + "]");
						}catch(NumberFormatException e) {
							throw new AMException(
									"Could not set value for option [" + opt + 
									"] because of number format exception:" + e.getMessage(),
									AMException.OPERATION_ERROR);
						}
					} else if (opttype.equals("String")) {
						opt2value.put(opt, args[argi]);
						logger.info("String option [" + opt + "] set to [" + opt2value.get(opt) + "]");
					} else {
						throw new AMException(
								"Unknown type [" + opttype +"] of option [" + opt + "]",
								AMException.OPERATION_ERROR);
					}
				}
			}
			
		}
		
		logger.info("Parse the positional parameters");
		for(int posi = 0;argi < args.length && posi < positionalArguments.size(); argi++, posi++){
			String opt = positionalArguments.get(posi);
			String opttype = opt2type.get(opt).getClass().getSimpleName();
			
			if (opttype.equals("Integer")){
				// try to convert the option argument to integer
				try {
					Integer newvalue = new Integer(args[argi]);
					opt2value.put(opt, newvalue);
					logger.info("Integer option [" + opt + "] set to [" + opt2value.get(opt) + "]");
				}catch(NumberFormatException e) {
					throw new AMException(
							"Could not set value for option [" + opt + 
							"] because of number format exception:" + e.getMessage(),
							AMException.OPERATION_ERROR);
				}
			} else if (opttype.equals("String")) {
				opt2value.put(opt, args[argi]);
				logger.info("String option [" + opt + "] set to [" + opt2value.get(opt) + "]");
			} else {
				throw new AMException(
						"Unknown type [" + opttype +"] of option [" + opt + "]",
						AMException.OPERATION_ERROR);
			}
		}
		
		
		// check if all mandatory options are initialized
		for(String opt:opt2mandatory.keySet()){
			// set the default value if none set so far
			if (opt2mandatory.get(opt).booleanValue()){
				if (opt2value.get(opt) == null){
					throw new AMException(
							"Mandatory option [" + opt + "] not initialized!",
							AMException.OPERATION_ERROR);
				}
			}
		}
	}
	
	/**
	 * Prepares a string with all the options registered so far
	 * @return String 
	 */
	public String getCommandLineDescription(){
		String cli = "";
		for(String opt:opt2flag.keySet()) {
			if ( ! opt2flag.get(opt).getClass().getSimpleName().equals("String")){
				continue;
			}
			boolean mandatory = opt2mandatory.get(opt).booleanValue();
			cli += (mandatory)? MANDATORY_START : NON_MANDATORY_START;
			cli += opt2flag.get(opt);
			if ( ! opt2type.get(opt).getClass().getSimpleName().equals("Boolean")) {
				cli += " " + opt;
			}
			cli += (mandatory)? MANDATORY_END : NON_MANDATORY_END;
			cli += " ";
		}
		
		for(String posopt: positionalArguments){
			if (opt2mandatory.get(posopt).booleanValue()){
				cli += MANDATORY_START + posopt + MANDATORY_END + " ";
			} else {
				cli += NON_MANDATORY_START + posopt + NON_MANDATORY_END + " ";
			}
		}
		return cli;
	};
	
	/**
	 * Prepares a string with the description of all the options registered up to now
	 * @return string
	 */
	public String getOptionsDescription(){
		String optsDesc = "";
		optsDesc += "Flags and options:\n";
		for(String opt:opt2flag.keySet()) {
			if ( ! opt2flag.get(opt).getClass().getSimpleName().equals("String")){
				continue;
			}
			optsDesc += "     " + String.format("%-8s",opt2flag.get(opt)) + opt2description.get(opt) + ".";
			Object value = opt2defaultValue.get(opt);
			if (value != null) {
				optsDesc += " Default [" + value + "].";
			}
			optsDesc += "\n";
		}
		
		if (positionalArguments.size() == 0) {
			return optsDesc;
		}
		
		optsDesc += "Positional arguments:\n";
		for(String posopt: positionalArguments){
			optsDesc += "     " + String.format("%-10s",posopt) + opt2description.get(posopt) + ".";
			Object value = opt2defaultValue.get(posopt);
			if (value != null) {
				optsDesc += " Default [" + value + "].";
			}
			optsDesc += "\n";
		}
		return optsDesc;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return 	"Command line arguments: " + getCommandLineDescription() + "\n" +
				getOptionsDescription();
	}
	
//	public static void main(String[] args){
//		try {
//			CommandLineInterface cli = new CommandLineInterface();
//			cli.addStringOption("string1", "string1 description", "s", "string1 defaultValue", false);
//			cli.addStringOption("string2", "string2 description", "s2", "string2 defaultValue", true);
//			cli.addStringOption("string3", "string3 description", null, "string3 defaultValue", true);
//			cli.addIntegerOption("integer1", "integer1 description", "i", new Integer(1), true);
//			cli.addIntegerOption("integer2", "integer2 description", "i2", null, true);
//			cli.addIntegerOption("integer3", "integer3 description", null, new Integer(3), false);
//			cli.addBooleanOption("boolean1", "boolean1 description", "b", false, false);
//			cli.addBooleanOption("boolean2", "boolean2 description", "b2", true, true);
//			
//			System.out.println(cli);
//			
//			
//			System.out.println("Value of [" + "string1" + "] is " + cli.getStringValue("string1"));
//			System.out.println("Value of [" + "string2" + "] is " + cli.getStringValue("string2"));
//			System.out.println("Value of [" + "string3" + "] is " + cli.getStringValue("string3"));
//			System.out.println("Value of [" + "integer1" + "] is " + cli.getIntegerValue("integer1"));
//			System.out.println("Value of [" + "integer2" + "] is " + cli.getIntegerValue("integer2"));
//			System.out.println("Value of [" + "integer3" + "] is " + cli.getIntegerValue("integer3"));
//			System.out.println("Value of [" + "boolean1" + "] is " + cli.getBooleanValue("boolean1"));
//			System.out.println("Value of [" + "boolean2" + "] is " + cli.getBooleanValue("boolean2"));
//			
//			
//			cli.processArguments(args);
//			
//			System.out.println("Value of [" + "string1" + "] is " + cli.getStringValue("string1"));
//			System.out.println("Value of [" + "string2" + "] is " + cli.getStringValue("string2"));
//			System.out.println("Value of [" + "string3" + "] is " + cli.getStringValue("string3"));
//			System.out.println("Value of [" + "integer1" + "] is " + cli.getIntegerValue("integer1"));
//			System.out.println("Value of [" + "integer2" + "] is " + cli.getIntegerValue("integer2"));
//			System.out.println("Value of [" + "integer3" + "] is " + cli.getIntegerValue("integer3"));
//			System.out.println("Value of [" + "boolean1" + "] is " + cli.getBooleanValue("boolean1"));
//			System.out.println("Value of [" + "boolean2" + "] is " + cli.getBooleanValue("boolean2"));
//		} catch (AMException e) {
//			logger.warning("Arguments parsing exception:" + e.getMessage());
//		}
//	}	
	
}
