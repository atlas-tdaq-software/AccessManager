package am.server;

import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import am.pap.storage.PolicyStorageToFile;
import am.pdp.PDPFactory;
import am.pdp.PolicyDecisionPoint;
import am.pdp.PolicyDecisionPointBUSY;
import am.pip.RBACPIP;
import am.pip.db.DBFactory;
import am.server.NIOReactor.PerformanceMonitor;
import am.server.NIOReactor.Reactor;
import am.server.authlogger.AuthorizationLogger;
import am.server.authlogger.LoggerHandlerToFile;
import am.server.control.AccessManagerServerController;
import am.server.control.MessageHandler;
import am.server.control.MessageHandlerCacheInvalidation;
import am.server.control.MessageHandlerGenericString;
import am.server.control.MessageHandlerSTART_TIME;
import am.server.control.MessageHandlerUP_TIME;
import am.server.statistics.JVMMonitoring;
import am.server.statistics.StatisticReporter;
import am.server.statistics.StatisticsCollector;
import am.server.util.CommandLineArguments;
import am.util.AMException;

//public class AccessManagerServer implements Observer, MessageHandler {
public class AccessManagerServer implements MessageHandler {

	private static final String VERSION = "$Revision$$Date$";
	
	private static final Logger logger =
        Logger.getLogger(AccessManagerServer.class.getName());

    /**
     * The shutdown hook
     */
    private Thread shutdownHook = null;

    /**
     * Shutdown hook to stop AM server on a signal.
     */
    private final class ShutdownHook extends Thread {
    	private AccessManagerServer amServer = null;
    	
        ShutdownHook(AccessManagerServer server) {
        	amServer = server;
        }
        
        @Override
        public void run() {
			logger.finer("stop " + amServer);
        	amServer.stop();
        }
    }


	/**
	 * The Access Manager Server identifier
	 */
	private String id;
	
	private String localIPAddress;
	private String localHostName;
	
	/**
	 * The server configuration object
	 */
	private AccessManagerServerConfig  amSrvCfg;
	
	/**
	 * The threads pool
	 */
	private ExecutorService threadPool;

	/**
	 * The request receiver thread object;
	 */
	private Reactor reactor;

    /**
     * The counters collector object for all counters.
     */
    private StatisticsCollector statisticsCollector;

    /**
     * The controller for the datagram messages sent by clients
     */
    private AccessManagerServerController amServerController;
    
	/**
	 * The performance monitor object.
	 */
	private PerformanceMonitor performanceMonitor;
	
    /**
     * The authorization logger object to be used during the processing of the client request
     */
    private AuthorizationLogger authorizationLogger;
	
    /**
	 * The remote controller message to stop the server 
	 */
	private static String STOP_SERVER_REMOTE_CONTROL = "SERVER_STOP";
	
	/**
	 * The default constructor;
	 */
	public AccessManagerServer(String id, AccessManagerServerConfig amSrvCfg){
		this.id = id;
		this.localIPAddress = "";
		this.localHostName = "";
		this.amSrvCfg = amSrvCfg;
		this.reactor = null;
		this.statisticsCollector = null;
		this.threadPool = null;
		this.authorizationLogger = null;
	}
	
	/**
	 * Setup the server
	 * 
	 */
	public void setup(){

		
		try {
			this.localIPAddress = InetAddress.getLocalHost().getHostAddress();
			this.localHostName = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			logger.severe(this + "could not be setup! The error is below:");
			logger.severe(e.getMessage());
			System.err.println(this + "could not be setup!");
			System.err.println(e.getMessage());
			System.exit(1);
		}
		
		logger.info("Setting up the " + this + " on " + this.localHostName + "(" + this.localIPAddress + ")");
		System.out.println("Setting up the " + this + " on " + this.localHostName + "(" + this.localIPAddress + ")");
		
		Thread.currentThread().setName(this.id);
		
		try {

			logger.info(amSrvCfg.toString());
			System.out.println(amSrvCfg.toString());
			
			if (amSrvCfg.areServerStatisticsEnabledVerbose() || amSrvCfg.areServerStatisticsSilent()){
	            logger.info("Statistics enabled!");
	            statisticsCollector = new StatisticsCollector();
	            statisticsCollector.setSilentMode(amSrvCfg.areServerStatisticsSilent());
	        } else {
	            logger.info("Statistics disabled!");
	        }
			
			authorizationLogger = new AuthorizationLogger(
					new LoggerHandlerToFile(amSrvCfg.getLogsDir()),
					amSrvCfg.getServerProcessingThreads());
			if (amSrvCfg.isServerAuthorizationLoggerEnabled()){
				String msg = "Enable authorization logger...";
				authorizationLogger.enable();
				if (amSrvCfg.isServerAuthorizationLoggerMinimal()){
					authorizationLogger.minimal();
					msg = "Enable a minimal authorization logger...";
				}
	            logger.info(msg);
	            System.out.println(msg);
	        } else {
	            logger.info("Disable authorization logger...");
	            System.out.println("Disable authorization logger...");
	            authorizationLogger.disable();
	        }
			
			// get the LDAP parameters
			String[] ldapParameters = new String[2];
			ldapParameters[0] = amSrvCfg.getLDAPHost();
			ldapParameters[1] = amSrvCfg.getLDAPBaseDN();
			
			// initialize the RBAC specific DB object
			RBACPIP pip = new RBACPIP(
					DBFactory.getDB(DBFactory.DB_TYPE_LDAP, ldapParameters));
			
			List<URI> RPSs = PolicyStorageToFile.getPolicyIdsFromStoragePath(
					amSrvCfg.getPoliciesPath(),"rps", false);
			
			// prepare the PDP factory object
			PDPFactory pdpFactory = new PDPFactory();
			PolicyDecisionPoint mainPDP = 
				new PolicyDecisionPoint(
						amSrvCfg.getPoliciesPath(), 
						RPSs, 
						pip, 
						amSrvCfg.getSecondaryServers());
			
			pdpFactory.registerHandler(
					PDPFactory.HANDLER_TO_PROCESS_REQUEST, 
					mainPDP);
			
			pdpFactory.registerHandler(
					PDPFactory.HANDLER_TO_RESPOND_BUSY, 
					new PolicyDecisionPointBUSY(amSrvCfg.getSecondaryServers()));
			
			threadPool = Executors.newFixedThreadPool(amSrvCfg.getServerProcessingThreads());
			
			
			
			// initialize the controller and register the message handlers
			amServerController = new AccessManagerServerController();
			amServerController.registerMessageHandler(this);
			amServerController.registerMessageHandler(new MessageHandlerSTART_TIME());
			amServerController.registerMessageHandler(new MessageHandlerUP_TIME());
			amServerController.registerMessageHandler(
					new MessageHandlerCacheInvalidation("LDAP_UPDATED", mainPDP.getPIPCacheUser()));
			amServerController.registerMessageHandler(
					new MessageHandlerCacheInvalidation("PAP_UPDATED", mainPDP.getPAPCacheUser()));
			amServerController.registerMessageHandler(
					new MessageHandlerGenericString("SERVER_ID", this.id));
			amServerController.registerMessageHandler(
					new MessageHandlerGenericString("SECONDARY_SERVERS", amSrvCfg.getSecondaryServers()));
						
			performanceMonitor = new PerformanceMonitor(
										statisticsCollector, 
										amServerController,
										amSrvCfg.getServerSaturationFactor());
			
			logger.info("Overload detection enabled:" + performanceMonitor.isOverloadDetectionEnabled());
			
			this.reactor = new Reactor(
					amSrvCfg.getServerPortNumber(),
					amSrvCfg.getServerSocketBacklog(),
					threadPool,
					pdpFactory,
					performanceMonitor,
					amServerController,
					authorizationLogger);

			// register in the end the jvm monitoring object
			if (this.statisticsCollector != null) {
	            StatisticReporter jvmReporter = new JVMMonitoring();
	            this.statisticsCollector.registerReporter(jvmReporter);				
			}
			
			logger.info("Server language:" + amServerController);
			
		} catch (AMException e) {
			logger.severe(this + "could not be setup! The error is below:");
			logger.severe(e.getMessage());
			System.err.println(this + "could not be setup!");
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}
	
	/**
	 * Start the server
	 */
	public void start(){
        // Install the shutdown hook to process signals
		try {
			shutdownHook = new ShutdownHook(this);
			Runtime.getRuntime().addShutdownHook(shutdownHook);
		} catch (final Exception ex) {
			logger.severe("The shutdown hook could not be installed: " + ex);
		}
		
		logger.info(this + " ready to start! Closing the standard output and error streams...");
		System.out.println(this + " ready to start! Closing the standard output and error streams...");
		
		// close the standard output and error streams to not redirect the server output to /dev/null
		System.out.close();
		System.err.close();
		
		// run the reactor
		this.reactor.run();		
	}
	
	/**
	 * Stops the server
	 */
	public void stop(){
		logger.info(this + " is stopping...");

		// stop the reactor
		this.reactor.stop();
		this.threadPool.shutdown();

		logger.info(this + " has been stopped!");
	}
	
	/* (non-Javadoc)
	 * @see am.server.control.MessageHandler#getMessageToHandle()
	 */
	@Override
	public String getMessageToHandle() {
		return STOP_SERVER_REMOTE_CONTROL;
	}

	/* (non-Javadoc)
	 * @see am.server.control.MessageHandler#handleMessage(java.net.SocketAddress)
	 */
	@Override
	public String handleMessage(SocketAddress clientAddress) {
		String clientIpAddress = "";
		if (clientAddress != null && clientAddress.toString().length() > 0) {
			clientIpAddress = clientAddress.toString();
			clientIpAddress = clientIpAddress.substring(clientIpAddress.indexOf('/') + 1, clientIpAddress.indexOf(':'));
		}
		logger.info("Message [" + getMessageToHandle() + "] received from [" + clientIpAddress + "] to server [" + this.localIPAddress + "]");
		if (clientIpAddress.equals(this.localIPAddress)) {
			logger.severe("Server shutdown initiated!");

			// Remove the shutdown hook
	        try {
	            Runtime.getRuntime().removeShutdownHook(shutdownHook);
	            logger.info("The shutdown hook has been removed");
	        }
	        catch(final Exception ex) {
	        	logger.severe("The shutdown hook could not be removed: " + ex);
	        }

			stop();
		} else {
			logger.warning("Server shutdown command ignored!");
		}
		return null;
	}

	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return "AccessManagerServer(" + VERSION + ")" + "[" + id + "] ";
	}
	
	/**
	 * Main program
	 * @param args
	 */
	public static void main(String[] args){
		
		String serverId = null;
		String configLDAPHost = null;
		String configLDAPBaseDN = null;
		CommandLineArguments cli = new CommandLineArguments();
		try {
			cli.addStringOption("config_ldap_host", "the LDAP server host name with the AM server configuration", "D", "", false);
			cli.addStringOption("config_ldap_basedn", "the base dn of the LDAP server with the AM server configuration", "B", "", false);
			cli.addStringOption("server_id", "the AM Server identifier", "i", "Default AM Server", false);
			cli.addBooleanOption("help", "the help screen", "h", false, false);
			
			cli.processArguments(args);
			
			if (cli.getBooleanValue("help")) {
				System.out.println(cli);
				System.exit(0);
			}
			
			serverId = cli.getStringValue("server_id");
			configLDAPHost = cli.getStringValue("config_ldap_host");			
			configLDAPBaseDN = cli.getStringValue("config_ldap_basedn");			
			
		} catch (AMException e) {
			logger.severe("Arguments parsing exception:" + e.getMessage());
			System.exit(0);
		}
		
		
		AccessManagerServerConfig amSrvCfg = new AccessManagerServerConfig(configLDAPHost, configLDAPBaseDN);
		AccessManagerServer amServer = new AccessManagerServer(serverId, amSrvCfg);
		amServer.setup();
		amServer.start();
	}
	
}
