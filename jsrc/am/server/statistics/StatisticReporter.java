package am.server.statistics;

/**
 * Interface to be implemented by the classes that want to report statistics information to a statistics 
 * collector.
 * @author mleahu
 * @version 1.0
 */
public interface StatisticReporter {

	/**
	 * Prepare a report and returned it as a string
	 * @return string with statistic information
	 */
	public String reportStatistics();
	
	/**
	 * @param samplingInterval the sampling time interval in miliseconds
	 */
	public void updateStatistics(long samplingInterval);
}
