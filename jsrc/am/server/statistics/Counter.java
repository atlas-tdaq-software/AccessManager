package am.server.statistics;

/**
 * The class keeps a counter used by the StatisticsCollector to compute statistics.
 * @author mleahu
 * @version 1.1
 */
public class Counter implements StatisticReporter{

	/**
	 * The counter identifier.
	 */
	private String id;
	
	/**
	 * The counter description. 
	 */
	private String description;
	
	/**
	 * The current counter value
	 */
	private long currentCounter;
	
	/**
	 * The counter value last time when it was read to compute the currentFrequency
	 */
	private long lastReadCounter;
	
	/**
	 * The counter value last time when it was read to compute the averageFrequency
	 */
	private long lastReadAverageCounter;

	/**
	 * The total elapsed time in ms since the counter was reset. THis is updated each time the 
	 * frequency is computed.
	 */
	private long totalElapsedTime;
	
	/**
	 * The current frequency computed between last counter read and the current counter
	 */
	private double currentFrequency;
	
	/**
	 * The last peak value of currentFrequency
	 */
	private double peakFrequency;
	
	/**
	 * The average frequency since the counter was reset
	 */
	private double averageFrequency;
	
	/**
	 * Creates a counter object with an id and description.
	 * @param id short string to be used in the statistics print outs
	 * @param description description of this counter
	 */
	public Counter(String id, String description){
		this.id = id;
		this.description = description;
		reset();
	}
	
	/**
	 * Reset the counter internal values.
	 */
	public synchronized void reset(){
		currentCounter = 0;
		lastReadCounter = 0;
		lastReadAverageCounter = 0;
		totalElapsedTime = 0;
		currentFrequency = 0;
		averageFrequency = 0;
		peakFrequency	 = 0;
	}
	
	/**
	 * Increase the counter with the value
	 * @param value the long number to increase the counter with
	 */
	public synchronized void increase(long value){
		currentCounter += value;
	}
	
	/**
	 * Increment the counter value with 1.
	 */
	public synchronized void inc(){
		currentCounter++;
	}

	/**
	 * Compute the current frequency after the time elapsed
	 * @param elapsedTime time value in milliseconds
	 */
	public synchronized void updateStatistics(long elapsedTime){
		if (elapsedTime != 0){
			currentFrequency = ((currentCounter - lastReadCounter) * 1000.0 ) / elapsedTime;
			lastReadCounter  = currentCounter;
			totalElapsedTime += elapsedTime;
			if (currentFrequency > peakFrequency) {
				peakFrequency = currentFrequency;
			}
		} else {
			currentFrequency = 0;
		}
	}
	
	/**
	 * Compose a string with the counter value, the current and average frequency.
	 * @return string value
	 */
	public synchronized String getAllStatistics(){
		return "Counter [" + id + ", " + description + "]: " +
			   "value=" + currentCounter + " " +
			   "elapsed_time=" + totalElapsedTime + "ms " +
			   "current_freq=" + currentFrequency + "Hz " +
			   "average_freq=" + averageFrequency + "Hz";
	}
	
	/**
	 * Get the counter id
	 * @return
	 */
	public String getId(){
		return id;
	}
	
	/**
	 * Get the counter value
	 * @return counter value
	 */
	public long getValue(){
		return currentCounter;
	}
	
	/**
	 * Get the last computed frequency
	 * @return current frequency
	 */
	public double getCurrentFrequency(){
		return currentFrequency;
	}
	
	/**
	 * Compute the average frequency
	 * @return average frequency
	 */
	public synchronized double getAverageFrequency(){
		//compute the average frequency
		if (totalElapsedTime != 0){
			averageFrequency = ((currentCounter - lastReadAverageCounter ) * 1000.0) / totalElapsedTime;
			lastReadAverageCounter = currentCounter;
			totalElapsedTime = 0;
		} else {
			averageFrequency = 0;
		}
		
		return averageFrequency;
	}

	/**
	 * Compute the last peak frequency
	 * @return peak frequency
	 */
	public synchronized double getPeakFrequency(){
		double tempPeakFrequency = peakFrequency;
		peakFrequency = 0;
		return tempPeakFrequency;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		// print the current frequency
		return id + "[" + description + "]:" + reportStatistics();
	}

	/* (non-Javadoc)
	 * @see am.server.statistics.StatisticReporter#reportStatistics()
	 */
	public String reportStatistics() {
		return "" + currentFrequency + "\t" + id + "/s";
	}

}
