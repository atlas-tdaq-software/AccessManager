package am.server.statistics;

/**
 * The counter to monitor the server load. Each time a request comes in the counter increases and
 * each time a response is sent, the counter is decreased. 
 * @author mleahu
 * @version 1.1
 */
public class LoadCounter implements StatisticReporter{

	/**
	 * The counter identifier.
	 */
	private String id;
	
	/**
	 * The counter description. 
	 */
	private String description;
	
	/**
	 * The current load value
	 */
	private long load;
	
	/**
	 * The peak of the current load
	 */
	private long peak;

	/**
	 * The last peak load
	 */
	private long lastPeak;
		
	/**
	 * Creates a counter object with an id and description.
	 * @param id short string to be used in the statistics print outs
	 * @param description description of this counter
	 */
	public LoadCounter(String id, String description){
		this.id = id;
		this.description = description;
		reset();
	}
	
	/**
	 * Reset the counter internal values.
	 */
	public synchronized void reset(){
		load = 0;
		peak = 0;
		lastPeak = 0;
	}
		
	/**
	 * Increment the counter value with 1.
	 */
	public synchronized void inc(){
		load++;
		peak = Math.max(peak, load);
	}

	/**
	 * Decrement the counter value with 1.
	 */
	public synchronized void dec(){
		load--;
	}
	
	/**
	 * Store the peak value in the last peak
	 * @param elapsedTime time value in milliseconds
	 */
	public synchronized void updateStatistics(long elapsedTime){
		lastPeak = peak;
		peak = load;
	}
	
	/**
	 * Compose a string with the counter value, the current and average frequency.
	 * @return string value
	 */
	public synchronized String getAllStatistics(){
		return "Counter [" + id + ", " + description + "]: " +
			   "load=" + load + " " +
			   "peak=" + peak + " " +
			   "last peak=" + lastPeak + " ";
	}
	
	/**
	 * Get the counter id
	 * @return
	 */
	public String getId(){
		return id;
	}
	
	/**
	 * Get the counter value
	 * @return counter value
	 */
	public long getLoad(){
		return load;
	}
	
	/**
	 * Get the peak value
	 * @return peak value
	 */
	public double getPeak(){
		return peak;
	}
	
	/**
	 * Get the last peak value
	 * @return peak value
	 */
	public double getLastPeak(){
		return lastPeak;
	}
		
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		// print the current frequency
		return "Load counter: current load [" + load + "\t" + id + "] last peak [" + reportStatistics() + "]";
	}

	/* (non-Javadoc)
	 * @see am.server.statistics.StatisticReporter#reportStatistics()
	 */
	public String reportStatistics() {
		return "" + lastPeak + "\t" + id + "_peak";
	}

}
