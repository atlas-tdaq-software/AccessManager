package am.server.statistics;

/**
 * Java Virtual Machine monitoring object.
 * @author mleahu
 *
 */
public class JVMMonitoring implements StatisticReporter {

	/**
	 * The runtime object
	 */
	private Runtime runtimeObj;
	
	/**
	 * Constructor to build an JVMMonitoring monitoring object to get statistic information.
	 */
	public JVMMonitoring(){
		this.runtimeObj = Runtime.getRuntime();
	}
	
	/* (non-Javadoc)
	 * @see am.server.statistics.StatisticReporter#reportStatistics()
	 */
	public synchronized String reportStatistics() {
		return "" +
			runtimeObj.freeMemory() + "\tJVM_free_mem\t"	+ 
			runtimeObj.totalMemory() + "\tJVM_total_mem\t"	+  
			runtimeObj.maxMemory() + "\tJVM_max_mem";
	}

	/* (non-Javadoc)
	 * @see am.server.statistics.StatisticReporter#updateStatistics(long)
	 */
	public synchronized void updateStatistics(long samplingInterval) {
		// nothing to update for the moment

	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return "JVM monitoring:" + reportStatistics();
	}

}
