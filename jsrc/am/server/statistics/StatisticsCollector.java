package am.server.statistics;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;

import am.util.cache.Ping;

/**
 * Counters collector that computes statistic at fixed time intervals.
 * @author mleahu
 *
 */
public class StatisticsCollector implements Observer {

	private static final Logger logger =
        Logger.getLogger(StatisticsCollector.class.getName());


	/**
	 * The sampling time interval in miliseconds
	 */
	private long samplingTimeInterval;
	private static final long DEFAULT_SAMPLING_TIME_INTERVAL = 1000; // 1 sec
	
	/**
	 * The list of counters objects.
	 */
	private List<StatisticReporter> reporters;
	
	
	/**
	 * Thread that signals the current object for statistics computations.
	 */
	private Thread samplingClock;
	
	/**
	 * Variable for the silent mode.
	 */
	private boolean silent;
	
	/**
	 * Builds a counter collector object with a default sampling time interval.
	 */
	public StatisticsCollector(){
		
		this.samplingTimeInterval = DEFAULT_SAMPLING_TIME_INTERVAL;
		
		this.reporters = new ArrayList<StatisticReporter>();
		
		// we want to be notified from time to time by the ping daemon
		Ping ping = new Ping(this.samplingTimeInterval);
		ping.addObserver(this);
		
		//set up the cleaner daemon
		samplingClock = new Thread(ping, "Sampling clock");
		samplingClock.setDaemon(true);
		samplingClock.start();
		
		if (logger.isLoggable(Level.FINE)){
			logger.fine("Sampling clock object initialized with sampling time interval " 
					+ this.samplingTimeInterval + " ms");
		}
		
		silent = false;
	}
	
	/**
	 * Set the silent mode when no message is written to the log file, but the computations are 
	 * available for remote interrogations.
	 * @param silentOn true=enable the silent mode, false=disable it
	 */
	public void setSilentMode(boolean silentOn){
		silent = silentOn;
	}
	
	/**
	 * Register a new statistic reporter for monitoring
	 * @param reporter reporter object that has to implement the StatisticReporter interface
	 * @return true if the registration is successful
	 */
	public boolean registerReporter(StatisticReporter reporter){
		if (reporter != null){
			reporters.add(reporter);
			logger.info("Register " + reporter);
			return true;
		} else {
			logger.warning("Null reporter not registered!");
			return false;
		}
	}
	
	/* (non-Javadoc)
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	public void update(Observable arg0, Object arg1) {
		// compute the frequency of each counter
		for(StatisticReporter rep:reporters){
			rep.updateStatistics(this.samplingTimeInterval);
		}

		if (! silent && logger.isLoggable(Level.INFO)){
			logger.info(this.toString());
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		// display each counter
		String msg = "Reporters[" + this.reporters.size() + "]:\t";
		for(StatisticReporter rep:reporters){
			msg += rep.reportStatistics() + "\t";
		}
		return msg;
	}
}
