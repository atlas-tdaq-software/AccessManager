/**
 * 
 */
package am.server.authlogger;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;

import am.util.AMCommon;

/**
 * The logger handler to file to be used by the authorization logger class.
 * @author mleahu
 *
 */
public class LoggerHandlerToFile implements LoggerHandler {

	private static final java.util.logging.Logger logger =
		java.util.logging.Logger.getLogger(LoggerHandlerToFile.class.getName());
	
	/**
	 * The default log file prefix
	 */
	private static String LOGFILE_PREFIX = "am.server.auth.";

	/**
	 * The extension of the log files.
	 */
	private static String LOGFILE_EXTENSION = ".log";
	
	/**
	 * The maximum log file size set to 1MB
	 */
	private static int LOGFILE_SIZE = 1024 * 1024;
	
	/**
	 * The maximum number of log files stored after rotation
	 */
	private static int LOGFILE_COUNT = 5;
	
	
	/**
	 * The full path and name of the log file.
	 */
	private String authLogDir;
	
	/**
	 * The FileHandler object built by this class.
	 */
	private class ThreadLocalFileHandler extends ThreadLocal<FileHandler> {
			protected FileHandler initialValue(){
				String fileName = authLogDir + AMCommon.getFileSeparator() +
					LOGFILE_PREFIX + Thread.currentThread().getName() + LOGFILE_EXTENSION;
				logger.info("Initialize the auth logger file handler to write in the file:" 
						+ fileName);
				FileHandler fileHandler = null;
				try {
					fileHandler = new FileHandler(
							fileName,
							LOGFILE_SIZE,
							LOGFILE_COUNT);
					fileHandler.setLevel(java.util.logging.Level.ALL);
					fileHandler.setFormatter(logFormatter);
				} catch (Exception e) {
					logger.severe("File handler initialization failed:" + e.getMessage());
					//e.printStackTrace();
				}
				return fileHandler;
			}
	}
	private ThreadLocalFileHandler threadLocalFileHandler; 
	
	/**
	 * My own log formatter
	 */
	private static java.util.logging.Formatter logFormatter = new java.util.logging.Formatter() {
		public String format(java.util.logging.LogRecord record){
			StringBuffer out = new StringBuffer();
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMdd HH:mm:ss,SSS");

			// add date
			out.append("[");
			out.append(dateFormat.format(new Date(record.getMillis())));
			out.append("] ");
			
			// add the message
			String message = record.getMessage();
			// cut the URI prefix 
			out.append(message.replaceAll("urn:oasis:names:tc:xacml:1.0:[^:]+:", ""));
			
			// add new line
			out.append("\n");
			
			return out.toString();
		}
	};

	/**
	 * Creates an authorization logger handler that writes the messages into
	 * a file in the provided directory.
	 * @param authLogDir the directory where the log file is written
	 */
	public LoggerHandlerToFile(String authLogDir){
		this.authLogDir = authLogDir;
		threadLocalFileHandler = null;
	}
	
	/**
	 * Initialize the file handler.
	 */
	private void initializeHandler() throws Exception{
		if (threadLocalFileHandler == null){
			threadLocalFileHandler = new ThreadLocalFileHandler();
		}
		if (threadLocalFileHandler.get() == null){
			throw new Exception("File handler could not be initialized!");
		}
	}

	/* (non-Javadoc)
	 * @see am.server.authlogger.LoggerHandler#getHandler()
	 */
	public java.util.logging.Handler getHandler() throws Exception{
		initializeHandler();
		return threadLocalFileHandler.get();
	}

}
