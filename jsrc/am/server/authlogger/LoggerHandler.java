/**
 * 
 */
package am.server.authlogger;

/**
 * Interface for handler to be used by the authorization logger.
 * @author mleahu
 *
 */
public interface LoggerHandler {

	/**
	 * Provide the Handler object of be used as a logging handler.
	 * @return
	 */
	public java.util.logging.Handler getHandler() throws Exception;
}
