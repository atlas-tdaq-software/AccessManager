package am.server.authlogger;

import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

/**
 * Class to record the requests from clients and the decision taken by the AM server
 * @author mleahu
 * @version 1.0
 */
public class AuthorizationLogger {

	private static final Logger logger =
		Logger.getLogger(AuthorizationLogger.class.getName());

	/**
	 * The hash map to store the messages to be printed in the log
	 * The key is handed back to the callers to be used for the subsequent calls.
	 */
	private ConcurrentHashMap<Long, String> messages;
	
	/**
	 * The messages can be separated in categories of allowed/denied decisions. 
	 */
	private static String[] DECISIONS = {
										"UNKNOWN",
										"!ERROR!",
										"ALLOWED",
										"DENIED ",
										"DEFERRED"
										};
	public static Integer DECISION_UNKNOWN = new Integer(0);
	public static Integer DECISION_ERROR   = new Integer(1);
	public static Integer DECISION_ALLOWED = new Integer(2);
	public static Integer DECISION_DENIED  = new Integer(3);
	public static Integer DECISION_DEFERRED  = new Integer(4);
	
	/**
	 * The hash map to store the messages to be printed in the log
	 * The key is handed back to the callers to be used for the subsequent calls.
	 */
	private ConcurrentHashMap<Long, Integer> decisions;
	
	/**
	 * The key counter for the messages hash map.
	 */
	private long keyCounter = 0;
		
	/**
	 * Variable to store the enabled/disabled state of the current logger.
	 */
	private static int LOGGING_DISABLED = 0;
	private static int LOGGING_MINIMAL 	= 1;
	private static int LOGGING_ALL 		= 2;
	private int loggingState;
	
	
	/**
	 * The logger handler object.
	 */
	private LoggerHandler loggerHandler;

	/**
	 * The boolean variable to store the state of per thread auth logger objects
	 */
	private static ThreadLocal<Boolean> threadLocalLoggerConfigured = 
		new ThreadLocal<Boolean> (){
			protected Boolean initialValue(){
				return new Boolean(false);
			}
		};
		
	/**
	 * The thread local authorization logger.
	 */
	private static ThreadLocal<Logger> threadLocalAuthLogger = 
		new ThreadLocal<Logger> (){
		protected Logger initialValue(){
			return Logger.getLogger(
					"meta." + AuthorizationLogger.class.getPackage().getName() + 
					"." + Thread.currentThread().getName());
		}
	};
		
	/**
	 * @param loggerHandler
	 * @param numberOfThreads
	 */
	public AuthorizationLogger(LoggerHandler loggerHandler, int numberOfThreads){
		
		this.loggerHandler = loggerHandler;
		// set the concurrency level equal to the number of processing threads
		this.messages = new ConcurrentHashMap<Long, String>(16, (float)0.75, numberOfThreads);
		this.decisions = new ConcurrentHashMap<Long, Integer>(16, (float)0.75, numberOfThreads);
		keyCounter = 0;
		loggingState = LOGGING_DISABLED;
	}
	

	/**
	 * Configure the auth logger.
	 */
	private boolean configureAuthLogger(){
		if (threadLocalLoggerConfigured.get().booleanValue()) {
			return true;
		}
		
		Logger authLogger = threadLocalAuthLogger.get();
		
		// remove all the handlers of the authLogger logger
		for (java.util.logging.Handler handler:authLogger.getHandlers()){
			authLogger.removeHandler(handler);
		}
		
		// add the logger handler passed in the constructor
		try {
			authLogger.addHandler(loggerHandler.getHandler());
			authLogger.setUseParentHandlers(false);
			if (loggingState == LOGGING_MINIMAL){
				authLogger.setLevel(java.util.logging.Level.SEVERE);
			} else if (loggingState == LOGGING_ALL) {
				authLogger.setLevel(java.util.logging.Level.WARNING);
			} else {
				logger.severe("Could not set the auth logger level: unknown logging state.");
			}
			threadLocalLoggerConfigured.set(new Boolean(true));
		} catch (Exception e) {
			logger.severe("Can not configure the authorization logger handler:" + e.getMessage());
			//e.printStackTrace();
		}		
		return threadLocalLoggerConfigured.get().booleanValue();
	}
	
	/**
	 * Set the logging state.
	 * @param enableLogging if true, the enable the logging, if false the disable
	 */
	private synchronized void setLoggingState(int newLoggingState){
		loggingState = newLoggingState;
		if (loggingState > LOGGING_DISABLED) {
			messages.clear();
			decisions.clear();
			Logger authLogger = null;
			if (threadLocalLoggerConfigured.get().booleanValue()) {
				authLogger = threadLocalAuthLogger.get();
			}
			if (loggingState == LOGGING_ALL){
				logger.warning("Authorization logging ENABLED!");
				if (authLogger != null){
					authLogger.setLevel(java.util.logging.Level.WARNING);
				}
			} else if (loggingState == LOGGING_MINIMAL) {
				logger.warning("Authorization logging MINIMAL!");
				if (authLogger != null){
					authLogger.setLevel(java.util.logging.Level.SEVERE);
				}
			} else {
				logger.severe("Authorization logging: UNKNOWN STATE!");
			}
		} else {
			messages.clear();
			decisions.clear();
			logger.warning("Authorization logging DISABLED!");
		}
	}
	
	/**
	 * Return the current logger state.
	 * @return true if the logging is enabled, false otherwise
	 */
	public boolean isEnabled() {
		return (loggingState != LOGGING_DISABLED);
	}
	
	/**
	 * Return the current logger state.
	 * @return true if the logging is minimal, false otherwise
	 */
	public boolean isMinimal() {
		return (loggingState == LOGGING_MINIMAL);
	}
	
	/**
	 * Enable the authorization logger.
	 */
	public void enable() {
		setLoggingState(LOGGING_ALL);
	}
	
	/**
	 * Enable the authorization logger.
	 */
	public void minimal() {
		setLoggingState(LOGGING_MINIMAL);
	}
	
	/**
	 * Disable the authorization logger.
	 */
	public void disable() {
		setLoggingState(LOGGING_DISABLED);
	}
	
	/**
	 * Append another message to the messages already provided for the given key 
	 * @param authLoggerClientKey
	 * @param messageToAppend
	 * @return the same or a new authLoggerClientKey
	 */
	private Long appendMessage(Long authLoggerClientKey, String messageToAppend){
		if (!isEnabled()){
			return authLoggerClientKey;
		}
		
		if (authLoggerClientKey == null){
			// a new client wants to add messages to the auth logger
			authLoggerClientKey = new Long(keyCounter++);
			messages.put(authLoggerClientKey, messageToAppend);
			decisions.put(authLoggerClientKey, DECISION_UNKNOWN);
		} else {
			String clientMessage = messages.get(authLoggerClientKey);
			if (clientMessage != null){
				clientMessage += messageToAppend;
				messages.put(authLoggerClientKey, clientMessage);
			} else {
				logger.severe("Auth logger client key not found:" + authLoggerClientKey);
			}
		}
		return authLoggerClientKey;
	}

	/**
	 * Method to be called when the client has no key and wants to start logging messages
	 * @param initialMessage the initial message
	 * @return the key to be used all the way until setDecision method is called
	 */
	public Long initialMessage(String initialMessage){
		return appendMessage(null, initialMessage);
	}
	
	/**
	 * Method to be called when the client has a key and wants to log another messages
	 * @param authLoggerClientKey the client key obtained from the initialMessage call
	 * @param messageToAdd the message to be added to the other messages
	 */
	public void addMessage(Long authLoggerClientKey, String messageToAdd){
		if (authLoggerClientKey != null){
			appendMessage(authLoggerClientKey, messageToAdd);
		}
	}

	/**
	 * Specify the decision taken by the AM server for a authorization request specific to the
	 * client key.
	 * @param authLoggerClientKey the auth logger key specific to a client
	 * @param decision the decision must be one of the DECISION constants
	 */
	public void setDecision(Long authLoggerClientKey, Integer decision){
		if (isEnabled()) {
			if (decision < 0 || decision >= DECISIONS.length ){
				logger.severe("Invalid decision id:" + decision);
			} else if ( ! decisions.containsKey(authLoggerClientKey)){
				logger.severe("Auth logger client key not found:" + authLoggerClientKey);
			} else{
				decisions.put(authLoggerClientKey, decision);
			}
		}
	}
	
	/**
	 * The messages associated to this key are dumped to the auth logger and
	 * the key is destroyed.
	 * @param authLoggerClientKey the auth logger key specific to a client
	 * @param message the last message to be appended before all the clients messages are logged
	 */
	public void dismiss(Long authLoggerClientKey, String message){
		if (isEnabled()) {
			if ( ! messages.containsKey(authLoggerClientKey)){
				logger.severe("Auth logger client key not found:" + authLoggerClientKey);
			} else if (configureAuthLogger()) {
				Integer decision = decisions.get(authLoggerClientKey);
				java.util.logging.Level level = java.util.logging.Level.SEVERE;
				if (decision.equals(DECISION_ALLOWED)){
					level = java.util.logging.Level.WARNING;
				}
				threadLocalAuthLogger.get().log(level, 
						DECISIONS[decision] + " " + 
						messages.get(authLoggerClientKey) + 
						message);
				messages.remove(authLoggerClientKey);
				decisions.remove(authLoggerClientKey);
				authLoggerClientKey = null;
			}
		}
	}
	
	/**
	 * The messages associated to this key are dumped to the auth logger and
	 * the key is destroyed.
	 * @param authLoggerClientKey the auth logger key specific to a client
	 */
	public void dismiss(Long authLoggerClientKey){
		dismiss(authLoggerClientKey, "");
	}
}
