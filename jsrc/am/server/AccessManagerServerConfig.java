package am.server;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Logger;

import am.util.AMException;
import am.util.AMLogger;
import am.util.AMCommon;

/**
 * Provide AM server configuration settings retrieved from different sources like 
 * the system environment variables, configuration files, configurations in data bases.
 * @author mleahu
 *
 */
public class AccessManagerServerConfig {

	/*
	 * The configuration LDAP server
	 */
	private static final String CFG_LDAP_BASE_DN = "ou=atlas,o=cern,c=ch";
	private static final String CFG_LDAP_RDN = "cn=AccessManagerServer,ou=TDAQServices";
	private static final String[] CFG_LDAP_ATTRS = new String[] { "description" };
	private static final String CFG_LDAP_FILTER_EXPR_START = "(&(objectClass=ipService)(ipHostNumber=";
	private static final String CFG_LDAP_FILTER_EXPR_END = "))";

	/**
	 * AM LDAP host name.
	 */
	public static final String PARAMETER_AM_LDAP_HOST="TDAQ_AM_LDAP_HOST";

	/**
	 * AM LDAP basedn
	 */
	public static final String PARAMETER_AM_LDAP_BASEDN="TDAQ_AM_LDAP_BASEDN";

	
	/**
	 * The AM Server logs directory
	 */
	public static final String PARAMETER_AM_LOGS_DIR="TDAQ_AM_LOGS_DIR";

	/**
	 * The AM Server log level parameter name
	 */
	public static final String PARAMETER_AM_SERVER_LOG_LEVEL="TDAQ_AM_SERVER_LOG_LEVEL";

	/**
	 * The AM Server authentication logger status parameter name
	 */
	public static final String PARAMETER_AM_SERVER_AUTH_LOG="TDAQ_AM_SERVER_AUTH_LOG";
	
	/**
	 * The AM Server port number parameter
	 */
	public static final String PARAMETER_AM_SERVER_PORT="TDAQ_AM_SERVER_PORT";
	
	/**
	 * The AM Server socket backlog
	 */
	public static final String PARAMETER_AM_SERVER_SOCKET_BACKLOG="TDAQ_AM_SERVER_SOCKET_BACKLOG";
	
	/**
	 * The path where the policies are stored
	 */
	public static final String PARAMETER_AM_POLICIES_PATH="TDAQ_AM_POLICIES_PATH";

	/**
	 * The number of processing threads to use
	 */
	public static final String PARAMETER_AM_SERVER_PROC_THREADS="TDAQ_AM_SERVER_PROC_THREADS";

	/**
	 * Enable the statistics
	 */
	public static final String PARAMETER_AM_SERVER_STATISTICS="TDAQ_AM_SERVER_STATISTICS";

	/**
	 * The saturation factor
	 */
	public static final String PARAMETER_AM_SERVER_SATURATION_FACTOR="TDAQ_AM_SERVER_SATURATION_FACTOR";
	
	/**
	 * The saturation factor
	 */
	public static final String PARAMETER_AM_SERVER_SECONDARY_SERVERS="TDAQ_AM_SERVER_SECONDARY_SERVERS";
	
	/**
	 * The default value for LDAP host name
	 */
	public static final String DEFAULT_VALUE_AM_LDAP_HOST = "atlas-ldap.cern.ch";
	
	/**
	 * The default value for LDAP base dn
	 */
	public static final String DEFAULT_VALUE_AM_LDAP_BASEDN = "ou=atlas,o=cern,c=ch";

	/**
	 * The default value for AM server port number
	 */
	public static final int DEFAULT_VALUE_AM_SERVER_PORT = 20000;
		
	/**
	 * The default value for AM server socket backlog
	 */
	public static final int DEFAULT_VALUE_AM_SERVER_SOCKET_BACKLOG = 200;
	
	/**
	 * The default value for AM processing threads number
	 */
	public static final int DEFAULT_VALUE_AM_SERVER_PROC_THREADS = 2;

	/**
	 * The default value for AM saturation factor
	 */
	public static final int DEFAULT_VALUE_AM_SERVER_SATURATION_FACTOR = 0; //disabled
	
	private static String[] packagesToLog = {
		// am.common
		"am.config",
		"am.util.cache",
		"am.pip",
		"am.pap",
		// am.server
		"am.pdp",
		"am.server",
		"am.server.control",
		"am.server.statistics",
		// am.rbac
		"am.rbac",
		// am.xacml
		"am.xacml.context",
		"am.xacml.pap",
		// sunxacml
		"com.sun.xacml",
		"com.sun.xacml.Finder",
		"com.sun.xacml.combine"
	};
	
	/**
	 * The AM specific logger
	 */
	private AMLogger amLogger = null;
	
	private am.config.Configuration amConfig;

	private static final Logger logger =
	        Logger.getLogger(AccessManagerServerConfig.class.getName());

	
	public AccessManagerServerConfig(String configLDAPServer, String _configLDAPBaseDN){

		amConfig = new am.config.Configuration();

		if (configLDAPServer != null && configLDAPServer.length() > 0) {
			try {
				String localHostName = InetAddress.getLocalHost().getHostName();
				if (localHostName.indexOf('.') > 0){
					localHostName = localHostName.substring(0, localHostName.indexOf('.'));
				}
				String configLDAPBaseDN = _configLDAPBaseDN;
				if ( configLDAPBaseDN == null || configLDAPBaseDN.length() == 0) {
					configLDAPBaseDN = CFG_LDAP_BASE_DN;
				}
				
				am.config.ConfigurationFromLDAP cfgFromLDAP = 
					new am.config.ConfigurationFromLDAP (
							configLDAPServer,
							configLDAPBaseDN,
							CFG_LDAP_RDN,
							CFG_LDAP_ATTRS,
							CFG_LDAP_FILTER_EXPR_START + localHostName + CFG_LDAP_FILTER_EXPR_END);
				amConfig.addConfigurationSource(cfgFromLDAP);
			} catch (UnknownHostException e) {
				logger.severe("Can not create the object to read server configuration from LDAP:" + e.getMessage());
			} catch (AMException e) {
				logger.severe("Can not add LDAP configuration source:" + e.getMessage());
			}
			
		}
			
		try {
			amConfig.addConfigurationSource(new am.config.ConfigurationFromEnvironment());
		} catch (AMException e) {
			logger.severe("Can not add environment configuration source:" + e.getMessage());
		}

		logger.finest(this.toString());
		
		enableLogging();
	}
	
	/**
	 * Reads the LDAP server name;
	 * @return the LDAP server name
	 */
	public String getLDAPHost(){
		return amConfig.get(PARAMETER_AM_LDAP_HOST, DEFAULT_VALUE_AM_LDAP_HOST);
	}
	
	/**
	 * Reads the AM LDAP base DN;
	 * @return the LDAP base DN
	 */
	public String getLDAPBaseDN(){
		return amConfig.get(PARAMETER_AM_LDAP_BASEDN, DEFAULT_VALUE_AM_LDAP_BASEDN);
	}

	/**
	 * Get the AM server log level;
	 * @return the log level in string format
	 */
	public String getLogLevel(){
		return amConfig.get(PARAMETER_AM_SERVER_LOG_LEVEL, "NORMAL");
	}
	
	/**
	 * Get the AM policies storage path;
	 * @return the path where the policies are stored
	 */
	public String getPoliciesPath(){
		return amConfig.get(PARAMETER_AM_POLICIES_PATH, "PARAMETER_NOT_DEFINED");
	}

	/**
	 * Get the AM logs directory path;
	 * @return the path where the log files will be stored
	 */
	public String getLogsDir(){
		String amLogsDir = amConfig.get(PARAMETER_AM_LOGS_DIR, ".");
		amLogsDir += ((amLogsDir.length() > 0) ? AMCommon.getFileSeparator() : ""); 
		amLogsDir += "amServer" + AMCommon.getFileSeparator();
		return amLogsDir;
	}
	
	/**
	 * Get the AM server processing threads
	 * @return the number of AM server processing threads
	 */
	public int getServerProcessingThreads(){
		int pt = amConfig.getInt(PARAMETER_AM_SERVER_PROC_THREADS, DEFAULT_VALUE_AM_SERVER_PROC_THREADS);		
		if (pt < 1 || pt > 50){
			System.err.println("AM Server Configuration: Invalid value [" + pt +"] for number of processing threads! " 
					+ "The value should be in the interval [1, 50]. Using default value: " + DEFAULT_VALUE_AM_SERVER_PROC_THREADS);
			pt = DEFAULT_VALUE_AM_SERVER_PROC_THREADS;
		}
		
		return pt;
	}

	/**
	 * Test if the AM server statistics are enabled in verbose mode (written to the log files). 
	 * @return true if the statistics are enabled with the "on" value for the environment variable
	 */
	public boolean areServerStatisticsEnabledVerbose(){
		return ("on".equals(amConfig.get(PARAMETER_AM_SERVER_STATISTICS, "off")));
	}

	/**
	 * Test if the AM server statistics are enabled in the silent mode. 
	 * @return true if the statistics are enabled with the "silent" value for the environment variable
	 */
	public boolean areServerStatisticsSilent(){
		return ("silent".equals(amConfig.get(PARAMETER_AM_SERVER_STATISTICS, "not_silent")));
	}
	
	/**
	 * Test if the AM server authorization logger is enabled. 
	 * @return true if the authorization logger is enabled with the "on" value for the environment variable
	 */
	public boolean isServerAuthorizationLoggerEnabled(){
		return ("on".equals(amConfig.get(PARAMETER_AM_SERVER_AUTH_LOG, "off")) || 
				isServerAuthorizationLoggerMinimal());
	}
	
	/**
	 * Test if the AM server authorization logger is set to a minimal level. 
	 * @return true if the authorization logger is enabled with the "minimum" value for the environment variable
	 */
	public boolean isServerAuthorizationLoggerMinimal(){
		return ("minimum".equals(amConfig.get(PARAMETER_AM_SERVER_AUTH_LOG, "maximum")));
	}
	
	/**
	 * Get the AM server port number;
	 * @return the AM server port number
	 */
	public int getServerPortNumber(){
		return amConfig.getInt(PARAMETER_AM_SERVER_PORT, DEFAULT_VALUE_AM_SERVER_PORT);
	}
	
	/**
	 * Get the AM server socket backlog;
	 * @return the AM server socket backlog
	 */
	public int getServerSocketBacklog(){
		return amConfig.getInt(PARAMETER_AM_SERVER_SOCKET_BACKLOG, DEFAULT_VALUE_AM_SERVER_SOCKET_BACKLOG);
	}
	
	/**
	 * Get the AM server saturation factor
	 * @return the AM server saturation factor
	 */
	public int getServerSaturationFactor(){
		return amConfig.getInt(PARAMETER_AM_SERVER_SATURATION_FACTOR, DEFAULT_VALUE_AM_SERVER_SATURATION_FACTOR);
	}
	
	/**
	 * Get the AM secondary servers information;
	 * @return the string with the secondary servers information
	 */
	public String getSecondaryServers(){		
		return amConfig.get(PARAMETER_AM_SERVER_SECONDARY_SERVERS, "");
	}

	/**
	 * Start the logging for the AM server
	 */
	private void enableLogging(){
		
		try {
			if (amLogger == null) {
				amLogger = new AMLogger(getLogsDir());
			}
		} catch (AMException e) {
			System.err.println("Could not create AM logger! Error: " + e.getMessage());
			return;
		}
		
		// first of all enable the logging for am.config package in case there is a problem 
		// while the configuration parameters are read		
		//amLogger.enableLoggingToFile(AMConfig.class.getPackage().getName());
		
		// get the AM log level id
		String sLogLevel = getLogLevel().toUpperCase();
		
		int amLogLevelId;
		try {
			//check if it's a number
			amLogLevelId = Integer.valueOf(sLogLevel);
			amLogLevelId = AMLogger.validateAMLogLevelId(amLogLevelId);
		} catch (NumberFormatException e){
			// it's not a number, consider it a string and get the id
			amLogLevelId = AMLogger.getAMLogLevelId(sLogLevel);
		}
		
		//disable first all the loggers handlers defined or predefined so far
		Logger rootLogger = Logger.getLogger("");
		for (java.util.logging.Handler handler:rootLogger.getHandlers()){
			rootLogger.removeHandler(handler);
		}
		
		// enable the loggers for all the packages we are interested in
		for(String packageName: packagesToLog){
			amLogger.enableLoggingToFile(packageName, amLogLevelId);
		}
		amLogger.setAMFormatterToAllLoggers();
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		String message = "Access Manager Server configurations:\n";
		message += "LDAPHost: [" + getLDAPHost() + "]\n";
		message += "LDAPBaseDN: [" + getLDAPBaseDN() + "]\n";
		message += "LogLevel: [" + getLogLevel() + "]\n";
		message += "PoliciesPath: [" + getPoliciesPath() + "]\n";
		message += "ServerProcessingThreads: [" + getServerProcessingThreads() + "]\n";
		message += "ServerStatisticsEnabledVerbose: [" + areServerStatisticsEnabledVerbose() + "]\n";
		message += "ServerStatisticsSilent: [" + areServerStatisticsSilent() + "]\n";
		message += "ServerPortNumber: [" + getServerPortNumber() + "]\n";
		message += "ServerSocketBacklog: [" + getServerSocketBacklog() + "]\n";
		message += "ServerSaturationFactor: [" + getServerSaturationFactor() + "]\n";
		message += "SecondaryServers: [" + getSecondaryServers() + "]";
		return message;
	}
}
