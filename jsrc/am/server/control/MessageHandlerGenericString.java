package am.server.control;

import java.net.SocketAddress;

/**
 * Generic message handler to return the same response each time a certain message is received.
 * @author mleahu
 *
 */
public class MessageHandlerGenericString implements MessageHandler {

	private String messageToHandle;
	private String response;
	
	/**
	 * Creates a generic message handler that responds with 'response' each time
	 * the message 'messageToHandle' is received.
	 * @param messageToHandle
	 * @param response
	 */
	public MessageHandlerGenericString(String messageToHandle, String response){
		this.messageToHandle = messageToHandle;
		this.response = response;
	}
	
	/* (non-Javadoc)
	 * @see am.server.control.MessageHandler#getMessageToHandle()
	 */
	public String getMessageToHandle() {
		return messageToHandle;
	}

	/* (non-Javadoc)
	 * @see am.server.control.MessageHandler#handleMessage(java.net.SocketAddress)
	 */
	public String handleMessage(SocketAddress clientAddress) {
		return response;
	}

}
