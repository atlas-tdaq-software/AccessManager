package am.server.control;

import java.net.SocketAddress;

/**
 * The interface for a control message handler. The MessageHandler implementations must be 
 * registered with the AccessManagerServerController object.
 * @author mleahu
 * @version 1.0
 */
public interface MessageHandler {

	/**
	 * The message this object is able to handle 
	 * @return the message name
	 */
	public String getMessageToHandle();
		
	/**
	 * Handle the message and return a response
	 * @return null if no response as a result of message handling, !=null if there is a response
	 */
	public String handleMessage(SocketAddress clientAddress);
}
