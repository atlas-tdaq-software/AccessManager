package am.server.control;

import java.net.SocketAddress;
import java.util.Date;

/**
 * The handler for the UPTIME message. Return the server up time.
 * @author mleahu
 * @version 1.0
 */
public class MessageHandlerUP_TIME implements MessageHandler {

	/**
	 * The message this calls can handle.
	 */
	private static final String MESSAGE_TO_HANDLE = "UP_TIME";
	
	private Date initialDate;
	
	public MessageHandlerUP_TIME(){
		initialDate = new Date();
	}

	/* (non-Javadoc)
	 * @see am.server.control.MessageHandler#getMessageToHandle()
	 */
	public String getMessageToHandle() {
		return MESSAGE_TO_HANDLE;
	}

	/* (non-Javadoc)
	 * @see am.server.control.MessageHandler#handleMessage(java.net.SocketAddress)
	 */
	public String handleMessage(SocketAddress clientAddress) {
		Date currentDate = new Date();
		long diffMs = currentDate.getTime() - initialDate.getTime();
		long diffS = diffMs / 1000, printS = diffS % 60;
		long diffM = diffS  / 60  , printM = diffM % 60;
		long diffH = diffM  / 60  , printH = diffH % 24;
		long diffD = diffH  / 24;
		
		String diffString = "";
		if (diffD > 0) {
			diffString += diffD + "d";
		}
		
		if (printH > 0 || diffString.length() > 0) {
			diffString += printH + "h";
		}
			
		if (printM > 0 || diffString.length() > 0) {
			diffString += printM + "m";
		}
		
		diffString += printS + "s";
		
		return diffString;
	}
	
}
