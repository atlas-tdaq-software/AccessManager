package am.server.control;

import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Set;
import java.util.logging.Logger;

/**
 * Controller for the Access Manager Server. It processes messages received usually from remote clients.
 * @author mleahu
 * 
 */
public class AccessManagerServerController {

    private static Logger logger =
        Logger.getLogger(AccessManagerServerController.class.getName());
    
	/**
	 * The hash map for the message handler objects.
	 */
	private Hashtable<String, MessageHandler> messageHandlers;
	
	public AccessManagerServerController(){
		messageHandlers = new Hashtable<String, MessageHandler>();
	}
	
	/**
	 * Register a message handler
	 * @param messageHandler the message handler object
	 */
	public void registerMessageHandler(MessageHandler messageHandler){
		messageHandlers.put(messageHandler.getMessageToHandle(), messageHandler);
	}
	
	/**
	 * Handle the message and return a response
	 * @return null if no response as a result of message handling, !=null if there is a response
	 */
	public String processMessage(String message, SocketAddress clientAddress){
		String response = "UNKNOWN MESSAGE";
		if (messageHandlers.containsKey(message)){
			response = messageHandlers.get(message).handleMessage(clientAddress);
			logger.info("Message [" + message + "] received from " + 
					clientAddress + " was processed and the response will be [" + response + "]");
		} else {
			logger.info("Message [" + message + "] received from " + 
					clientAddress + ", was NOT recognized and the response will be [" + response + "]");
		}
		return response;
	}
	
	/**
	 * Return the set of messages registered to the controller
	 * @return set of string messages
	 */
	public Set<String> getRegisteredMessages(){
		return messageHandlers.keySet();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		Set<String> keywords = getRegisteredMessages();
		if (keywords == null) {
			return "";
		}
		ArrayList<String> keywordsList = new ArrayList<String>(keywords);
		Collections.sort(keywordsList);
		return keywordsList.toString();
	}
}
