package am.server.control;

import java.net.SocketAddress;
import java.text.DateFormat;
import java.util.Date;

/**
 * The handler for the START_TIME messages. Returns the date when the server has started.
 * @author mleahu
 * @version 1.0
 */
public class MessageHandlerSTART_TIME implements MessageHandler {

	/**
	 * The message this calls can handle.
	 */
	private static final String MESSAGE_TO_HANDLE = "START_TIME";
	
	private String initialDateString;
	
	public MessageHandlerSTART_TIME(){
		DateFormat dateFormat = DateFormat.getDateTimeInstance();
		initialDateString = dateFormat.format(new Date());
	}
	
	/* (non-Javadoc)
	 * @see am.server.control.MessageHandler#getMessageToHandle()
	 */
	public String getMessageToHandle() {
		return MESSAGE_TO_HANDLE;
	}

	/* (non-Javadoc)
	 * @see am.server.control.MessageHandler#handleMessage(java.net.SocketAddress)
	 */
	public String handleMessage(SocketAddress clientAddress) {
		return initialDateString;
	}

}
