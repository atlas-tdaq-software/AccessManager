package am.server.control;

import java.net.SocketAddress;

import am.server.statistics.Counter;

/**
 * Class to process messages that get their responses from a Counter object
 * @author mleahu
 *
 */
public class MessageHandlerAverageRate implements MessageHandler {

	private Counter counter;
	
	/**
	 * Creates a message handler for a counter object.
	 * @param counter
	 */
	public MessageHandlerAverageRate(Counter counter){
		this.counter = counter;
	}
	
	/* (non-Javadoc)
	 * @see am.server.control.MessageHandler#getMessageToHandle()
	 */
	public String getMessageToHandle() {
		return "AVG_RATE_" + counter.getId().toUpperCase();
	}

	/* (non-Javadoc)
	 * @see am.server.control.MessageHandler#handleMessage(java.net.SocketAddress)
	 */
	public String handleMessage(SocketAddress clientAddress) {
		return String.valueOf(counter.getAverageFrequency());
	}

}
