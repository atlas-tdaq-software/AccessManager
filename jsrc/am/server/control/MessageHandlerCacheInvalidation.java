package am.server.control;

import java.net.SocketAddress;

import am.util.cache.CacheUser;

/**
 * Message handler for the cache user to invalidate their cache when a message is received,
 * @author mleahu
 *
 */
public class MessageHandlerCacheInvalidation implements MessageHandler {

	private String messageToHandle;
	private CacheUser cacheUser;
	/**
	 * Creates a message handler object that invalidate the cache of a cache user when messageToHandle
	 * is received. 
	 * @param messageToHandle the message that triggers the cache invalidation
	 * @param cacheUser the CacheUser object
	 */
	public MessageHandlerCacheInvalidation(String messageToHandle, CacheUser cacheUser){
		this.messageToHandle = messageToHandle;
		this.cacheUser = cacheUser;
	}
	
	/* (non-Javadoc)
	 * @see am.server.control.MessageHandler#getMessageToHandle()
	 */
	public String getMessageToHandle() {
		return messageToHandle;
	}

	/* (non-Javadoc)
	 * @see am.server.control.MessageHandler#handleMessage(java.net.SocketAddress)
	 */
	public String handleMessage(SocketAddress clientAddress) {
		cacheUser.invalidateCache();
		// don't return any response
		return null;
	}

}
