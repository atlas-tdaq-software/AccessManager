package am.server.NIOReactor;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import am.server.authlogger.AuthorizationLogger;

/**
 * Interface to specify the method an input handler has to implement.
 * The input handler objects are used by the HandlerLogic object to handle the data received
 * from the clients.
 * @author mleahu
 * @version 1.0
 */
public interface InputHandler {
	
	/**
	 * Process the data from an input stream and return the result in an output stream object.
	 * @param inputStream the input data stream
	 * @param outputstream object where the processing result is stored
	 * @param authorizationLogger the authorization logger to log the client request
	 * @param authLoggerClientKey the client key for the authorization logger
	 */
	public void process(
			ByteArrayInputStream inputStream, 
			ByteArrayOutputStream outputStream,
			AuthorizationLogger authorizationLogger,
			Long authLoggerClientKey);
}
