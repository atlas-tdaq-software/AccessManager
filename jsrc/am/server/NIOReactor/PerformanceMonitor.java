/**
 * 
 */
package am.server.NIOReactor;

import am.server.control.AccessManagerServerController;
import am.server.control.MessageHandlerAverageRate;
import am.server.control.MessageHandlerCounter;
import am.server.control.MessageHandlerPeakRate;
import am.server.statistics.Counter;
import am.server.statistics.LoadCounter;
import am.server.statistics.StatisticsCollector;

/**
 * Gathers counters that can be used in various situations
 * @author mleahu
 *
 */
public class PerformanceMonitor {

    /**
     * Counters for statistics
     */
    private Counter cntNewRequest;		// new client connection
    private Counter cntReqProcOK;		// processed clients
    private Counter cntReqProcWithError;		// clients not processed
    private Counter cntReqWhenBusy;		// request received when the server is busy
    private Counter cntBusyResp;		// busy responses sent to clients
    private Counter cntBusyRespWithError;// busy responses sent to clients
    
    private LoadCounter cntLoad;		// counts the server load

    private int saturationFactor;	//the saturation factor
    
    /**
     * Creates a performance monitor object that gathers more counters.
     * @param statisticsCollector
     * @param amServerController
     * @param saturationFactor the saturation factor. 0 disables the load check
     */
    public PerformanceMonitor(
    		StatisticsCollector statisticsCollector,
    		AccessManagerServerController amServerController,
    		int saturationFactor){
    	
    	this.saturationFactor = saturationFactor;
    	
        // initialize the counters
        this.cntNewRequest = new Counter("new_req", "new requests to be processed");
        this.cntReqProcOK = new Counter("req_proc_ok", "requests processed successfully");
        this.cntReqProcWithError = new Counter("req_proc_err", "requests processed with errors");
        this.cntReqWhenBusy = new Counter("req_when_busy", "requests received when the server is busy");
        this.cntBusyResp = new Counter("busy_resp", "busy reponses sent to clients");
        this.cntBusyRespWithError = new Counter("busy_resp_err", "errors while sending busy responses");
        this.cntLoad = new LoadCounter("srv_load", "the server load counter");
        
        if (statisticsCollector != null){
            statisticsCollector.registerReporter(this.cntNewRequest);
            statisticsCollector.registerReporter(this.cntReqProcOK);
            statisticsCollector.registerReporter(this.cntReqProcWithError);
            statisticsCollector.registerReporter(this.cntLoad);
            statisticsCollector.registerReporter(this.cntReqWhenBusy);
            statisticsCollector.registerReporter(this.cntBusyResp);
            statisticsCollector.registerReporter(this.cntBusyRespWithError);
        }

        // register a few counters in the controller
        amServerController.registerMessageHandler(new MessageHandlerCounter(this.cntNewRequest));
        amServerController.registerMessageHandler(new MessageHandlerCounter(this.cntReqProcOK));
        amServerController.registerMessageHandler(new MessageHandlerCounter(this.cntReqProcWithError));
        amServerController.registerMessageHandler(new MessageHandlerCounter(this.cntReqWhenBusy));
        amServerController.registerMessageHandler(new MessageHandlerCounter(this.cntBusyResp));
        amServerController.registerMessageHandler(new MessageHandlerCounter(this.cntBusyRespWithError));
        
        amServerController.registerMessageHandler(new MessageHandlerAverageRate(this.cntNewRequest));
        amServerController.registerMessageHandler(new MessageHandlerAverageRate(this.cntReqProcOK));
        amServerController.registerMessageHandler(new MessageHandlerAverageRate(this.cntReqProcWithError));
        amServerController.registerMessageHandler(new MessageHandlerAverageRate(this.cntReqWhenBusy));
        amServerController.registerMessageHandler(new MessageHandlerAverageRate(this.cntBusyResp));
        amServerController.registerMessageHandler(new MessageHandlerAverageRate(this.cntBusyRespWithError));
        
        amServerController.registerMessageHandler(new MessageHandlerPeakRate(this.cntNewRequest));
        amServerController.registerMessageHandler(new MessageHandlerPeakRate(this.cntReqProcOK));
        amServerController.registerMessageHandler(new MessageHandlerPeakRate(this.cntReqProcWithError));    	
        amServerController.registerMessageHandler(new MessageHandlerPeakRate(this.cntReqWhenBusy));    	
        amServerController.registerMessageHandler(new MessageHandlerPeakRate(this.cntBusyResp));    	
        amServerController.registerMessageHandler(new MessageHandlerPeakRate(this.cntBusyRespWithError));	
    }
    
    /**
     * Notify about a new request arrival
     */
    public void newRequestToProcess(){
    	cntNewRequest.inc();
    	cntLoad.inc();
    }
        
    /**
     * Notify about a request processed with errors
     */
    public void requestProcessedWithError(){
    	cntReqProcWithError.inc();
    	cntLoad.dec();
    }
    
    /**
     * Notify about a request processed successfully
     */
    public void requestProcessedSuccessfully(){
    	cntReqProcOK.inc();
    	cntLoad.dec();
    }
    
    /**
     * Notify about a new request arrival, but the server is busy and will not process this one
     */
    public void newRequestArrivedWhenServerIsBusy(){
    	cntReqWhenBusy.inc();
    }
    
    /**
     * Notify about a busy response sent to clients
     */
    public void busyResponseSentToClient(){
    	cntBusyResp.inc();
    }
    
    /**
     * Notify about an error while delivering a busy response to clients
     */
    public void busyResponseDeliveryError(){
    	cntBusyRespWithError.inc();
    }

    /**
     * Checks if the overload state is detected
     * @return true if the current load is bigger than the saturation factor
     */
    public boolean overloaded(){
    	return (saturationFactor > 0 && cntLoad.getLoad() > saturationFactor);
    }
    
    /**
     * Check if the overload state detection is enabled
     * @return true if enabled
     */
    public boolean isOverloadDetectionEnabled(){
    	return (saturationFactor > 0);
    }
}
