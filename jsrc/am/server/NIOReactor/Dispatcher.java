package am.server.NIOReactor;

import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;

/**
 * Dispatcher interface to be used by the handler logic to register or unregister
 * a channel with an attachment.
 * @author mleahu
 * @version 1.0  
 */
public interface Dispatcher {
	
	/**
	 * Change the interested operations for a key.
	 * @param key the key
	 * @param ops the new operations
	 * @return the updated key
	 * @throws ClosedChannelException
	 */
	public SelectionKey changeKeyInterestedOps(SelectionKey key, int ops) throws ClosedChannelException;
	
	/**
	 * Unregister the channel with the key.
	 * @param key the key to be unregistered.
	 */
	public void unregisterChannel(SelectionKey key);	
}
