package am.server.NIOReactor;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousCloseException;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.NotYetConnectedException;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.logging.Level;
import java.util.logging.Logger;

import am.server.authlogger.AuthorizationLogger;

/**
 * This class contains the logic necessary to process a client connection.
 * It receives the request from the client, process it and send back the response.
 * This object is run by a pool of threads.
 * @author mleahu
 * @version 1.0
 */
public class BusyHandlerLogic implements Runnable {

    private static Logger logger =
        Logger.getLogger(BusyHandlerLogic.class.getName());
    
    /**
     * The object identifier to be used in toString()
     */
    private String identifier = "";
    
	private Dispatcher dispatcher;
	private SelectionKey selectionKey;
	private SocketChannel socketChannel;
	private InputHandler inputHandler;
	
	// buffer used to create the output stream
	private byte [] transferBuffer = new byte [512];
	
	// the byte buffer size is 512; usual request size is ~2000; response is usual ~200
	private ByteBuffer buffer = ByteBuffer.wrap(transferBuffer);
	
	// the output stream to collect data from the buffer 
	// initial size is the buffer.capacity
	private ByteArrayOutputStream outputStream;
	
	
	//buffer used for filling the byte buffer from the output buffer
	private byte [] outputBuffer = null;
	private int outputBufferRemaining = 0;
	
	private boolean receivingDataAndProcessing; 
	
	/**
	 * The performance monitor object.
	 */
	private PerformanceMonitor performanceMonitor;
	
	/**
	 * The AM server authorization logger object
	 */
	private AuthorizationLogger authorizationLogger;
	/**
	 * The authorization logger key for the current client 
	 */
	private Long authLoggerClientKey;
	
	/**
	 * Constructs and handler logic object to take care of the client data and respond to him.
	 * @param dispatcher the dispatcher object to be used
	 * @param selectionKey the selection key registered in the selector
	 * @param inputHandlerFactory the factory to be used to get the input handler objects
	 * @param cntReqProc the counter for the requests processed successful
	 * @param cntReqNotProc the counter for the requests processed unsuccessful
	 * @param authorizationLogger the authorization logger object
	 */
	public BusyHandlerLogic(
			Dispatcher dispatcher, 
			SelectionKey selectionKey, 
			InputHandlerFactory inputHandlerFactory,
			PerformanceMonitor performanceMonitor,
			AuthorizationLogger authorizationLogger){
		
		this.dispatcher = dispatcher;
		// the key is a result of a registration for read in the allocator object
		this.selectionKey = selectionKey;
		this.socketChannel = (SocketChannel)selectionKey.channel();
		this.inputHandler = inputHandlerFactory.getInputHandler(InputHandlerFactory.HANDLER_TO_RESPOND_BUSY);
		this.performanceMonitor = performanceMonitor;
		this.authorizationLogger = authorizationLogger;
		
		this.receivingDataAndProcessing = true;
		
		this.identifier = 
			"[" + 
			((this.socketChannel == null) ? 
					"null" : 
					this.socketChannel.socket().getInetAddress().getHostAddress() + ":" + this.socketChannel.socket().getPort()) + 
			"]";
		
		authLoggerClientKey = authorizationLogger.initialMessage("Client" + this.identifier);

		// the output stream is used to collect data from the byte buffer 
		// and the buf[] extracted from the output stream is used 
		// to create the input stream for the input handler
		this.outputStream = new ByteArrayOutputStream(buffer.capacity());
		
		if (logger.isLoggable(Level.FINER)){
			logger.finer(this + " Object created!");
		}
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		if (socketChannel.isOpen() && socketChannel.isConnected()) {
			
			if (receivingDataAndProcessing){
				
				// clean the output stream to be used for storing the result
				outputStream.reset();
							
				// pass the request to the handler to prepare the busy response
				inputHandler.process(
						null, 
						outputStream,
						authorizationLogger,
						authLoggerClientKey);
				
				// prepare the output buffer
				if (outputStream.size() > 0){
					outputBuffer = outputStream.toByteArray();
					outputBufferRemaining = outputBuffer.length;
				}

				// change the state
				receivingDataAndProcessing = false;
				
				// prepare for writing
				try {
					dispatcher.changeKeyInterestedOps(selectionKey, SelectionKey.OP_WRITE);
				} catch (ClosedChannelException e) {
					String errorMsg = "Dispatcher key change to write error:" + e.getMessage(); 
					logger.severe(this + errorMsg);
					selectionKey.cancel();
					// try to close the socket if not already closed
					try {
						socketChannel.close();
					} catch (IOException ioe) {
						if (logger.isLoggable(Level.FINE)){
							logger.fine(this + 
									"Error while closing the socket after a dispatcher key change error:" + ioe.getMessage());
						}
					}
					
					performanceMonitor.busyResponseDeliveryError();					
					authorizationLogger.setDecision(authLoggerClientKey, AuthorizationLogger.DECISION_ERROR);
					authorizationLogger.dismiss(authLoggerClientKey, errorMsg);
				}
				
			} else {
				
				try{
					send();
					authorizationLogger.dismiss(authLoggerClientKey);
				} catch (IOException e) {
					String errorMsg = "Busy response send error:" + e.getMessage(); 
					logger.severe(this + errorMsg);
					
					selectionKey.cancel();
					// try to close the socket if not already closed
					try {
						socketChannel.close();
					} catch (IOException ioe) {
						if (logger.isLoggable(Level.FINE)){
							logger.fine(this + 
									"Error while closing the socket after a send error:" + ioe.getMessage());
						}
					}
					
					performanceMonitor.busyResponseDeliveryError();
					authorizationLogger.setDecision(authLoggerClientKey, AuthorizationLogger.DECISION_ERROR);
					authorizationLogger.dismiss(authLoggerClientKey, errorMsg);					
				}
			}
		} else {
			String errorMsg = "";
			if (!socketChannel.isOpen()) {
				errorMsg = "socket channel not opened anymore!";
				logger.severe(this + errorMsg);
			} else if (!socketChannel.isConnected()) {
				errorMsg = "socket channel not connected anymore!";
				logger.severe(this + errorMsg);
			}
			
			// maybe unregister channel should be called???
			selectionKey.cancel();
			
			try {
				socketChannel.close();
			} catch (IOException e) {
				if (logger.isLoggable(Level.FINE)){
					logger.fine(this + 
						"Error while closing the socket after a socket error:" + e.getMessage());
				}
			}
			
			performanceMonitor.busyResponseDeliveryError();	

			authorizationLogger.setDecision(authLoggerClientKey, AuthorizationLogger.DECISION_ERROR);
			authorizationLogger.dismiss(authLoggerClientKey, errorMsg);
		}
	}
	
	/**
	 * Sent the data to the client
	 * @throws IOException
	 */
	private void send() throws IOException {
		if (outputBufferRemaining > 0){
			if (logger.isLoggable(Level.FINER)){
				logger.finer(this + "Data size to be sent:" + outputBufferRemaining);
			}
			
			buffer.clear();
			buffer.put(
					outputBuffer,
					outputBuffer.length - outputBufferRemaining, 
					Math.min(buffer.capacity(), outputBufferRemaining));
			buffer.flip();
			try {
				outputBufferRemaining -= socketChannel.write(buffer);
			} catch (NotYetConnectedException e ) {
				logger.severe(this + "Socket write exception due to the channel is not yet connected:" 
						+ e.getMessage());
				throw e;
			} catch (ClosedByInterruptException e) {
				logger.severe(this + "Socket write exception due to another thread interrupts the current thread while the read operation is in progress, thereby closing the channel and setting the current thread's interrupt status:" 
						+ e.getMessage());
				throw e;
			} catch (AsynchronousCloseException e) {
				logger.severe(this + "Socket write exception due to another thread closes this channel while the read operation is in progress:" 
						+ e.getMessage());
				throw e;
			} catch (ClosedChannelException e) {
				logger.severe(this + "Socket write exception due to the channel is closed:" 
						+ e.getMessage());
				throw e;
			}
			
			if (logger.isLoggable(Level.FINER)){
				logger.finer(this + "Remaining data to be sent:" + outputBufferRemaining);
			}
		}
		
		if (outputBufferRemaining > 0) {
			// prepare for the next writing
			dispatcher.changeKeyInterestedOps(selectionKey, SelectionKey.OP_WRITE);
		} else {
			if (logger.isLoggable(Level.FINER)){
				logger.fine(this + "Data sent! Canceling the key and closing the socket");
			}
			
			// the writing is done
			selectionKey.cancel();
			try {
				socketChannel.socket().shutdownOutput();
			} catch (IOException e) {
				logger.warning(this + "IOException when shutting down the socket channel:" + e.getMessage());
			}
			
			try {
				socketChannel.close();
			} catch (IOException e) {
				logger.warning(this + "IOException when closing the socket channel:" + e.getMessage());
			}
			
			performanceMonitor.busyResponseSentToClient();
		}
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return this.identifier;
	}
}
