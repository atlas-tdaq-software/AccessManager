package am.server.NIOReactor;

/**
 * The factory class interface for input handler objects.
 * @author mleahu
 *
 */
public interface InputHandlerFactory {
	
	/**
	 * The input handler types
	 */
	public static final int HANDLER_TO_PROCESS_REQUEST = 0;
	public static final int HANDLER_TO_RESPOND_BUSY = 1;
	
	public static final String[] HANDLER_TYPES = {
		"Handler to process a request from client",
		"Handler to respond allways with server busy"
	};
	
	/**
	 * Return the input handler object.
	 * @return InputHandler object
	 */
	public InputHandler getInputHandler(int handlerType);

}
