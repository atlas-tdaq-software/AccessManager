/**
 * 
 */
package am.server.NIOReactor;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.util.logging.Level;
import java.util.logging.Logger;

import am.server.control.AccessManagerServerController;

/**
 * @author mleahu
 *
 */
public class ControlChannelHandler implements Runnable {
	
    private static Logger logger =
        Logger.getLogger(ControlChannelHandler.class.getName());
    
	private Dispatcher dispatcher;
	private SelectionKey selectionKey;
	private AccessManagerServerController amServerController;
	
	private DatagramChannel controlChannel;
	private SocketAddress clientAddress;
	
	// the byte buffer size is 128;
	private byte[] messageBuffer = new byte[128];
	private ByteBuffer buffer = ByteBuffer.wrap(messageBuffer);
	private String message = null;

	public ControlChannelHandler(
			Dispatcher dispatcher, 
			SelectionKey selectionKey,
			AccessManagerServerController amServerController) {
		this.dispatcher = dispatcher;
		this.selectionKey = selectionKey;
		this.amServerController = amServerController;
		this.controlChannel = (DatagramChannel)selectionKey.channel();
		this.clientAddress = null;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		try {			
			if (this.selectionKey.isReadable()) {
				message = readMessage();
				if (message != null && message.length() > 0){
					
					message = amServerController.processMessage(message, clientAddress);
					
					// if the response message is null, then don't send any response
					if (message == null) {
						if (logger.isLoggable(Level.FINE)){
							logger.fine("No response from the amServerController,waiting another message");
						}
					} else {
						// send back the response 
						if (logger.isLoggable(Level.FINE)){
							logger.fine("Sending response message [" + message + "]");
						}
						sendMessage();
					}
				}
				
				//prepare to read another message
				clientAddress = null;
				message = null;				
				dispatcher.changeKeyInterestedOps(this.selectionKey, SelectionKey.OP_READ);
			}			
		} catch (ClosedChannelException e) {
			logger.severe(this + "Error:" + e.getMessage());
		}		
	}

	/**
	 * Receive a message sent over a datagram channel
	 * @return the message as a string
	 */
	private String readMessage(){
		String message = null;
		try {
			buffer.clear();
			clientAddress = controlChannel.receive(buffer);
			if (clientAddress != null){
				message = new String(messageBuffer, 0, buffer.position());				
				if (logger.isLoggable(Level.FINE)){
					logger.fine(this + "Message received from " +
							"[" + clientAddress + "]" +
							":" + message);
				}
			} else if (logger.isLoggable(Level.FINEST)){
				logger.finest(this + "Datagram socket not immediately available!");
			}
		} catch (IOException e) {
			logger.warning(this + "Could not read the message! Error:" + e.getMessage());
		}
		return message;
	}
	
	/**
	 * Send a message to the requester. If the message has a zero size, then it is silently discarded.
	 */
	private void sendMessage(){
		if (this.clientAddress == null){
			logger.warning(this + "Can not send the response to a null client address");
			return;
		}
		if (message == null){
			logger.warning(this + "Can not send a null response to the address" + clientAddress);
			return;
		}
		
		if (message.length() == 0 && logger.isLoggable(Level.FINEST)){
			logger.finest(this + "Don't send empty message to " + clientAddress);
		}
		
		if (message.length() > messageBuffer.length){
			message = message.substring(0, messageBuffer.length - 1);
			logger.warning("Message to be sent is longer than the buffer size [" + messageBuffer.length + "] " +
					" and will be truncated to [" + message + "]");
		}
		if (logger.isLoggable(Level.FINE)){
			logger.fine(this + "Sending the message " +
					"[" + message + "] of length "+ message.length() + 
					" to [" + clientAddress + "]");
		}
		
		ByteBuffer tempBuffer = ByteBuffer.wrap(message.getBytes());
		try {
			int numbytes = controlChannel.send(tempBuffer, clientAddress);
			if (logger.isLoggable(Level.FINE)){
				logger.fine(this + "Sent " + numbytes + " bytes.");
			}
		} catch (IOException e) {
			logger.warning(this + "Error while sending message to " + clientAddress);
		}
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return "[CTRL CHANNEL]";
	}
}
