package am.server.NIOReactor;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.concurrent.Executor;
import java.util.logging.Level;
import java.util.logging.Logger;

import am.server.authlogger.AuthorizationLogger;
import am.server.control.AccessManagerServerController;
import am.util.AMException;

/**
 * The reactor object runs in a separate thread as a server socket, accept connections
 * and plays the dispatcher role for other events like read/write operations.
 * @author mleahu
 * @version 1.0
 */
public class Reactor implements Dispatcher, Runnable {

    private static Logger logger =
        Logger.getLogger(Reactor.class.getName());
    
    /**
     * The selector guard object idiom
     */
    private Object guard = new Object();
    
	/**
	 * The selector object
	 */
	private Selector selector;
	
	/**
	 * The server socket channel object 
	 */
	private ServerSocketChannel serverSocket;
	
	/**
	 * The port used by the server socket.
	 */
	private int port;
	
	/**
	 * The local IP address to use; to be used in future; 
	 */
	private InetAddress ifAddress;
	
	/**
	 * how many connections are queued
	 */
	private int backlog; 

	/**
	 * The UDP channel used to receive notifications and report the status
	 */
	private DatagramChannel controlChannel;
	
	/**
	 * The input handler factory object to be passed to the handler logic objects
	 */
	private InputHandlerFactory inputHandlerFactory;
	
	/**
	 * Pools
	 */
	private Executor threadPool;
	
	private boolean initialized;
	private boolean isRunning;
	
	/**
	 * The performance monitor object.
	 */
	private PerformanceMonitor performanceMonitor;
	
	/**
     * The AM server authorization logger object
     */
    private AuthorizationLogger authorizationLogger;
    
	/**
	 * Creates a reactor object that opens a server socket on the port number provided and
	 * uses the acceptor object for new connections.
	 * @param port port number on which the server will listen
	 * @param backlog the backlog value
	 * @param threadPool the thread pool to process the authorization requests
	 * @param inputHandlerFactory the input handler factory
	 * @param performanceMonitor the performance monitor object
	 * @param amServerController the controller to process the messages sent by clients
	 * @param authorizationLogger the authorization logger object
	 * @throws AMException
	 */
	public Reactor(
			int port,
			int backlog,
			Executor threadPool,
			InputHandlerFactory inputHandlerFactory,
			PerformanceMonitor performanceMonitor,
			AccessManagerServerController amServerController,
			AuthorizationLogger authorizationLogger) throws AMException {
		
		this.selector = null;
		this.serverSocket = null;
		this.controlChannel = null;
		this.port = port;
		this.ifAddress = null;
		this.backlog = backlog;
		this.threadPool = threadPool;
		this.inputHandlerFactory = inputHandlerFactory;
		this.performanceMonitor = performanceMonitor;
		this.authorizationLogger = authorizationLogger;
		this.initialized = false;
		this.isRunning = false;

        init(amServerController);
		
	}
	
	/**
	 * Initialize the reactor object by setting up the server socket and the selection object.
	 * @param amServerController
	 * @throws AMException
	 */
	private void init(AccessManagerServerController amServerController) throws AMException {
		
		try{
			selector = Selector.open();
			
			serverSocket = ServerSocketChannel.open();
			SocketAddress socketAddress = null;
			if (ifAddress != null){
				logger.config("Initializing network server socket: " +
						"[port=" + port + "] " +
						"[backlog=" + backlog + "] " +
						"[ifAddress=" + ifAddress + "]");
				socketAddress = new InetSocketAddress(ifAddress, port);				
			}
			else{
				logger.config("Initializing network server socket: " +
						"[port=" + port + "] " +
						"[backlog=" + backlog + "] ");
				socketAddress = new InetSocketAddress(port);				
			}
			serverSocket.socket().bind(socketAddress, backlog);
			// use it in non blocking mode
			serverSocket.configureBlocking(false);
			logger.info("Socket server successfully initialized!");
			
			////////////////////////////////////
			// initialize the datagram channel
			////////////////////////////////////
			controlChannel = DatagramChannel.open();
			int controlPort = port + 1;
			logger.config("Initializing control socket: " +
					"[port=" + controlPort + "] ");
			SocketAddress controlAddress = new InetSocketAddress(controlPort);				
			controlChannel.socket().bind(controlAddress);			
			// use it in non blocking mode
			controlChannel.configureBlocking(false);
			logger.info("Control socket successfully initialized!");
			
			this.initialized = true;
			
		} catch (Exception e){
			logger.severe("Server failed to start! Error: " + e.getMessage());
			throw new AMException(
					"Server failed to start on port " + port + ". Error: " + e.getMessage(), 
					AMException.AM_SRV_INIT_ERROR);
		}
		
		
		
		try {
			// register the server socket with the selector to accept connections
			serverSocket.register(selector, SelectionKey.OP_ACCEPT);
			
			// register the control channel for read operations			
			SelectionKey ctrlSelectionKey = controlChannel.register(selector, SelectionKey.OP_READ);
			ControlChannelHandler controlChannelHandler = 
				new ControlChannelHandler(this, ctrlSelectionKey, amServerController);
			ctrlSelectionKey.attach(controlChannelHandler);
			
		} catch (ClosedChannelException e) {
			logger.severe("Failed to register the server socket with the selector to accept connections! Error: " + e.getMessage());
			throw new AMException(
					"Failed to register the server socket with the selector to accept connections. Error: " + e.getMessage(),
					AMException.AM_SRV_INIT_ERROR);
		}
	}	
	
	/**
	 * Check if the server is initialized.
	 * @return
	 */
	public boolean isInitialized(){
		return initialized;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		logger.info("Reactor started!");
		isRunning = true;
		try {
			while (! Thread.interrupted() && isRunning) {
				synchronized (guard) {
					// suspend the dispatcher thread if the guard is locked
				}
				selector.select();
				
				// check if the reactor is being stopped
				if (!selector.isOpen()) {
					continue; // the "isRunning" should be false by now 
				}
				
				Iterator<SelectionKey> it = selector.selectedKeys().iterator();
				while (it.hasNext()){
					dispatch(it.next());
					it.remove();
				}
			}
		} catch (IOException ex) {
			logger.severe("IO exception: " + ex.getMessage());
		}
	}
	
	/**
	 * Runs the key attachment in the thread pool.
	 * @param k the selection key
	 */
	private void dispatch(SelectionKey key) {
		if (key.isAcceptable()) {
			// accept the new connection
			this.accept(key);
		} else {
			try {
				// stop the event notifications while processing
				// just change the interested ops to 0
				changeKeyInterestedOps(key, 0);

				// the key operation should be here READ or WRITE
				Runnable r = (Runnable)(key.attachment());
				if (r != null ){
					if (logger.isLoggable(Level.FINE)) {
						logger.fine("Executing the key attachment:" + r);
					}
					try {
						// run the attachment
						threadPool.execute(r);
					} catch (Exception e){
						logger.severe("Thread pool execution exception:" + e.getMessage());
						e.printStackTrace();
					}
				} else {
					logger.severe("Selection key without attachment!");
				}

			} catch (ClosedChannelException e) {
				logger.severe("Invalid key because the channel is closed:" + e.getMessage());
				unregisterChannel(key);
			}

		}
	}

	/* (non-Javadoc)
	 * @see am.server.NIOReactor.Dispatcher#changeKeyInterestedOps(java.nio.channels.SelectionKey, int)
	 */
	public SelectionKey changeKeyInterestedOps(SelectionKey key, int ops) throws ClosedChannelException {
		synchronized (guard) {
			this.selector.wakeup();
			return key.interestOps(ops);
		}
	}

	/* (non-Javadoc)
	 * @see am.server.NIOReactor.Dispatcher#unregisterChannel(java.nio.channels.SelectionKey)
	 */
	public void unregisterChannel(SelectionKey key) {
		synchronized (guard) {
			this.selector.wakeup();
			key.cancel();
		}
	}
	
	/**
	 * Stop the reactor thread
	 */
	public synchronized void stop(){
		isRunning = false;
		try {
			if (selector.isOpen()){
				selector.close();
				logger.info("Selector closed!");
			}
			if (serverSocket.isOpen()){
				serverSocket.close();
				logger.info("Server socket closed!");
			}
			if (controlChannel.isOpen()){
				controlChannel.close();
				logger.info("Control channel socket closed!");
			}
		} catch (IOException e) {
			logger.severe("Close failed:" + e.getMessage());
		}
	}
	
	/**
	 * Accepts a new connection
	 * @param key the key that has the ACCEPT operation enabled
	 */
	public void accept(SelectionKey key) {
		try {
			SocketChannel clientChannel = serverSocket.accept();
			if (clientChannel == null){
				logger.severe("Connection accept failed! Returned socket is null.");
			} else {
				if (logger.isLoggable(Level.FINE)) {
					logger.fine("Connection accepted from " + clientChannel.socket().getInetAddress());
				}
				
				try {
					clientChannel.configureBlocking(false);
				} catch (IOException e) {
					logger.severe("Can not set the socket in non blocking mode:" + e.getMessage());
				}
				
				try {
					clientChannel.socket().setTcpNoDelay(true);
				} catch (SocketException se) {
					logger.severe("Can not set the socket to TCP NO DELAY:" + se.getMessage());
				}
								
				synchronized (guard) {
					this.selector.wakeup();
					// register the client channel for read  without attachment JUST to get the key
					SelectionKey clientKey = clientChannel.register(this.selector, SelectionKey.OP_READ);
					// create the handler logic object for this client
					Object handler;
					
					if (performanceMonitor.overloaded()) {
						// the request will not be processed and a BUSY answer will be sent back to the client
						handler = 
							new BusyHandlerLogic(
									this, 
									clientKey, 
									this.inputHandlerFactory, 
									this.performanceMonitor,
									this.authorizationLogger);
						performanceMonitor.newRequestArrivedWhenServerIsBusy();
					} else {
						// the request will be processed
						handler = 
							new HandlerLogic(
									this, 
									clientKey, 
									this.inputHandlerFactory,
									this.performanceMonitor,
									this.authorizationLogger);						
						performanceMonitor.newRequestToProcess();
					}
					
					// attach the object to the key
					clientKey.attach(handler);
				}				
			}
		} catch (IOException e) {
			logger.severe("Connection can not be accepted: " + e.getMessage());
		}
	}
}
