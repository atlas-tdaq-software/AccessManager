/**
 * 
 */
package am.tests.client;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import junit.framework.JUnit4TestAdapter;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import am.client.RequestorInfo;
import am.client.ServerInterrogator;
import am.pap.input.PoliciesInput;
import am.pap.input.PoliciesInputFromFiles;
import am.util.AMCommon;
import am.util.AMException;
import am.util.AMLogger;
import am.xacml.context.Resource;
import am.xacml.context.impl.ResourceFactory;

/**
 * @author mleahu
 *
 */
public class ServerInterrogatorTest {

	/**
	 * The logs directory 
	 */
	private static String LOGS_DIR = 
		"logs" + AMCommon.getFileSeparator() + 
		"ServerInterrogator" + AMCommon.getFileSeparator();

	private static String inputFileName;
	private static PoliciesInput policiesInput;
		
	/**
	 * The requester information is usually initialized once at the beginning of run.
	 */
	private static RequestorInfo accessSubject;
	
	/**
	 * Server interrogator object
	 */
	private static ServerInterrogator srv;
	
	/**
	 * The AM specific logger 
	 */
	private static AMLogger amLogger;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		// set the loggers
		try {
			amLogger = new AMLogger(LOGS_DIR);
			amLogger.enableLoggingToFile("am.client", AMLogger.LOG_LEVEL_ID_ALL);
			amLogger.enableLoggingToFile("am.rbac", AMLogger.LOG_LEVEL_ID_ALL);
			amLogger.enableLoggingToFile("am.pap", AMLogger.LOG_LEVEL_ID_ALL);
			amLogger.enableLoggingToFile("am.util", AMLogger.LOG_LEVEL_ID_ALL);
			amLogger.enableLoggingToFile("am.communication", AMLogger.LOG_LEVEL_ID_ALL);
			amLogger.enableLoggingToFile("am.xacml.context", AMLogger.LOG_LEVEL_ID_ALL);
			amLogger.disableAllLogging("");
		} catch (AMException e) {
			System.err.println("Could not set up the loggers!");
		}		
		
		inputFileName = "AMrules.txt";
		policiesInput = new PoliciesInputFromFiles(inputFileName);
		// read the rules from the file
		policiesInput.getAttributes();

		accessSubject = new RequestorInfo();
		srv = new ServerInterrogator();		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		amLogger.disableLoggingToFile();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		Set<String> rules = policiesInput.getValues("Rule");
		
		if (rules.size() == 0) {
			System.err.println("No rules found in file:" + inputFileName);
			System.exit(1);
		} else {
			System.out.println("Rules found in the input file [" + inputFileName + "]:" + rules.size());
		}
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link am.client.ServerInterrogator#isAuthorizationGranted(am.xacml.context.Resource, am.client.RequestorInfo)}.
	 */
	@Test
	public void testIsAuthorizationGrantedResourceRequestorInfo() {
		List<HashMap<String, List<String>>> maps;

		assertNotNull("Check requestor info object", accessSubject);
		assertNotNull("Check server interrogator", srv);
		
		int current = 0;
		// for each rule, run the tests
		for(String rule: policiesInput.getValues("Rule")){
			current++;
			maps = policiesInput.getAttributesForKeyValue("Rule", rule);
			System.out.println("(" + current + ") Rules=[" + rule + "]");
			Resource resource = null;
			for(HashMap<String, List<String>> attributesMap: maps) {			

				if (attributesMap.containsKey("Role")) {
					// skip this set of attributes map because it is in fact a rules to role assignment
					// we are interested only in rules definitions
					continue;
				}
				
				long timeStart = System.currentTimeMillis();
				
				// prepare the resource action object
				try {
					// create the resource object;
					resource = ResourceFactory.getResource(attributesMap);					
					System.out.print(attributesMap);
					assertEquals("Check authorization for: " + attributesMap,
							true, srv.isAuthorizationGranted(resource, accessSubject));
				} catch (AMException e) {
					System.out.println("Server interrogation error:" + e.getMessage());
					if (e.getErrorCode() == AMException.AM_CLNT_COMMUNICATION_ERROR){
						fail("Test aborted!");
						return;
					}
				}
				
				System.out.println(" *** STATUS[" + srv.getStatusMessage() + "]*** " + 
						" Time(ms)=[" + (System.currentTimeMillis() - timeStart) + "]");
			}
						
		}
	}
	
	public static junit.framework.Test suite() {
		return new JUnit4TestAdapter(ServerInterrogatorTest.class);
	}

	public static void main(String[] args) {
		junit.textui.TestRunner.run(suite());
	}

}
