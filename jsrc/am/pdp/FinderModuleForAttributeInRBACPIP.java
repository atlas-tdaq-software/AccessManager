package am.pdp;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import am.pip.RBACPIP;
import am.rbac.RBACUser;
import am.rbac.Role;
import am.util.AMException;
import am.util.cache.CacheUser;
import am.util.cache.Cachetable;

import am.xacml.context.*;

import com.sun.xacml.EvaluationCtx;
import com.sun.xacml.attr.AttributeDesignator;
import com.sun.xacml.attr.AttributeFactory;
import com.sun.xacml.attr.AttributeValue;
import com.sun.xacml.attr.BagAttribute;
import com.sun.xacml.cond.EvaluationResult;
import com.sun.xacml.finder.AttributeFinderModule;
import com.sun.xacml.ctx.*;

/**
 * This attribute finder looks up in the RBAC DB for information needed to process
 * the policies. The typical information is the roles assigned or enabled for an user.
 * @author mleahu
 * @version 1.0
 */
public class FinderModuleForAttributeInRBACPIP extends AttributeFinderModule implements CacheUser{

	private static final Logger logger =
        Logger.getLogger(FinderModuleForAttributeInRBACPIP.class.getName());
	
	
	/**
	 * The evaluation result cache enabled
	 */
	private boolean erCacheEnabled;
	/**
	 * The evaluation result cache expiration time
	 */
	private static long ER_CACHE_TIMEOUT = 5000; // 5 sec
	/**
	 * The evaluation result cache indexed by context; key=EaluationCtx, value=EvaluationResult
	 */
	private Cachetable<EvaluationCtx> evaluationResultCache;
	
	/**
	 * Create an AttributeValue object based on the data type identifier
	 * and using the attribute factory.
	 * @param dataType the URI defining the type of the attribute
	 * @param value the value of attribute
	 * @return the AttributeValue object
	 */
	protected static AttributeValue createAttribute(URI dataType, String value) {
		try {
			AttributeFactory factory = AttributeFactory.getInstance();
			return factory.createValue(dataType, value);
		} catch (Exception e) {
			logger.severe("Could not create the attribute: type [" + dataType + "] value [" + value + "]");
			return null;
		}
	}
	
	/**
	 * The RBAC DB to be used by this attribute finder. 
	 */
	private RBACPIP pip;
	
	/**
	 * Creates an attribute finder to look up attributes in RBAC specific DB
	 * @param pip
	 */
	public FinderModuleForAttributeInRBACPIP(RBACPIP db){
		this.pip = db;
		
		// enable the cache
		this.erCacheEnabled = true;
		
		// initialize the cache
		if(this.erCacheEnabled){
			this.evaluationResultCache = new Cachetable<EvaluationCtx>(ER_CACHE_TIMEOUT, "evaluation result");
			logger.fine("Evaluation result cache enabled! Timeout (ms):" + ER_CACHE_TIMEOUT);
		}
	}
	
    /**
     * Returns true always because this module supports designators.
     *
     * @return true always
     */
    public boolean isDesignatorSupported() {
        return true;
    }
	
    /**
     * Returns a <code>Set</code> with a single <code>Integer</code>
     * specifying that subject attributes are supported by this
     * module.
     *
     * @return a <code>Set</code> with
     * <code>AttributeDesignator.SUBJECT_TARGET</code> included
     */
    public Set<Integer> getSupportedDesignatorTypes() {
        HashSet<Integer> set = new HashSet<Integer>();
        set.add(new Integer(AttributeDesignator.SUBJECT_TARGET));
        return set;
    }
	
    /**
     * Get the values of a subject attribute from the context 
     * @param attributeId the attribute id
     * @param category subject category
     * @param context context object
     * @return list of values
     */
	private List<String> getSubjectAttribute(URI attributeId, URI category, EvaluationCtx context){
    	List<String> values = new ArrayList<String>();
		EvaluationResult sbjAttr = context.getSubjectAttribute(
				Subjects.DEFAULT_DATA_TYPE,
				attributeId,
				category);
		
		AttributeValue attrV = sbjAttr.getAttributeValue();
		
		if (attrV.isBag()){
			for(Iterator<AttributeValue> it = (Iterator<AttributeValue>)((BagAttribute)attrV).iterator(); it.hasNext();){
				values.add(it.next().encode());
			}
    	}
    	
    	return values;
    }
    
    /**
     * Used to get roles enabled for the user who's requesting the access.
     * If one of those values isn't being asked for, or if the types are wrong,
     * then an empty bag is returned.
     *
     * @param attributeType the datatype of the attributes to find, which
     *                      must be 'anyURI' (Role.DATA_TYPE_ROLE) for this module
     *                      to resolve a value
     * @param attributeId the identifier of the attributes to find, which
     *                    must be Role.IDENTIFIER_SUBJECT_ROLE for
     *                    this module to resolve a value
     * @param issuer the issuer of the attributes, or null if unspecified
     * @param subjectCategory the category of the attribute or null, which
     *                        ignored since this only handles non-subjects
     * @param context the representation of the request data
     * @param designatorType the type of designator, which must be
     *                       SUBJECT_TARGET for this module to resolve
     *                       a value
     *
     * @return the result of attribute retrieval, which will be a bag with
     *         a single attribute, an empty bag, or an error
     */
    public EvaluationResult findAttribute(URI attributeType, URI attributeId,
                                          URI issuer, URI subjectCategory,
                                          EvaluationCtx context,
                                          int designatorType) {
        // we only know about subject attributes
        if (designatorType != AttributeDesignator.SUBJECT_TARGET)
            return new EvaluationResult(BagAttribute.
                                        createEmptyBag(attributeType));

        // figure out which attribute we're looking for   	
		if (attributeId.equals(Role.IDENTIFIER_SUBJECT_ROLE)) {
			return handleRole(attributeType, issuer, subjectCategory, context);
		}
		        
        // if we got here, then it's an attribute that we don't know
        return new EvaluationResult(BagAttribute.
                                    createEmptyBag(attributeType));
    }
    
    /**
     * Get the user's role information. The user name is retrieved from the context where
     * the request details are. Hence, the evaluation result for a context object can be cached 
     * so that the next time the role information is requested in the same context,
     * the RBAC PIP is not needed to be interrogated again. 
     * @param attributeType
     * @param issuer
     * @param subjectCategory
     * @param context
     * @return the evaluation result object
     */
    private EvaluationResult handleRole(
    		URI attributeType,
    		URI issuer,
    		URI subjectCategory,
    		EvaluationCtx context){
    	
    	EvaluationResult er = null;
    	
    	// check the context cache for role information
    	if (erCacheEnabled){
    		er = (EvaluationResult)evaluationResultCache.get(context);
    		
    		if (er != null){
        		if (logger.isLoggable(Level.FINEST)){
        			logger.finest("Cache hit: " +
        					"Evaluation Result for Evaluation Context object " + context);
        		}
    			return er;
    		} else{
        		if (logger.isLoggable(Level.FINEST)){
        			logger.finest("The Evaluation Result for Evaluation Context object " + 
        					context + " not found in the cache");
        		}    			
    		}
    	}
    	
    	// check only the roles for the access subjects
    	if (subjectCategory.equals(Subjects.CATEGORY_ACCESS_SUBJECT)){	
	        List<String> requestorsUserNames = getSubjectAttribute(
					Subjects.IDENTIFIER_SUBJECT_ID, 
					subjectCategory,
					context);
	    	
			if (requestorsUserNames.isEmpty()){
				return makeProcessingResult(Status.STATUS_PROCESSING_ERROR, "Could not identify the requestor's user name!");
			}
	
			// consider only the first user name if exists
			String username = requestorsUserNames.get(0);
			
			if (logger.isLoggable(Level.FINE)){
				logger.fine("Lookup the user [" + username + "] in the DB for roles information");
			}
			
			try {
				RBACUser user = pip.getUser(username);
				
				if (! user.isRBACValid()){
		            er = makeProcessingResult(Status.STATUS_OK, "The subject [" + username + "] has no roles assigned!");
				} else {
					// the user is RBAC valid, so let's get his ENABLED roles and put them in a bag
					Set<AttributeValue> set = new HashSet<AttributeValue>();

					for (String roleEnabled : user.getRolesEnabled()) {
						set.add(createAttribute(Role.DATA_TYPE_ROLE, roleEnabled));
					}

					if (logger.isLoggable(Level.FINEST)) {
						logger.finest("Enabled roles for " + user + "=[" + set + "]");
					}
					if (set.isEmpty()) {
						er = makeProcessingResult(Status.STATUS_OK, "The subject [" + username + "] has no roles enabled!");
					} else {
						er = new EvaluationResult(new BagAttribute(Role.DATA_TYPE_ROLE, set));
					}
				}

			} catch (AMException e) {
				if (e.getErrorCode() == AMException.USER_NOT_FOUND){
					logger.log(Level.FINE, "The requestor's user name was not found in DB: {0}", e.getMessage());
		    		er = makeProcessingResult(Status.STATUS_OK, "The requestor's user name was not found in the DB");
				} else if (e.getErrorCode() == AMException.LDAP_SEARCH_FAILED){
					logger.log(Level.FINE, "Failed lookup for roles information for user {0}", username);
					// do not store evaluation result in the cache when LDAP search failed
					return makeProcessingResult(Status.STATUS_PROCESSING_ERROR, "Failed lookup for roles information for user " + username + ": " + e.getMessage());
				} else {
					logger.fine("DB error");
					er = makeProcessingResult(Status.STATUS_PROCESSING_ERROR, "DB error: " + e.getMessage());
				}
			}
	    
    	} else {
    		logger.log(Level.FINE, "Subject skipped because is not in the access subject category, but [ ${0} ]", subjectCategory);
    		er = makeProcessingResult(Status.STATUS_OK, "Subject skipped because is not in the access subject category!");
    	}
    	
		if (erCacheEnabled){
			// add the evaluation result to the cache
			evaluationResultCache.put(context, er);
    		if (logger.isLoggable(Level.FINEST)){
    			logger.finest("Cache update: " +
    					"Put Evaluation Result object [" + er + "] " +
    					"for Evaluation Context object [" + context + "]");
    		}			
		}
		
        return er;
    }


    /**
     * Private helper that generates new processing status and
     * includes the given string.
     */
    private EvaluationResult makeProcessingResult(String status, String message) {
        ArrayList<String> code = new ArrayList<String>();
        code.add(status);
        return new EvaluationResult(new Status(code, message));
    }

	/* (non-Javadoc)
	 * @see com.sun.xacml.finder.PolicyFinderModule#invalidateCache()
	 */
	public void invalidateCache() {
		int pipEntries = pip.flushCache(); 
		if (logger.isLoggable(Level.INFO)){
			logger.info("Flush the PIP cache (" + pipEntries + " entries) and evaluation results (" + evaluationResultCache.size() + " entries)");
		}
		evaluationResultCache.clear();
	}

//    /**
//     * Private helper that makes a bag containing only the given attribute.
//     */
//    private EvaluationResult makeBag(AttributeValue attribute) {
//        Set<AttributeValue> set = new HashSet<AttributeValue>();
//        set.add(attribute);
//
//        BagAttribute bag = new BagAttribute(attribute.getType(), set);
//
//        return new EvaluationResult(bag);
//    }
//    
    
}
