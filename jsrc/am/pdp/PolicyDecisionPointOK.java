/**
 * 
 */
package am.pdp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.logging.Logger;

import am.server.NIOReactor.InputHandler;
import am.server.authlogger.AuthorizationLogger;

/**
 * The Policy Decicions Point object that handles the clients requests.
 * @author mleahu
 *
 */
public class PolicyDecisionPointOK implements InputHandler {

	private static final Logger logger =
        Logger.getLogger(PolicyDecisionPointOK.class.getName());

	/* (non-Javadoc)
	 * @see am.server.NIOReactor.InputHandler#process(java.io.ByteArrayInputStream, java.io.ByteArrayOutputStream)
	 */
	public void process(
			ByteArrayInputStream inputStream, 
			ByteArrayOutputStream outputStream,
			AuthorizationLogger authorizationLogger,
			Long authLoggerClientKey) {
		
		logger.fine("Input stream available:" + inputStream.available());
		String str = "";
		int c;		
		while ((c=inputStream.read())!= -1)
			str +=(char)c;
		
		logger.fine("Input stream content:" + str);
		
		str = 
			"<Response>" +
			"  <Result ResourceId=\"\">" +
			"    <Decision>Permit</Decision>" +
			"    <Status>" +
			"      <StatusCode Value=\"urn:oasis:names:tc:xacml:1.0:status:ok\"/>" +
			"    </Status>" +
			"  </Result>" +
			"</Response>";
		for(int i=0;i <str.length();i++)
			outputStream.write(str.charAt(i));
		logger.fine("Output stream content:" + str);
	}

}
