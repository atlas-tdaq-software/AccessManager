/**
 * 
 */
package am.pdp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.sun.xacml.Indenter;
import com.sun.xacml.PDP;
import com.sun.xacml.PDPConfig;
import com.sun.xacml.ParsingException;
import com.sun.xacml.ctx.RequestCtx;
import com.sun.xacml.ctx.ResponseCtx;
import com.sun.xacml.ctx.Result;
import com.sun.xacml.ctx.Status;
import com.sun.xacml.finder.AttributeFinder;
import com.sun.xacml.finder.AttributeFinderModule;
import com.sun.xacml.finder.PolicyFinder;
import com.sun.xacml.finder.PolicyFinderModule;
import com.sun.xacml.finder.ResourceFinder;
import com.sun.xacml.finder.impl.CurrentEnvModule;

import am.pap.storage.PolicyStorage;
import am.pap.storage.PolicyStorageFactory;
import am.pip.RBACPIP;
import am.server.NIOReactor.InputHandler;
import am.server.authlogger.AuthorizationLogger;
import am.util.AMException;
import am.util.cache.CacheUser;
import am.xacml.context.ResponseProcessor;

/**
 * @author mleahu
 *
 */
public class PolicyDecisionPoint implements InputHandler {

	private static final Logger logger =
        Logger.getLogger(PolicyDecisionPoint.class.getName());
	
	/**
	 * The policy storage object
	 */
	PolicyStorage policyStorage;
	
	/**
	 * The list of Role Policy Set ids; it plays the role of PAP
	 */
	private List<URI> RPSs;
	
	/**
	 * The PIP to be used;
	 */
	private RBACPIP rbacPIP; 
		
	
	private CacheUser PIPCacheUser;
	private CacheUser PAPCacheUser;
	
	/**
	 * The static indenter to be used during the response encoding
	 */
	private static Indenter responseIndenter = new Indenter(0);
	
	/**
	 * The PDP configuration object.
	 */
	private PDPConfig pdpConfig;
	
	private class ThreadLocalPDP extends ThreadLocal<PDP>{

		/* (non-Javadoc)
		 * @see java.lang.ThreadLocal#initialValue()
		 */
		@Override
		protected PDP initialValue() {
			PDP pdp = new PDP(pdpConfig);
		    if (pdp == null){
				logger.severe("Could not create a PDP object");
		    }	    
			return pdp;
		}
	}
	/**
	 * The Sun XACML PDP object
	 */
	private ThreadLocalPDP threadLocalPDP;
	
	
	/**
	 * The XML document builder
	 */
	private class ThreadLocalXMLDocumentBuilder extends ThreadLocal<DocumentBuilder>{

		/* (non-Javadoc)
		 * @see java.lang.ThreadLocal#initialValue()
		 */
		@Override
		protected DocumentBuilder initialValue() {
			
			DocumentBuilder db = null;
			
			// set up the document builder
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	        factory.setIgnoringComments(true);
	        // as of 1.2, we always are namespace aware
	        factory.setNamespaceAware(true);
	        // we're not validating
	        factory.setValidating(false);

	        try {
				db = factory.newDocumentBuilder();
			} catch (ParserConfigurationException e) {
				logger.severe("Document builder creation failed:" + e.getMessage());	    	
			}
			
			return db;
		}
	}
	private ThreadLocalXMLDocumentBuilder threadLocalXMLDocumentBuilder;
	
	
	// the message to redirect the client to another server
	private String redirectMsg;	
	
	/**
	 * Create an AM specific Policy Decision Point object that uses behind a sunxacml PDP.
	 * @param policiesStoragePath the path for policies storage object
	 * @param RPSs the list of Role Policies Sets IDs
	 * @param rbacPIP the RBAC PIP object
	 * @param nextServers the string with the information about the next servers
	 * @throws AMException
	 */
	public PolicyDecisionPoint(
			String policiesStoragePath, 
			List<URI> RPSs, 
			RBACPIP rbacPIP,
			String nextServers) throws AMException{
			
		// use the default policy storage implementation
		this.policyStorage = PolicyStorageFactory.getDefaultPolicyStorage(policiesStoragePath);
		
		this.threadLocalXMLDocumentBuilder = new ThreadLocalXMLDocumentBuilder();
		
		setPAP(RPSs);
		setPIP(rbacPIP);
		
		initPDP();
		
		// initialize the redirect message with the status message + the list of backup servers
		redirectMsg = ResponseProcessor.STATUS_MSG_CLIENT_REDIRECTED + "[" + nextServers + "]";
	}
	
	/**
	 * PAP configuration: the list of Role Policies Sets IDs
	 * @param RPSs the list of Role Policies Sets IDs
	 * @throws AMException
	 */
	private void setPAP(List<URI> RPSs) throws AMException {
		this.RPSs = RPSs; 
		if (this.RPSs == null || this.RPSs.size() <= 0){
			throw new AMException("No RPS ids are specified!", AMException.AM_PAP_ERROR);
		} else {
			logger.fine("Root RPS:" + this.RPSs);
		}
		PAPCacheUser = null;
	}
	
	/**
	 * Set the RBAC PIP
	 * @param rbacPIP the RBAC PIP object
	 */
	private void setPIP(RBACPIP rbacPIP) throws AMException {
		this.rbacPIP = rbacPIP; 
		if (this.rbacPIP == null){
			throw new AMException("No RBAC PIP specified!", AMException.AM_PIP_ERROR);
		}
		PIPCacheUser = null;
	}

	/**
	 * Initialize the PDP 
	 */
	private void initPDP() throws AMException {
		 
		 /*
		  * initialize the Policy Finder with FilePolicyFinder
		  */		
		
		// setup the PolicyFinder that this PDP will use
		PolicyFinder policyFinder = new PolicyFinder();
		
		Set<PolicyFinderModule> policyModules = new HashSet<PolicyFinderModule>();
		FinderModuleForPolicyStorage finderModuleForPolicy = 
			new FinderModuleForPolicyStorage(policyStorage, RPSs);
		PAPCacheUser = finderModuleForPolicy;
		policyModules.add(finderModuleForPolicy);
		policyFinder.setModules(policyModules);
		
		 /*
		  * initialize the Attribute Finder with FinderModuleForAttributeInRBACPIP
		  */		
	    AttributeFinder attrFinder = new AttributeFinder();
	    
	    List<AttributeFinderModule> attrModules = new ArrayList<AttributeFinderModule>();
	    attrModules.add(new CurrentEnvModule());
	    FinderModuleForAttributeInRBACPIP finderModuleForPIP = 
		    new FinderModuleForAttributeInRBACPIP(rbacPIP);
	    PIPCacheUser = finderModuleForPIP;
	    attrModules.add(finderModuleForPIP);
	    
	    attrFinder.setModules(attrModules);
		
		 /*
		  * initialize the Resource Finder: no resource finder for the moment
		  */
	    ResourceFinder resFinder = null;
	    
	    
		/*
		 * Initialize the PDP configuration
		 */
	    pdpConfig = new PDPConfig(attrFinder, policyFinder, resFinder);	    
	    if (pdpConfig == null){
			throw new AMException("PDP configuration failed!", AMException.AM_PDP_ERROR);	    	
	    }

	    // the PDP objects are created for each thread
		threadLocalPDP = new ThreadLocalPDP();
	}
	
	
	/**
	 * Decodes the XACML request. This method is not synchronized because there should be one
	 * PolicyDecisionPOint object per thread.
	 * @param inputStream the input stream containing the XML document
	 * @return the DOM node
	 * @throws ParsingException
	 */
	private Node decodeXACMLRequest(ByteArrayInputStream inputStream) throws ParsingException{
        Document doc;
        
        if (inputStream == null) {
            throw new ParsingException("XML parsing aborted(null input stream)!");
        }
        
        int availableInInputStream = inputStream.available();
        if (availableInInputStream == 0) {
            throw new ParsingException("XML parsing aborted(empty input stream)!");
        }
		
        try {
			doc = this.threadLocalXMLDocumentBuilder.get().parse(inputStream);
		} catch (SAXException e) {
			logger.info("SAX exception when parsing the input stream with available bytes:" + availableInInputStream);
			throw new ParsingException("SAX exception during XML parsing of input stream:" + e.getMessage(), e);
		} catch (IOException e) {
			logger.info("IO exception when parsing the input stream with available bytes:" + availableInInputStream);
			throw new ParsingException("IO exception during XML parsing of input stream:" + e.getMessage(), e);
		} catch (Exception e) {
			logger.info("Exception when parsing the input stream with available bytes:" + availableInInputStream);
			throw new ParsingException("Exception during XML parsing of input stream:" + e.getMessage(), e);
		}
		
        NodeList nodes = doc.getChildNodes();
        if (nodes.getLength() != 1) {
            throw new ParsingException("Only one child node allowed in the request! " +
            		"Currently there are:" + nodes.getLength());
        }

        return nodes.item(0);
	}
	
	
	/* (non-Javadoc)
	 * @see am.server.NIOReactor.InputHandler#process(java.io.ByteArrayInputStream, java.io.ByteArrayOutputStream)
	 */
	public void process(
			ByteArrayInputStream inputStream, 
			ByteArrayOutputStream outputStream,
			AuthorizationLogger authorizationLogger,
			Long authLoggerClientKey) {
		RequestCtx request = null;
		ResponseCtx response = null;
		try {
			if (logger.isLoggable(Level.FINEST))
				logger.finest("Retrieve the request");
			request = RequestCtx.getInstance(decodeXACMLRequest(inputStream));

			if (authorizationLogger.isEnabled()){
				authorizationLogger.addMessage(authLoggerClientKey, 
						am.xacml.context.RequestProcessor.toBriefString(request));
			}			
			
			// the toString method invoked below consumes a lot of processing
//			if (logger.isLoggable(Level.FINE))
//				logger.fine(RequestProcessor.toString(request));

		} catch (ParsingException e) {
			logger.severe("Request parsing error: " + e.getMessage());
			
			authorizationLogger.addMessage(authLoggerClientKey, "Request parsing error (" + e.getMessage() + ")");
			
/*			// prepare an answer to redirect the client to the backup servers 
			List<String> codes = new ArrayList<String>();
	        codes.add(Status.STATUS_OK);
	        Status redirectStatus = new Status(codes, redirectMsg);
			response = new ResponseCtx(new Result(Result.DECISION_INDETERMINATE, redirectStatus));
*/			
			// build an error response
			List<String> codes = new ArrayList<String>();
	        codes.add(Status.STATUS_PROCESSING_ERROR);
	        codes.add(Status.STATUS_SYNTAX_ERROR);
	        Status errorStatus = new Status(codes, "Request parsing error: " + e.getMessage());
			response = new ResponseCtx(new Result(Result.DECISION_INDETERMINATE, errorStatus));

		}

		// evaluate the request
		if (response == null){
			logger.finest("Evaluate the request");
			try {
				response = threadLocalPDP.get().evaluate(request);
			} catch (Exception e) {
				String stackTrace = "";
				for(StackTraceElement st: e.getStackTrace()){
					stackTrace += st.toString() + "\n";
				}
				logger.severe("Policy evaluation failed due to: " + e.getMessage() + 
						"\nSTACK TRACE: \n" + stackTrace);
				// build an error response
				List<String> codes = new ArrayList<String>();
		        codes.add(Status.STATUS_PROCESSING_ERROR);
		        codes.add(Status.STATUS_SYNTAX_ERROR);
		        Status errorStatus = new Status(codes, "Internal server error!");
				response = new ResponseCtx(new Result(Result.DECISION_INDETERMINATE, errorStatus));
			}
		}
		
		// the toString method invoked below consumes a lot of processing
//		if (logger.isLoggable(Level.FINE))
//			logger.fine(ResponseProcessor.toString(response));
		
		// log the authorization decision
		if (authorizationLogger.isEnabled()){
			if (ResponseProcessor.isResponsePositive(response)){
				authorizationLogger.setDecision(authLoggerClientKey, 
						AuthorizationLogger.DECISION_ALLOWED);
			} else if (ResponseProcessor.isClientRedirected(response)) {
				authorizationLogger.setDecision(authLoggerClientKey, 
						AuthorizationLogger.DECISION_DEFERRED);
				
				//add also the status
				authorizationLogger.addMessage(authLoggerClientKey, 
						"Status detail " + ResponseProcessor.getStatusDetails(response));
			} else {
				authorizationLogger.setDecision(authLoggerClientKey, 
						AuthorizationLogger.DECISION_DENIED);
				//add also the status
				authorizationLogger.addMessage(authLoggerClientKey, 
						"Status detail " + ResponseProcessor.getStatusDetails(response));
			}			
		}
		
		if (logger.isLoggable(Level.FINEST))
			logger.finest("Encode the response into the output stream");
		
		response.encode(outputStream, PolicyDecisionPoint.responseIndenter);
	}

	/**
	 * Get the PAP CacheUser object to be used for cache invalidation.
	 * @return the CacheUser object
	 */
	public CacheUser getPAPCacheUser(){
		return PAPCacheUser;
	}
	
	/**
	 * Get the PIP CacheUser object to be used for cache invalidation.
	 * @return the CacheUser object
	 */
	public CacheUser getPIPCacheUser() {
		return PIPCacheUser;
	}
}
