/**
 * 
 */
package am.pdp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.Logger;

import am.server.NIOReactor.InputHandler;
import am.server.authlogger.AuthorizationLogger;
import am.xacml.context.ResponseProcessor;


/**
 * The Policy Decision Point object that handles the clients requests and answers with BUSY.
 * @author mleahu
 *
 */
public class PolicyDecisionPointBUSY implements InputHandler {

	private static final Logger logger =
        Logger.getLogger(PolicyDecisionPointBUSY.class.getName());


	private String busyMessage;
	private byte[] busyAnswerByte;
	
	/**
	 * Build a PDP that always responds with BUSY and indicates other AM servers to be queried. 
	 * @param nextServers the string with the information about the next servers
	 */
	public PolicyDecisionPointBUSY(String nextServers){
		busyMessage = ResponseProcessor.STATUS_MSG_SERVER_BUSY + "[" + nextServers + "]";
		String busyAnswer = "<Response>" +
		"  <Result ResourceId=\"\">" +
		"    <Decision>Indeterminate</Decision>" +
		"    <Status>" +
		"      <StatusCode Value=\"urn:oasis:names:tc:xacml:1.0:status:ok\"/>" +
		"      <StatusMessage>" + busyMessage + "</StatusMessage>" +
		"    </Status>" +
		"  </Result>" +
		"</Response>";
		busyAnswerByte = busyAnswer.getBytes();
	}
	
	@Override
	public void process(
			ByteArrayInputStream inputStream, 
			ByteArrayOutputStream outputStream,
			AuthorizationLogger authorizationLogger,
			Long authLoggerClientKey) {
		
		try {
			outputStream.write(busyAnswerByte);
			if (authorizationLogger.isEnabled()){
				authorizationLogger.setDecision(authLoggerClientKey, 
						AuthorizationLogger.DECISION_DEFERRED);
				//add also the busy message
				authorizationLogger.addMessage(authLoggerClientKey, busyMessage);
			}
		} catch (IOException e) {
			logger.severe("Cannot write BUSY response");
			e.printStackTrace();
		}
	}

}
