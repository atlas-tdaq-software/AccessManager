
/*
 * @(#)AMFilePolicyModule.java
 * 
 * @author mleahu 
 * 
 */

package am.pdp;


import java.net.URI;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import am.pap.storage.PolicyStorage;
import am.util.AMException;
import am.util.cache.CacheUser;
import am.util.cache.Cachetable;

import com.sun.xacml.AbstractPolicy;
import com.sun.xacml.EvaluationCtx;
import com.sun.xacml.MatchResult;
import com.sun.xacml.ParsingException;
import com.sun.xacml.Policy;
import com.sun.xacml.PolicyMetaData;
import com.sun.xacml.PolicySet;
import com.sun.xacml.VersionConstraints;
import com.sun.xacml.ctx.Status;
import com.sun.xacml.finder.PolicyFinder;
import com.sun.xacml.finder.PolicyFinderModule;
import com.sun.xacml.finder.PolicyFinderResult;


/**
 * This module represents a collection of files containing polices,
 * each of which will be searched through when trying to find a
 * policy that is applicable to a specific request.
 * <p>
 * Note: this module is provided only as an example and for testing
 * purposes. It is not part of the standard, and it should not be
 * relied upon for production systems. In the future, this will likely
 * be moved into a package with other similar example and testing
 * code.
 *
 * @since 1.0
 * @author mleahu
 */
public class FinderModuleForPolicyStorage extends PolicyFinderModule implements CacheUser {


	// the finder that is using this module
    private PolicyFinder finder;

    
	/**
	 * The PolicyStorage object 
	 */
	private PolicyStorage ps;

	/**
	 * These are the root policies. They will be loaded first and the references from them will
	 *  be resolved afterwards.
	 */
	private List<URI> rootPoliciesIDs;
	
	/**
	 * The root policies objects
	 */
	private Hashtable<URI, AbstractPolicy> rootPolicies;
	
    /**
     * The policies table; the key is the policy id and the value is the AbstactPolicy object
     */
    private Cachetable<URI> policiesCache;
    
    
    /**
     * Indicates if the cache is enabled for this class.
     */
    private boolean cacheEnabled;
    
    /**
     * The default value for cache timeout
     */
    private long DEFAULT_CACHE_TIMEOUT = 5 * 60 * 1000;  // 5 minutes 

    // the logger we'll use for all messages
    private static final Logger logger =
        Logger.getLogger(FinderModuleForPolicyStorage.class.getName());

    /**
     * The constructor who takes a policyStorage implementation as parameter, the choice to 
     * enable or not the cache mechanism and the list of root policies.
     * @param ps
     * @param cacheEnabled
     * @param rootPoliciesIDs
     * @throws AMException
     */
    public FinderModuleForPolicyStorage(
    		PolicyStorage ps,
    		boolean cacheEnabled,
    		List<URI> rootPoliciesIDs) throws AMException{
    	
        logger.fine("Initializing the policy finder module...");
        
        this.ps = ps;
        this.cacheEnabled = cacheEnabled;
        this.rootPoliciesIDs = rootPoliciesIDs;
        
        if (this.ps == null){
        	throw new AMException("Null policy storage object!", AMException.AM_PAP_ERROR);        	
        }
        
        if (!this.ps.isXMLDocumentSupported()){
        	throw new AMException("The policy storage object can not process XML documents! Exit!", AMException.AM_PAP_ERROR);        	
        }
        
        if (this.rootPoliciesIDs == null || this.rootPoliciesIDs.size() <= 0){
        	throw new AMException("No root policies for the policy finder!", AMException.AM_PAP_ERROR);
        }
        
        
        this.rootPolicies = new Hashtable<URI, AbstractPolicy>();

        if (this.cacheEnabled){
            this.policiesCache = new Cachetable<URI>(DEFAULT_CACHE_TIMEOUT, "policies");
			if (logger.isLoggable(Level.FINE))
				logger.fine("Cache enabled; timeout=" + DEFAULT_CACHE_TIMEOUT + ", " + this.policiesCache);
        } else {
        	this.policiesCache = null;
			if (logger.isLoggable(Level.FINE))
				logger.fine("Cache not enabled.");
        }        	
    }

    /**
     * The constructor who takes a policyStorage implementation as parameter and the list of root policies.
     * The cache is enabled
     * @param ps
     * @param rootPolicies
     * @throws AMException
     */
    public FinderModuleForPolicyStorage(PolicyStorage ps,	List<URI> rootPolicies) throws AMException{
    	this(ps, true, rootPolicies);
    }    
    
    /**
     * Indicates whether this module supports finding policies based on
     * a request (target matching). Since this module does support
     * finding policies based on requests, it returns true.
     *
     * @return true, since finding policies based on requests is supported
     */
    public boolean isRequestSupported() {
        return true;
    }
    
    /**
     * Return true since this class is meant to support references.
     * @return true
     */
    public boolean isIdReferenceSupported() {
        return true;
    }

    /**
     * Initializes the <code>FinderModuleForPolicyStorage</code> by loading
     * the policies contained in the collection of files associated
     * with this module. This method also uses the specified 
     * <code>PolicyFinder</code> to help in instantiating PolicySets.
     *
     * @param finder a PolicyFinder used to help in instantiating PolicySets
     */
    public void init(PolicyFinder finder) {
        this.finder = finder;

        if (logger.isLoggable(Level.FINE)){
            logger.fine("Loading [" + rootPolicies.size() + "] root policies: " + rootPolicies);        	
        }
        
        // clean the policies cache
        policiesCache.clear();
        
        // clean the root policies
        rootPolicies.clear();
        
        // generate the root policies
        for(URI policyId: rootPoliciesIDs){
        	rootPolicies.put(policyId, loadPolicy(policyId));
        }
    }


    /**
     * Loads a policy using the policy storage object
     * @param policyId the policy id
     * @return the abstract policy object
     */
    private AbstractPolicy loadPolicy(URI policyId) {
    	
        AbstractPolicy aPolicy = null;
    	
    	// get the xml document; don't care about the policy type
    	Document doc;
		try {
			doc = ps.getXMLDocument(policyId, 0);
		} catch (AMException e) {
			logger.severe("Policy read error! Abort!" + e.getMessage());
			return aPolicy; 
		} 
    	
        // handle the policy, if it's a known type
        Element root = doc.getDocumentElement();
        String name = root.getTagName();
        
        try {
        	if (name.equals("Policy")) {
        		aPolicy = Policy.getInstance(root);
            } else if (name.equals("PolicySet")) {
                aPolicy = PolicySet.getInstance(root, finder);
            } else {
                // this isn't a root type that we know how to handle
                logger.severe("Unknown root document type: " + name);
            }
		} catch (ParsingException e) {
			logger.severe("Policy loading failed:" + e.getMessage());
		}
    	
        return aPolicy;
    }
    
    /**
     * Finds a policy based on a request's context. This may involve using
     * the request data as indexing data to lookup a policy. This will always
     * do a Target match to make sure that the given policy applies. If more
     * than one applicable policy is found, this will return an error.
     * NOTE: this is basically just a subset of the OnlyOneApplicable Policy
     * Combining Alg that skips the evaluation step. See comments in there
     * for details on this algorithm.
     *
     * @param context the representation of the request data
     *
     * @return the result of trying to find an applicable policy
     */
    public PolicyFinderResult findPolicy(EvaluationCtx context) {
        AbstractPolicy selectedPolicy = null;

        logger.fine("Search a policy in the root policies list to match a given context ");
        if (logger.isLoggable(Level.FINEST)){
        	logger.finest("Context:" + context);
        }
        
        for(URI pid: rootPolicies.keySet()){
        	
        	AbstractPolicy policy = (AbstractPolicy) rootPolicies.get(pid);
        	
            // see if we match
            MatchResult match = policy.match(context);
            int result = match.getResult();
            
            // if there was an error, we stop right away
            if (result == MatchResult.INDETERMINATE)
                return new PolicyFinderResult(match.getStatus());

            if (result == MatchResult.MATCH) {
                // if we matched before, this is an error...
                if (selectedPolicy != null) {
                    List<String> code = new ArrayList<String>();
                    code.add(Status.STATUS_PROCESSING_ERROR);
                    Status status = new Status(code, "too many applicable root policies");
                    return new PolicyFinderResult(status);
                }

                // ...otherwise remember this policy
                selectedPolicy = policy;
            }
        }
        
        if (logger.isLoggable(Level.FINE)){
        	logger.finest("Root policy found: " + ((selectedPolicy != null)? selectedPolicy.getId() : "NONE"));
        }

        // if we found a policy, return it, otherwise we're N/A
        if (selectedPolicy != null)
            return new PolicyFinderResult(selectedPolicy);
        else
            return new PolicyFinderResult();
    }

    /* (non-Javadoc)
     * @see com.sun.xacml.finder.PolicyFinderModule#findPolicy(java.net.URI, int, com.sun.xacml.VersionConstraints, com.sun.xacml.PolicyMetaData)
     */
    public PolicyFinderResult findPolicy(
    		URI idReference,
    		int type, 
    		VersionConstraints constraints,
    		PolicyMetaData parentMetaData) {
    	
        if (logger.isLoggable(Level.FINE)){
        	logger.fine("Search a policy to match a given reference [" + idReference + "]");        	
        }
    	
        AbstractPolicy selectedPolicy = null;
        
        if (cacheEnabled){
        	// lookup in the cache
        	selectedPolicy = (AbstractPolicy)policiesCache.get(idReference);        	
        	if (selectedPolicy != null){
    			if (logger.isLoggable(Level.FINE))
    				logger.fine("Policy found in cache!");        	
        	} else{
        		// not found, so let's load it
        		selectedPolicy = loadPolicy(idReference);
        		// and put it in the cache if loaded successfully
        		if (selectedPolicy != null){
        			policiesCache.put(idReference, selectedPolicy);
        			if (logger.isLoggable(Level.FINE))
        				logger.fine("Policy [" + selectedPolicy.getId() + "] added to cache");
        		}
        	}
        	
        } else {
        	// load directly the policy
    		selectedPolicy = loadPolicy(idReference);        	
        }

        if (logger.isLoggable(Level.FINE)){
        	logger.finest("Policy found: " + ((selectedPolicy != null)? selectedPolicy.getId() : "NONE"));
        }        
        
        if (selectedPolicy != null)
            return new PolicyFinderResult(selectedPolicy);
        else
            return new PolicyFinderResult();         
    }
    
	/* (non-Javadoc)
	 * @see com.sun.xacml.finder.PolicyFinderModule#invalidateCache()
	 */
	public void invalidateCache() {
		if (cacheEnabled){
			if (logger.isLoggable(Level.INFO)){
				logger.info("Flush the PAP cache (" + policiesCache.size() + " entries)");
			}
			policiesCache.clear();
		} else {
			logger.fine("Policy cache not enabled: flush not possible.");
		}		
	}
        
}
