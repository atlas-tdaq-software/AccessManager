package am.pdp;

import am.server.NIOReactor.InputHandler;
import am.server.NIOReactor.InputHandlerFactory;
import am.util.AMException;

/**
 * The factory object for the PDP objects responsible with request processing.
 * It is implemented as a pool of PDP objects so that the threads processing requests in parallel
 * are using different PDP object.
 * @author mleahu
 *
 */
public class PDPFactory implements InputHandlerFactory {

	
	private InputHandler[] inputHandlers;
	/**
	 * Creates a PDP pool object to be used as factory for input handler objects in the Handler Logic.
	 */
	public PDPFactory() throws AMException {		
		inputHandlers = new InputHandler[HANDLER_TYPES.length];
	}
	
	/**
	 * Register an input handler
	 * @param inputHandlerType
	 * @param inputHandler
	 * @throws AMException
	 */
	public void registerHandler(int inputHandlerType, InputHandler inputHandler) throws AMException{
		if (inputHandlerType < 0 || inputHandlerType >= HANDLER_TYPES.length) {
			throw new AMException("Invalid input handler type" + inputHandlerType, 
					AMException.WRONG_ARGUMENTS);
		}
		if (inputHandler == null) {
			throw new AMException("null input handler", 
					AMException.WRONG_ARGUMENTS);
		}
		inputHandlers[inputHandlerType] = inputHandler;
	}
	
	@Override
	public InputHandler getInputHandler(int handlerType) {
		return inputHandlers[handlerType];
	}
}
