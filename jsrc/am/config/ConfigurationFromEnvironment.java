/**
 * 
 */
package am.config;

import java.util.Map;

import am.util.AMException;

/**
 * Get the configuration parameters from environment
 * @author mleahu
 *
 */
public class ConfigurationFromEnvironment implements ConfigurationSource {

	/* (non-Javadoc)
	 * @see am.config.ConfigurationSource#getParametersAndValues()
	 */
	@Override
	public Map<String, String> getParametersAndValues() throws AMException {
		return System.getenv();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return "Environment";
	}
	
}
