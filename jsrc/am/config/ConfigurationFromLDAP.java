/**
 * 
 */
package am.config;

import java.util.Hashtable;
import java.util.Map;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import am.util.AMException;

/**
 * Get the configuration parameters from environment
 * @author mleahu
 *
 */
public class ConfigurationFromLDAP implements ConfigurationSource {

	private static final Logger logger =
        Logger.getLogger(ConfigurationFromLDAP.class.getName());

	private static final String LDAP_SCHEME = "ldap";
	private static final String LDAP_PORT = "389";
	private static final String LDAP_SCOPE_SUB = "sub";

	/*
	 * The attribute value should be like this: '{ KEY_NAME = VALUE1,VALUE2 }'
	 */
	private static final String LDAP_ATTRIBUTE_VALUE_REGEXP = "\\{\\s*\\S+\\s*=\\s*\\S+\\s*}"; 
	private String ldapUrl;
	private String ldapBaseDN;
	private String ldapRDN;
	private String[] ldapAttrNames;
	private String ldapFilter;
	private DirContext ctx;
	
	private Map<String, String> content;	

	/**
	 * Creates a ConfigurationSource object that reads the parameters and their values from a LDAP.
	 * @param host the ldap host
	 * @param basedn
	 * @param rdn
	 * @param attrNames the attributes to retrieve
	 * @param filterExpr the ldap filter expression for the search operation
	 * @param filterArgs the filter arguments to be used in the expression
	 * @throws AMException
	 */
	public ConfigurationFromLDAP(
			String host, 
			String basedn, 
			String rdn, 
			String[] attrNames, 
			String filter) throws AMException {
		
		this.ldapBaseDN = basedn;
		this.ldapRDN = rdn;
		this.ldapAttrNames = attrNames;
		this.ldapFilter = filter;
		
//		 LDAP URI FORMAT
//		ldapurl    = scheme "://" [hostport] ["/"
//		                    [dn ["?" [attributes] ["?" [scope]
//		                    ["?" [filter] ["?" extensions]]]]]]
//		       scheme     = "ldap"
//		       attributes = attrdesc *("," attrdesc)
//		       scope      = "base" / "one" / "sub"
//		       dn         = distinguishedName from Section 3 of [1]
//		       hostport   = hostport from Section 5 of RFC 1738 [5]
//		       attrdesc   = AttributeDescription from Section 4.1.5 of [2]
//		       filter     = filter from Section 4 of [4]
//		       extensions = extension *("," extension)
//		       extension  = ["!"] extype ["=" exvalue]
//		       extype     = token / xtoken
//		       exvalue    = LDAPString from section 4.1.2 of [2]
//		       token      = oid from section 4.1 of [3]
//		       xtoken     = ("X-" / "x-") token

		ldapUrl = LDAP_SCHEME + "://" + host + ":" + LDAP_PORT + 
						"/" + ldapBaseDN + 
						"?"; 
		if ( ldapAttrNames != null && ldapAttrNames.length > 0) {
			for (String attr: ldapAttrNames){
				ldapUrl += attr + ",";
			}
			// delete the last ,
			ldapUrl = ldapUrl.substring(0, ldapUrl.length() - 1);
		}
		ldapUrl += "?" + LDAP_SCOPE_SUB +
					"?" + filter;
		
		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, ldapUrl);

		logger.info("JNDI LDAP: [" + this + "]");
		
		ctx = null;
		try {
			ctx = new InitialDirContext(env);
		} catch (NamingException e) {
			throw new AMException("JNDI initial dir context exception [" + e.getMessage() + "]" + 
					" due to [" + e.getRootCause().getClass().getName() + ":" + e.getRootCause().getMessage() + "]");
		}
		
		content = null;
		
	}
	
	/* (non-Javadoc)
	 * @see am.config.ConfigurationSource#getParametersAndValues()
	 */
	@Override
	public Map<String, String> getParametersAndValues() throws AMException {
		if (content != null) {
			return content;
		}
		
		// Perform the search
		try {
			NamingEnumeration<SearchResult> results = ctx.search(
					this.ldapRDN,
					this.ldapFilter,
					new SearchControls(SearchControls.SUBTREE_SCOPE, 5000, 0, ldapAttrNames, false, false));
			
			while(results.hasMore()){
				SearchResult result = results.next();
				Attributes attrs = result.getAttributes();
				NamingEnumeration<String> attrsIds = attrs.getIDs();
				while (attrsIds.hasMore()){
					String attrId = attrsIds.next();
					Attribute attr = attrs.get(attrId);
					logger.fine("Values for attribute id [" + attrId + "]");
					NamingEnumeration values = attr.getAll();
					while(values.hasMore()){
						Object value = values.next();
						if (value.getClass().getName().equals("java.lang.String")){
							String svalue = (String)value;
							logger.fine("Value:" + svalue);
							
							// check if the attribute value matches the patter '{key=value}'
							
							if (! svalue.matches(LDAP_ATTRIBUTE_VALUE_REGEXP)) {
								logger.fine("Value [" + svalue + "] doesn't match the regexp [" + LDAP_ATTRIBUTE_VALUE_REGEXP + "]");
							} else {
								// delete the { and }
								svalue = svalue.replaceAll("\\{|\\}", "");
								
								// add to the content map
								String[] fields = svalue.split("=", 2); // only the first '=' is used to split the svalue
								String _key = null;
								String _value = null;
								if (fields.length > 0) {
									_key = fields[0].trim();
									_value = (fields.length > 1) ? fields[1].trim() : "";
								}
								if (_key != null) {
									if (content == null){
										content = new Hashtable<String, String>();
									}
									if (content.containsKey(_key)){
										logger.warning("Key[" + _key + "] with value [" + content.get(_key) + "] " +
												"will be overwritten by value [" + _value + "]");
									}
									content.put(_key, _value);
								}
							}							
						} else {
							logger.fine("Unrecognized value object type [" + value.getClass().getName());
						}
					}
					
				}
			}
			
			if (content == null) {
				System.err.println("Server configuration not found in LDAP!");
			} else {
				System.out.println("Configuration read from LDAP:");
				for(String key:content.keySet()) {
					System.out.println(key + "=" + content.get(key));
				}
			}
			
		} catch (NamingException e) {
			throw new AMException("LDAP search exception:" + e.getMessage());
		}
		
		return content;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return ldapUrl;
	}
	
}
