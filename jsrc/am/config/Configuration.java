/**
 * 
 */
package am.config;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import java.util.prefs.AbstractPreferences;
import java.util.prefs.BackingStoreException;

import am.util.AMException;

/**
 * Configuration variables collected from multiple sources (environment, files, etc);
 * @author mleahu
 *
 */
public class Configuration extends AbstractPreferences {

	private static final Logger logger =
        Logger.getLogger(Configuration.class.getName());

	/**
	 * An ordered list of configuration sources.
	 */
	protected List<ConfigurationSource> configSrc;
	
	/**
	 * The set of keys from all configuration sources.
	 */
	protected Set<String> keys;
	
	public Configuration() {
		super(null, "");
		configSrc = new ArrayList<ConfigurationSource>();
		keys = new HashSet<String>();
	}

	/**
	 * Add a configuration source implementation to the current configuration. 
	 * The order is important because the value of a parameter is retrieved from first configuration
	 * source where is found.
	 * @param cfgSrc ConfigurationSource implementation 
	 * @throws AMException
	 */
	public void addConfigurationSource(ConfigurationSource cfgSrc) throws AMException{
		if (cfgSrc == null){
			throw new AMException("Null configuration source object!", AMException.WRONG_ARGUMENTS);
		}
		
		Map<String, String> paramsValues = null;
		try {
			paramsValues = cfgSrc.getParametersAndValues(); 		
			if (paramsValues != null){		
				logger.fine("Add configuration source [" + cfgSrc + "] with priority " + configSrc.size());
				configSrc.add(cfgSrc);
				keys.addAll(paramsValues.keySet());
			} else {
				logger.warning("No parameters and values from configuration source [" + cfgSrc + "]");
			}
		} catch (AMException ex){
			logger.warning("Configuration source [" + cfgSrc + "] not added due to: " + ex.getMessage());
		}
	}
	
	/* (non-Javadoc)
	 * @see java.util.prefs.AbstractPreferences#childSpi(java.lang.String)
	 */
	@Override
	protected AbstractPreferences childSpi(String arg0) {
		logger.warning("Function not implemented!");
		return null;
	}

	/* (non-Javadoc)
	 * @see java.util.prefs.AbstractPreferences#childrenNamesSpi()
	 */
	@Override
	protected String[] childrenNamesSpi() throws BackingStoreException {
		logger.warning("Function not implemented!");
		return null;
	}

	/* (non-Javadoc)
	 * @see java.util.prefs.AbstractPreferences#flushSpi()
	 */
	@Override
	protected void flushSpi() throws BackingStoreException {
		logger.warning("Function not implemented!");
	}

	/* (non-Javadoc)
	 * @see java.util.prefs.AbstractPreferences#getSpi(java.lang.String)
	 */
	@Override
	protected String getSpi(String arg0) {
		if (configSrc.size() == 0) {
			logger.warning("No configuration source found!");
			return null;
		}
		
		String value = null;
		for(ConfigurationSource cfgSrc: configSrc) {
			String newValue = null;
			try {
				newValue = cfgSrc.getParametersAndValues().get(arg0);
				if (newValue != null) {
					value = newValue;
					logger.fine("Returned value[" + value + "] for key [" + arg0 + "] from configuration source [" + cfgSrc + "]");
					break;
				}
			} catch (AMException e) {
				// TODO Auto-generated catch block
				logger.warning("Error when retrieving value for [" + arg0 + "]:" + e.getMessage());
			}
		}
		
		return value;
	}

	/* (non-Javadoc)
	 * @see java.util.prefs.AbstractPreferences#keysSpi()
	 */
	@Override
	protected String[] keysSpi() throws BackingStoreException {
		String[] keysArray = new String[keys.size()];
		return keys.toArray(keysArray);
	}

	/* (non-Javadoc)
	 * @see java.util.prefs.AbstractPreferences#putSpi(java.lang.String, java.lang.String)
	 */
	@Override
	protected void putSpi(String arg0, String arg1) {
		logger.warning("Function not implemented!");
	}

	/* (non-Javadoc)
	 * @see java.util.prefs.AbstractPreferences#removeNodeSpi()
	 */
	@Override
	protected void removeNodeSpi() throws BackingStoreException {
		logger.warning("Function not implemented!");
	}

	/* (non-Javadoc)
	 * @see java.util.prefs.AbstractPreferences#removeSpi(java.lang.String)
	 */
	@Override
	protected void removeSpi(String arg0) {
		logger.warning("Function not implemented!");
	}

	/* (non-Javadoc)
	 * @see java.util.prefs.AbstractPreferences#syncSpi()
	 */
	@Override
	protected void syncSpi() throws BackingStoreException {
		logger.warning("Function not implemented!");
	}

}
