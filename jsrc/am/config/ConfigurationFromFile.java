/**
 * 
 */
package am.config;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import am.util.AMCommon;
import am.util.AMException;

/**
 * Get the configuration parameters from files on the disk.
 * The input file is expected to be a text file with the following syntax:
 * 
 * #Comments start with #
 * PARAMETER=VALUE
 * 
 * @author mleahu
 *
 */
public class ConfigurationFromFile implements ConfigurationSource {

	private String inputFileName;
	private Map<String, String> content;
	
	/**
	 * Creates a ConfigurationSource object that reads the parameters and their values from a text file.
	 * @param inputFileName
	 */
	public ConfigurationFromFile(String inputFileName){
		this.inputFileName = AMCommon.makeFileNameSystemCompatible(inputFileName);
		content = null;
	}
	
	/* (non-Javadoc)
	 * @see am.config.ConfigurationSource#getParametersAndValues()
	 */
	@Override
	public Map<String, String> getParametersAndValues() throws AMException {
		if (content != null) {
			return content;
		}
		
		BufferedReader input = null;
		content = new HashMap<String, String>();
		try{
			input = new BufferedReader(new FileReader(inputFileName));
			
			String line = null; //not declared within while loop
			while (( line = input.readLine()) != null){
				
				line = line.trim();
				
				//if the line starts with #, the line is skipped and considered a comment
				if (line.startsWith("#") || line.length() == 0) {
					continue;
				}
				
				String[] fields = line.split("=");
				String key = null;
				String value = null;
				if (fields.length > 0) {
					key = fields[0].trim();
					value = (fields.length > 1) ? fields[1].trim() : "";
				}
				if (key != null) {
					content.put(key, value);
				}
			}
			
		} catch (FileNotFoundException ex) {
	        throw new AMException(ex.getMessage(), AMException.OPERATION_ERROR);
	    }
	    catch (IOException ex){
	        throw new AMException(ex.getMessage(), AMException.OPERATION_ERROR);
	    }
	    finally {
	      try {
	        if (input!= null) {
	          //flush and close both "input" and its underlying FileReader
	          input.close();
	        }
	      }
	      catch (IOException ex) {
	        throw new AMException(ex.getMessage(), AMException.OPERATION_ERROR);
	      }
	    }
		
		return content;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return "file(" + inputFileName + ")";
	}
	
}
