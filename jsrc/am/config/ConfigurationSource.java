/**
 * 
 */
package am.config;

import java.util.Map;

import am.util.AMException;

/**
 * Specify the functions to be defined by a configuration source implementation
 * @author mleahu
 *
 */
public interface ConfigurationSource {

	/**
	 * Get a map of parameters and their values.
	 * @return map of string keys and string values
	 */
	public Map<String, String> getParametersAndValues() throws AMException;
	
}
