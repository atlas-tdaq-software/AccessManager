#!/bin/bash

# Access Manager script to manage the users-roles. relationship.

# Versions:
#
#  18/02/08 - mleahu
# 	Initial version
# 22/05/08 - mleahu
#	Default user to bind as to LDAP is AccessManagerAdmin
# 07/08/08 - mleahu
#	Fixed a bug in the MSG(N) functions
# 04/09/08 - mleahu
#	Added -G flag to specify a role category to prefix the roles to be processed
# 	Allow to process more users in the same time provided either in the command line or in a input text file
# 05/09/08 - mleahu
#	Added -L flag to display the roles defined in LDAP
# 08/09/08 - mleahu
#	Convert usernames to lower case
# 21/04/09 - lvalsan
#       Added support for the uri configuration directive
# 12/05/09 - lvalsan
#       Added the integration with the roles -> groups synchronization script
# 23/08/09 - lvalsan
#       Changed the interaction protocol used for synchrionizing with the DCS Access Manager
# 04/02/10 - lvalsan
#       Whenever an action is made that implies a change on the roles that a user has enabled, the user entry is changed by setting the shadowMax attribute to a standard value
# 15/03/10 - lvalsan
#       Added the -y command line argument for ldap operations

version='$Revision$'

TRUE=1
FALSE=0
VERBOSE=$FALSE
NO_SYNC=$FALSE
FORCE_MODE=$FALSE

retcode=0

function INFO()
{
	if [ "$VERBOSE" = "$TRUE" ]; then 
		echo "$@"
	fi
}

function MSG()
{
	echo -n ">>> "
	echo "$@"
}

function MSGN()
{
	echo -n ">>> "
	echo -n "$@"
}

function init_grep_filter()
{
	local -a ldapattr=( $@ )
	# generate the grep filter
	grepfilter="${ldapattr[0]}:"
	for((i=1;i<${#ldapattr[@]};i++))
	do
		grepfilter="${grepfilter}|${ldapattr[$i]}:"
	done
}

user_default=`whoami`
managerDN="cn=Manager"
userBindDN="uid=AccessManagerAdmin,ou=People"
passwdBind=""
ldapserver=`awk '/^host/ {printf $2}' /etc/ldap.conf`
basedn=`awk '/^base/ {printf $2}' /etc/ldap.conf`
ldapuri=`awk '/^uri/ {printf $2}' /etc/ldap.conf`
if [ -n "${SUDO_USER}" ]; then
	currentUser="${SUDO_USER}"
else
	currentUser=`whoami`
fi

# the LDAP specific attributes and values for roles
LDAP_ROLE_OBJECTCLASS="amRole"
LDAP_ROLE_ATTR_ASSIGNABLE="amRoleAssignable"
LDAP_ROLE_ASSIGNABLE="true"
LDAP_ROLE_NOT_ASSIGNABLE="false"
LDAP_ROLE_PREFIX_ASSIGNED="RA"
LDAP_ROLE_PREFIX_ENABLED="RE"
LDAP_ROLE_PREFIX="${LDAP_ROLE_PREFIX_ASSIGNED}"
LDAP_ROLE_SEPARATOR="-"
LDAP_USER_ROLE_ATTR="nisNetgroupTriple"
LDAP_USER_ATTR_NAME="shadowMax"
LDAP_USER_ATTR_VALUE="99999"

function format_user_for_ldap_filter()
{
	local _user=$1
	echo "\\(,${user},\\)"
}

function format_user_for_role()
{
	local _user=$1
	echo "(,${user},)"
}

# the roles operations
OP_HELP=0
OP_SHOW_USER_ROLES=1
OP_SHOW_ROLES=2
OP_SHOW_ROLES_CONTENT=3
OP_ASSIGN=4
OP_ASSIGN_ENABLE=5
OP_REVOKE=6
OP_ENABLE=7
OP_DISABLE=8

SHOW_ONLY_ASSIGNABLE_ROLES="${FALSE}"

ROLE_STATE_ASSIGNED="assigned"
ROLE_STATE_ENABLED="enabled"
role_state="";
output_for_file="${FALSE}"
input_file=""
password_file=""

role_category=""

OP_SHOWS=${OP_SHOW_ROLES_CONTENT}
OPERATION="${OP_HELP}"
declare -a roles=( );
declare -a users=( );

# Keeps a list of users for which at least a role has been enabled or disabled
declare -a processedUsers=( );

serverOverride="${FALSE}"

function set_op_value(){
	if [ "${OPERATION}" -gt "${OP_HELP}" ]
	then
		echo "Only one operation at the time is allowed!"
		exit 1
	fi
	OPERATION="$1"
}

while getopts "hG:Lia:A:r:e:d:c:C:sSfnyu:F:m:Mp:l:H:b:x:v" option
do
    case $option in
     h) echo "`basename $0`, version $version";
        echo "Access Manager script to manage the users-roles RBAC relationship.";
        echo ""
		echo -n "Usage: `basename $0` [-h] [-G role_category] [-L] [-i] [-a|-A|-r|-e|-d|-c|-C roles] [-s|-S]  [-f] [-u usernames] [-F input_file]";
		if [ -z "${role_category}" ]; then
			#  the user is not restricted to a category,
			echo -n " [-m user] [-M] [-p password] [-l ldapserver] [-H ldapuri] [-b basedn] [-x password_file] [-v]";
		fi
		echo ""
        echo "  -h this info"
		echo ""
        echo "  -G <role_category> to be used as prefix for the <roles>;"
		if [ -n "${role_category}" ]; then
			echo "     EXAMPLE: the role_category ${role_category} and the roles 'shifter DAQ:expert' "
			echo "     will produce the roles '${role_category}:shifter ${role_category}:DAQ:expert' "
			echo "     which will be processed according to the other flags"
		fi
		echo ""
		echo "  Roles operations (only ONE operation from the list below can be called):"
        echo "  -L list the roles defined in LDAP in the role_category (if specified)"
        echo "  -i list the assignable roles defined in LDAP in the role_category (if specified)"
        echo "  -a assign the <roles> to the <username>"
        echo "  -A assign and enable the <roles> to the <username>"
        echo "  -r revoke the <roles> for the <username>"
        echo "  -e enable the <roles> for the <username>"
        echo "  -d disable the <roles> for the <username>"
        echo "  -c display the users with the <roles> assigned"
        echo "  -C display the users with the <roles> assigned and enabled"
        echo "  -s show the roles assigned to the user"
        echo "  -S show the roles assigned and enabled to the user"
		echo ""
        echo "  -f display the roles in a format to be stored in files"
        echo "  -u user names; default user is [${user_default}]"
        echo "  -F input text file with usernames one per line at the beginning; comments start with #"
		if [ -z "${role_category}" ]; then
	        echo "  -m authenticate as <user> to the LDAP server; default is AccessManagerAdmin"
	        echo "  -M authenticate as [${managerDN}] to the LDAP server"
	        echo "  -p password to bind to the LDAP server"
	        echo "  -l use this ldapserver;	default is [${ldapserver}]"
		echo "  -H use this ldapuri; default is [${ldapuri}]"
	        echo "  -b use this basedn;		default is [${basedn}]"
	        echo "  -x use the specified password file for authenticating to LDAP"
#	        echo "  -n no sync; disables the role -> POSIX group synchronization"
#	        echo "  -y assume yes; doesn't ask for a confirmation before performing a bulk operation"
	        echo "  -v verbose mode"
		fi
        echo ""
        echo "Author: mleahu@CERN"
        exit 0;;
     G) role_category=$OPTARG; user_default="";;
     L) set_op_value ${OP_SHOW_ROLES};;
     i) set_op_value ${OP_SHOW_ROLES}; SHOW_ONLY_ASSIGNABLE_ROLES=$TRUE;;   
     v) VERBOSE=$TRUE;;
     n) NO_SYNC=$TRUE;;
     y) FORCE_MODE=$TRUE;;
     a) set_op_value ${OP_ASSIGN}; roles=( $OPTARG );;
     A) set_op_value ${OP_ASSIGN_ENABLE}; roles=( $OPTARG );;
     r) set_op_value ${OP_REVOKE}; roles=( $OPTARG );;
     e) set_op_value ${OP_ENABLE}; roles=( $OPTARG );;
     d) set_op_value ${OP_DISABLE}; roles=( $OPTARG );;
     c) set_op_value ${OP_SHOW_ROLES_CONTENT}; roles=( $OPTARG );role_state="${ROLE_STATE_ASSIGNED}";;
     C) set_op_value ${OP_SHOW_ROLES_CONTENT}; roles=( $OPTARG );role_state="${ROLE_STATE_ENABLED}";;
     s) set_op_value ${OP_SHOW_USER_ROLES};role_state="${ROLE_STATE_ASSIGNED}";;
     S) set_op_value ${OP_SHOW_USER_ROLES};role_state="${ROLE_STATE_ENABLED}";;
     f) output_for_file="${TRUE}";;
     u) users=( $OPTARG );;
     F) input_file=$OPTARG;;
     m) userBindDN="uid=$OPTARG,ou=People";;
     M) userBindDN=${managerDN};;
     p) passwdBind=$OPTARG;;
     x) password_file=$OPTARG;;
     l) ldapserver=$OPTARG; serverOverride="${TRUE}";;
     H) ldapuri=$OPTARG;;
     b) basedn=$OPTARG;;
    esac
done
shift $(($OPTIND - 1))

if [ "${OPERATION}" = "${OP_HELP}" ]; then
	$0 -G "${role_category}" -h
	exit 0
fi

userBindDN="${userBindDN},$basedn"
rolesBaseDN="ou=netgroup,$basedn"
usersBaseDN="ou=people,$basedn"

if ( ! which ldapsearch > /dev/null 2>&1 ) 
then
	echo "ldapsearch tool not found!"
	exit 2
fi

if [ -n "${ldapuri}" -a "${serverOverride}" = "${FALSE}" ]; then
        connString="-H ${ldapuri}"
else
        connString="-h ${ldapserver}"
fi

ldapoptions="${connString} -b ${rolesBaseDN} -x"
# sort the output based on cn
ldapoptions="${ldapoptions} -S cn -LLL"
ldapfilter="(objectClass=${LDAP_ROLE_OBJECTCLASS})"
ldapwriteoptions=" -W "

# display the roles available in LDAP
if [ "${OPERATION}" = "${OP_SHOW_ROLES}" ]; then
	ldapfilter="(&${ldapfilter}(cn=${LDAP_ROLE_PREFIX}${LDAP_ROLE_SEPARATOR}${role_category}*))"
	if [ "${SHOW_ONLY_ASSIGNABLE_ROLES}" = "${TRUE}" ]; then
		ldapfilter="(&${ldapfilter}(amRoleAssignable=TRUE))"
	fi
	ldapattrs="cn"
	ldapscope="SUB"
	MSG "ROLES AVAILABLE IN LDAP [${connString}/${rolesBaseDN}?${ldapattrs}?${ldapscope}?${ldapfilter}]"
	if [ -n "${role_category}" ]; then
		MSG "ROLES FROM CATEGORY [${role_category}]"
	fi
	ldapsearch ${ldapoptions} "${ldapfilter}" ${ldapattrs} | awk "/^${ldapattrs}:/ { print substr(\$2,4) }"
	exit 0
fi


# read the users from file
if [ -n "${input_file}" ]; then
	if [ ! -r "${input_file}" ]; then
		echo "ERROR: Can't read the users from input file [${input_file}]"
	else
		users_from_file=( `grep -v "^#" ${input_file} | awk '{print $1}'` )
		INFO "${#users[@]} users from command line arguments"
		INFO "${#users_from_file[@]} users found in input file"
		users=( ${users[@]} ${users_from_file[@]} )
	fi
fi

#transform user names from upper case to lower case
for((i=0;i<${#users[@]};i++))
do
	users[$i]=`echo ${users[$i]} | tr '[:upper:]' '[:lower:]'`
done

# update the role names in the list with the category prefix
if [ -n "${role_category}" -a "${#roles[@]}" -gt "0" ]; then
	for ((i=0;i<${#roles[@]}; i++)); do
		roles[$i]="${role_category}:${roles[$i]}"
	done
	echo "### ROLES TO PROCESS: [${roles[@]}]"
fi

if [ "${OPERATION}" != "${OP_SHOW_ROLES_CONTENT}" ]; then
	if [ "${#users[@]}" -eq 0 ]; then
		users=( "${user_default}" )
	fi
	
	if [ "${#users[@]}" -eq 0 ]; then
		echo "ERROR: User names must be provided!"
		exit 2
	else 
		INFO "Check user names against LDAP..."
		#check if users are valid
		valid_users=( )
		for user in ${users[@]}
		do
			if ( ! ldapsearch -x -b "uid=$user,ou=People,$basedn" ${connString} "uid=$user" uid >/dev/null ); then
				MSG "WARNING: Skip user [$user] because was not found in LDAP! " 1>&2
			else
				valid_users=( ${valid_users[@]} $user )
			fi
		done
		if [ "${#valid_users[@]}" -gt 0 ]; then
			users=( ${valid_users[@]} )
			if [ "${#users[@]}" -gt 1 ]; then
				MSG "TOTAL: ${#users[@]} users to process: [ ${users[@]} ]"
				if [ "${OPERATION}" -gt "${OP_SHOWS}" -a "${FORCE_MODE}" -eq "${FALSE}" ]; 
				then
				    read -p "ARE YOU SURE YOU WANT TO PROCESS THEM (y/n)?" -n 1 answer
				    echo
				    if [ "${answer}" != "y" ]; then
						echo "Operation aborted!"
						exit 2
				    fi
				fi
			fi
		else
			MSG "No users to process!"
			exit 0
		fi
	fi
fi




if [ "${OPERATION}" -gt "${OP_SHOWS}" ]; then
		
	# get the password to bind without
	if [ -n "${password_file}" ]; then
	    ldapwriteoptions=" -y ${password_file} "
	else
	    if [ -z "${passwdBind}" ]
	    then
		echo -e "Password for ${userBindDN}:"
		read -s passwdBind
		if [ -z "${passwdBind}" ];
		then
			echo "Empty password for ${userBindDN} account!"
			exit 1
		fi	
	    fi
	    ldapwriteoptions=" -w ${passwdBind} "
	fi
fi

if [ "${VERBOSE}" = "${TRUE}" ]; then
	ldapwriteoptions="${ldapwriteoptions} -v"
fi

function if_role_exists_assignable()
{
	local _role=$1
	local _options="${ldapoptions}"
	local _filter="(&${ldapfilter}(&(cn=${LDAP_ROLE_PREFIX}${LDAP_ROLE_SEPARATOR}${_role})(${LDAP_ROLE_ATTR_ASSIGNABLE}=${LDAP_ROLE_ASSIGNABLE})))"
	local -a _attr=( "cn" )
	ldapsearch ${_options} "${_filter}" ${_attr[@]} | grep -q ${_role}
}

function is_user_netgroup_member()
{
	local _user=$1
	local _netgroup=$2
	local _options="${ldapoptions}"
	local _filter="(&${ldapfilter}(&(${LDAP_USER_ROLE_ATTR}=`format_user_for_ldap_filter ${_user}`)(cn=${_netgroup})))"
	local -a _attr=( "${LDAP_USER_ROLE_ATTR}" )
	ldapsearch ${_options} "${_filter}" ${_attr[@]} | grep -q ${_user}
}

function array_element_exists()
{
    if [ -z "$1" ]
    then
	return
    fi

    for i in ${processedUsers[@]}
    do
	if [ $i == $1 ]
	then
	    return 0
	fi
    done

    return 1
}

function mark_user_entry_as_modified()
{
    # Whenever an action is made that implies a change on the roles that a user has enabled, the user entry is changed by setting the shadowMax attribute to a standard value
    
    local _user=$1

    local _ldif="dn: uid=${_user},${usersBaseDN}
changetype: modify
replace: ${LDAP_USER_ATTR_NAME}
${LDAP_USER_ATTR_NAME}: ${LDAP_USER_ATTR_VALUE}"
    echo "${_ldif}" | ldapmodify -x ${ldapwriteoptions} -D "${userBindDN}" ${connString} > /dev/null
    if [ "${PIPESTATUS[1]}" != "0" ]; then
	MSG "FAILED CHANGING THE USER ENTRY!"
	INFO "LDIF: ${ldap_ldif}"
    #else
	#MSGN "OK CHANGING THE USER ENTRY!"
    fi
}

function sync_role_2_group()
{
	if [ "${NO_SYNC}" = "${FALSE}" ]; then
		pushd `dirname $0` > /dev/null
		scriptDir=`pwd`
		popd >/dev/null
		writeOptions=""
		if [ -n "${password_file}" ];
		then
		    writeOptions="-x '${password_file}'"
		else
		    writeOptions="-p '${passwdBind}'"
		fi
		syncCmd="${scriptDir}/amSyncRoles2PosixGroups -s -D $userBindDN $writeOptions"
		eval $syncCmd
		# echo "Doing the sync between the roles and the CMF groups"
		sudo /sw/sysadmin-tools/am/protected/amSyncRoles2CMFGroups -s
		# amSyncRoles2PosixGroups -s -D $userBindDN -p $passwdBind
	fi

	# We get all the DCS roles
	declare -a dcsRoles
	role_category="DCS:"
	LDAP_ROLE_PREFIX="${LDAP_ROLE_PREFIX_ASSIGNED}"
	ldapfilter="(objectClass=${LDAP_ROLE_OBJECTCLASS})"
	ldapfilter="(&${ldapfilter}(cn=${LDAP_ROLE_PREFIX}${LDAP_ROLE_SEPARATOR}${role_category}*))"
	ldapattrs="cn"                                                                                                                                                             
        ldapscope="SUB"
	dcsRoles=( `ldapsearch ${ldapoptions} "${ldapfilter}" ${ldapattrs} | awk "/^${ldapattrs}:/ { print substr(\\$2,4)}"` )

	output=""
	for user in ${users[@]}
	do	
	    declare -a disabledRoles=( )                                                                                                                                                   
            declare -a enabledRoles=( )
	    for role in ${dcsRoles[@]}
	    do
		#echo "Checking role ${role} for user ${user}"
		roleName="${LDAP_ROLE_PREFIX_ENABLED}${LDAP_ROLE_SEPARATOR}${role}"
		getent netgroup "${roleName}" | grep -q ${user}
		if [ $? -eq 0 ]; then
		    #echo "The user seems to have the role enabled"
		    roleUsers=( `getent netgroup "${roleName}" | sed 's| (|\n|g' | sed 's| , ||' | sed 's|, )$||' | sed 1d | sort -u` )
		    for roleUser in ${roleUsers[@]}
		    do
			if [[ "$roleUser" = "$user" ]]; then
			    #echo "The user does have the role enabled"
			    enabledRoles=( "${enabledRoles[@]}" "$role" )
			    break
			fi
		    done
		#else
		#    roleName="${LDAP_ROLE_PREFIX_ASSIGNED}${LDAP_ROLE_SEPARATOR}${role}"
		#    getent netgroup "${roleName}" | grep -q ${user}
		#    if [ $? -eq 0 ]; then                                                                                                                                              
                	#echo "The user seems to have the role disabled"
            	#        roleUsers=( `getent netgroup "${roleName}" | sed 's| (|\n|g' | sed 's| , ||' | sed 's|, )$||' | sed 1d | sort -u` )                                            
            	#	for roleUser in ${roleUsers[@]}                                                                                                                                
                #	do                                                                                                                                                             
        	#            if [[ "$roleUser" = "$user" ]]; then                                                                                                                       
    	                        #echo "The user does have the role disabled"                                                                                                             
    	    	#               disabledRoles=( "${disabledRoles[@]}" "$role" )                                                                                                            
		#		break
	        #            fi                                                                                                                                                         
                #        done
		    #elif 
		#    fi
		fi
	    done
	    if [ ${#enabledRoles} -gt 0 ]; then
		output="${output}\"${user}|"
		for role in ${enabledRoles[@]}
		do
		    output="${output}${role},"
		done
		output="${output:0:${#output}-1}\" "
	    else
		output="${output}\"${user}|none\" "
	    fi
	    #if [ ${#disabledRoles} -gt 0 ]; then                                                                                                                                  
            #    output="disable|${user}|"                                                                                                                                           
            #    for role in ${disabledRoles[@]}                                                                                                                                     
            #    do                                                                                                                                                                 
            #        output="${output}${role},"                                                                                                                                     
            #    done                                                                                                                                                               
            #    echo "${output:0:${#output}-1}"                                                                                                                           
            #fi
	    #echo "Enabled roles for user ${user}: ${enabledRoles[@]}"
	    #echo "Assigned roles for user ${user}: ${assignedRoles[@]}"
	done

	output="${output:0:${#output}-1}"

	if [[ "${output}" != "" ]]; then
	    echo "${output}"
	    ssh pcatlgcsacs.cern.ch /scratch/pvss/scripts/DCSUserRolesChanged.sh "${output}"
	fi
}

# should be called like this: add_remove_user_to_netgroup <"add"|"delete"> ${user} ${role}
function add_remove_user_to_netgroup()
{
	local _operation=$1
	local _user=$2
	local _netgroup=$3
	if [ "${_operation}" = "add" ]; then
		if ( is_user_netgroup_member ${_user} ${_netgroup} ); then
			# already added
			MSGN "ALREADY DONE!"
			return 0
		fi
	elif [ "${_operation}" = "delete" ]; then
		if ( ! is_user_netgroup_member ${_user} ${_netgroup} ); then
			# already deleted
			MSGN "ALREADY DONE!"
			return 0
		fi
	else
		echo "WRONG OPERATION: ${_operation}"
		return 1
	fi
	
	local _ldif="dn: cn=${_netgroup},${rolesBaseDN}
${_operation}: ${LDAP_USER_ROLE_ATTR}
${LDAP_USER_ROLE_ATTR}: `format_user_for_role ${_user}`"
	echo "${_ldif}" | ldapmodify -x ${ldapwriteoptions} -D "${userBindDN}" ${connString} > /dev/null
	if [ "${PIPESTATUS[1]}" != "0" ]; then
		MSG "FAILED!"
		INFO "LDIF: ${ldap_ldif}"
	else
		MSGN "OK!"
		if [[ ${_netgroup} == ${LDAP_ROLE_PREFIX_ENABLED}* ]]; then 
		    if ( ! array_element_exists "${_user}" ); then
			mark_user_entry_as_modified "${_user}"
			processedUsers=( "${processedUsers[@]}" "${_user}" )
		    fi
		fi
	fi
	
}

if [ "${OPERATION}" = "${OP_SHOW_USER_ROLES}" ]; then
	for user in ${users[@]}
	do
		if [ "${#users[@]}" -gt 1 -a "${output_for_file}" = "${FALSE}" ]; then echo "### ${user}"; fi
		
		#############################
		INFO "### DISPLAY USER[${user}]'S ROLES IN STATE [`echo ${role_state}| tr a-z A-Z`] ###"
		#############################
		prefix="${LDAP_ROLE_PREFIX_ASSIGNED}"
		if [ "${role_state}" = "${ROLE_STATE_ENABLED}" ]; then
			prefix="${LDAP_ROLE_PREFIX_ENABLED}"
		fi
		filter="(&${ldapfilter}(&(cn=${prefix}${LDAP_ROLE_SEPARATOR}*)(${LDAP_USER_ROLE_ATTR}=`format_user_for_ldap_filter $user`)))"
		attr=( "cn" )
		init_grep_filter ${attr[@]}
		if [ "${output_for_file}" = "${TRUE}" ]; then
			echo -n "$user:	"
			ldapsearch ${ldapoptions} "${filter}" ${attr[@]} | grep -P ${grepfilter} | cut -d "${LDAP_ROLE_SEPARATOR}" -f 2- | sort | tr '\n' '\t' 
			echo
		else
			ldapsearch ${ldapoptions} "${filter}" ${attr[@]} | grep -P ${grepfilter} | cut -d "${LDAP_ROLE_SEPARATOR}" -f 2- | sort
		fi
	done
elif [ "${OPERATION}" = "${OP_SHOW_ROLES_CONTENT}" ]; then
	#############################
	INFO "DISPLAY THE CONTENT OF ROLES [${roles[@]}] IN STATE [`echo ${role_state}| tr a-z A-Z`]"
	#############################
	prefix="${LDAP_ROLE_PREFIX_ASSIGNED}"
	if [ "${role_state}" = "${ROLE_STATE_ENABLED}" ]; then
		prefix="${LDAP_ROLE_PREFIX_ENABLED}"
	fi
	attr=( "${LDAP_USER_ROLE_ATTR}" )
	init_grep_filter ${attr[@]}
	for role in ${roles[@]}
	do
		if [ "${#roles[@]}" -gt "1" ]; then
			echo "### ROLE [$role]"
		fi
		filter="(&${ldapfilter}(cn=${prefix}${LDAP_ROLE_SEPARATOR}${role}))"
		
		# check first if the role exists
		if ( ! ldapsearch ${ldapoptions} "${filter}" cn | grep -q "${role}" ); then
			MSG "ROLE [$role] NOT DEFINED!"
			continue;
		fi
		
		if [ "${output_for_file}" = "${TRUE}" ]; then
			ldapsearch ${ldapoptions} "${filter}" ${attr[@]} | grep -P ${grepfilter} | sort | cut -d ',' -f2 | tr -d ")" | while read line
			do 
				echo "$line: ${role}"
			done
		else
			ldapsearch ${ldapoptions} "${filter}" ${attr[@]} | grep -P ${grepfilter} | sort | cut -d ',' -f2 | tr -d ')'
			if [ "${PIPESTATUS[0]}" -ne 0 ]; then
				MSG "ROLE NOT FOUND!"
			fi
		fi
	done
elif [ "${OPERATION}" = "${OP_ASSIGN}" ]; then
	#############################
	INFO "ASSIGN ROLES "
	#############################
	for user in ${users[@]}
	do
		MSG "Assign roles to user [$user]..."
		for role in ${roles[@]}
		do
			MSGN "...role [$role]..."
			if ( ! if_role_exists_assignable ${role} ); then
				echo "SKIPPED! Role doesn't exist!"
				continue
			fi
			echo -n "assign "
			add_remove_user_to_netgroup "add" "${user}" "${LDAP_ROLE_PREFIX_ASSIGNED}${LDAP_ROLE_SEPARATOR}${role}"
			echo
		done
	done
	#sync_role_2_group
	INFO "DONE"
elif [ "${OPERATION}" = "${OP_ASSIGN_ENABLE}" ]; then
	#############################
	INFO "ASSIGN AND ENABLE ROLES "
	#############################
	for user in ${users[@]}
	do
		MSG "Assign and enable roles to user [$user]..."
		for role in ${roles[@]}
		do
			MSGN "...role [$role]..."
			if ( ! if_role_exists_assignable ${role} ); then
				echo "SKIPPED! Role doesn't exist!"
				continue
			fi
			echo -n "assign "
			add_remove_user_to_netgroup "add" "${user}" "${LDAP_ROLE_PREFIX_ASSIGNED}${LDAP_ROLE_SEPARATOR}${role}"
			echo -n " ...enable "
			add_remove_user_to_netgroup "add" "${user}" "${LDAP_ROLE_PREFIX_ENABLED}${LDAP_ROLE_SEPARATOR}${role}"
			echo
		done
	done
	#sync_role_2_group
	INFO "DONE"
elif [ "${OPERATION}" = "${OP_REVOKE}" ]; then
	#############################
	INFO "REVOKE ROLES "
	#############################
	for user in ${users[@]}
	do
		MSG "Revoke roles from user [$user]..."
		for role in ${roles[@]}
		do
			MSGN "...role [$role]..."
			if ( ! if_role_exists_assignable ${role} ); then
				echo "SKIPPED! Role doesn't exist!"
				continue
			fi
			if ( ! is_user_netgroup_member ${user} "${LDAP_ROLE_PREFIX_ASSIGNED}${LDAP_ROLE_SEPARATOR}${role}" ); then
				echo "SKIPPED! Role not assigned!"
				continue
			fi
			echo -n "disable "
			add_remove_user_to_netgroup "delete" "${user}" "${LDAP_ROLE_PREFIX_ENABLED}${LDAP_ROLE_SEPARATOR}${role}"
			echo -n " ...revoke "
			add_remove_user_to_netgroup "delete" "${user}" "${LDAP_ROLE_PREFIX_ASSIGNED}${LDAP_ROLE_SEPARATOR}${role}"
			echo
		done
	done
	#sync_role_2_group
	INFO "DONE"
elif [ "${OPERATION}" = "${OP_ENABLE}" ]; then
	#############################
	INFO "ENABLE ROLES "
	#############################
	for user in ${users[@]}
	do
		MSG "Enable roles for user [$user]..."
		for role in ${roles[@]}
		do
			MSGN "...role [$role]..."
			if ( ! is_user_netgroup_member ${user} "${LDAP_ROLE_PREFIX_ASSIGNED}${LDAP_ROLE_SEPARATOR}${role}" ); then
				echo "SKIPPED! Role not assigned!"
				continue
			fi
			echo -n "enable "
			add_remove_user_to_netgroup "add" "${user}" "${LDAP_ROLE_PREFIX_ENABLED}${LDAP_ROLE_SEPARATOR}${role}"
			echo
		done
	done
	#sync_role_2_group
	INFO "DONE"
elif [ "${OPERATION}" = "${OP_DISABLE}" ]; then
	#############################
	INFO "DISABLE ROLES "
	#############################
	for user in ${users[@]}
	do
		MSG "Disable roles for user [$user]..."
		for role in ${roles[@]}
		do
			MSGN "...role [$role]..."
			if ( ! is_user_netgroup_member ${user} "${LDAP_ROLE_PREFIX_ASSIGNED}${LDAP_ROLE_SEPARATOR}${role}" ); then
				echo "SKIPPED! Role not assigned!"
				continue
			fi
			if ( ! is_user_netgroup_member ${user} "${LDAP_ROLE_PREFIX_ENABLED}${LDAP_ROLE_SEPARATOR}${role}" ); then
				echo "SKIPPED! Role not enabled!"
				continue
			fi
			echo -n "disable "
			add_remove_user_to_netgroup "delete" "${user}" "${LDAP_ROLE_PREFIX_ENABLED}${LDAP_ROLE_SEPARATOR}${role}"
			echo
		done
	done
	#sync_role_2_group
	INFO "DONE"
fi

exit 0
