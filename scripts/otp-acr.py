#!/usr/bin/env tdaq_python

# Script to enable roles for shifts in ACR 
# tdaq release setup is required: source /sw/tdaq/setup/setup_tdaq-02-00-03.sh
# written by Diana and Liviu


from otptz import cx_Oracle, Common

import time
import datetime
import os
import sys
import getopt
import pickle
import logging, logging.handlers
import subprocess
	
def main ():

	global allocated_status, person_status, status;
	global yesterday_date, today_date, tomorrow_date, end_date;
	global date_from, date_to;
	global search_fromHour, search_toHour;
	global Dictionary, Dictionary_keys;
	global Level;
	global level_name;

	ldapServer = None
	checkP1UserScript = '/sw/sysadmin-tools/ldap/checkP1User.sh'
	mailto = ["Diana.Scannicchio@cern.ch"]
	mailfrom = ["Diana.Scannicchio@cern.ch"]
	emergency = False
	noLog = False
	PathLog = '/var/log/otp/'
	PathCache = '/var/spool/otp/'
	DelayCache = 7
	
	Dictionary_keys=[]
	allocated_status=[]
	person_status=[]
	status=[]

	blue = '\033[34m'
	green = '\033[32m'
	yellow= '\033[33m'
	red = '\033[31m'
	white = '\033[37m'
	reset = '\033[0m'
	
	
	def usage():
		print """
NAME 
	This script automatically enable/disable roles using information retrieved from OTP DataBase.
	
SYNOPSIS
	%s [-l ldapserver] [-d YYYY-MM-DD] [-t hour] [-p pathLog] [-c cacheLog] [-r delayCache] [-o outputLevel] [-e] [-n] [-h help]
	
OPTIONS:
	-d, --date: provide a date (YYYY-MM-DD) [default is today date]
	-t, --time: provide a number for the Hour from 00 to 24 [default is now]
	-l, --ldapserver: provide an ldap server name [default is atlas-ldap, for test please use atlas-ldap-test]
	-p, --pathLog: to define a path [Default is /var/log/otp]
	-c, --pathCache: to define a path [Default is /var/spool/otp]	
	-r, --delayCache: to define number of next days to be cached/recorded [default is 7]
	-o, --outputLevel: verbose, debug, info, warning, error, critical [default is info]
	-e, --emergency: to simulate a connection problem to OTP DB [default is False]
	-n, --noLog: to DISABLE writing in the log file [Default is True = writing enabled]
	-h, --help: this message

AUTHORS
	Diana Scannicchio, Liviu Valsan	
		""" % sys.argv[0]
		exit()

		
	try:                                
		opts, args = getopt.getopt(sys.argv[1:], "d:t:l:o:p:c:r:enh", ["date=", "time=", "ldapServer=", "outputLevel", "pathLog", "pathCache", "delayCache", "emergency", "noLog","help"])
	except getopt.GetoptError, err:
		print '\nATTENTION PLEASE:\t', str(err)
		usage()
		sys.exit(2)


######################
# Define log levels
######################

	logging.VERBOSE = 5			# new log level definition 

	LogLevels = {'verbose': logging.VERBOSE, 
				'debug': logging.DEBUG,
				'info': logging.INFO, 
				'warning': logging.WARNING,
				'error': logging.ERROR,
				'critical': logging.CRITICAL}
	
	logging.addLevelName(logging.VERBOSE, 'VERBOSE')

	Logger=logging.getLogger(' AM-OTP ')

	Level= LogLevels.get('info')
	level_name = 'info'
	
	
#################################
# Defining current DATE and TIME
#################################	

	today = datetime.datetime.today()
		
	Time= time.strftime("%H:%M:%S", time.localtime())
	now_hour = time.strftime("%H", time.localtime())
	now_minute = time.strftime("%M", time.localtime())

# Command line OPTIONS
	for opt, arg in opts:
		if opt in ("-d", "--date"):
			print "We've got a date: \t\t" + arg
			today = datetime.datetime.strptime(arg, "%Y-%m-%d")
		elif opt in ("-t", "--hour"):
			print "We've got an hour: \t\t" + arg
			now_hour = arg
		elif opt in ("-l", "--ldapServer"):
			print "We've got an ldapServer: \t" + arg
			ldapServer = arg
		elif opt in ("-o", "--outputLevel"):
			if arg in ( 'verbose', 'debug', 'info', 'warning', 'error', 'critical'):	
				level_name = arg
				Level = LogLevels.get(level_name,logging.NOTSET)
				print "We've got a log Level name: \t", level_name,'(', Level,')'
			else :
				usage()
		elif opt in ("-p", "--pathLog"):
			print "We've got a path for Log: \t\t" + arg
			PathLog = arg
		elif opt in ("-c", "--pathCache"):
			print "We've got a path for Cache files: \t" + arg
			PathCache = arg
		elif opt in ("-r", "--delayCache"):
			print "We've got a number for days to be recorded in Cache files: \t" + arg
			DelayCache = int(arg)	
		elif opt in ("-e", "--emergency"):
			emergency = True
			print "We're simulating an emergency: \t", emergency
		elif opt in ("-n", "--noLog"):
			noLog = True;
			print "We're Disabling log file: \t", noLog
		elif opt in ("-h", "--help"):
			usage()
			sys.exit()


################################
# To enable/disable a log file #
################################

	log_file = PathLog + 'otp-acr.log'
 
######################
# Rotating log files
######################

#       log_handler = logging.handlers.RotatingFileHandler(log_file, maxBytes=100000, backupCount=7)
#       Logger.addHandler(log_handler)
        
#	log_handler = logging.handlers.TimedRotatingFileHandler(log_file, when='D' ,interval=7, backupCount=7)
#	Logger.addHandler(log_handler)
	
	if (noLog) :
		logging.basicConfig(level=Level)
	else :
		logging.basicConfig(level=Level, filename=log_file, filemode='a')

	Logger.info('\t\t Log Level Name is %s and Log level value is %s', level_name, Level)

	Logger.info ('\t\t ***********************')
	Logger.info ('\t\t * OTP ROLE MANAGEMENT *')
	Logger.info ('\t\t ***********************\n')

	Logger.info ('\t\t Starting at %s', datetime.datetime.now().strftime("%d-%b-%Y %H:%M:%S"))

	
##############################
# Calculating DATE and TIME
##############################	

	today_date = today.strftime("%d-%b-%Y")
	date_to = today_date
	date_from = today_date
	
	tomorrow = today + datetime.timedelta(1);
	tomorrow_date = tomorrow.strftime("%d-%b-%Y")	

	yesterday=today - datetime.timedelta(1)
	yesterday_date = yesterday.strftime("%d-%b-%Y")
	
	end = today + datetime.timedelta(DelayCache);
	end_date = end.strftime("%d-%b-%Y")
	
	Logger.info ('\t\t Today real date is \t %s \tand current time is %s', today_date, Time)	
	Logger.info ('\t\t Tomorrow  date is  \t %s', tomorrow_date)
	Logger.info ('\t\t Yesterday date was \t %s', yesterday_date)
	Logger.info ('\t\t End date is \t %s', end_date)
	
	Logger.debug ('\t\t Query uses hour:minute:\t %s:%s', now_hour, now_minute)
	
	Hour = now_hour
	
	Hour_diff = 2
	
	search_toHour = int(Hour) - Hour_diff
	search_fromHour = int(Hour) + 1 + Hour_diff
                                                                                                
	if search_toHour <= 00: 
		date_to = yesterday_date
		search_toHour = 24 + search_toHour		
		Logger.info ('\t\t Today date, when Hour is 00, is %s and search_toHour is %s', date_to, search_toHour)
				
	if search_fromHour >= 24:
		date_from = tomorrow_date
		search_fromHour = 0 + search_fromHour - 24	
		Logger.info ('Today date, when Hour is 23, is %s and search_fromHour is %s', date_from, search_fromHour)
			
	Logger.info ('\t\t DB Query performed using:\n\t\t\t Current Hour = %s, \t toHour = %s, \t fromHour = %s \n', \
				Hour, search_toHour, search_fromHour)	
		

######################################	       
# Read information from the Database 
######################################
	
	def fetchdata():
		cursor = connection.cursor()
		cursor.arraysize = 3000

#		Logger.info ('\t\t Querying OTP with yestarday (%s), today (%s) and tomorrow (%s) dates \n', \
#					yesterday_date,today_date, tomorrow_date)	

		Logger.info ('\t\t Querying OTP from yesterday (%s) to next 10 days (%s) dates \n', \
                                        yesterday_date, end_date)

###############################################################	
#	RETRIEVAL SHIFT Information
# Retrieving from table SUM_ALLOCATION_V: 
# the task id and the person allocated for shift 
###############################################################
	
#		cursor.execute("select task_id, allocated_person_id, dt, hour_from, hour_to from SUM_ALLOCATION_V \
#	      	where (dt='%s' or dt='%s' or dt='%s') and \
#			task_id in (%s) " \
#			%(yesterday_date, today_date, tomorrow_date, Dictionary_keys))						

		cursor.execute("select task_id, allocated_person_id, dt, hour_from, hour_to from SUM_ALLOCATION_V \
			WHERE dt BETWEEN '%s' AND '%s' AND \
			task_id IN (%s) " \
			%(yesterday_date, end_date, Dictionary_keys))

			      
		allocated_info = cursor.fetchmany()		
	
		Logger.debug ('\t\tLEN allocated_info %s ', len(allocated_info))
		Logger.debug ('\t\tLIST allocated_info \n %s \n', allocated_info)
 
		allocated_status.append(allocated_info)
	   
		for i in range(0,len(allocated_info)):
			task_id = allocated_info[i][0]
			shifter = allocated_info[i][1]
			fromHour = allocated_info[i][3]
			toHour = allocated_info[i][4]

############################################################	  
# Retrieving from table PERSON: 
# last name, first name, otp id, cern id, username, email 
############################################################

			cursor.execute("select last_name, first_name, id, cernid, username, email, ais_person_id from PERSON where id='%s'" %shifter)
			person_info = cursor.fetchmany()	  
			person_status.append(person_info)

# END fetch info about EMERGENCY shift

		status.append(person_status)
		status.append(allocated_status)
	
		Logger.info ('\t\t Closing the cursor used to fetch the information from OTP DB')
		cursor.close()			
		
		output1 = open(PathCache + 'data_status.pk', 'wb')
		output2 = open(PathCache + 'data_person.pk', 'wb')
		
		pickle.dump(status,output1)
		pickle.dump(person_status,output2)
		
		output1.close()
		output2.close()

		return  status, allocated_status, person_status		

#######################		
# END of fetchdata()		
#######################

######################################	       
# Read information from the OTP Database 
######################################

	def run(allocated_status, person_status, status):
		
		Logger.info ('\t\t Looking now for shifts and shifters \n')		
		
		if (emergency) :
			Logger.warning ('\t We are in Emergency mode, all information will be retrieved from files')
			
			input1 = open(PathCache + 'data_status.pk', 'rb')
			input2 = open(PathCache + 'data_person.pk', 'rb')
		
			status = pickle.load(input1)
			person_status = pickle.load(input2)		

			input1.close()
			input2.close()
		
			Logger.warning ('\t Information have been retrieved by EMERGENCY files (pickle)')
			Logger.warning ('\t Querying Emergency files for retrieval with date = %s \n', today_date)

		Logger.debug ('\t\t LEN status %s', len(status) )	
		Logger.debug ('\t\t Range LEN status is %s', range(len(status)) )
		Logger.debug ('\t\t LIST status\n %s \n', status)
		Logger.debug ('\t\t LEN person_status %s', len(person_status))			
		Logger.debug ('\t\t LIST person _status\n %s \n', person_status)

				
		if len(person_status)==0 :
			Logger.info ('\t\t NO INFO for shift')
				
								
		for i in range(0,len(person_status)):
			lastname = status[0][i][0][0]	  
			firstname = status[0][i][0][1]
			otpid = status[0][i][0][2]
			cernid = status[0][i][0][3]	  
			username = status[0][i][0][4]
			email = status[0][i][0][5]
			ais = status[0][i][0][6]
			taskId = status[1][0][i][0]
			otpId = status[1][0][i][1]
			date = status[1][0][i][2]
			fromHour = status[1][0][i][3]
			toHour = status[1][0][i][4]
		
			def PrintInfo():
				Logger.log (Level,'\t\t Lastname : \t\t %s', lastname)
				Logger.log (Level,'\t\t Firstname :\t\t %s', firstname)
				Logger.log (Level,'\t\t OTP Id :\t\t %s', otpid)
				Logger.log (Level,'\t\t CERN Id :\t\t %s', cernid)
				Logger.log (Level,'\t\t CERN username :\t %s', username)
				Logger.log (Level,'\t\t CERN email :\t\t %s', email)
				Logger.log (Level,'\t\t CERN AIS :\t\t %s', ais)
				Logger.log (Level,'\t\t -------')	  
				Logger.log (Level,'\t\t TaskId : \t\t %s', taskId)
				Logger.log (Level,'\t\t OTP ID (shifter) is: \t %s', otpId)
				Logger.log (Level,'\t\t Date : \t\t %s', date.strftime("%d-%b-%Y"))
				Logger.log (Level,'\t\t From Hour : \t\t %s', fromHour)
				Logger.log (Level,'\t\t To Hour : \t\t %s', toHour)

								

			if (emergency) :
				Logger.warning ('\t\t Data are retrieved from Emergency files ')
				Logger.warning ('\t\t *************************************** ')
				
			if (Level==LogLevels.get('verbose')) :
				Logger.log (Level,'\t *********************************************** ')
				Logger.log (Level,'\t *** ALL SHIFTS (yesterday, today, tomorrow) *** ')
				Logger.log (Level,'\t *********************************************** ')
				PrintInfo()			
				Logger.log (Level, '\t ----------------------------------------------')
				
			otp_date= date.strftime("%d-%b-%Y")
			
			if (emergency) :
				Logger.warning ('\t\t Date retrieved from emergency files is %s and today is %s', otp_date, today_date)
				
			Logger.log (LogLevels.get('verbose'),'\t SEARCH_FROM is %s and SEARCH_TO is %s ', search_fromHour, search_toHour)

			
###############################
# Looking for NEXT SHIFTER
###############################			

										
			if (fromHour == search_fromHour) and (otp_date == date_from)  :
			
				Logger.info ('\t\t Hello! we got the NEXT SHIFTER')
				
				role_name=Common.getRoleByID(taskId)
			
				Logger.log (LogLevels.get('verbose'), \
									'\t taskId from OTP is %s \n\t\t\t role name is %s \n', taskId, role_name)	  				
#				stdout_handle = os.popen(checkUserScript + " " + str(ais), "r")
#				P1_username = stdout_handle.read()
				
				stdout_handle = subprocess.Popen(checkP1UserScript + " " + str(ais), shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
				P1_username = stdout_handle.communicate()[0].strip()
				stdout_handle.stdout.close()

#				if P1_username == '':
#					Logger.error ('\t\t User  %s %s  does NOT have a P1 account and he/she booked a shift with role %s for taskId %s',\
#							 lastname, firstname, role_name, taskId)
#				else:
				if stdout_handle.wait() != 0:
					Logger.warning ('The user %s %s has more P1 account', lastname, firstname)
					P1_username = username.lower()			
			
			
				Logger.info ('\t\t Username is \t %s \n\t\t\t role is \t %s \n\t\t\t taskId is \t %s',P1_username, role_name, taskId)

				if (Level==LogLevels.get('debug') or Level==LogLevels.get('verbose')) :
					Logger.log (Level,'\t\t *************** ')
					Logger.log (Level,'\t\t *** DETAILS *** ')
					Logger.log (Level,'\t\t *************** ')
					PrintInfo()
					Logger.log (Level,'\t\t --------------------------------------')

				extra = ''
				if ldapServer != None:
					extra = ' -l ' + ldapServer
					

				Logger.info ('\t\t sudo /sw/sysadmin-tools/am/protected/amUserRoles' + extra + ' -e ' + role_name + ' -u ' + P1_username)
				print 'sudo /sw/sysadmin-tools/am/protected/amUserRoles' + extra + ' -e ' + role_name + ' -u '+ P1_username
				
				os.system('sudo /sw/sysadmin-tools/am/protected/amUserRoles' + extra + ' -e ' + role_name + ' -u '+ P1_username)
			else :
				Logger.debug ('\t\t There is NO next shift for today %s starting at %s', today_date,search_fromHour)
					
			
###############################
# Looking for PREVIOUS SHIFTER
###############################		
						
			if ((toHour==search_toHour) and (otp_date==date_to) and (fromHour<toHour)) \
				or \
				((toHour==search_toHour) and (otp_date==yesterday_date) and (fromHour> toHour)) :
			 	
				Logger.info ('\t\t Hello! We got the PREVIOUS SHIFTER')
						
				role_name=Common.getRoleByID(taskId)
				
				Logger.log (LogLevels.get('verbose'), \
									'\t taskId from OTP is %s \n\t\t\t and role name is %s \n', taskId, role_name)

#				stdout_handle = os.popen(checkUserScript + " " + str(ais), "r")
#				P1_username = stdout_handle.read()


				stdout_handle = subprocess.Popen(checkP1UserScript + " " + str(ais), shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
				P1_username = stdout_handle.communicate()[0].strip()
				stdout_handle.stdout.close()

#				if P1_username == '':
#					Logger.error ('\t\t User  %s %s  does NOT have a P1 account and he/she booked a shift with role %s for taskId %s',\
#							 lastname, firstname, role_name, taskId)
#				else:
				if stdout_handle.wait() != 0:
					Logger.warning ('The user %s %s has more P1 account', lastname, firstname)
					P1_username = username.lower()			
						
				Logger.info ('\t\t Username is \t %s \n\t\t\t and role is \t %s \n\t\t\t taskId is \t %s', P1_username, role_name, taskId)
				
				if (Level==LogLevels.get('debug') or Level==LogLevels.get('verbose')) :
					Logger.log (Level,'\t *************** ')
					Logger.log (Level,'\t *** DETAILS *** ')
					Logger.log (Level,'\t *************** ')
					PrintInfo()
																
				extra = ''
				if ldapServer != None:
					extra = ' -l ' + ldapServer
					
				Logger.info ('\t\t sudo /sw/sysadmin-tools/am/protected/amUserRoles' + extra + ' -d ' + role_name + ' -u ' + P1_username)
				print 'sudo /sw/sysadmin-tools/am/protected/amUserRoles' + extra + ' -d ' + role_name + ' -u '+ P1_username
				
				os.system('sudo /sw/sysadmin-tools/am/protected/amUserRoles' + extra + ' -d ' + role_name + ' -u '+ P1_username)

			else :
				Logger.debug ('\t\t There is NO previous shift for today %s finishing at %s',today_date, search_toHour)

	
####################				
# END of run()		
####################

####################
### 	MAIN 	####		
####################
	

##############
# Dictionary
##############
			
###############################################
# Fetching the Dictionary containing OTP tak ID
###############################################

#	Dictionary_status, Dictionary, Dictionary_keys = fetchDictionary()			

	Dictionary = Common.Dictionary()

	Logger.log (LogLevels.get('verbose'),'\t Dictionary keys: \n %s \n', Dictionary.keys())
	Logger.log (LogLevels.get('verbose'),'\t Dictionary values: \n %s \n', Dictionary.values())
	Logger.log (LogLevels.get('verbose'),'\t Dictionary items: \n %s \n', Dictionary.items())
	Logger.log (LogLevels.get('verbose'),'\t Dictionary: \n %s \n', Dictionary)

	Logger.info ('\t\t ------------------------------')
	Logger.info ('\t\t Role Name \t\t TaskId' )
	Logger.info ('\t\t --------- \t\t ------' )
	
	all_values =""

	for keys, values in Dictionary.items():
		all_values= all_values + values.replace("'",'') +','
#		print "keys", keys
#		print "all_values", all_values
#		print "values", values
		Logger.info ('\t\t %s\t\t %s', keys, values.replace("'", ''))	
		Dictionary_keys.append(keys)				
	Logger.info ('\t\t ------------------------------\n')
	
	Dictionary_keys=all_values[:-1]



	Logger.debug('\tDictionary_keys: \n %s \n', Dictionary_keys)
				
					
########################
# Connection to OTP DB
########################
		
	Logger.debug ('\nToday is %s', datetime.datetime.now().strftime("%d-%b-%Y %H:%M:%S"))				  
	Logger.info ('\t\t Opening the connection to Oracle OTP DB')
		

	if (emergency):		
		Logger.info ('\t\t ... but we are simulating an emergency, no data will be fetched by the DB')

		subject = "TEST! OTP DB connection broken"
		message = "Sorry for the spamming ... \n Simulating OTP Problem. "
		Common.sendMail(subject, message, mailto, mailfrom)		
			
	else :
		
		try:
			connection = cx_Oracle.connect('PPT_MAO_PUBLIC/mjc89Kin@ppt_service_prod')	
			Logger.info ('\t\t We got connected')
		
			status, allocated_status, person_status = fetchdata()		
			
			Logger.info ('\t\t Closing the connection to the Oracle OTP DB')
			connection.close()
		
		except cx_Oracle.DatabaseError, msg:
			Logger.error ('\t\t Error in DB Connection: %s', msg) 				
			emergency = True
			Logger.warning ('\t Emergency ongoing \t %s', emergency)
			
			subject = "ERROR in OTP DB Connection"
			message = "The OTP DB Connection error is:\n\n%s" % msg
			Common.sendMail (subject, message, mailto, mailfrom)
			
#			return 
			
	run(allocated_status, person_status, status)
		
	Logger.info ('\t\t THE END at %s\n\n', datetime.datetime.now().strftime("%d-%b-%Y %H:%M:%S"))


	    
if __name__ == "__main__":
	main()
