#!/bin/bash

# Valgrind command line to check the C++ API

v_opts="--tool=memcheck\
 --show-reachable=yes\
 --leak-check=yes\
 --track-fds=yes\
"

v_bin="/usr/bin/valgrind"
if [ ! -x "${v_bin}" ]; then
    echo "${v_bin} NOT EXECUTABLE!"
    exit 1;
fi

echo "VALGRIND OPTIONS=[${v_opts}]"

${v_bin} ${v_opts} $@

